<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArchivoComentario extends Model
{
    protected $table = 'archivo_comentario';

    protected $fillable = ['nombre'];

    public $timestamps = false;

    public function comentario(){
        return $this->belongsTo(ComentarioPost::class, 'comentario_post_id', 'id');
    }
}
