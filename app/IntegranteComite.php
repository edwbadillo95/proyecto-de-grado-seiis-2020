<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IntegranteComite extends Model
{
    protected $table = 'integrante_comite';
    public $timestamps = false;
    protected $fillable = [
        'grupo_comite_id',
        'descripcion',
        'programar_sesiones',
        'docente_id'
    ];

    public function grupoComite()
    {
        return $this->belongsTo(GrupoComite::class);
    }

    public function docente()
    {
        return $this->belongsTo(Docente::class);
    }
}
