<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GrupoComite extends Model
{
    protected $table = 'grupo_comite';
    protected $fillable = [
        'nombre',
        'estado',
        'fecha_inicio',
        'fecha_finalizacion',
        'descripcion',
        'tipo_comite_id'
    ];

    public function tipoComite()
    {
        return $this->belongsTo(TipoComite::class);
    }

    public function integrantes()
    {
        return $this->hasMany(IntegranteComite::class);
    }

    public function existeDocente($docente)
    {
        return $this->integrantes()->where('docente_id', $docente->id)->exists();
    }

    // Trae los grupos de comité vigentes por su fecha de operación y que estén activos.
    // Este método es llamado cuando se va a crear o editar una sesión de comité.
    public static function getGruposVigentes()
    {
        $today = today();
        return GrupoComite::select('id', 'nombre')
            ->where('estado', true)
            ->where('fecha_inicio', '<=', $today)
            ->where('fecha_finalizacion', '>=', $today)
            ->get();
    }

    // Trae un grupo por su id y que dicho grupo esté vigente.
    public static function getGrupoVigente($id)
    {
        $today = today();
        return GrupoComite::where('estado', true)
            ->where('id', $id)
            ->where('fecha_inicio', '<=', $today)
            ->where('fecha_finalizacion', '>=', $today)
            ->firstOrFail();
    }

    public static function comprobarGrupoVigente($id)
    {
        $today = today();
        return GrupoComite::where('estado', true)
            ->where('id', $id)
            ->where('fecha_inicio', '<=', $today)
            ->where('fecha_finalizacion', '>=', $today)
            ->exists();
    }
}
