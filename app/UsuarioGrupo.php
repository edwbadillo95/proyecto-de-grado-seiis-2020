<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsuarioGrupo extends Model
{
    protected $table = 'usuario_grupo';

    protected $fillable = ['*'];

    public $timestamps = false;

    public function grupoForo(){
        return $this->belongsTo(GrupoForo::class);
    }

    public function usuario(){
        return $this->belongsTo(User::class);
    }
}
