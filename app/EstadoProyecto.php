<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EstadoProyecto extends Model
{
    public const ESTADO_PREDETERMINADO = 1;
    public const REVISION_APROBADA = 2;
    public const REVISION_RECHAZADA = 3;
    public const EN_DESARROLLO = 4;
    public const APROBADO = 5;
    public const RECHAZADO = 6;

    protected $table = 'estado_proyecto';

    public function calificado()
    {
        return $this->id == self::APROBADO ||$this->id == self::RECHAZADO;
    }
}
