<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Etiqueta extends Model
{
    protected $table = 'etiqueta';

    protected $fillable = ['*'];

    protected static function listarEtiquetas(){
        return Etiqueta::all();
    }

    public function etiquetas(){
        return $this->belongsToMany(Post::class, 'post_etiqueta');
    }
}
