<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class SesionComite
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Auth::user()->tieneRolCoordinador() && !Auth::user()->integranteComite()) {
            abort('403', 'No tiene los suficientes permisos para realizar esta acción.');
        }
        return $next($request);
    }
}
