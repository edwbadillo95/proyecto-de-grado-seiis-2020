<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class SeguimientoProyecto
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Auth::user()->puedeRegistrarSeguimientos()) {
           abort(403, 'No tiene los permisos suficientes para esta operación.');
        }

        return $next($request);
    }
}
