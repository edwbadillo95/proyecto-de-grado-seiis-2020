<?php

namespace App\Http\Requests;

use App\ProyectoGrado;
use Illuminate\Foundation\Http\FormRequest;

class SeguimientoProyectoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return ProyectoGrado::estaCalificado($this->route('proyecto'));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'asunto'        => 'required|max:100',
            'descripcion'   => 'required'
        ];
    }
}
