<?php

namespace App\Http\Requests;

use App\Estudiante;
use App\User;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class EstudianteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre1' => 'required|max:45',
            'nombre2' => 'nullable|max:45',
            'apellido1' => 'required|max:45',
            'apellido2' => 'required|max:45',
            'tipo_documento' => 'required',
            'numero_documento' => [
                'required',
                'max:100',
                Rule::unique('estudiante')->ignore($this->route('estudiante'))
            ],
            'fecha_nacimiento' => 'required|date',
            'sexo' => 'required',
            'correo' => [
                'required',
                'email',
                Rule::unique('estudiante')->ignore($this->route('estudiante')),
                Rule::unique('users', 'email')->ignore($this->userId())
            ]
        ];
    }

    public function userId()
    {
        $id = $this->route('estudiante');
        if (!$id) return null;

        $estudiante = Estudiante::select('user_id')->whereId($id)->first();
        return $estudiante ? $estudiante->user_id : null;
    }
}
