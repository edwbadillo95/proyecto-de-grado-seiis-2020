<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class noticiaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'titulo'                =>  [
                'required',
                'max:100',
                Rule::unique('noticia', 'id')->ignore($this->route('noticias'))
            ],

            'fecha_creacion'          => 'required|date',
            'autor'                =>  'required','max:100',

        ];
    }
}
