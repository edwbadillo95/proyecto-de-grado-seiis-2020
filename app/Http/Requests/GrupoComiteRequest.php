<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class GrupoComiteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre'                => [
                'required',
                'max:100',
                Rule::unique('grupo_comite', 'id')->ignore($this->route('grupo'))
            ],
            'tipo_comite'           => 'required|exists:tipo_comite,id',
            'fecha_inicio'          => 'required|date',
            'fecha_finalizacion'    => 'required|date|after:fecha_inicio'
        ];
    }
}
