<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class HorarioRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'dia'                => 'required',
            'hora_inicio'        => 'required|date_format:H:i',
            'hora_fin'           => 'required|date_format:H:i|after:hora_inicio',
            'salon'              => 'required|exists:salon,id',
            'grupo'              => 'required|exists:grupo,id'
        ];
    }
}
