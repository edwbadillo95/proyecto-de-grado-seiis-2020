<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class DocenteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'documento'              => [
                'required',
                'numeric',
                'digits_between:1,10',
                Rule::unique('docente', 'id')->ignore($this->route('docente'))
            ],
            'nombres'                => 'required|max:50',
            'apellidos'              => 'required|max:50',
            'correo'                 => [
                'required',
                'email',
                Rule::unique('docente', 'id')->ignore($this->route('docente'))
            ],
            'genero'                 => 'required',
            'fecha_nacimiento'       => 'required|date',
            // 'clasificacion'          => 'required|exists:clasificacion,id',
            // 'tipo_documento'         => 'required|exists:tipo_documento,id'
            //'usuario'                => 'required|exists:users,id'
        ];
    }
}
