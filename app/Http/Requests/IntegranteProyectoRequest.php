<?php

namespace App\Http\Requests;

use App\ProyectoGrado;
use Illuminate\Foundation\Http\FormRequest;

class IntegranteProyectoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return ProyectoGrado::puedeGestionarIntegrantes($this->route('proyecto'));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'rol_integrante_id'     => 'required|exists:rol_integrante,id',
            'integrante_id'         => 'required',
            'descripcion'           => 'nullable'
        ];
    }
}
