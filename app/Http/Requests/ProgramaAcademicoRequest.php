<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class ProgramaAcademicoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre'                => [
                'required',
                'max:50',
                Rule::unique('programa_academico', 'id')->ignore($this->route('programa_academico'))
            ],
            'tipo_programa'        => 'required|exists:tipo_programa,id',
            'facultad'             => 'required|exists:facultad,id'
        ];
    }
}
