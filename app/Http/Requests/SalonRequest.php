<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class SalonRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre'                => [
                'required',
                'max:50',
                Rule::unique('salon', 'id')->ignore($this->route('salon'))
            ],
            'capacidad'          => 'required|numeric|between:1,50',
            'edificio'           => 'required|exists:edificio,id'
        ];
    }
}
