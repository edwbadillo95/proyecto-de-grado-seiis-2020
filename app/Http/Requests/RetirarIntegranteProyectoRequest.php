<?php

namespace App\Http\Requests;

use App\ProyectoGrado;
use Illuminate\Foundation\Http\FormRequest;

class RetirarIntegranteProyectoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return ProyectoGrado::puedeGestionarIntegrantes($this->route('proyecto'));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id'                => 'required|exists:integrante_proyecto,id',
            'descripcion'       => 'nullable'
        ];
    }
}
