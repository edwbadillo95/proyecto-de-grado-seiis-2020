<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class GrupoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre'              => [
                'required',
                'max:50',
                Rule::unique('grupo', 'id')->ignore($this->route('grupo'))
            ],
            'materia'             => 'required|exists:materia,id',
            // 'docente'             => 'required|exists:docente,id'
        ];
    }
}
