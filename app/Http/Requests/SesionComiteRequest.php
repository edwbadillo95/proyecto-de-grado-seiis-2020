<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SesionComiteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'asunto'        => 'required|max:100',
            'descripcion'   => 'nullable',
            'fecha'         => 'required|date|after_or_equal:today',
            'grupo_comite'  => 'required|exists:grupo_comite,id'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'fecha.after_or_equal' => 'La fecha debe ser posterior o igual a hoy.',
        ];
    }
}
