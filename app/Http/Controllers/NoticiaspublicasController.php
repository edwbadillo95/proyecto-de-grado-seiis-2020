<?php

namespace App\Http\Controllers;
use App\Noticia;
use App\Http\Requests\noticiaRequest;
use Illuminate\Http\Request;

class NoticiaspublicasController extends Controller
{


     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function contenidov($id)
    {
        $avisos = Noticia:: where('id',$id)->get();
        return view('inicio.contenidov',['contenidov' =>$avisos]);
    }

    public function contenidoa($id)
    {
        $avisos = Noticia:: where('id',$id)->get();
        return view('inicio.contenidoa',['contenidoa' =>$avisos]);
    }

    public function noticiap()
    {

        $noticiap= Noticia::whereNull('fecha_expiracion')->orderBy('id', 'DESC')->paginate(6);
        return view('sbadmin.noticias',compact('noticiap'));

    }
    public function paginacion()
    {
    $noticia=Noticia::whereNull('fecha_expiracion')->orderBy('id', 'DESC')->paginate(6);
    return view('inicio.noticiav',compact('noticia'));
    }
    public function avisosv()
    {
        $avisos = Noticia::whereNotNull('fecha_expiracion')->orderBy('id', 'DESC')->paginate(6);
        return view('inicio.avisosv',['avisos' =>$avisos]);

    }



    public function index()
    {

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(noticiaRequest $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }
}
