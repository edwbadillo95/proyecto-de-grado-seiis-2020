<?php

namespace App\Http\Controllers;

use App\GrupoComite;
use App\Http\Requests\GrupoComiteRequest;
use App\TipoComite;
use Illuminate\Http\Request;

class GrupoComiteController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
        $this->middleware('coordinador');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $grupos = GrupoComite::latest()->get();
        return view('comite.grupos.index', compact('grupos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tiposComite = TipoComite::all();
        $grupo = new GrupoComite();
        $grupo->estado = true;
        return view('comite.grupos.create', compact('tiposComite', 'grupo'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(GrupoComiteRequest $request)
    {
        $grupo = new GrupoComite($request->all());
        $grupo->tipo_comite_id = $request->tipo_comite;
        $grupo->save();
        return redirect()
            ->route('comite.grupos.show', ['grupo' => $grupo->id])
            ->with(['saved' => true]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $grupo = GrupoComite::findOrFail($id);
        return view('comite.grupos.show', compact('grupo'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $grupo = GrupoComite::findOrFail($id);
        $tiposComite = TipoComite::all();
        return view('comite.grupos.update', compact('tiposComite', 'grupo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(GrupoComiteRequest $request, $id)
    {
        $grupo = GrupoComite::findOrFail($id);
        $grupo->fill($request->all());
        $grupo->tipo_comite_id = $request->tipo_comite;
        $grupo->save();
        return redirect()
            ->route('comite.grupos.show', ['grupo' => $grupo->id])
            ->with(['saved' => true]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        GrupoComite::destroy($id);

        return redirect()
            ->route('comite.grupos.index')
            ->with(['deleted' => true]);
    }
}
