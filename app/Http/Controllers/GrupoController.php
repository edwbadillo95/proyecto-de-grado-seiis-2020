<?php

namespace App\Http\Controllers;

use App\Docente;
use App\Grupo;
use App\Http\Requests\GrupoRequest;
use App\Materia;
use App\Horario;
use Illuminate\Http\Request;

class GrupoController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
        $this->middleware('docente');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (auth()->user()->tieneRolCoordinador()) {
            $grupos = Grupo::latest()->get();
        } else {
            $grupos = Grupo::where('docente_id', auth()->user()->docente->id)->orderBy('nombre', 'asc')->get();
        }
        return view('docente.grupos.index', compact('grupos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $materias = Materia::all();
        // $docentes = Docente::where('id', auth()->user()->docente->id)->get();
        $grupo = new Grupo();
        return view('docente.grupos.create', compact('materias', 'grupo'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(GrupoRequest $request)
    {
        $grupo = new Grupo($request->all());
        $grupo->materia_id = $request->materia;
        $grupo->docente_id = auth()->user()->docente->id;
        $grupo->save();
        return redirect()
            ->route('docente.grupos.show', ['grupo' => $grupo->id])
            ->with(['saved' => true]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        Grupo::comprobarGrupoDocente($id);
        $grupo = Grupo::findOrFail($id);
        return view('docente.grupos.show', compact('grupo'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        Grupo::comprobarGrupoDocente($id);
        $grupo = Grupo::findOrFail($id);
        $materias = Materia::all();
        $docentes = Docente::where('id', auth()->user()->docente->id)->get();
        return view('docente.grupos.update', compact('materias', 'docentes', 'grupo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(GrupoRequest $request, $id)
    {
        Grupo::comprobarGrupoDocente($id);
        $grupo = Grupo::findOrFail($id);
        $grupo->fill($request->all());
        $grupo->materia_id = $request->materia;
        $grupo->docente_id = auth()->user()->docente->id;
        $grupo->save();
        return redirect()
            ->route('docente.grupos.show', ['grupo' => $grupo->id])
            ->with(['saved' => true]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Grupo::destroy($id);

        return redirect()
            ->route('docente.grupos.index')
            ->with(['deleted' => true]);
    }

    public function horarioPorMateria($materia)
    {
        $nombreMateria = Grupo::select('materia.nombre')
            ->join('materia', 'materia.id', '=', 'grupo.materia_id')
            ->where('grupo.materia_id', $materia)
            ->get();

        $horarioDocente = Horario::join('grupo', 'grupo.id', '=', 'horario.grupo_id')
            ->where('grupo.materia_id', $materia)
            ->orderBy('horario.hora_inicio', 'asc')
            ->get();

        return view('docente.grupos.horario', compact('horarioDocente','nombreMateria'));
    }
}
