<?php

namespace App\Http\Controllers;

use App\Estudiante;
use App\Http\Requests\EstudianteRequest;
use App\Materia;
use App\Matricula;
use App\MatriculaAsignatura;
use App\Notifications\AprobarUsuario;
use App\Notifications\EstudiantePorAprobar;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;
use Illuminate\Validation\Rule;
use Throwable;

class EstudianteController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('coordinador')->except(['show', 'update', 'cambiar_correo']);
    }

    public function index()
    {
        $estudiantes = Estudiante::where('estado', true)->latest()->get();
        return view('estudiantes.index', compact('estudiantes'));
    }
    public function create()
    {
        return view('estudiantes.create', [
            'estudiante' => new Estudiante
        ]);
    }
    public function store(EstudianteRequest $request)
    {
        try {
            DB::beginTransaction();
            $usuario = User::create([
                'name' => $request->nombre1 . ' ' . $request->apellido1,
                'email' => $request->correo,
                'password' => bcrypt($request->numero_documento)
            ]);

            $estudiante = Estudiante::create([
                'nombre1' => $request->nombre1,
                'nombre2' => $request->nombre2,
                'apellido1' => $request->apellido1,
                'apellido2' => $request->apellido2,
                'correo' => $request->correo,
                'fecha_nacimiento' => $request->fecha_nacimiento,
                'sexo' => $request->sexo,
                'tipo_documento' => $request->tipo_documento,
                'numero_documento' => $request->numero_documento,
                'user_id' => $usuario->id,
                'tipo_carrera' => $request->tipo_carrera

            ]);
            $this->materias($estudiante);
            $this->notificar_corrdinacion_secretaria($estudiante);
            DB::commit();

            return redirect()->route('estudiantes.show', ['estudiante' => $estudiante->id])
                ->with(['saved' => true]);
        } catch (Throwable $error) {
            return $error;
        }
    }
    public function materias($estudiante)
    {

        if ($estudiante->tipo_carrera == 'TECNOLOGIA') {
            $matricula = Matricula::create([
                'fecha_matricula' => $f1 = Carbon::now(),
                'fecha_pago' => $f1 = Carbon::now(),
                'estudiante_id' => $estudiante->id,
                'periodo' => 'Sexto'
            ]);
            $matricula2 = Matricula::create([
                'fecha_matricula' => $f1->subMonth(6),
                'fecha_pago' => $f1->subMonth(6),
                'estudiante_id' => $estudiante->id,
                'periodo' => 'Quinto'
            ]);
            $materias_t = Materia::whereBetween('id', [1, 42])->get();
            foreach ($materias_t as $m) {
                if ($m->id > 35) {
                    MatriculaAsignatura::create([
                        'matricula_id' => $matricula->id,
                        'nota1' => mt_rand((3 * 10), (5 * 10)) / 10,
                        'nota2' => '0.0',
                        'nota3' => '0.0',
                        'estado' => 'MATRICULADA',
                        'materia_id' => $m->id,
                        'grupo_id' => 1
                    ]);
                } else {
                    MatriculaAsignatura::create([
                        'matricula_id' => $matricula2->id,
                        'nota1' => '4.0',
                        'nota2' => '4.0',
                        'nota3' => '4.0',
                        'nota_definitiva' => mt_rand((3 * 10), (5 * 10)) / 10,
                        'fecha_aprobacion' => Carbon::now()->subMonth(1),
                        'estado' => 'APROBADA',
                        'materia_id' => $m->id,
                        'grupo_id' => 2
                    ]);
                }
            }
        } else if ($estudiante->tipo_carrera == 'PROFESIONAL') {
            $matricula = Matricula::create([
                'fecha_matricula' => Carbon::now(),
                'fecha_pago' => Carbon::now(),
                'estudiante_id' => $estudiante->id,
                'periodo' => 'Decimo'
            ]);
            $matricula2 = Matricula::create([
                'fecha_matricula' => Carbon::now()->subMonth(6),
                'fecha_pago' => Carbon::now()->subMonth(6),
                'estudiante_id' => $estudiante->id,
                'periodo' => 'Noveno'
            ]);
            $materias_p = Materia::whereBetween('id', [43, 68])->get();

            foreach ($materias_p as $m) {
                if ($m->id > 61) {
                    MatriculaAsignatura::create([
                        'matricula_id' => $matricula->id,
                        'nota1' => mt_rand((3 * 10), (5 * 10)) / 10,
                        'nota2' => '0.0',
                        'nota3' => '0.0',
                        'estado' => 'MATRICULADA',
                        'materia_id' => $m->id,
                        'grupo_id' => 3
                    ]);
                } else {
                    MatriculaAsignatura::create([
                        'matricula_id' => $matricula2->id,
                        'nota1' => '4.0',
                        'nota2' => '4.0',
                        'nota3' => '4.0',
                        'nota_definitiva' => mt_rand((3 * 10), (5 * 10)) / 10,
                        'fecha_aprobacion' => Carbon::now()->subMonth(1),
                        'estado' => 'APROBADA',
                        'materia_id' => $m->id,
                        'grupo_id' => 3
                    ]);
                }
            }
        }
    }
    public function show($id)
    {
        $estudiante = Estudiante::findOrfail($id);

        $this->authorize('show1', $estudiante);

        return view('estudiantes.show', compact('estudiante'));
    }


    public function edit($id)
    {
        $estudiante = Estudiante::findOrfail($id);
        return view('estudiantes.edit', compact('estudiante'));
    }

    public function update(EstudianteRequest $request, $id)
    {
        $estudiante = Estudiante::find($id);
        $this->authorize('update1', $estudiante);

        $estudiante->update([
            'nombre1' => $request->nombre1,
            'nombre2' => $request->nombre2,
            'apellido1' => $request->apellido1,
            'apellido2' => $request->apellido2,
            'correo' => $request->correo,
            'fecha_nacimiento' => $request->fecha_nacimiento,
            'sexo' => $request->sexo,
            'tipo_documento' => $request->tipo_documento,
            'numero_documento' => $request->numero_documento,

        ]);
        return redirect()->route('estudiantes.show', ['estudiante' => $estudiante->id])
            ->with(['saved' => true]);
    }
    public function destroy($id)
    {
        $estudiante = Estudiante::findOrfail($id);
        $estudiante->delete();
        return redirect()->route('lista-registro-ingreso-plataforma')->with(['saved' => true]);
    }
    public function lista_registro_ingreso_plataforma()
    {
        $lista = Estudiante::where('estado', false)->get();
        return view('estudiantes.registroPlataforma.listaRegistroPlataforma', compact('lista'));
    }
    public function aceptar_estudiante(Estudiante $estudiante)
    {
        try {
            DB::beginTransaction();
            $usuario = User::create([
                'name' => $estudiante->nombre1 . ' ' . $estudiante->apellido1,
                'email' => $estudiante->correo,
                'password' => bcrypt($estudiante->numero_documento),
                'email_verified_at' => Carbon::now()
            ]);
            $estudiante->update([
                'estado' => true,
                'user_id' => $usuario->id
            ]);
            $this->materias($estudiante);
            DB::commit();
            $this->notificarEstudiante($estudiante);
            return redirect()->route('lista-registro-ingreso-plataforma')
                ->with(['saved' => true]);
        } catch (Throwable $error) {
            return $error;
        }
    }
    public function cambiar_correo(Request $request, Estudiante $estudiante)
    {
        $request->validate([
            'correo' =>
            ['required', 'email', Rule::unique('estudiante')->ignore($estudiante->id)]
        ]);
        $this->authorize('update1', $estudiante);
        $estudiante->update([
            'correo' => $request->correo
        ]);
        return redirect()->route('estudiantes.show', ['estudiante' => $estudiante->id])
            ->with(['saved' => true]);
    }

    private function notificarEstudiante($estudiante)
    {
        Notification::send($estudiante->user, new AprobarUsuario($estudiante));
    }
}
