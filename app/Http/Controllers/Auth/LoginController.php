<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Validation\ValidationException;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /***
     * Refirecciona a la página de inicio.
     */
    public function showLoginForm()
    {
        return redirect()->route(RouteServiceProvider::LOGIN_ROUTE);
    }

    /**
     * Retorna un mensaje de error de login personalizado.
     */
    public function sendFailedLoginResponse($request)
    {
        throw ValidationException::withMessages([
            'auth.failed' => 'Credenciales incorrectas.',
        ]);
    }
}
