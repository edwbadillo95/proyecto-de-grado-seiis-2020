<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\GrupoForo;
use App\UsuarioGrupo;

class GrupoForoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $grupos = GrupoForo::orderBy('nombre')->get();
        return view('foro.grupos.index', compact('grupos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //$tiposComite = TipoComite::all();
        $grupo = new GrupoForo();
        $grupo->privado = false;
        return view('foro.grupos.create', compact('grupo'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $grupo = new GrupoForo($request->all());
        $grupo->codigo_grupo = uniqid();
        $grupo->save();
        return redirect()
            ->route('foro.grupos.show', ['grupo' => $grupo->id])
            ->with(['saved' => true]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $grupo = GrupoForo::findOrFail($id);
        $usuariosGrupo = $grupo->usuarios_grupo;
        return view('foro.grupos.show', compact('grupo', 'usuariosGrupo'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $grupo = GrupoForo::findOrFail($id);
        //$tiposComite = TipoComite::all();
        return view('foro.grupos.update', compact('grupo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $grupo = GrupoForo::findOrFail($id);
        $grupo->nombre = $request->nombre;
        //$grupo->fill($request->all());
        //$grupo->tipo_comite_id = $request->tipo_comite;
        $grupo->save();
        return redirect()
            ->route('foro.grupos.show', ['grupo' => $grupo->id])
            ->with(['saved' => true]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $grupo = GrupoForo::findOrFail($id);
        $grupo->delete();
        //GrupoForo::destroy($id);

        return redirect()
            ->route('foro.grupos.index')
            ->with(['deleted' => true]);
    }

    public function buscarGrupo(Request $request)
    {
        $grupo = GrupoForo::where('codigo_grupo', $request->idgrupo)->first();
        if (!is_null($grupo)) {
            $usuarioGrupoExistente = UsuarioGrupo::where("usuario_id",1)->where("grupo_foro_id",$grupo->id)->first();
            if(is_null($usuarioGrupoExistente)){
                $usuarioGrupo = new UsuarioGrupo();
                $usuarioGrupo->usuario_id = 1;
                $usuarioGrupo->grupo_foro_id = $grupo->id;
                $usuarioGrupo->administrador = false;
                $usuarioGrupo->save();
            }
            return redirect()
                ->route('foro.grupos.show', ['grupo' => $grupo->id]);
        } else {
            $idGrupo = $request->idgrupo;
            return view('foro.grupos.buscargrupo', compact('idGrupo'));
        }
    }
}
