<?php

namespace App\Http\Controllers;

use App\GrupoForo;
use App\User;
use App\UsuarioGrupo;
use Illuminate\Http\Request;

class UsuarioGrupoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, $grupoId)
    {
        $grupo = GrupoForo::findOrFail($grupoId);
        $usuario = new User();
        $usuarioGrupo = new UsuarioGrupo();
        $email = $request->email;
        if ($email) {
            $usuario = User::where('email', $email)->first();
            $usuarioGrupo = UsuarioGrupo::where('grupo_foro_id', $grupoId)->where('usuario_id', $usuario->id)->firstOrNew([]);
        }

        return view('foro.usuariosgrupo.create', compact('grupo', 'usuario', 'usuarioGrupo'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $grupoId)
    {
        $usuarioGrupo = new UsuarioGrupo();
        $usuarioGrupo->grupo_foro_id = $grupoId;
        $usuarioGrupo->usuario_id = $request->usuario;
        $usuarioGrupo->administrador = ($request->es_admin == "on") ? 1 : 0;
        $usuarioGrupo->save();

        return redirect()
            ->route('foro.grupos.usuariosgrupo.create', ['grupo' => $grupoId])
            ->with(['saved' => true]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $grupoId, $idUser)
    {
        $usuarioGrupo = UsuarioGrupo::where('grupo_foro_id', $grupoId)->where('usuario_id', $idUser)->firstOrFail();
        $grupo = GrupoForo::findOrFail($grupoId);
        $usuario = User::findOrFail($idUser);
        return view('foro.usuariosgrupo.update', compact('grupo', 'usuario', 'usuarioGrupo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $grupoId, $idUser)
    {
        $usuarioGrupo = UsuarioGrupo::where('grupo_foro_id', $grupoId)->where('usuario_id', $idUser)->firstOrFail();
        $usuarioGrupo->administrador = ($request->es_admin == "on") ? 1 : 0;
        $usuarioGrupo->save();

        return redirect()
            ->route('foro.grupos.usuariosgrupo.create', ['grupo' => $grupoId])
            ->with(['saved' => true]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $idGrupo, $idUser)
    {
        $usuarioGrupo = UsuarioGrupo::where('grupo_foro_id', $idGrupo)->where('usuario_id', $idUser)->first();
        if (!is_null($usuarioGrupo)) {
            $usuarioGrupo->delete();
            return response()->json(['success' => true]);
        }
        return response()->json(['success' => false]);
    }
}
