<?php

namespace App\Http\Controllers;

use App\Docente;
use App\Grupo;
use App\GrupoComite;
use App\Http\Requests\IntegranteComiteRequest;
use App\IntegranteComite;
use Illuminate\Http\Request;

class IntegranteComiteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($grupo)
    {
        $grupo = GrupoComite::findOrFail($grupo);
        return view('comite.integrantes.index', compact('grupo'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $grupoId
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, $grupoId)
    {
        $grupo = GrupoComite::getGrupoVigente($grupoId);
        $docente = null;
        $documento = $request->documento;
        $integrante = new IntegranteComite();
        if ($documento) {
            $docente = Docente::where('documento', $documento)->first();
            if ($docente) {
                if ($grupo->existeDocente($docente)) {
                    $docente = null;
                    $integranteDuplicado = true;
                    return view('comite.integrantes.create', compact('grupo', 'integrante', 'docente', 'integranteDuplicado'));
                } else {
                    return view('comite.integrantes.create', compact('grupo', 'integrante', 'docente'));
                }
            } else {
                return view('comite.integrantes.create', compact('grupo', 'integrante', 'docente'));
            }
        }

        return view('comite.integrantes.create', compact('grupo', 'integrante', 'docente'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $grupoId
     * @return \Illuminate\Http\Response
     */
    public function store(IntegranteComiteRequest $request, $grupoId)
    {
        $existeGrupo = GrupoComite::comprobarGrupoVigente($grupoId);
        if (!$existeGrupo) {
            abort(404);
        }
        $integrante = new IntegranteComite();
        $integrante->grupo_comite_id = $grupoId;
        $programar_sesiones = $request->has('programar_sesiones');
        $integrante->programar_sesiones = $programar_sesiones;
        $integrante->descripcion = $request->descripcion;
        $integrante->fecha_integracion = now();
        $integrante->docente_id = $request->docente;
        $integrante->save();

        return redirect()
            ->route('comite.grupos.integrantes.show', ['grupo' => $grupoId, 'integrante' => $integrante->id])
            ->with(['saved' => true]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $integranteId
     * @param  int  $grupoId
     * @return \Illuminate\Http\Response
     */
    public function show($grupoId, $integranteId)
    {
        $integrante = IntegranteComite::where('grupo_comite_id', $grupoId)
            ->where('id', $integranteId)
            ->firstOrFail();

        $grupo = $integrante->grupoComite;
        return view('comite.integrantes.show', compact('integrante', 'grupo'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $integranteId
     * @param  int  $grupoId
     * @return \Illuminate\Http\Response
     */
    public function edit($grupoId, $integranteId)
    {
        $existeGrupo = GrupoComite::comprobarGrupoVigente($grupoId);
        if (!$existeGrupo) {
            abort(404);
        }

        $integrante = IntegranteComite::where('grupo_comite_id', $grupoId)
            ->where('id', $integranteId)
            ->whereNull('fecha_retiro')
            ->firstOrFail();

        $grupo = $integrante->grupoComite;
        $docente = $integrante->docente;
        return view('comite.integrantes.update', compact('integrante', 'grupo', 'docente'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(IntegranteComiteRequest $request, $grupoId, $integranteId)
    {
        $existeGrupo = GrupoComite::comprobarGrupoVigente($grupoId);
        if (!$existeGrupo) {
            abort(404);
        }

        $integrante = IntegranteComite::where('grupo_comite_id', $grupoId)
            ->where('id', $integranteId)
            ->whereNull('fecha_retiro')
            ->firstOrFail();

        if ($request->has('retirar')) {
            $integrante->fecha_retiro = now();
        }

        $integrante->descripcion = $request->descripcion;
        $integrante->programar_sesiones = $request->has('retirar') ? false : $request->has('programar_sesiones');
        $integrante->save();

        return redirect()
            ->route('comite.grupos.integrantes.show', ['grupo' => $grupoId, 'integrante' => $integrante->id])
            ->with(['saved' => true]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
