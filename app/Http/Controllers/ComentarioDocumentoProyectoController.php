<?php

namespace App\Http\Controllers;

use App\ComentarioDocumentoProyecto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ComentarioDocumentoProyectoController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request ,$documento)
    {
        $request->validate(['comentario'=>'required|min:8']);
        ComentarioDocumentoProyecto::create([
            'comentario'=>$request->comentario,
            'user_id'=> Auth::id(),
            'documento_proyecto_id'=>$documento
        ]);
        return back()->with(['saved' => true]);
    }
}
