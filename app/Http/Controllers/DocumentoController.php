<?php

namespace App\Http\Controllers;

use App\ComentarioDocumentoProyecto;
use App\Docente;
use App\DocumentoProyecto;
use App\Estudiante;
use App\EstadoProyecto;
use App\Http\Requests\DocumentoProyectoRequest;
use App\IntegranteProyecto;
use App\Notifications\SubirArchivo;
use App\ProyectoGrado;
use App\RolIntegrante;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;
use Madnest\Madzipper\Facades\Madzipper;

class DocumentoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('documentacion');
    }

    public function index($proyecto)
    {
        $proyecto = ProyectoGrado::findOrFail($proyecto);

        if (Auth::user()->estudiante) {
            $usuario = Auth::user()->estudiante->id;
            $integrante = IntegranteProyecto::where('integrante_id', $usuario)
                ->where('rol_integrante_id', '=', RolIntegrante::ESTUDIANTE)
                ->first();
        } else if (Auth::user()->docente) {
            $usuario = Auth::user()->docente->id;
            $integrante = IntegranteProyecto::where('integrante_id', $usuario)
                ->where('rol_integrante_id', '<>', RolIntegrante::ESTUDIANTE)
                ->where('proyecto_grado_id', $proyecto->id)
                ->first();
        } else {
            abort(403, 'No puede ingresar a la documentación porque no eres parte del grupo.');
        }

        $documentos = DocumentoProyecto::with([
            'comentarios', 'tipoDocumento', 'proyectoGrado'
        ])
            ->where('proyecto_grado_id', $integrante->proyecto_grado_id)
            ->orderBy('tipo_documento_proyecto_id', 'ASC')
            ->get();

        // dd($documentos);
        return view('documento.index', ['documentos' => $documentos]);
    }

    public function indexDocente($proyectoId)
    {
        $proyecto = ProyectoGrado::where('id', $proyectoId)->firstOrFail();

        return view('documento.indexdocente', compact('proyecto'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DocumentoProyectoRequest $request)
    {
        $documentoProyecto = new DocumentoProyecto($request->all());

        if ($request->hasFile('url_documento')) {
            $archivo = $request->file('url_documento');
            $documentoProyecto->url_documento = $archivo->store('public/documentos');
        }
        $documentoProyecto->fecha_ult_cargue = now();
        $documentoProyecto->proyecto_grado_id = 1;
        $documentoProyecto->tipo_documento_proyecto_id = 1;
        $documentoProyecto->save();
        return redirect()
            ->route('documento.index')
            ->with(['saved' => true]);
    }
    public function edit($id)
    {
        $documentoProyecto = DocumentoProyecto::findOrFail($id);

        $comentarioDocumento = DocumentoProyecto::findOrFail($id)->comentarios()->where('documento_proyecto_id', '=', $id)->latest()->first();

        return view('documento.update', compact('documentoProyecto', 'comentarioDocumento'));
    }

    public function update(DocumentoProyectoRequest $request, $id)
    {
        $documentoG = DocumentoProyecto::with('proyectoGrado')->whereId($id)->first();
        $comentarioDocumento = ComentarioDocumentoProyecto::where('documento_proyecto_id', $id);

        if ($request->hasFile('archivo_corregido')) {
            $archivo = $request->file('archivo_corregido');
            $nombreArchivo = $archivo->getClientOriginalName();
            $url_doc = $archivo->storeAs('public/proyectos/' . $documentoG->proyectoGrado->id, $nombreArchivo);
            $documentoG->url_documento = $url_doc;
        }

        $documentoG->fecha_utl_revision = $request->fecha_revision;
        $documentoG->estado = $request->estado;
        $documentoG->save();

        if ($request->get('comentario')) {
            $comentarioDocumento->create([
                'comentario' => $request->comentario,
                'documento_proyecto_id' => $id,
                'user_id' => Auth::id()
            ]);
        }

        return back()->with(['saved' => true]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $documentoProyecto = DocumentoProyecto::findOrFail($id);
        DocumentoProyecto::destroy($id);

        return redirect()
            ->route('proyecto.documentacion', ['proyecto' => $documentoProyecto->proyecto_grado_id])
            ->with(['deleted' => true]);
    }

    public function subir_archivo_proyecto_estudiante(Request $request, DocumentoProyecto $documento)
    {
        $request->validate(['archivo' => 'required|file|max:30000|mimes:doc,docx,zip,rar,7zip']);
        if ($request->hasFile('archivo')) {
            $documentoG = DocumentoProyecto::with('proyectoGrado')->whereId($documento->id)->first();
            $tipo_documento = $documentoG->tipo_documento_proyecto_id;
            $archivo = $request->file('archivo');
            $nombreArchivo = $archivo->getClientOriginalName();
            $documentoG->url_documento = $archivo->storeAs('public/proyectos/' . $documentoG->proyectoGrado->nombre, $nombreArchivo);
            if (Auth::user()->estudiante) {
                if ($tipo_documento == 4 || $tipo_documento == 5 || $tipo_documento == 6) {
                    $documentoG->update([
                        'fecha_ult_cargue' => Carbon::now(),
                        'estado' => "APROBADO"
                    ]);
                } else {
                    $documentoG->update([
                        'fecha_ult_cargue' => Carbon::now(),
                        'estado' => "EN PROCESO"
                    ]);
                    $this->notificar_carga_archivo_docentes($documentoG, Auth::user());
                }
            } elseif (Auth::user()->docente) {
                $documentoG->update([
                    'fecha_ult_revision' => Carbon::now()
                ]);
                $this->notificar_carga_archivo_estudiantes_director($documentoG, Auth::user());
            }
        }
        if ($tipo_documento == 3) {
            return back();
        }
        return back()->with(['saved' => true]);
    }
    public function aprobar_documento(DocumentoProyecto $documento)
    {
        // dd($documento->tipoDocumento->id);
        if ($documento->tipoDocumento->id == 1 && Auth::user()->esCalificadorProyecto($documento->proyectoGrado)) {
            DB::beginTransaction();
            $documento->update([
                'fecha_ult_revision' => Carbon::now(),
                'estado' => 'APROBADO'
            ]);
            $documento->proyectoGrado->estado_proyecto_id = EstadoProyecto::EN_DESARROLLO;
            $documento->proyectoGrado->save();
            DB::commit();
        } elseif (Auth::user()->docente && !Auth::user()->esCalificadorProyecto($documento->proyectoGrado)) {
            $documento->update([
                'fecha_ult_revision' => Carbon::now(),
                'estado' => 'APROBADO'
            ]);
        } else {
            DB::beginTransaction();
            $documento->update([
                'fecha_ult_revision' => Carbon::now(),
                'estado' => 'APROBADO'
            ]);

            $documento->proyectoGrado->aprobado = true;
            $documento->proyectoGrado->estado_proyecto_id = EstadoProyecto::APROBADO;
            $documento->proyectoGrado->save();
            DB::commit();
        }
        return back()->with(['saved' => true]);
    }
    public function rechazar_documento(DocumentoProyecto $documento)
    {
        DB::beginTransaction();
        $documento->update([
            'fecha_ult_revision' => Carbon::now(),
            'estado' => 'RECHAZADO'
        ]);
        $documento->proyectoGrado->aprobado = false;
        $documento->proyectoGrado->estado_proyecto_id = EstadoProyecto::RECHAZADO;
        $documento->proyectoGrado->save();
        DB::commit();
        return back()->with(['saved' => true]);
    }

    public function comprimirYDescargar(DocumentoProyecto $documento)
    {
        $documentoG = DocumentoProyecto::with('proyectoGrado')->whereId($documento->id)->first();

        $documentacion = glob(storage_path('app/public/proyectos/' . $documentoG->proyectoGrado->nombre . '/*'));

        Madzipper::make(storage_path('app/public/proyectos/' . $documentoG->proyectoGrado->nombre . '.zip'))->add($documentacion)->close();

        return response()->download(storage_path('app/public/proyectos/' . $documentoG->proyectoGrado->nombre . '.zip'));
    }

    public function notificar_carga_archivo_docentes($documento, $remitente)
    {
        $users = [];
        $docentes = Docente::join('integrante_proyecto', 'docente.id', 'integrante_proyecto.integrante_id')
            ->select('docente.*')
            ->where('integrante_proyecto.proyecto_grado_id', $documento->proyectoGrado->id)
            ->whereIn('integrante_proyecto.rol_integrante_id', [2, 4])->get();

        foreach ($docentes as $docente) {
            array_push($users, $docente->usuario);
        }

        Notification::send($users, new SubirArchivo($documento, $remitente));
    }

    public function notificar_carga_archivo_estudiantes_director($documento, $remitente)
    {
        $users = [];
        // $docente = Docente::join('integrante_proyecto', 'docente.id', 'integrante_proyecto.integrante_id')
        //     ->select('docente.*')
        //     ->where('integrante_proyecto.proyecto_grado_id', $documento->proyectoGrado->id)
        //     ->whereIn('integrante_proyecto.rol_integrante_id', [2])->first();

        $estudiantes = Estudiante::join('integrante_proyecto', 'estudiante.id', 'integrante_proyecto.integrante_id')
            ->select('estudiante.*')
            ->where('integrante_proyecto.proyecto_grado_id', $documento->proyectoGrado->id)
            ->whereIn('integrante_proyecto.rol_integrante_id', [1])->get();


        // array_push($users, $docente->usuario);
        foreach ($estudiantes as $estudiante) {
            // dd($estudiantes);
            array_push($users, $estudiante->user);
        }

        Notification::send($users, new SubirArchivo($documento, $remitente));
    }
}
