<?php

namespace App\Http\Controllers;
use App\Noticia;
use App\Http\Requests\noticiaRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\TipoDocumento;

class NoticiaController extends Controller
{
    public function __construct() {
        $this->middleware('secretaria')
            ->except('contenidov', 'noticiav', 'avisosv');
    }

     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function contenidov($id)
    {
        $noticia= noticia::findOrFail($id);
        return view('inicio.contenidov', compact('contenidov'));
    }

    public function noticiav()
    {
        $noticia = Noticia::whereNull('fecha_expiracion')->latest()->get();
        return view('inicio.noticiav', compact('noticiav'));

    }

    public function avisosv()
    {
        $noticiav= Noticia::all();
        return view('inicio.avisosv', compact('avisosv'));

    }


    public function index()
    {

        $noticia=Noticia::whereNull('fecha_expiracion')->orderBy('id', 'DESC')->paginate(3);

        $avisos= Noticia::whereNotNull('fecha_expiracion')->orderBy('id', 'DESC')->paginate(3);
        return view('comite.noticias.index', compact('noticia','avisos'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
            $noticia = noticia::all();
            $noticia = new noticia();
            $noticia->estado = true;
            return view('comite.noticias.create', compact('noticia'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(noticiaRequest $request)
    {
        $noticia = new noticia($request->all());
        $noticia->id = $request->noticia;
        if($request->hasFile('imagen')){

           $imagen= $request->file('imagen');
           $nombreArchivo = Storage::put('public/noticias', $imagen);
           $noticia->imagen = $nombreArchivo;
        }
        $noticia->save();
        return redirect()
            ->route('comite.noticias.show', ['noticia' => $noticia->id])
            ->with(['saved' => true]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $noticia= noticia::findOrFail($id);
        return view('comite.noticias.show', compact('noticia'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $noticia = noticia::findOrFail($id);
         return view('comite.noticias.update', compact('noticia'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $noticia = noticia::findOrFail($id);
        $noticia->fill($request->all());
        if($request->hasFile('imagen')){

            $imagen= $request->file('imagen');
            $nombreArchivo = Storage::put('public/noticias', $imagen);
            $noticia->imagen = $nombreArchivo;
         }

        $noticia->save();
        return redirect()
            ->route('comite.noticias.show', ['noticia' => $noticia->id])
            ->with(['saved' => true]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Noticia::destroy($id);

        return redirect()
            ->route('comite.noticias.index')
            ->with(['deleted' => true]);

    }
}
