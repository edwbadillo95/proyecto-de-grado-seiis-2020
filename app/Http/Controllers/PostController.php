<?php

namespace App\Http\Controllers;

use App\ArchivoComentario;
use App\ArchivoPost;
use App\ComentarioPost;
use App\GrupoForo;
use App\Post;
use App\PostGrupo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($grupo)
    {
        $grupo = GrupoForo::findOrFail($grupo);
        $posts = $grupo->posts()->where("estado", 1)->orderBy('fechapublicacion')->paginate(5);
        return view('foro.posts.index', compact('grupo', 'posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $nuevoPost = new Post();
        $nuevoPost->nombre = $request->nombre;
        $nuevoPost->contenidopost = $request->contenidopost;
        $nuevoPost->estado = 1;
        $nuevoPost->fechapublicacion = date("Y-m-d H:i:s");
        $nuevoPost->usuario_id = 1;
        $nuevoPost->save();

        $postGrupo = new PostGrupo();
        $postGrupo->post_id = $nuevoPost->id;
        $postGrupo->grupo_foro_id = $request->grupo;
        $postGrupo->save();

        if ($request->hasfile('adjuntos')) {
            foreach ($request->file('adjuntos') as $file) {
                $rutaArchivo = $file->store('foro', 'public');
                $archivoPost = new ArchivoPost();
                $archivoPost->nombre = $file->getClientOriginalName();
                $archivoPost->post_id = $nuevoPost->id;
                $archivoPost->ruta_archivo = $rutaArchivo;
                $archivoPost->save();
            }
        }

        return redirect()->route('foro.posts.show', ['post' => $nuevoPost->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::findOrFail($id);
        $usuario = $post->usuario;
        return view('foro.posts.show', compact('post', 'usuario'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::findOrFail($id);
        $post->estado = 0;
        $post->save();

        return redirect()->route('foro.posts');
    }

    public function cargarTodosPosts()
    {
        $posts = Post::where("estado", 1)->orderBy('fechapublicacion')->paginate(5);
        //Aca deben ir solo a los vinculados 
        //AJUSTARUSUARIO
        $grupos = GrupoForo::all();
        return view("foro.posts.panelgeneral", compact('posts', 'grupos'));
    }

    /**
     * Aca se vincula el Comentario al respectivo Post
     */
    public function vincularComentarioPost(Request $request, $idPost)
    {
        $comentario = new ComentarioPost();
        $comentario->comentario = $request->comentariopost;
        $comentario->fechapublicacion = date("Y-m-d H:i:s");
        $comentario->estado = 1;
        $comentario->post_id = $idPost;
        $comentario->usuario_id = 1;
        $comentario->save();

        if ($request->hasfile('adjuntosComentario')) {
            foreach ($request->file('adjuntosComentario') as $file) {
                $rutaArchivo = $file->store('foro', 'public');
                $archivoComentario = new ArchivoComentario();
                $archivoComentario->nombre = $file->getClientOriginalName();
                $archivoComentario->comentario_post_id = $comentario->id;
                $archivoComentario->ruta_archivo = $rutaArchivo;
                $archivoComentario->save();
            }
        }
        return Redirect::back();
    }

    public function eliminarComentario(Request $request, $idComentario)
    {
        $comentarioPost = ComentarioPost::findOrFail($idComentario);
        $comentarioPost->estado = 0;
        $comentarioPost->save();
        return Redirect::back();
    }

    /**
     * Aca se vincula el Comentario al respectivo Comentario
     */
    public function vincularComentarioAComentario(Request $request, $idComentario)
    {
        $comentario = new ComentarioPost();
        $comentario->comentario = $request->comentario;
        $comentario->fechapublicacion = date("Y-m-d H:i:s");
        $comentario->estado = 1;
        $comentario->comentario_post_id = $idComentario;
        $comentario->usuario_id = 1;
        $comentario->save();

        if ($request->hasfile('adjuntosComentario')) {
            foreach ($request->file('adjuntosComentario') as $file) {
                $rutaArchivo = $file->store('foro', 'public');
                $archivoComentario = new ArchivoComentario();
                $archivoComentario->nombre = $file->getClientOriginalName();
                $archivoComentario->comentario_post_id = $comentario->id;
                $archivoComentario->ruta_archivo = $rutaArchivo;
                $archivoComentario->save();
            }
        }
        return Redirect::back();
    }

    /**
     * Descarga un archivo del post por id
     */
    public function descargarArchivoPost($idArchivoPost)
    {
        $archivoPost = ArchivoPost::findOrFail($idArchivoPost);
        return Storage::disk('public')->download($archivoPost->ruta_archivo, $archivoPost->nombre);
    }

    /**
     * Descarga un archivo del comentario por id
     */
    public function descargarArchivoComentario($id)
    {
        $archivoComentario = ArchivoComentario::findOrFail($id);
        return Storage::disk('public')->download($archivoComentario->ruta_archivo, $archivoComentario->nombre);
    }
}
