<?php

namespace App\Http\Controllers;

use App\GrupoComite;
use App\SesionComite;
use App\IntegranteComite;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\AsistenciaRequest;

class AsistenciaController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
        $this->middleware('coordinador');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($sesionId)
    {
        // Lista todos los asistentes de una sesión.

        $sesion = SesionComite::findOrFail($sesionId);
        $asistencia = $sesion->asistentes()->get();

        return view('comite.sesiones.asistencia.index', compact('sesion', 'asistencia'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($sesionId)
    {
        $sesion = SesionComite::findOrFail($sesionId);
        $grupoComite = GrupoComite::getGrupoVigente($sesion->grupo_comite_id);
        $excluirIDs = [];
        $asistentes = $sesion->asistentes()->select('integrante_comite_id')->get()->toArray();
        foreach ($asistentes as $a ) {
            array_push($excluirIDs, $a['integrante_comite_id']);
        }
        $integrantes = $grupoComite->integrantes()
            ->whereNotIn('integrante_comite.id', $excluirIDs)
            ->get();
        return view('comite.sesiones.asistencia.create', compact('sesion', 'integrantes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AsistenciaRequest $request)
    {
        $integrantes = $request->integrantes;
        $sesion = SesionComite::findOrFail($request->route('sesion'));
        DB::beginTransaction();
        if ($integrantes) {
            $data = [];
            foreach ($integrantes as $i) {
                array_push($data, [
                    'sesion_comite_id'      => $sesion->id,
                    'integrante_comite_id'  => $i
                ]);
            }

            DB::table('asistencia_sesion')->insert($data);
        }
        DB::commit();

        return redirect()
            ->route('comite.sesiones.asistentes.index', ['sesion' => $sesion->id])
            ->with(['saved' => true]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
