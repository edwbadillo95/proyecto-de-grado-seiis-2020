<?php

namespace App\Http\Controllers;

use App\Estudiante;
use App\Http\Requests\EstudianteRequest;
use App\Notifications\EstudiantePorAprobar;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;

class RegistroEstudianteController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest');
    }
    public function registroEstudiante()
    {
        $estudiante = new Estudiante;
        return view('estudiantes.registroPlataforma.registro', compact('estudiante'));
    }
    public function crear_registro_ingreso_plataforma(EstudianteRequest $request)
    {
        $estudiante = Estudiante::create([
            'nombre1' => $request->nombre1,
            'nombre2' => $request->nombre2,
            'apellido1' => $request->apellido1,
            'apellido2' => $request->apellido2,
            'correo' => $request->correo,
            'fecha_nacimiento' => $request->fecha_nacimiento,
            'sexo' => $request->sexo,
            'tipo_documento' => $request->tipo_documento,
            'numero_documento' => $request->numero_documento,
            'estado' => false,
            'user_id' => null,
            'tipo_carrera' => $request->tipo_carrera
        ]);
        $this->notificar_corrdinacion_secretaria($estudiante);
        return back()->with(['mensaje' => true]);
    }

    private function notificar_corrdinacion_secretaria($estudiante)
    {
        $users = User::whereIn('rol_id', [2])->get();
        Notification::send($users, new EstudiantePorAprobar($estudiante));
    }
}
