<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class NotificacionController extends Controller
{
    public function markasread(Request $request)
    {
        $user = User::find($request->user_id);
        $user->unreadNotifications->markAsRead();
        // foreach ($request->notifications as $notification_id) {
        //     foreach ($user->unreadNotifications() as $notification) {
        //         if ($notification->id == $notification_id) {
        //             $notification->markAsRead();
        //         }
        //     }
        // }
        // return response()->json($request->data, 200);
    }

    public function notifications(Request $request)
    {
        $user = User::find($request->user_id);

        $notificaciones = $user->notifications;

        return response()->json($notificaciones, 200);
    }
}
