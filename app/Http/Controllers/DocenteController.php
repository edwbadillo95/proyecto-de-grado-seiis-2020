<?php

namespace App\Http\Controllers;

use App\Clasificacion;
use App\Docente;
use App\Horario;
use App\User;
use App\Http\Requests\DocenteRequest;
use Illuminate\Support\Facades\Storage;
use App\TipoDocumento;
use Illuminate\Http\Request;

class DocenteController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
        $this->middleware('docente');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (auth()->user()->tieneRolCoordinador()) {
            $docentes = Docente::latest()->get();
        } else {
            $docentes = Docente::where('id', auth()->user()->docente->id)->latest()->get();
        }
        return view('docente.index', compact('docentes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $clasificaciones = Clasificacion::all();
        $tiposDocumento = TipoDocumento::all();
        $docente = new Docente();
        return view('docente.create', compact('clasificaciones', 'tiposDocumento', 'docente'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DocenteRequest $request)
    {
        // $docente = new Docente($request->all());
        // $docente->clasificacion_id = $request->clasificacion;
        // $docente->tipo_documento_id = $request->tipo_documento;
        // // $docente->usuario_id = auth()->id();
        // $docente->usuario_id = auth()->docente()->id;
        // $docente->save();
        // return redirect()
        //     ->route('docente.show', ['docente' => $docente->id])
        //     ->with(['saved' => true]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        Docente::comprobarDocente($id);
        $docente = Docente::findOrFail($id);
        return view('docente.show', compact('docente'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        Docente::comprobarDocente($id);
        $docente = Docente::findOrFail($id);
        $clasificaciones = Clasificacion::all();
        $tiposDocumento = TipoDocumento::all();
        return view('docente.update', compact('clasificaciones', 'tiposDocumento', 'docente'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(DocenteRequest $request, $id)
    {
        Docente::comprobarDocente($id);
        $docente = Docente::findOrFail($id);

        if($request->hasFile('archivo')){

            Storage::delete('public/docente/'.$docente->documento.'/'.$docente->archivo);
            $archivo = $request->file('archivo');
            $nombreArchivo = $archivo->getClientOriginalName();
            $request->archivo->storeAs('public/docente/'.$docente->documento, $nombreArchivo);
            $docente->archivo = $nombreArchivo;
        }

        if($request->hasFile('horario')){

            Storage::delete('public/docente/'.$docente->documento.'/'.$docente->horario);
            $horario = $request->file('horario');
            $nombreHorario = $horario->getClientOriginalName();
            $request->horario->storeAs('public/docente/'.$docente->documento, $nombreHorario);
            $docente->horario = $nombreHorario;
        }
        
        $docente->documento = $request->documento;
        $docente->nombres = $request->nombres;
        $docente->apellidos = $request->apellidos;
        $docente->fecha_nacimiento = $request->fecha_nacimiento;
        $docente->genero = $request->genero;
        $docente->correo = $request->correo;
        // $docente->clasificacion_id = $request->clasificacion;
        // $docente->tipo_documento_id = $request->tipo_documento;
        $docente->save();
        return redirect()
            ->route('docente.show', ['docente' => $docente->id])
            ->with(['saved' => true]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Docente::destroy($id);

        return redirect()
            ->route('docente.index')
            ->with(['deleted' => true]);
    }

    public function horario()
    {
        $horarioDocente = Horario::join('grupo', 'grupo.id', '=', 'horario.grupo_id')
            // ->where('grupo.docente_id', auth()->docente()->id)
            ->where('grupo.docente_id', auth()->user()->docente->id)
            ->get();

        return view('docente.horario', compact('horarioDocente'));
    }
}
