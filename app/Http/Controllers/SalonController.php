<?php

namespace App\Http\Controllers;

use App\Edificio;
use App\Http\Requests\SalonRequest;
use App\Salon;
use Illuminate\Http\Request;

class SalonController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
        $this->middleware('docente');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $salones = Salon::latest()->get();
        return view('docente.salones.index', compact('salones'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $edificios = Edificio::all();
        $salon = new Salon();
        return view('docente.salones.create', compact('edificios', 'salon'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SalonRequest $request)
    {
        $salon = new Salon($request->all());
        $salon->edificio_id = $request->edificio;
        $salon->save();
        return redirect()
            ->route('docente.salones.show', ['salon' => $salon->id])
            ->with(['saved' => true]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $salon = Salon::findOrFail($id);
        return view('docente.salones.show', compact('salon'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $salon = Salon::findOrFail($id);
        $edificios = Edificio::all();
        return view('docente.salones.update', compact('edificios', 'salon'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SalonRequest $request, $id)
    {
        $salon = Salon::findOrFail($id);
        $salon->fill($request->all());
        $salon->edificio_id = $request->edificio;
        $salon->save();
        return redirect()
            ->route('docente.salones.show', ['salon' => $salon->id])
            ->with(['saved' => true]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Salon::destroy($id);

        return redirect()
            ->route('docente.salones.index')
            ->with(['deleted' => true]);
    }
}
