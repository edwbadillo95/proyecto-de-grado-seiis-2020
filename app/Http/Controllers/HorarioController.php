<?php

namespace App\Http\Controllers;

use App\Grupo;
use App\Horario;
use App\Http\Requests\HorarioRequest;
use App\Salon;
use Illuminate\Http\Request;

class HorarioController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
        $this->middleware('docente');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $horarios = Horario::join('grupo', 'grupo.id', '=', 'horario.grupo_id')
        ->join('materia', 'materia.id', '=', 'grupo.materia_id')
        ->where('grupo.docente_id', auth()->user()->docente->id)
        ->orderBy('horario.hora_inicio', 'asc')
        // ->orderBy('materia.nombre', 'asc')
        ->get();

        return view('docente.horarios.index', compact('horarios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $salones = Salon::all();
        // $grupos = Grupo::all();
        $grupos = Grupo::where('docente_id', auth()->user()->docente->id)->orderBy('nombre', 'asc')->get();
        $horario = new Horario();
        return view('docente.horarios.create', compact('salones', 'grupos', 'horario'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(HorarioRequest $request)
    {
        $horario = new Horario($request->all());
        $horario->salon_id = $request->salon;
        $horario->grupo_id = $request->grupo;
        $horario->save();
        return redirect()
            ->route('docente.horarios.index')
            ->with(['saved' => true]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $horario = Horario::findOrFail($id);
        return view('docente.horarios.show', compact('horario'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $horario = Horario::findOrFail($id);
        $salones = Salon::all();
        // $grupos = Grupo::all();
        $grupos = Grupo::where('docente_id', auth()->user()->docente->id)->get();
        return view('docente.horarios.update', compact('salones', 'grupos', 'horario'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(HorarioRequest $request, $id)
    {
        $horario = Horario::findOrFail($id);
        $horario->fill($request->all());
        $horario->salon_id = $request->salon;
        $horario->grupo_id = $request->grupo;
        $horario->save();
        return redirect()
            ->route('docente.horarios.show', ['horario' => $horario->id])
            ->with(['saved' => true]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Horario::destroy($id);

        return redirect()
            ->route('docente.horarios.index')
            ->with(['deleted' => true]);
    }
}
