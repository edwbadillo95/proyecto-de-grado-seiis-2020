<?php

namespace App\Http\Controllers;

use App\Estudiante;
use App\MatriculaAsignatura;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NotasActualHistorialController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('estudiante');
    }

    public function historialNotas($id)
    {
        $estudiante = Estudiante::find($id);
        if (Auth::user()->esEstudiante() != $estudiante->id) {
            abort(403, 'No tiene los permisos suficientes para realizar esta operación.');
        }
        $materias = MatriculaAsignatura::with('materia')->join('matricula', 'matricula.id', 'matricula_asignatura.matricula_id')
            ->select('matricula_asignatura.*')
            ->where('matricula.estudiante_id', $estudiante->id)
            ->where('estado', 'APROBADA')
            ->get();
        return view('estudiantes.notasActualHistorial.historialNotas', compact('materias', 'estudiante'));
    }
    public function notasActuales($id)
    {
        $estudiante = Estudiante::find($id);
        if (Auth::user()->esEstudiante() != $estudiante->id) {
            abort(403, 'No tiene los permisos suficientes para realizar esta operación.');
        }
        $materias = MatriculaAsignatura::with('materia')->join('matricula', 'matricula.id', 'matricula_asignatura.matricula_id')
            ->select('matricula_asignatura.*')
            ->where('matricula.estudiante_id', $estudiante->id)
            ->where('estado', 'MATRICULADA')
            ->get();
        return view('estudiantes.notasActualHistorial.NotasActuales', compact('materias', 'estudiante'));
    }
}
