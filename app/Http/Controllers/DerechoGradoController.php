<?php

namespace App\Http\Controllers;

use PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DerechoGradoController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    public function descargar()
    {
        // Genera y descarga el pago de derechos de grado.
        $this->verificarEstudiante();
        $usuario = Auth::user();
        $liquidacion = PDF::loadView('documento.derechosgrado.vistaliquidacion', compact('usuario'));
        return $liquidacion->stream();
    }

    private function verificarEstudiante()
    {
        if (!Auth::user()->estudiante) {
            abort(403, 'No tiene los permisos suficientes para realizar esta acción.');
        }
    }
}
