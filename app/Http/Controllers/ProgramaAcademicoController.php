<?php

namespace App\Http\Controllers;

use App\Facultad;
use App\Http\Requests\ProgramaAcademicoRequest;
use App\ProgramaAcademico;
use App\TipoPrograma;
use Illuminate\Http\Request;

class ProgramaAcademicoController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
        $this->middleware('docente');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $programas = ProgramaAcademico::latest()->get();
        return view('docente.programas.index', compact('programas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tiposPrograma = TipoPrograma::all();
        $facultades = Facultad::all();
        $programa = new ProgramaAcademico();
        return view('docente.programas.create', compact('tiposPrograma', 'facultades', 'programa'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProgramaAcademicoRequest $request)
    {
        $programa = new ProgramaAcademico($request->all());
        $programa->tipo_programa_id = $request->tipo_programa;
        $programa->facultad_id = $request->facultad;
        $programa->save();
        return redirect()
            ->route('docente.programas.show', ['programa' => $programa->id])
            ->with(['saved' => true]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $programa = ProgramaAcademico::findOrFail($id);
        return view('docente.programas.show', compact('programa'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $programa = ProgramaAcademico::findOrFail($id);
        $tiposPrograma = TipoPrograma::all();
        $facultades = Facultad::all();
        return view('docente.programas.update', compact('tiposPrograma', 'facultades', 'programa'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProgramaAcademicoRequest $request, $id)
    {
        $programa = ProgramaAcademico::findOrFail($id);
        $programa->fill($request->all());
        $programa->tipo_programa_id = $request->tipo_programa;
        $programa->facultad_id = $request->facultad;
        $programa->save();
        return redirect()
            ->route('docente.programas.show', ['programa' => $programa->id])
            ->with(['saved' => true]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ProgramaAcademico::destroy($id);

        return redirect()
            ->route('docente.programas.index')
            ->with(['deleted' => true]);
    }
}
