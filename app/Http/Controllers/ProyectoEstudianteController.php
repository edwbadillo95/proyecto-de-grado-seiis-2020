<?php

namespace App\Http\Controllers;

use App\EstadoProyecto;
use App\Estudiante;
use App\IntegranteProyecto;
use App\Notifications\AgregarIntegranteProyecto;
use App\Notifications\AprobarIntegrante;
use App\Notifications\RechazarIntegrante;
use App\Notifications\SolicitudIntegrante;
use App\ProyectoGrado;
use App\RolIntegrante;
use App\SolicitudEstudianteProyecto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;

class ProyectoEstudianteController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('estudiante')->only(['proyectos_inscripcion', 'enviar_solicitud_proyecto_grado']);
        $this->middleware('documentacion')->only(['aceptar_solicitud_proyecto', 'eliminar_solicitud_proyecto']);
    }
    public function proyectos_inscripcion()
    {
        $proyectos = ProyectoGrado::where('estado_proyecto_id', EstadoProyecto::REVISION_APROBADA)
            ->get();
        return view('estudiantes.proyecto.inscripcion', compact('proyectos'));
    }
    public function enviar_solicitud_proyecto_grado($estudianteId, $proyectoId)
    {
        $estudiante = Estudiante::find($estudianteId);
        if (Auth::user()->esEstudiante() != $estudiante->id) {
            abort(403, 'No tiene los permisos suficientes para realizar esta operación.');
        }
        $solicitud = new SolicitudEstudianteProyecto();
        $solicitud->proyecto_grado_id = $proyectoId;
        $solicitud->estudiante_id = $estudianteId;
        $solicitud->save();

        $this->notificar_solicitud_enviada($proyectoId, $solicitud->estudiante);
        return redirect()->route('proyectosInscripcion')->with(['saved' => true]);
    }
    public function aceptar_solicitud_proyecto($id, $estudianteId, $proyectoId)
    {
        DB::beginTransaction();
        $integrante = new IntegranteProyecto();
        $integrante->integrante_id = $estudianteId;
        $integrante->rol_integrante_id = RolIntegrante::ESTUDIANTE;
        $integrante->fecha_integracion = now();
        $integrante->proyecto_grado_id = $proyectoId;
        $integrante->save();
        $this->eliminar_solicitud_proyecto($id, $proyectoId);
        DB::commit();

        $this->notificar_aprobacion_solicitud($proyectoId, $estudianteId);
        return redirect()->route('proyectos.integrantes.index', $proyectoId)->with(['saved' => true]);
    }

    public function eliminar_solicitud_proyecto($id, $proyectoId)
    {

        $solicitud = SolicitudEstudianteProyecto::findOrfail($id);

        $validacion = IntegranteProyecto::where('integrante_id', $solicitud->estudiante_id)->where('proyecto_grado_id', $proyectoId)->first();
        if (!$validacion) {
            $this->notificar_rechazo_solicitud($proyectoId,$solicitud->estudiante_id);
        }
        $solicitud->delete();

        return redirect()->route('proyectos.integrantes.index', $proyectoId)->with(['saved' => true]);
    }

    public function notificar_solicitud_enviada($proyectoId, $estudiante)
    {
        $proyecto = ProyectoGrado::find($proyectoId);

        $usuario = $proyecto->docente->usuario;

        Notification::send($usuario, new SolicitudIntegrante($proyecto, $estudiante));
    }

    public function notificar_aprobacion_solicitud($proyectoId, $estudianteId)
    {
        $proyecto = ProyectoGrado::find($proyectoId);
        $estudiante = Estudiante::find($estudianteId);

        Notification::send($estudiante->user, new AprobarIntegrante($proyecto, $estudiante));
    }

    public function notificar_rechazo_solicitud($proyectoId, $estudianteId)
    {
        $proyecto = ProyectoGrado::find($proyectoId);
        $estudiante = Estudiante::find($estudianteId);
        Notification::send($estudiante->user, new RechazarIntegrante($proyecto));
    }
}
