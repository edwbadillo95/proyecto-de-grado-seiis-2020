<?php

namespace App\Http\Controllers;

use App\DocumentoProyecto;
use App\ProyectoGrado;
use App\EstadoProyecto;
use App\GrupoComite;
use App\IntegranteProyecto;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\ProyectoGradoRequest;
use App\Notifications\AprobarProyecto;
use App\Notifications\RechazarProyecto;
use App\Notifications\RegistroDeProyecto;
use App\RolIntegrante;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Notification;

class ProyectoGradoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $proyectos = ProyectoGrado::latest();
        if ($request->misproyectos && Auth::user()->docente) {
            $proyectos = $proyectos->where('docente_id', Auth::user()->docente->id);
        } elseif (Auth::user()->estudiante) {
            if ($request->miproyecto) {
                $proyectos = Auth::user()->estudiante->proyectoInscrito();
                return view('comite.proyectos.index', compact('proyectos'));
            } else {
                $proyectos = $proyectos->where('estado_proyecto_id', EstadoProyecto::REVISION_APROBADA);
            }
        }
        $proyectos = $proyectos->get();
        return view('comite.proyectos.index', compact('proyectos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // TODO: Verificar que el usuario autenticado sea un docente o coordinador.
        $proyecto = new ProyectoGrado();
        return view('comite.proyectos.create', compact('proyecto'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProyectoGradoRequest $request)
    {
        $proyecto = new ProyectoGrado($request->all());
        $proyecto->estado_proyecto_id = EstadoProyecto::ESTADO_PREDETERMINADO;

        $proyecto->docente_id = Auth::user()->docente->id;
        $proyecto->save();

        $this->NotificarAlComite($proyecto);

        return redirect()
            ->route('comite.proyectos.show', ['proyecto' => $proyecto->id])
            ->with(['saved' => true]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // Cualquier docente puede ver cualquier proyecto
        $proyecto = ProyectoGrado::findOrFail($id);
        return view('comite.proyectos.show', compact('proyecto'));
    }

    /**
     * Muestra el formulario para revisar un proyecto que ha sido registrado.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function verificar($id)
    {
        $proyecto = ProyectoGrado::getParaRevisar($id);

        $this->puedeVerificarProyecto($proyecto);
        // $this->notificarAprobacionAlDocente($proyecto);
        return view('comite.proyectos.check', compact('proyecto'));
    }

    /**
     * Cambia el estado del proyecto de grado a "APROBADO" si el proyecto estaba
     * bajo el estado "POR REVISAR".
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function aprobarVerificacion($id)
    {
        DB::beginTransaction();
        $proyecto = ProyectoGrado::getParaRevisar($id);
        $this->puedeVerificarProyecto($proyecto);
        $proyecto->estado_proyecto_id = EstadoProyecto::REVISION_APROBADA;
        $proyecto->save();

        IntegranteProyecto::registrarDirector($proyecto);
        DocumentoProyecto::iniciarDocumentacion($proyecto);

        DB::commit();
        $this->notificarAprobacionAlDocente($proyecto);
        return redirect()
            ->route('comite.proyectos.show', ['proyecto' => $proyecto->id])
            ->with(['saved' => true]);
    }

    /**
     * Cambia el estado del proyecto de grado a "RECHAZADO" si el proyecto estaba
     * bajo el estado "POR REVISAR".
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function rechazarVerificacion($id)
    {
        $proyecto = ProyectoGrado::getParaRevisar($id);
        $this->puedeVerificarProyecto($proyecto);
        $proyecto->estado_proyecto_id = EstadoProyecto::REVISION_RECHAZADA;
        $proyecto->aprobado = false;
        $proyecto->save();
        $this->notificarAprobacionAlDocente($proyecto);
        return redirect()
            ->route('comite.proyectos.show', ['proyecto' => $proyecto->id])
            ->with(['saved' => true]);
    }

    private function puedeVerificarProyecto($proyecto)
    {
        if (!Auth::user()->puedeVerificarProyecto($proyecto)) {
            abort(403, 'No tiene permisos para esta operación.');
        }
    }

    private function NotificarAlComite($proyecto)
    {
        $integrantesComite = [];
        foreach (GrupoComite::where('tipo_comite_id', 1)->get() as $comite) {
            foreach ($comite->integrantes as $integrante) {
                array_push($integrantesComite, $integrante->docente->usuario);
            }
        }
        Notification::send($integrantesComite, new RegistroDeProyecto($proyecto));
    }

    private function notificarAprobacionAlDocente($proyecto)
    {
        $docente = $proyecto->docente->usuario;

        Notification::send($docente, new AprobarProyecto($proyecto));
    }

    private function notificarRechazoAlDocente($proyecto)
    {
        $docente = $proyecto->docente->usuario;

        Notification::send($docente, new RechazarProyecto($proyecto));
    }

    /**
     * Muestra los proyectos de grado asignados al usuario integrante comité calificador.
     */
    public function mostrarAsignadosACalificador()
    {
        if (Auth::user()->integranteComite()) {
            $proyectos = [];

            $p = IntegranteProyecto::join('proyecto_grado', 'integrante_proyecto.proyecto_grado_id', '=', 'proyecto_grado.id')
                ->whereNull('proyecto_grado.aprobado')
                ->where('integrante_id', Auth::user()->docente->id)
                ->where('rol_integrante_id', RolIntegrante::CALIFICADOR)
                ->whereNull('fecha_retiro')
                ->get();

            foreach ($p as $pi) {
                array_push($proyectos, $pi->proyectoGrado);
            }

            return view('comite.proyectos.calificador', compact('proyectos'));
        }

        abort(403, "No tiene asignado el rol de calificador.");
    }
}
