<?php

namespace App\Http\Controllers;

use App\GrupoComite;
use App\SesionComite;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\SesionComiteRequest;
use App\Notifications\SesionComite as NotificationsSesionComite;
use Illuminate\Support\Facades\Notification;

class SesionComiteController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('sesioncomite')->except('destroy');
        $this->middleware('coordinador')->only('destroy');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sesiones = SesionComite::latest()->get();
        return view('comite.sesiones.index', compact('sesiones'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->verificarSiPuedeRegistrarSesion();
        if (Auth::user()->tieneRolCoordinador()) {
            $gruposComite = GrupoComite::getGruposVigentes();
            if (count($gruposComite) == 0) {
                $viewData = ['gruposComite' => false];
            } else {
                $sesion = new SesionComite();
                $viewData = compact('sesion', 'gruposComite');
            }
        } else {
            $sesion = new SesionComite();
            $gruposComite = [Auth::user()->integranteComite()->grupoComite];
            $viewData = compact('sesion', 'gruposComite');
        }
        return view('comite.sesiones.create', $viewData);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SesionComiteRequest $request)
    {
        $this->verificarSiPuedeRegistrarSesion();

        $sesion = new SesionComite($request->all());
        $sesion->grupo_comite_id = $request->grupo_comite;

        if (Auth::user()->integranteComite()) {
            $sesion->integrante_comite_id = Auth::user()->integranteComite()->id;
        }

        $sesion->save();

        $this->notificar_sesion_comite($sesion);
        return redirect()
            ->route('comite.sesiones.show', ['sesion' => $sesion->id])
            ->with(['saved' => true]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $sesion = SesionComite::findOrFail($id);
        return view('comite.sesiones.show', compact('sesion'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->verificarSiPuedeRegistrarSesion();

        if (Auth::user()->tieneRolCoordinador()) {
            $gruposComite = GrupoComite::getGruposVigentes();
            if (count($gruposComite) == 0) {
                $viewData = ['gruposComite' => false];
            } else {
                $sesion = SesionComite::findOrFail($id);
                $viewData = compact('sesion', 'gruposComite');
            }
        } else {
            $sesion = SesionComite::findOrFail($id);
            if ($sesion->integrante_comite_id != Auth::user()->integranteComite()->id) {
                abort('403', 'No tiene los permisos suficientes para editar esta sesión de comité.');
            }
            $gruposComite = [Auth::user()->integranteComite()->grupoComite];
            $viewData = compact('sesion', 'gruposComite');
        }
        return view('comite.sesiones.update', $viewData);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SesionComiteRequest $request, $id)
    {
        $this->verificarSiPuedeRegistrarSesion();

        $sesion = SesionComite::findOrFail($id);
        if (Auth::user()->integranteComite() && ($sesion->integrante_comite_id != Auth::user()->integranteComite()->id)) {
            abort('403', 'No tiene los permisos suficientes para editar esta sesión de comité.');
        }
        $sesion->fill($request->all());
        $sesion->grupo_comite_id = $request->grupo_comite;
        $sesion->save();
        return redirect()
            ->route('comite.sesiones.show', ['sesion' => $sesion->id])
            ->with(['saved' => true]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        SesionComite::destroy($id);

        return redirect()
            ->route('comite.sesiones.index')
            ->with(['deleted' => true]);
    }

    /**
     * Verifica si el usuario autenticado puede registrar sesiones de comité.
     */
    private function verificarSiPuedeRegistrarSesion()
    {
        if (!Auth::user()->puedeRegistrarSesiones()) {
            abort('403', 'No tiene los permisos suficientes para esta acción.');
        }
    }

    private function notificar_sesion_comite($sesion)
    {
        $users = [];

        $integrantes = $sesion->grupoComite->integrantes;
        foreach ($integrantes as $integrante) {
            array_push($users, $integrante->docente->usuario);
        }
        Notification::send($users, new NotificationsSesionComite($sesion));
    }
}
