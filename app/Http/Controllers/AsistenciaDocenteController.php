<?php

namespace App\Http\Controllers;

use App\Asistencia;
use App\Estudiante;
use App\Grupo;
use App\Http\Requests\AsistenciaDocenteRequest;
use Illuminate\Http\Request;

class AsistenciaDocenteController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
        $this->middleware('docente');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($grupo)
    {
        $grupo = Grupo::findOrFail($grupo);
        return view('docente.asistencia.index', compact('grupo'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, $grupoId)
    {
        $grupo = Grupo::findOrFail($grupoId);
        $estudiante = null;
        $documento = $request->documento;
        $asistencia = new Asistencia();
        if ($documento) {
            $estudiante = Estudiante::where('numero_documento', $documento)->first();
            if ($estudiante) {
                if ($grupo->existeEstudiante($estudiante)) {
                    $estudiante = null;
                    $estudianteDuplicado = true;
                    return view('docente.asistencia.create', compact('grupo', 'asistencia', 'estudiante', 'estudianteDuplicado'));
                } else {
                    return view('docente.asistencia.create', compact('grupo', 'asistencia', 'estudiante'));
                }
            } else {
                return view('docente.asistencia.create', compact('grupo', 'asistencia', 'estudiante'));
            }
        }
        return view('docente.asistencia.create', compact('grupo', 'asistencia', 'estudiante'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AsistenciaDocenteRequest $request, $grupoId)
    {
        $asistencia = new Asistencia();
        $asistencia->grupo_id = $grupoId;
        $asistencia->estudiante_id = $request->estudiante;
        $asistencia->save();

        return redirect()
            ->route('docente.grupos.asistencia.index', ['grupo' => $grupoId])
            ->with(['saved' => true]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Asistencia  $asistencia
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Asistencia  $asistencia
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Asistencia  $asistencia
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Asistencia  $asistencia
     * @return \Illuminate\Http\Response
     */
    public function destroy($grupoId, $asistenciaId)
    {
        // Asistencia::where('grupo_id', $grupoId)->whereId($asistenciaId)->delete();
        Asistencia::where('grupo_id', $grupoId)->delete();
        
        return redirect()
            ->route('docente.grupos.asistencia.index', [ 'grupo' => $grupoId ])
            ->with(['deleted' => true]);
    }
}
