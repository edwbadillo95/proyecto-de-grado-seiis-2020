<?php

namespace App\Http\Controllers;

use App\Http\Requests\MateriaRequest;
use App\Materia;
use Illuminate\Http\Request;

class MateriaController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
        $this->middleware('docente');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $materias = Materia::latest()->get();
        return view('docente.materias.index', compact('materias'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $materia = new Materia();
        return view('docente.materias.create', compact('materia'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MateriaRequest $request)
    {
        $materia = new Materia($request->all());
        $materia->save();
        return redirect()
            ->route('docente.materias.show', ['materia' => $materia->id])
            ->with(['saved' => true]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $materia = Materia::findOrFail($id);
        return view('docente.materias.show', compact('materia'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $materia = Materia::findOrFail($id);
        return view('docente.materias.update', compact('materia'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(MateriaRequest $request, $id)
    {
        $materia = Materia::findOrFail($id);
        $materia->fill($request->all());
        $materia->save();
        return redirect()
            ->route('docente.materias.show', ['materia' => $materia->id])
            ->with(['saved' => true]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Materia::destroy($id);

        return redirect()
            ->route('docente.materias.index')
            ->with(['deleted' => true]);
    }
}
