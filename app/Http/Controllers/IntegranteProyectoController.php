<?php

namespace App\Http\Controllers;

use App\Docente;
use App\ProyectoGrado;
use App\RolIntegrante;
use App\EstadoProyecto;
use App\Estudiante;
use App\Http\Requests\IntegranteProyectoRequest;
use App\Http\Requests\RetirarIntegranteProyectoRequest;
use App\IntegranteProyecto;
use App\Notifications\AgregarIntegranteProyecto;
use App\Notifications\CalificadorAsignado;
use App\Notifications\CalificadorAsignadoIntegrantes;
use App\SolicitudEstudianteProyecto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Notification;

class IntegranteProyectoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @param  int  $proyectoId
     *
     * @return \Illuminate\Http\Response
     */
    public function index($proyectoId)
    {
        // Lista todos los integrantes de un proyecto de grado.
        $proyecto = ProyectoGrado::proyectoVerificadoQuery($proyectoId)->firstOrFail();
        $solicitudes = SolicitudEstudianteProyecto::with('estudiante')
            ->where('proyecto_grado_id', $proyectoId)
            ->get();
        return view('proyectos.integrantes.index', compact('proyecto', 'solicitudes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, $proyectoId)
    {
        $estados = [
            EstadoProyecto::REVISION_APROBADA,
            EstadoProyecto::EN_DESARROLLO,
        ];
        $proyecto = ProyectoGrado::whereIn('estado_proyecto_id', $estados)
            ->where('id', $proyectoId)
            ->firstOrFail();

        if (!$proyecto->puedeAgregarIntegrantes()) {
            abort(403, 'No tiene permisos para agregar integrantes.');
        }

        $data = ['proyecto' => $proyecto];
        if ($request->documento && in_array($request->rol_integrante, ["1", "2", "3", "4"])) {
            if ($request->rol_integrante == RolIntegrante::ESTUDIANTE) {
                $estudiante = Estudiante::where('numero_documento', $request->documento)->first();
                if ($estudiante) {
                    if ($estudiante->estaEnUnProyecto()) {
                        $data['msg'] = "El estudiante con número de documento $request->documento ya está inscrito en un proyecto de grado.";
                    } else {
                        $data['persona'] = [
                            'id'            => $estudiante->id,
                            'documento'     => $estudiante->numero_documento,
                            'nombre'        => "$estudiante->nombre1 $estudiante->nombre2 $estudiante->apellido1 $estudiante->apellido2",
                        ];
                    }
                }
            } else {
                $docente = Docente::where('documento', $request->documento)->first();
                if ($docente) {
                    $data['persona'] = [
                        'id'            => $docente->id,
                        'documento'     => $docente->documento,
                        'nombre'        => "$docente->nombres $docente->apellidos",
                    ];
                }
            }
        }
        return view('proyectos.integrantes.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(IntegranteProyectoRequest $request, $proyectoId)
    {
        $estados = [
            EstadoProyecto::REVISION_APROBADA,
            EstadoProyecto::EN_DESARROLLO,
        ];
        $proyecto = ProyectoGrado::whereIn('estado_proyecto_id', $estados)
            ->where('id', $proyectoId)
            ->firstOrFail();

        if (!$proyecto->puedeAgregarIntegrantes()) {
            abort(403, 'No tiene permisos para agregar integrantes.');
        }

        $integrante = new IntegranteProyecto($request->all());
        $integrante->verificarIntegrante($proyecto);
        $integrante->fecha_integracion = now();
        $integrante->proyecto_grado_id = $proyectoId;
        $integrante->save();
        if ($request->rol_integrante_id == 4) {
            $this->notificarCalificador($proyecto, $integrante);
            $this->notificarCalificador_integrantes($proyecto);
        } else {
            $this->notificarEstudiante($proyecto, $integrante);
        }
        return redirect()
            ->route('proyectos.integrantes.show', [
                'proyecto' => $proyectoId,                'integrante' => $integrante->id
            ])
            ->with(['saved' => true]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($proyectoId, $id)
    {
        $integrante = IntegranteProyecto::where('proyecto_grado_id', $proyectoId)
            ->where('id', $id)
            ->firstOrFail();
        return view('proyectos.integrantes.show', compact('integrante'));
    }

    /**
     * Registra como retirado un integrante de proyectos.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(RetirarIntegranteProyectoRequest $request, $proyectoId)
    {
        $estados = [
            EstadoProyecto::REVISION_APROBADA,
            EstadoProyecto::EN_DESARROLLO,
        ];
        $proyecto = ProyectoGrado::whereIn('estado_proyecto_id', $estados)
            ->where('id', $proyectoId)
            ->firstOrFail();

        if (!$proyecto->puedeAgregarIntegrantes()) {
            abort(403, 'No tiene permisos para gestionar integrantes.');
        }

        $integrante = IntegranteProyecto::findOrFail($request->id);
        $integrante->fecha_retiro = now();
        if ($request->descripcion) {
            $integrante->descripcion = $request->descripcion;
        }
        $integrante->save();
        return redirect()
            ->route('proyectos.integrantes.index', ['proyecto' => $proyectoId])
            ->with(['saved' => true]);
    }

    private function notificarEstudiante($proyecto, $integrante)
    {
        $estudiante = Estudiante::find($integrante->integrante_id);
        $user = $estudiante->user;
        Notification::send($user, new AgregarIntegranteProyecto($proyecto));
    }

    private function notificarCalificador($proyecto, $integrante)
    {
        $calificador = Docente::find($integrante->integrante_id);
        Notification::send($calificador->usuario, new CalificadorAsignado($proyecto));
    }

    private function notificarCalificador_integrantes($proyecto)
    {
        $users = [];
        $docente = Docente::join('integrante_proyecto', 'docente.id', 'integrante_proyecto.integrante_id')
            ->select('docente.*')
            ->where('integrante_proyecto.proyecto_grado_id', $proyecto->id)
            ->whereIn('integrante_proyecto.rol_integrante_id', [2])->first();

        $estudiantes = Estudiante::join('integrante_proyecto', 'estudiante.id', 'integrante_proyecto.integrante_id')
            ->select('estudiante.*')
            ->where('integrante_proyecto.proyecto_grado_id', $proyecto->id)
            ->whereIn('integrante_proyecto.rol_integrante_id', [1])->get();


        array_push($users, $docente->usuario);
        foreach ($estudiantes as $estudiante) {
            array_push($users, $estudiante->user);
        }

        Notification::send($users, new CalificadorAsignadoIntegrantes($proyecto));
    }
}
