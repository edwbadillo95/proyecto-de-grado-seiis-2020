<?php

namespace App\Http\Controllers;

use App\ProyectoGrado;
use App\SeguimientoProyecto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\SeguimientoProyectoRequest;

class SeguimientoProyectoController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($proyectoId)
    {
        // Lista los seguimientos de un proyecto.
        $proyecto = ProyectoGrado::where('id', $proyectoId)
            ->firstOrFail();

        return view('proyectos.seguimientos.index', compact('proyecto'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($proyectoId)
    {
        $proyecto = ProyectoGrado::where('id', $proyectoId)
            ->whereNull('aprobado')
            ->firstOrFail();

        if (!Auth::user()->puedeAgregarSeguimientosAlProyecto($proyecto)) {
            abort(403, 'No tiene los permisos suficientes para esta operación.');
        }

        $seguimiento = new SeguimientoProyecto();
        return view('proyectos.seguimientos.create', compact('proyecto', 'seguimiento'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SeguimientoProyectoRequest $request)
    {
        $proyecto = ProyectoGrado::where('id', $request->route('proyecto'))
            ->whereNull('aprobado')
            ->firstOrFail();

        if (!Auth::user()->puedeAgregarSeguimientosAlProyecto($proyecto)) {
            abort(403, 'No tiene los permisos suficientes para esta operación.');
        }

        $seguimiento = new SeguimientoProyecto($request->all());
        $seguimiento->proyecto_grado_id = $request->route('proyecto');
        $seguimiento->integrante_comite_id = Auth::user()->integranteComite()->id;
        $seguimiento->save();
        return redirect()
            ->route('proyectos.seguimientos.show', [
                'seguimiento' => $seguimiento->id,
                'proyecto' => $seguimiento->proyecto_grado_id])
            ->with(['saved' => true]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($proyectoId, $seguimientoId)
    {
        $seguimiento = SeguimientoProyecto::where('proyecto_grado_id', $proyectoId)
            ->where('id', $seguimientoId)
            ->firstOrFail();

        return view('proyectos.seguimientos.show', compact('seguimiento'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($proyectoId, $seguimientoId)
    {
        $proyecto = ProyectoGrado::where('id', $proyectoId)
            ->whereNull('aprobado')
            ->firstOrFail();
        $seguimiento = $proyecto->seguimientos()->where('id', $seguimientoId)
            ->firstOrFail();
        return view('proyectos.seguimientos.update', compact('proyecto', 'seguimiento'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SeguimientoProyectoRequest $request, $proyectoId, $seguimientoId)
    {
        $seguimiento = SeguimientoProyecto::where('proyecto_grado_id', $proyectoId)
            ->where('id', $seguimientoId)
            ->firstOrFail();

        $seguimiento->fill($request->all());
        $seguimiento->save();
        return redirect()
            ->route('proyectos.seguimientos.show', [
                'seguimiento' => $seguimiento->id,
                'proyecto' => $seguimiento->proyecto_grado_id])
            ->with(['saved' => true]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
