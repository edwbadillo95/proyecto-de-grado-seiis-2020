<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ComentarioPost extends Model
{
    protected $table = 'comentario_post';

    protected $fillable = ['comentario', 'estado'];

    public $timestamps = false;

    public function post()
    {
        return $this->belongsTo(Post::class);
    }

    public function archivos()
    {
        return $this->hasMany(ArchivoComentario::class);
    }
    
    public function usuario(){
        return $this->belongsTo(User::class, 'usuario_id');
    }
    
    public function comentarios(){
        return $this->hasMany(ComentarioPost::class)->where('comentario_post.estado', '1');
    }
}
