<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ComentarioDocumentoProyecto extends Model
{
    protected $table = 'comentario_documento_proyecto';
    protected $guarded = [];

    public function documentoProyecto()
    {
        return $this->belongsTo(DocumentoProyecto::class);
    }
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
