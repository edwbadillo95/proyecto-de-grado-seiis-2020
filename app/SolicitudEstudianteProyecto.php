<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SolicitudEstudianteProyecto extends Model
{
    protected $table = 'solicitud_estudiante_proyecto';

    public function estudiante()
    {
        return $this->belongsTo(Estudiante::class);
    }
}
