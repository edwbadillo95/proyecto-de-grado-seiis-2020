<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProgramaAcademico extends Model
{
    protected $table = 'programa_academico';

    protected $fillable = [ 'nombre', 'tipo_programa_id', 'facultad_id'];

    public function tipoPrograma()
    {
        return $this->belongsTo(TipoPrograma::class);
    }

    public function facultad()
    {
        return $this->belongsTo(Facultad::class);
    }

}
