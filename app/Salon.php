<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Salon extends Model
{
    protected $table = 'salon';

    protected $fillable = ['nombre', 'capacidad', 'edificio_id'];

    public function edificio()
    {
        return $this->belongsTo(Edificio::class);
    }

    public function horarios()
    {
        return $this->hasMany(Horario::class);
    }

}
