<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Noticia extends Model
{
    protected $table = 'noticia';
    protected $fillable = [
        'titulo',
        'contenido',
        'imagen',
        'fecha_creacion',
        'fecha_expiracion',
        'autor',
                   ];
}
