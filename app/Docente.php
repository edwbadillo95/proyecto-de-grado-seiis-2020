<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;

class Docente extends Model
{
    protected $table = 'docente';

    protected $fillable = [ 'documento', 'nombres', 'apellidos', 'correo', 'genero', 'fecha_nacimiento', 'archivo', 'horario', 'clasificacion_id', 'tipo_documento_id', 'usuario_id'];

    public function clasificacion()
    {
        return $this->belongsTo(Clasificacion::class);
    }

    public function tipoDocumento()
    {
        return $this->belongsTo(TipoDocumento::class);
    }

    public function grupos()
    {
        return $this->hasMany(Grupo::class);
    }

    public function usuario()
    {
        return $this->belongsTo(User::class, 'usuario_id');
    }

    public function integranteComite()
    {
        return $this->hasOne(IntegranteComite::class);
    }

    public static function comprobarDocente($id)
    {
        if (Auth::user()->tieneRolCoordinador()) {
            return true;
        } elseif(Auth::user()->docente->id != $id) {
            return abort(404);
        }
        return true;
    }
}
