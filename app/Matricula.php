<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Matricula extends Model
{
    protected $table = 'matricula';
    protected $guarded = [];

    public function estudiante()
    {
        return $this->belongsTo(Estudiante::class);
    }
}
