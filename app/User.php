<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function rol()
    {
        return $this->belongsTo(Rol::class);
    }

    public function tieneRolSecretaria()
    {
        return $this->rol && $this->rol->nombre == 'SECRETARÍA';
    }

    public function docente()
    {
        return $this->hasOne(Docente::class, 'usuario_id', 'id');
    }
    public function estudiante()
    {
        return $this->hasOne(Estudiante::class);
    }

    public function integrantesProyecto()
    {
        if ($this->estudiante) {
            $usuario = Auth::user()->estudiante->id;
            return IntegranteProyecto::where('integrante_id', $usuario)
                ->where('rol_integrante_id', '=', RolIntegrante::ESTUDIANTE)
                ->exists();
        } else if ($this->docente) {
            $usuario = Auth::user()->docente->id;
            return IntegranteProyecto::where('integrante_id', $usuario)
                ->where('rol_integrante_id', '<>', RolIntegrante::ESTUDIANTE)
                ->exists();
        }
        return false;
    }

    public function esIntegranteProyecto($proyecto)
    {
        if ($this->estudiante) {
            return $proyecto->integrantes()
                ->where('rol_integrante_id', RolIntegrante::ESTUDIANTE)
                ->where('integrante_id', $this->estudiante->id)
                ->exists();
        } elseif ($this->docente) {
            return $proyecto->integrantes()
                ->where('rol_integrante_id', '<>', RolIntegrante::ESTUDIANTE)
                ->where('integrante_id', $this->docente->id)
                ->exists();
        }

        return false;
    }
    public function integranteComite()
    {
        if ($this->docente) {
            return $this->docente->integranteComite;
        }
        return null;
    }

    public function tieneRolCoordinador()
    {
        return $this->rol && $this->rol->nombre == 'COORDINADOR';
    }

    public function puedeRegistrarProyectos()
    {
        return $this->docente != null;
    }

    public function puedeRegistrarSeguimientos()
    {
        return $this->tieneRolCoordinador() || $this->integranteComite() != null;
    }

    public function puedeVerificarProyecto($proyecto)
    {
        $usuarioDocente = $this->docente;
        if ($usuarioDocente) {
            return !$proyecto->revisado && $this->integranteComite()
                && $this->integranteComite()->docente_id != $proyecto->docente_id;
        }
        return false;
    }

    public function puedeAgregarSeguimientosAlProyecto($proyecto)
    {
        return !$proyecto->calificado && $this->integranteComite();
    }

    public function esDirectorProyecto($proyecto)
    {
        $director = $proyecto->traerDirector();
        return $this->docente && $this->docente->id == $director->integrante_id;
    }

    public function puedeRegistrarSesiones()
    {
        return $this->tieneRolCoordinador() || ($this->integranteComite() && $this->integranteComite()->programar_sesiones);
    }

    public function tieneSolicitudPendiente()
    {
        if ($this->estudiante) {
            return SolicitudEstudianteProyecto::where('estudiante_id', $this->estudiante->id)
                ->exists();
        }
        return false;
    }

    public function esEstudianteProyecto($proyecto)
    {
        if ($this->estudiante) {
            return $proyecto->integrantes()
                ->where('rol_integrante_id', RolIntegrante::ESTUDIANTE)
                ->where('integrante_id', $this->estudiante->id)
                ->exists();
        }
        return false;
    }
    public function estaInscriptoProyecto()
    {
        if ($this->estudiante) {
            return IntegranteProyecto::where('rol_integrante_id', RolIntegrante::ESTUDIANTE)
                ->where('integrante_id', $this->estudiante->id)
                ->exists();
        }
        return false;
    }

    public function proyectoEstudiante()
    {
        if ($this->estudiante) {
            return IntegranteProyecto::where('rol_integrante_id', RolIntegrante::ESTUDIANTE)
                ->where('integrante_id', $this->estudiante->id)
                ->first();
        }
        return null;
    }
    public function esCalificadorProyecto($proyecto)
    {
        return $this->docente && $proyecto->integrantes()
            ->where('rol_integrante_id', RolIntegrante::CALIFICADOR)
            ->where('integrante_id', $this->docente->id)
            ->exists();
    }
    public function documentoAprobado($documento, $id)
    {
        if ($documento && $id) {
            return $documento->where('tipo_documento_proyecto_id', $id)
                ->where('estado', 'APROBADO')
                ->exists();
        }
        return null;
    }
    public function esEstudiante()
    {
        if ($this->estudiante) {
            return $this->estudiante->id;
        }
        return null;
    }
}
