<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Estudiante extends Model
{
    protected $table = 'estudiante';
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function matriculas()
    {
        return $this->hasMany(Matricula::class);
    }

    public function estaEnUnProyecto()
    {
        return IntegranteProyecto::join('proyecto_grado', 'proyecto_grado.id', '=', 'integrante_proyecto.proyecto_grado_id')
            ->where('integrante_id', $this->id)
            ->where('rol_integrante_id', RolIntegrante::ESTUDIANTE)
            ->whereNull('fecha_retiro')
            ->where('aprobado', null)
            ->exists();
    }

    public function proyectoInscrito()
    {
        return ProyectoGrado::select('proyecto_grado.*')
            ->join('integrante_proyecto', 'proyecto_grado.id', '=', 'integrante_proyecto.proyecto_grado_id')
            ->where('integrante_id', $this->id)
            ->where('rol_integrante_id', RolIntegrante::ESTUDIANTE)
            ->whereNull('fecha_retiro')
            ->where('aprobado', null)
            ->get();
    }
}
