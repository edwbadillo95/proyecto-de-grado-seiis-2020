<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DocumentoProyecto extends Model
{
    protected $table = 'documento_proyecto';
    protected $guarded = [];

    public function tipoDocumento()
    {
        return $this->belongsTo(TipoDocumentoProyecto::class, 'tipo_documento_proyecto_id');
    }

    public function comentarios()
    {
        return $this->hasMany(ComentarioDocumentoProyecto::class);
    }
    public function proyectoGrado()
    {
        return $this->belongsTo(ProyectoGrado::class, 'proyecto_grado_id');
    }

    public static function iniciarDocumentacion($proyecto)
    {
        $tiposDocumento = [1, 2, 3, 4, 5,6];
        foreach ($tiposDocumento as $t) {
            DocumentoProyecto::create([
                'proyecto_grado_id'             => $proyecto->id,
                'estado'                        => 'PENDIENTE',
                'tipo_documento_proyecto_id'    => $t
            ]);
        }
    }
}
