<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Asistencia extends Model
{
    protected $table = 'estudiante_grupo';
    protected $guarded = [];

    public $timestamps = false;

    public function grupo()
    {
        return $this->belongsTo(Grupo::class, 'grupo_id','id');
    }

    public function estudiante()
    {
        return $this->belongsTo(Estudiante::class, 'estudiante_id','id');
    }
}
