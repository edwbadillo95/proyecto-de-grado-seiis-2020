<?php

namespace App\Notifications;

use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\HtmlString;

class SesionComite extends Notification
{
    use Queueable;

    public $sesionComite;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($sesionComite)
    {
        $this->sesionComite = $sesionComite;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['broadcast', 'database', 'mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Sesión de comité programada.')
            ->greeting('Hola ' . $notifiable->name)
            ->line("Se ha programado una nueva sesión de comité.")
            ->line(new HtmlString('<strong>Asunto: </strong>' . $this->sesionComite->asunto))
            ->line(new HtmlString('<strong>Descripción: </strong>' . $this->sesionComite->descripcion))
            ->line(new HtmlString('<strong>Fecha: </strong>' . $this->sesionComite->fecha))
            ->action('Ingresar', url('/comite/sesiones/' . $this->sesionComite->id));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        //No es necesario enviar notificacion a la pagina web
        return [
            'id' => $this->id,
            'read_at' => null,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'user' => 'Comité de Curriculum',
            'color' => 'info',
            'title' => 'Sesion de Comité programada',
            'message' => 'Se ha programado una nueva sesión de comité.',
            'icon' => 'https://45segundos.com/wp-content/uploads/2020/05/download.png',
            'interface_icon' => 'fa-calendar-alt',
            'url' => url('/comite/sesiones/' . $this->sesionComite->id)
        ];
    }
}
