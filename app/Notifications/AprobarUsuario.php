<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\HtmlString;

class AprobarUsuario extends Notification
{
    use Queueable;
    public $estudiante;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($estudiante)
    {
        $this->estudiante = $estudiante;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Registro de Estudiante Aprobado')
            ->greeting('Hola ' . $notifiable->name)
            ->line("Has sido aceptado para ingreso a la plataforma ")
            ->line(new HtmlString('<strong> PLATAFORMA WEB PARA LA COMUNIDAD DE LA TECNOLOGÍA DE DESARROLLO DE SISTEMAS INFORMÁTICOS E INGENIERÍA DE SISTEMAS </strong>'))
            ->line(new HtmlString('<strong>Usuario: </strong> ' . $this->estudiante->correo))
            ->line(new HtmlString('<strong>Contraseña: </strong> ' . $this->estudiante->numero_documento))
            ->action('Ingresar',  request()->root());
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
