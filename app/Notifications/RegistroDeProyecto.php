<?php

namespace App\Notifications;

use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\HtmlString;

class RegistroDeProyecto extends Notification
{
    use Queueable;
    public $proyecto;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($proyecto)
    {
        $this->proyecto = $proyecto;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'broadcast', 'mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Se ha registrado un Nuevo Proyecto de Grado')
            ->greeting('Hola ' . $notifiable->name)
            ->line("Se ha Registrado un Nuevo Proyecto de Grado ")
            ->line(new HtmlString('<strong>' . $this->proyecto->nombre . '</strong>'))
            ->action('Ir al Proyecto', url('/comite/proyectos/' . $this->proyecto->id));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'id' => $this->id,
            'read_at' => null,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'user' => $this->proyecto->docente->nombres . ' ' . $this->proyecto->docente->apellidos,
            'color' => 'info',
            'title' => 'Nuevo Proyecto Registrado!',
            'message' => $this->proyecto->docente->nombres . ' ha creado un nuevo proyecto con titulo ' . $this->proyecto->nombre,
            'icon' => 'https://45segundos.com/wp-content/uploads/2020/05/download.png',
            'interface_icon' => 'fa-file-alt',
            'url' => url('/comite/proyectos/' . $this->proyecto->id)
        ];
    }
}
