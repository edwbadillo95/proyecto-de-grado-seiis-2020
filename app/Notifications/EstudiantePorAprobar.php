<?php

namespace App\Notifications;

use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class EstudiantePorAprobar extends Notification
{
    use Queueable;
    public $estudiante;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($estudiante)
    {
        $this->estudiante = $estudiante;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['broadcast', 'database', 'mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Solicitud de Ingreso a la Plataforma por Verificar')
            ->greeting('Hola ' . $notifiable->name)
            ->line("Tiene una Solicitud por Verificar para el Ingreso de un Estudiante a la Plataforma")
            ->action('Ingresar', url('/lista-registro-ingreso-plataforma'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $nombre_Estudiante = $this->estudiante->nombre1 . ' ' . $this->estudiante->nombre2 . ' ' . $this->estudiante->apellido2 . ' ' . $this->estudiante->apellido2;
        return [
            'id' => $this->id,
            'read_at' => null,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'user' => $nombre_Estudiante,
            'color' => 'info',
            'title' => 'Solicitud de Estudiante por Verificar!',
            'message' => 'Tiene una Solicitud de' . $nombre_Estudiante . ' para ingresar a la Plataforma por Verificar',
            'icon' => 'https://45segundos.com/wp-content/uploads/2020/05/download.png',
            'interface_icon' => 'fa-user-plus',
            'url' => url('/lista-registro-ingreso-plataforma')
        ];
    }
}
