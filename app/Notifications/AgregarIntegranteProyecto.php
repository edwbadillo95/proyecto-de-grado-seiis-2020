<?php

namespace App\Notifications;

use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\HtmlString;

class AgregarIntegranteProyecto extends Notification
{
    use Queueable;
    public $proyecto;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($proyecto)
    {
        $this->proyecto = $proyecto;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['broadcast', 'database', 'mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Solicitud de Integrante por Verificar')
            ->greeting('Hola ' . $notifiable->name)
            ->line("Tiene una Solicitud por Verificar para el proyecto de grado:")
            ->line(new HtmlString('<strong>' . $this->proyecto->nombre . '</strong>'))
            ->action('Ir al Proyecto', url('/proyectos/' . $this->proyecto->id . '/integrantes'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'id' => $this->id,
            'read_at' => null,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'user' => $this->proyecto->docente->usuario->name, //Usuario remitente
            'color' => 'success',
            'title' => 'Agregado a Proyecto de Grado!',
            'message' => 'Has sido agregado al proyecto de grado ' . $this->proyecto->nombre,
            'icon' => 'https://45segundos.com/wp-content/uploads/2020/05/download.png', //Para la notificacion Push
            'interface_icon' => 'fa-user-plus',
            'url' => url('/proyectos/' . $this->proyecto->id . '/integrantes')
        ];
    }
}
