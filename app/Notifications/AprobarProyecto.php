<?php

namespace App\Notifications;

use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\HtmlString;

class AprobarProyecto extends Notification implements ShouldQueue
{
    use Queueable;

    public $proyecto;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($proyecto)
    {
        $this->proyecto = $proyecto;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'broadcast', 'mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Proyecto Aprobado')
            ->greeting('Hola ' . $notifiable->name)
            ->line("Se ha aprobado su Proyecto de Grado")
            ->line(new HtmlString('<strong>' . $this->proyecto->nombre . '</strong>'))
            ->action('Ir al Proyecto', url('/comite/proyectos/' . $this->proyecto->id));
        // ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'id' => $this->id,
            'read_at' => null,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'user' => 'El Comite',
            'color' => 'success',
            'title' => 'Proyecto Aprobado!',
            'message' => 'Se ha aprobado el proyecto de grado ' . $this->proyecto->nombre,
            'icon' => 'https://45segundos.com/wp-content/uploads/2020/05/download.png', 
            'interface_icon' => 'fa-file-alt',
            'url' => url('/comite/proyectos/' . $this->proyecto->id)

        ];
    }
}
