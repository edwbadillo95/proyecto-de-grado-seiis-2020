<?php

namespace App\Notifications;

use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\HtmlString;

class CalificadorAsignadoIntegrantes extends Notification
{
    use Queueable;
    public $proyecto;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($proyecto)
    {
        $this->proyecto = $proyecto;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['broadcast', 'database', 'mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Calificador Asignado')
            ->greeting('Hola ' . $notifiable->name)
            ->line("Han Asignado calificador para el proyecto de grado:")
            ->line(new HtmlString('<strong>' . $this->proyecto->nombre . '</strong>'))
            ->action('Ir al Proyecto', url('/comite/proyectos/' . $this->proyecto->id));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'id' => $this->id,
            'read_at' => null,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'user' => 'Comité de Proyectos de Grado',
            'color' => 'success',
            'title' => 'Calificador Asignado!',
            'message' => $notifiable->name . ', Han Asignado Calificador a su Proyecto de Grado',
            'icon' => 'https://45segundos.com/wp-content/uploads/2020/05/download.png',
            'interface_icon' => 'fa-file-alt',
            'url' => url('/proyectos/' . $this->proyecto->id . '/integrantes')

        ];
    }
}
