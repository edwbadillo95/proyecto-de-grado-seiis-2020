<?php

namespace App\Notifications;

use App\Docente;
use App\IntegranteProyecto;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\HtmlString;

class SubirArchivo extends Notification
{
    use Queueable;
    public $remitente;
    public $documento;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($documento, $remitente)
    {
        $this->documento = $documento;
        $this->remitente = $remitente;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['broadcast', 'database', 'mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $nombre_documento = explode('/', $this->documento->url_documento)[3];
        if ($this->remitente->estudiante) {
            $user = $this->remitente->estudiante->user->name;
            $message = $user . ' ha cargado el documento ' . $nombre_documento;
        } else {
            $user = $this->remitente->docente->usuario->name;
            $message = $user . ' ha corregido el documento ' . $nombre_documento;
        }

        return (new MailMessage)
            ->subject('Se ha Cargado un Archivo')
            ->greeting('Hola ' . $notifiable->name)
            ->line($message)
            ->line(new HtmlString('<strong>' . $this->documento->proyectoGrado->nombre . '</strong>'))
            ->action('Ir al Proyecto', url('/proyectos/' . $this->documento->proyectoGrado->id) . '/documentacion');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $nombre_documento = explode('/', $this->documento->url_documento)[3];
        if ($this->remitente->estudiante) {
            $user = $this->remitente->estudiante->user->name;
            $interface_icon = 'fa-file-upload';
            $title = $nombre_documento . ' Se ha Cargado Para revisión!';
            $message = $user . ' ha cargado el documento ' . $nombre_documento;
            $color = 'success';
        } else {
            $user = $this->remitente->docente->usuario->name;
            $interface_icon = 'fa-file-signature';
            $title = $nombre_documento . ' Por Corregir!';
            $message = $user . ' ha corregido el documento ' . $nombre_documento;
            $color = 'info';
        }

        return [
            'id' => $this->id,
            'read_at' => null,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'user' => $user,
            'color' => $color,
            'title' => $title,
            'message' => $message,
            'icon' => 'https://45segundos.com/wp-content/uploads/2020/05/download.png',
            'interface_icon' => $interface_icon,
            'url' => url('/proyectos/' . $this->documento->proyectoGrado->id . '/documentacion')
        ];
    }
}
