<?php

namespace App\Notifications;

use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\HtmlString;

class SolicitudIntegrante extends Notification
{
    use Queueable;

    public $proyecto;
    public $estudiante;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($proyecto, $estudiante)
    {
        $this->proyecto = $proyecto;
        $this->estudiante = $estudiante;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['broadcast', 'database', 'mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Solicitud de Integrante por Aprobar')
            ->greeting('Hola ' . $notifiable->name)
            ->line("Tiene una nueva solicitud de Integrante de proyecto de grado por aprobar")
            ->line(new HtmlString('<strong>' . $this->proyecto->nombre . '</strong>'))
            ->action('Ir al Proyecto', url('/comite/proyectos/' . $this->proyecto->id));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'id' => $this->id,
            'read_at' => null,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'user' => $this->estudiante->user->name,
            'color' => 'info',
            'title' => 'Solicitud de Integrante por Aprobar!',
            'message' => 'Tiene una nueva solicitud de Integrante de proyecto de grado por aprobar para el proyecto ' . $this->proyecto->nombre,
            'icon' => 'https://45segundos.com/wp-content/uploads/2020/05/download.png',
            'interface_icon' => 'fa-user-plus',
            'url' => url('/proyectos/' . $this->proyecto->id . '/integrantes')
        ];
    }
}
