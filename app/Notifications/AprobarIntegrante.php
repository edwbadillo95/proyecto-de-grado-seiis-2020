<?php

namespace App\Notifications;

use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\HtmlString;

class AprobarIntegrante extends Notification
{
    use Queueable;
    public $proyecto;
    public $estudiante;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($proyecto, $estudiante)
    {
        $this->proyecto = $proyecto;
        $this->estudiante = $estudiante;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['broadcast', 'database', 'mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Solicitud Aprobado')
            ->greeting('Hola ' . $notifiable->name)
            ->line("Se ha aprobado su solicitud para integrar el proyecto ")
            ->line(new HtmlString('<strong>' . $this->proyecto->nombre . '</strong>'))
            ->action('Ir al Proyecto', url('/proyectos/' . $this->proyecto->id . '/integrantes'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'id' => $this->id,
            'read_at' => null,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'user' => $this->proyecto->docente->usuario->name,
            'color' => 'success',
            'title' => 'Solicitud Aprobada!',
            'message' => 'Se ha aprobado su solicitud para integrar el proyecto ' . $this->proyecto->nombre,
            'icon' => 'https://45segundos.com/wp-content/uploads/2020/05/download.png',
            'interface_icon' => 'fa-user-check',
            'url' => url('/proyectos/' . $this->proyecto->id . '/integrantes')
        ];
    }
}
