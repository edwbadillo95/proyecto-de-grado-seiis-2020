<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoPrograma extends Model
{
    protected $table = 'tipo_programa';

    public $timestamps = false;
}
