<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SeguimientoProyecto extends Model
{
    protected $table = 'seguimiento_proyecto';
    protected $fillable = [ 'asunto', 'descripcion', ];

    public function integranteComite()
    {
        return $this->belongsTo(IntegranteComite::class);
    }

    public function proyectoGrado()
    {
        return $this->belongsTo(ProyectoGrado::class);
    }
}
