<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Grupo extends Model
{
    protected $table = 'grupo';

    protected $fillable = [ 'nombre', 'materia_id', 'docente_id'];

    public function materia()
    {
        return $this->belongsTo(Materia::class);
    }

    public function docente()
    {
        return $this->belongsTo(Docente::class);
    }

    // public function estudiantes()
    // {
    //     return $this->belongsToMany(Estudiante::class, 'estudiante_grupo');
    // }

    public function horarios()
    {
        return $this->hasMany(Horario::class);
    }

    public function estudiantes()
    {
        return $this->hasMany(Asistencia::class);
    }

    public function existeEstudiante($estudiante)
    {
        return $this->estudiantes()->where('estudiante_id', $estudiante->id)->exists();
    }

    public static function comprobarGrupoDocente($id)
    {
        $grupoDocente = Grupo::whereId($id)->first();
        if (Auth::user()->tieneRolCoordinador()) {
            return true;
        } elseif (Auth::user()->docente->id != intval($grupoDocente->docente_id)){
            return abort(404);
        }
        return true;
    }
}
