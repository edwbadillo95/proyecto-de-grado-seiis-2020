<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class ArchivoPost extends Model
{
    protected $table = 'archivo_post';

    protected $fillable = ['nombre'];

    public $timestamps = false;

    public function post()
    {
        return $this->belongsTo(Post::class);
    }
}
