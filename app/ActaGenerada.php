<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActaGenerada extends Model
{
    protected $table = 'acta_generada';

    public function sesionComite()
    {
        return $this->belongsTo(SesionComite::class);
    }
}
