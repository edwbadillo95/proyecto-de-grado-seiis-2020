<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class IntegranteProyecto extends Model
{
    public $timestamps = false;
    protected $table = 'integrante_proyecto';
    protected $fillable = [ 'integrante_id', 'rol_integrante_id', 'descripcion', 'proyecto_grado_id', 'fecha_integracion'];

    public function rolIntegrante()
    {
        return $this->belongsTo(RolIntegrante::class);
    }

    public function proyectoGrado()
    {
        return $this->belongsTo(ProyectoGrado::class);
    }

    public function getNombreAttribute()
    {
        if ($this->rolIntegrante->esEstudiante()) {
            $estudiante = Estudiante::where('id', $this->integrante_id)->first();
            if ($estudiante) {
                return "$estudiante->nombre1 $estudiante->nombre2 $estudiante->apellido1 $estudiante->apellido2";
            }
            return "(Desconocido)";
        } else {
            // Consultar docente
            $docente = Docente::where('id', $this->integrante_id)->first();
            if ($docente) {
                return "$docente->nombres $docente->apellidos";
            }
            return "(Desconocido)";
        }
    }

    public function soyYo()
    {
        $usuario = Auth::user();
        if ($this->rolIntegrante->esEstudiante())
        {
            return $usuario->estudiante && $usuario->estudiante->id == $this->integrante_id;
        }
        else
        {
            $integrante = $usuario->integranteComite();
            if ($integrante) {
                return $integrante && $integrante->id == $this->integrante_id;
            }
            return $usuario->docente && $usuario->docente->id == $this->integrante_id;
        };

        return false;
    }

    public function verificarIntegrante($proyecto)
    {

        if ($this->rolIntegrante->esEstudiante())
        {
            if (!Estudiante::where('id', $this->integrante_id)->exists()) {
                abort(404, 'El estudiante no existe en el sistema.');
            }
        }
        else
        {
            $integrante = Auth::user()->integranteComite();
            if (!$integrante || ($integrante->docente_id == $proyecto->docente_id)) {
                abort(403, 'No tiene permisos para realizar esta acción.');
            }

            if (!Docente::where('id', $this->integrante_id)->exists()) {
                abort(404, 'El docente no existe en el sistema.');
            }
        };
    }

    public static function registrarDirector($proyecto)
    {
        IntegranteProyecto::create([
            'fecha_integracion'     => now(),
            'proyecto_grado_id'     => $proyecto->id,
            'integrante_id'         => $proyecto->docente_id,
            'rol_integrante_id'     => RolIntegrante::DIRECTOR
        ]);
    }
}
