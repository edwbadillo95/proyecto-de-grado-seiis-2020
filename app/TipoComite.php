<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoComite extends Model
{
    protected $table = 'tipo_comite';
}
