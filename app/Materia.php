<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Materia extends Model
{
    protected $table = 'materia';

    protected $fillable = [ 'nombre', 'creditos'];

    public function grupos()
    {
        return $this->hasMany(Grupo::class);
    }

    public function programasMaterias()
    {
        return $this->belongsToMany(ProgramaAcademico::class, 'programa_materia');
    }
}
