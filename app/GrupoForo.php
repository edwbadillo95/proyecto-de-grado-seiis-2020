<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GrupoForo extends Model
{
    protected $table = 'grupo_foro';

    protected $fillable = ['nombre', 'privado'];

    public $timestamps = false;

    public function usuarios_grupo()
    {
        return $this->hasMany(UsuarioGrupo::class);
    }

    public function posts(){
        return $this->belongsToMany(Post::class, 'post_grupo');
    }

}
