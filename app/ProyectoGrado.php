<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class ProyectoGrado extends Model
{
    protected $table = 'proyecto_grado';
    protected $fillable = [ 'nombre', 'descripcion', ];

    public function estadoProyecto()
    {
        return $this->belongsTo(EstadoProyecto::class);
    }

    public function seguimientos()
    {
        return $this->hasMany(SeguimientoProyecto::class);
    }

    public function integrantes()
    {
        return $this->hasMany(IntegranteProyecto::class);
    }

    public function docente()
    {
        return $this->belongsTo(Docente::class);
    }

    public function getRevisadoAttribute()
    {
        return $this->estado_proyecto_id != EstadoProyecto::ESTADO_PREDETERMINADO;
    }

    public function getCalificadoAttribute()
    {
        return $this->aprobado != null;
    }

    public function traerDirector()
    {
        // Trae el integrante de proyecto que es director.
        return $this->integrantes()
            ->where('rol_integrante_id', RolIntegrante::DIRECTOR)
            ->whereNull('fecha_retiro')
            ->first();
    }

    public function puedeAgregarIntegrantes()
    {
        $usuario = Auth::user();
        return !$this->calificado && (
            $usuario->esDirectorProyecto($this)
            || $usuario->integranteComite() != null
        );
    }

    public static function getParaRevisar($id)
    {
        return ProyectoGrado::where('estado_proyecto_id', EstadoProyecto::ESTADO_PREDETERMINADO)
            ->where('id', $id)
            ->firstOrFail();
    }

    public static function proyectoVerificadoQuery($id)
    {
        return ProyectoGrado::where('estado_proyecto_id', '<>', EstadoProyecto::ESTADO_PREDETERMINADO)
            ->where('id', $id);
    }

    public static function puedeGestionarIntegrantes($id)
    {
        $estados = [
            EstadoProyecto::REVISION_APROBADA,
            EstadoProyecto::EN_DESARROLLO,
        ];
        return ProyectoGrado::whereIn('estado_proyecto_id', $estados)
            ->where('id', $id)
            ->exists();
    }

    public static function estaCalificado($id)
    {
        return ProyectoGrado::where('id', $id)
            ->whereNull('aprobado')
            ->exists();
    }

    public function documentos()
    {
        return $this->hasMany(DocumentoProyecto::class,'proyecto_grado_id','id');
    }

    public function estaTerminado()
    {
        return $this->aprobado != null && $this->estadoProyecto->calificado();
    }
}
