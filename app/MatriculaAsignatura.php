<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MatriculaAsignatura extends Model
{
    protected $table = 'matricula_asignatura';

    protected $guarded = [];

    public function matricula()
    {
        return $this->belongsTo(Matricula::class);
    }
    public function materia()
    {
        return $this->belongsTo(Materia::class);
    }
}
