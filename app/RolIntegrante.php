<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RolIntegrante extends Model
{
    public const DIRECTOR = 2;
    public const ESTUDIANTE = 1;
    public const CALIFICADOR = 4;

    protected $table = 'rol_integrante';

    public function esEstudiante()
    {
        return $this->id == self::ESTUDIANTE;
    }
}
