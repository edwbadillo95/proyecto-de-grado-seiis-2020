<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Horario extends Model
{
    protected $table = 'horario';

    protected $fillable = [ 'dia', 'hora_inicio', 'hora_fin', 'salon_id', 'grupo_id'];

    public function salon()
    {
        return $this->belongsTo(Salon::class);
    }

    public function grupo()
    {
        return $this->belongsTo(Grupo::class);
    }
}
