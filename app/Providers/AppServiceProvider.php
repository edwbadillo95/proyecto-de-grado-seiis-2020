<?php

namespace App\Providers;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        // Componentes blade
        Blade::component('partials.card', 'panel');
        Blade::component('partials.btnsubmit', 'btnsubmit');
        Blade::component('partials.btnreset', 'btnreset');
        Blade::component('partials.btndelete', 'btndelete');
    }
}
