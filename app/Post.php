<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = 'post';

    protected $fillable = ['nombre', 'contenidopost', 'fechapublicacion'];

    public function usuario(){
        return $this->belongsTo(User::class, 'usuario_id');
    }

    public function comentarios(){
        return $this->hasMany(ComentarioPost::class)->where('comentario_post.estado', '1');
    }

    public function archivos(){
        return $this->hasMany(ArchivoPost::class);
    }

    public function grupoForo(){
        return $this->belongsToMany(GrupoForo::class, 'post_grupo');
    }
    
    public function etiquetas(){
        return $this->belongsToMany(Etiqueta::class, 'post_etiqueta');
    }
}
