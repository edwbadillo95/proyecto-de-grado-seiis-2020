<?php

namespace App\Policies;

use App\Estudiante;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class EstudiantePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    public function before($user , $ability)
    {
        if($user->tieneRolCoordinador()){
            return true;
        }
    }
    
    public function show1(User $autenticado, Estudiante $estudiante)
    {
        return $autenticado->esEstudiante() == $estudiante->id;
    }

    public function update1(User $autenticado, Estudiante $estudiante)
    {
        return $autenticado->esEstudiante() == $estudiante->id;
    }
}
