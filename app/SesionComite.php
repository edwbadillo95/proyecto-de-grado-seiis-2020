<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SesionComite extends Model
{
    protected $table = 'sesion_comite';
    protected $fillable = [ 'asunto', 'descripcion', 'grupo_comite_id', 'fecha' ];

    public function grupoComite()
    {
        return $this->belongsTo(GrupoComite::class);
    }

    public function asistentes()
    {
        return $this->belongsToMany(IntegranteComite::class, 'asistencia_sesion');
    }

    public function actas()
    {
        return $this->hasMany(ActaGenerada::class);
    }

    public static function puedeRegistrarAsistencia($sesionId)
    {
        return SesionComite::where('id', $sesionId)
            ->where('fecha', today())
            ->exists();
    }
}
