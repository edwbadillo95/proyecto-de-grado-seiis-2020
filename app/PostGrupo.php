<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostGrupo extends Model
{
    protected $table = 'post_grupo';

    public $timestamps = false;

    public function grupoForo(){
        return $this->belongsTo(GrupoForo::class);
    }

    public function post(){
        return $this->belongsTo(Post::class);
    }
}
