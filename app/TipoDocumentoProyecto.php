<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoDocumentoProyecto extends Model
{
    protected $table = 'tipo_documento_proyecto';
    protected $fillable = ['nombre'];

    
}
