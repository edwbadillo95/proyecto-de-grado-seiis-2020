$(document).ready(function () {
    $('#adjuntos').on('change', function () {
        var arFiles = $(this)[0].files;
        $("#nombreAdjuntos").empty();
        $(arFiles).each((i, obj) => {
            $("#nombreAdjuntos").append($("<li/>").text(obj.name));
        });
    });

    $("#adjuntosComentario").on('change', function () {
        var arFiles = $(this)[0].files;
        $("#nombreAdjuntos").empty();
        $(arFiles).each((i, obj) => {
            $("#nombreAdjuntos").append($("<li/>").text(obj.name));
        });
    });
});

function habilitarComentario(obj, idComentario) {
    $(obj).empty().removeAttr("onclick");
    var url = obtenerUrlComentario();
    var csrf = obtenerCsrf();
    var form = $("<form/>").attr({
        "action": url,
        "method": "post",
        "enctype": "multipart/form-data"
    });
    form.attr("action", form.attr("action").replace(':idComentario', idComentario));
    form.append(csrf);
    var divContenedor = $("<div/>").addClass("col-sm-12 col-md-12 mt-2");
    var textarea = $("<textarea/>").addClass("form-control font11").attr({
        "name": "comentario",
        "rows": 1
    });
    divContenedor.append(textarea);
    var div12 = $("<div/>").addClass("col-md-12 col-sm-12 p-0");
    var divRow = $("<div/>").addClass("row");
    var divFiles = $("<div/>").addClass("col-md-6 col-sm-6");
    var iAdjuntos = $("<i/>").attr({
        class: "fas fa-file-alt iconoAdjuntos",
        onclick: "$(\"#adjuntosComentario_" + idComentario + "\").trigger(\"click\")"
    });
    var inputAdjuntos = $("<input/>").attr({
        "style": "display:none;",
        "type": "file",
        "name": "adjuntosComentario[]",
        "id": "adjuntosComentario_" + idComentario,
        "multiple": "multiple"
    }).on('change', function () {
        var arFiles = $(this)[0].files;
        $("#span_" + idComentario).empty();
        $(arFiles).each((i, obj) => {
            $("#span_" + idComentario).append($("<li/>").text(obj.name));
        });
    });
    divFiles.append(iAdjuntos, $("<span/>").attr('id', 'span_' + idComentario).addClass("font11").text(" Archivos"), inputAdjuntos);
    var divSubmit = $("<div/>").addClass("col-md-6 col-sm-6");
    var btnSubmit = $("<button/>").attr({
        "class": "btn btn-uts font12 float-right",
        "type": "submit"
    }).text("Comentar");
    divSubmit.append(btnSubmit);

    div12.append(divRow.append(divFiles, divSubmit));
    divContenedor.append(div12);
    form.append(divContenedor);

    $(obj).append(form);
}

function eliminarComentario($idComentario){
    var form = $("#formEliminarComentario");
    form.attr("action", form.attr("action").replace(':idComentario', $idComentario));
    $("#modal-delete-comentario").modal("show");
}