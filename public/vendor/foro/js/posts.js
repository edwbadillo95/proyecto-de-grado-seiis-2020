$(document).ready(function () {
    $("#fireFiles").click(function () {
        $("#adjuntos").trigger('click');
    });

    $('#adjuntos').on('change', function () {
        console.log($(this));
        console.log($(this)[0].files);
        var arFiles = $(this)[0].files;
        $("#nombreAdjuntos").empty();
        $(arFiles).each((i, obj) => {
            $("#nombreAdjuntos").append($("<li/>").text(obj.name));
        });
    })
})
