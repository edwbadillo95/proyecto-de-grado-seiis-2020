function removeUsuarioGrupo(idGrupo, idUsuario) {
    confirm(function (res) {
        var url = deletePostUri;
        url = url.replace(':idGrupo', idGrupo);
        url = url.replace(':idUsuario', idUsuario);
        $.ajax(
            {
                url: url,
                type: 'DELETE',
                dataType: "JSON",
                data: {
                    "idGrupo": idGrupo,
                    "idUser": idUsuario,
                    "_method": 'DELETE',
                    "_token": $("input[name='_token']").attr("value")
                },
                success: function (data) {
                    if (data.success) {
                        $("#" + idGrupo + "_" + idUsuario).remove();
                    } else {
                        alert("Se presento un error elimiando el Usuario");
                    }
                }
            });
    }, undefined, "Esta seguro de eliminar el Usuario?, Este proceso es irreversible");
}