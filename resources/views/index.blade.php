@extends('sbadmin.index')

@section('sbadmin-body')
    @panel
    @slot('icon') user @endslot
    @slot('titulo') Bienvenido {{ Auth::user()->name }} a LARISA
    @endslot

    <title>Inicio - Unidades Tecnológicas de Santander</title>

    <style type="text/css" data-type="vc_shortcodes-custom-css">
        .vc_custom_1597008854300 {
            margin-top: 8px !important;
        }

        .vc_custom_1597008876967 {
            margin-top: -28px !important;
        }

        .vc_custom_1590980932203 {
            margin-top: 8px !important;
        }

        .vc_custom_1596850736290 {
            margin-top: -28px !important;
        }

        .vc_custom_1601322132446 {
            margin-top: -35px !important;
        }

        .vc_custom_1598472044502 {
            margin-top: -35px !important;
        }

    </style>

    <script src="https://www.uts.edu.co/sitio/wp-includes/js/wp-emoji-release.min.js?ver=5.2.9" type="text/javascript"
        defer=""></script>


    <link rel="stylesheet" id="google-language-translator-css"
        href="https://www.uts.edu.co/sitio/wp-content/plugins/google-language-translator/css/style.css?ver=6.0.8" media="">
    <link rel="stylesheet" id="glt-toolbar-styles-css"
        href="https://www.uts.edu.co/sitio/wp-content/plugins/google-language-translator/css/toolbar.css?ver=6.0.8"
        media="">



    <link rel="stylesheet" id="simple-share-buttons-adder-font-awesome-css"
        href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css?ver=5.2.9" media="all">



    <link rel="stylesheet" id="js_composer_front-css"
        href="https://www.uts.edu.co/sitio/wp-content/plugins/js_composer/assets/css/js_composer.min.css?ver=6.0.5"
        media="all">





    <div class="off-canvas-overlay"></div>


    <div class="wrap">

        <div class="main-page-content" id="content">

            <div class="site-content-inner vc-container" role="main">

                <article id="post-17" class="post-17 page type-page status-publish hentry">


                    <div class="vc_row-full-width vc_clearfix"></div>


                    <span class="vc_sep_line"></span>
                    <h4 align="center">Unidades Tecnológicas de Santander</h4><span class="vc_sep_holder vc_sep_holder_r"><span
                            class="vc_sep_line"></span></span>




                    <div class="vc_row-full-width vc_clearfix" >
                    <div class="wpb_column vc_column_container vc_col-sm-4">
                        <div class="vc_column-inner">
                            <div class="wpb_wrapper">
                                <div class="wpb_single_image wpb_content_element vc_align_center  vc_custom_1597008854300">

                                    <figure class="wpb_wrapper vc_figure">
                                        <a href="https://atena.uts.edu.co/login/index.php" target="_blank"
                                            class="vc_single_image-wrapper   vc_box_border_grey"><img width="50" height="44"
                                                src="https://www.uts.edu.co/sitio/wp-content/uploads/2020/08/atena-icon.jpg"
                                                class="vc_single_image-img attachment-full" alt=""></a>
                                    </figure>
                                </div>

                                <div class="wpb_text_column wpb_content_element  vc_custom_1597008876967">
                                    <div class="wpb_wrapper">
                                        <p style="text-align: center;"><a href="https://atena.uts.edu.co/login/index.php"
                                                target="_blank" rel="noopener noreferrer"><strong>ATENA</strong></a></p>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="wpb_column vc_column_container vc_col-sm-4">
                        <div class="vc_column-inner">
                            <div class="wpb_wrapper">
                                <div class="wpb_single_image wpb_content_element vc_align_center  vc_custom_1590980932203">

                                    <figure class="wpb_wrapper vc_figure">
                                        <a href="https://www.uts.edu.co/sitio/atencion" target="_self"
                                            class="vc_single_image-wrapper   vc_box_border_grey"><img width="50" height="44"
                                                src="https://www.uts.edu.co/sitio/wp-content/uploads/2020/05/casa-icon.jpg"
                                                class="vc_single_image-img attachment-full" alt=""></a>
                                    </figure>
                                </div>

                                <div class="wpb_text_column wpb_content_element  vc_custom_1596850736290">
                                    <div class="wpb_wrapper">
                                        <p style="text-align: center;"><strong><a
                                                    href="https://www.uts.edu.co/sitio/atencion" rel="noopener">Soporte
                                                    CASA</a></strong></p>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="wpb_column vc_column_container vc_col-sm-4">
                        <div class="vc_column-inner">
                            <div class="wpb_wrapper">
                                <div class="vc_icon_element vc_icon_element-outer vc_icon_element-align-center">
                                    <div
                                        class="vc_icon_element-inner vc_icon_element-color-peacoc vc_icon_element-size-lg vc_icon_element-style- vc_icon_element-background-color-grey">
                                        <span class="vc_icon_element-icon fa fa-globe"></span><a
                                            class="vc_icon_element-link" href="https://www.uts.edu.co/sitio/internacional/"
                                            title="Atención al Ciudadano" target="_self"></a>
                                    </div>
                                </div>
                                <div class="wpb_text_column wpb_content_element  vc_custom_1601322132446">
                                    <div class="wpb_wrapper">
                                        <p style="text-align: center;"><a
                                                href="https://www.uts.edu.co/sitio/internacional/">UTS
                                                Internacional</a></p>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="vc_row-full-width vc_clearfix"></div>
                                     <div class="vc_row wpb_row vc_row-fluid vc_custom_1528900679139">
                        <div class="wpb_column vc_column_container vc_col-sm-12">
                            <div class="vc_column-inner">
                                <div class="wpb_wrapper">


                                </div>
                                </div>




                                <div
                                    class="vc_separator wpb_content_element vc_separator_align_center vc_sep_width_100 vc_sep_pos_align_center vc_sep_color_green vc_custom_1599147566399 vc_separator-has-text">
                                    <span class="vc_sep_holder vc_sep_holder_l"><span class="vc_sep_line"></span></span>
                                    <h4>Infórmate</h4><span class="vc_sep_holder vc_sep_holder_r"><span
                                            class="vc_sep_line"></span></span>
                                </div>
                                <div class="vc_row wpb_row vc_inner vc_row-fluid">
                                    <div class="wpb_column vc_column_container vc_col-sm-1/5">
                                        <div class="vc_column-inner">
                                            <div class="wpb_wrapper">
                                                <div class="wpb_single_image wpb_content_element vc_align_left">

                                                    <figure class="wpb_wrapper vc_figure">
                                                        <a href="https://www.uts.edu.co/sitio/revista-soy-uteista/"
                                                            target="_blank"
                                                            class="vc_single_image-wrapper   vc_box_border_grey"><img
                                                                width="399" height="101"
                                                                src="https://www.uts.edu.co/sitio/wp-content/uploads/2020/09/icon-revista.jpg"
                                                                class="vc_single_image-img attachment-full" alt=""
                                                                srcset="https://www.uts.edu.co/sitio/wp-content/uploads/2020/09/icon-revista.jpg 399w, https://www.uts.edu.co/sitio/wp-content/uploads/2020/09/icon-revista-300x76.jpg 300w"
                                                                sizes="(max-width: 399px) 100vw, 399px"></a>
                                                    </figure>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="wpb_column vc_column_container vc_col-sm-1/5">
                                        <div class="vc_column-inner">
                                            <div class="wpb_wrapper">
                                                <div class="wpb_single_image wpb_content_element vc_align_left">

                                                    <figure class="wpb_wrapper vc_figure">
                                                        <a href="https://www.uts.edu.co/sitio/agenda-institucional/"
                                                            target="_blank"
                                                            class="vc_single_image-wrapper   vc_box_border_grey"><img
                                                                width="318" height="80"
                                                                src="https://www.uts.edu.co/sitio/wp-content/uploads/2020/11/iconos.jpg"
                                                                class="vc_single_image-img attachment-full" alt=""
                                                                srcset="https://www.uts.edu.co/sitio/wp-content/uploads/2020/11/iconos.jpg 318w, https://www.uts.edu.co/sitio/wp-content/uploads/2020/11/iconos-300x75.jpg 300w"
                                                                sizes="(max-width: 318px) 100vw, 318px"></a>
                                                    </figure>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="wpb_column vc_column_container vc_col-sm-1/5">
                                        <div class="vc_column-inner">
                                            <div class="wpb_wrapper">
                                                <div class="wpb_single_image wpb_content_element vc_align_left">

                                                    <figure class="wpb_wrapper vc_figure">
                                                        <a href="http://www.turadiouts.com/" target="_blank"
                                                            class="vc_single_image-wrapper   vc_box_border_grey"><img
                                                                width="399" height="101"
                                                                src="https://www.uts.edu.co/sitio/wp-content/uploads/2020/09/icon-turadio.jpg"
                                                                class="vc_single_image-img attachment-full" alt=""
                                                                srcset="https://www.uts.edu.co/sitio/wp-content/uploads/2020/09/icon-turadio.jpg 399w, https://www.uts.edu.co/sitio/wp-content/uploads/2020/09/icon-turadio-300x76.jpg 300w"
                                                                sizes="(max-width: 399px) 100vw, 399px"></a>
                                                    </figure>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="wpb_column vc_column_container vc_col-sm-1/5">
                                        <div class="vc_column-inner">
                                            <div class="wpb_wrapper">
                                                <div class="wpb_single_image wpb_content_element vc_align_left">

                                                    <figure class="wpb_wrapper vc_figure">
                                                        <a href="https://www.uts.edu.co/sitio/wp-content/uploads/2019/10/simbolos_uts.pdf"
                                                            target="_blank"
                                                            class="vc_single_image-wrapper   vc_box_border_grey"><img
                                                                width="399" height="101"
                                                                src="https://www.uts.edu.co/sitio/wp-content/uploads/2020/09/icon-bandera.jpg"
                                                                class="vc_single_image-img attachment-full" alt=""
                                                                srcset="https://www.uts.edu.co/sitio/wp-content/uploads/2020/09/icon-bandera.jpg 399w, https://www.uts.edu.co/sitio/wp-content/uploads/2020/09/icon-bandera-300x76.jpg 300w"
                                                                sizes="(max-width: 399px) 100vw, 399px"></a>
                                                    </figure>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="wpb_column vc_column_container vc_col-sm-1/5">
                                        <div class="vc_column-inner">
                                            <div class="wpb_wrapper">
                                                <div class="wpb_single_image wpb_content_element vc_align_left">

                                                    <figure class="wpb_wrapper vc_figure">
                                                        <a href="https://www.uts.edu.co/sitio/parque-la-triada/"
                                                            target="_blank"
                                                            class="vc_single_image-wrapper   vc_box_border_grey"><img
                                                                width="318" height="80"
                                                                src="https://www.uts.edu.co/sitio/wp-content/uploads/2020/12/iconos-5.jpg"
                                                                class="vc_single_image-img attachment-full" alt=""
                                                                srcset="https://www.uts.edu.co/sitio/wp-content/uploads/2020/12/iconos-5.jpg 318w, https://www.uts.edu.co/sitio/wp-content/uploads/2020/12/iconos-5-300x75.jpg 300w"
                                                                sizes="(max-width: 318px) 100vw, 318px"></a>
                                                    </figure>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div
                                    class="vc_separator wpb_content_element vc_separator_align_center vc_sep_width_100 vc_sep_pos_align_center vc_sep_color_green vc_custom_1599147587466">
                                    <span class="vc_sep_holder vc_sep_holder_l"><span
                                            class="vc_sep_line"></span></span><span
                                        class="vc_sep_holder vc_sep_holder_r"><span class="vc_sep_line"></span></span>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>

            <div class="vc_row-full-width vc_clearfix"></div>
            <div class="vc_row wpb_row vc_row-fluid vc_custom_1571103027084">
                <div class="wpb_column vc_column_container vc_col-sm-12">
                    <div class="vc_column-inner">
                        <div class="wpb_wrapper">
                            <div
                                class="vc_separator wpb_content_element vc_separator_align_center vc_sep_width_100 vc_sep_pos_align_center vc_sep_color_green vc_custom_1571103035047 vc_separator-has-text">
                                <span class="vc_sep_holder vc_sep_holder_l"><span class="vc_sep_line"></span></span>
                                <h4>Unidos por la Graduacion</h4><span class="vc_sep_holder vc_sep_holder_r"><span
                                        class="vc_sep_line"></span></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="vc_row wpb_row vc_row-fluid vc_custom_1571103083879">
                <div class="wpb_column vc_column_container vc_col-sm-8">
                    <div class="vc_column-inner">
                        <div class="wpb_wrapper">
                            <div class="wpb_text_column wpb_content_element  vc_custom_1606777327401">
                                <div class="wpb_wrapper">
                                    <p style="text-align: justify;">Los Estudiantes de ingeniería de sistemas de Las <strong>Unidades Tecnológicas de
                                        Santander</strong> realizan el trabajo de grado creando una plataforma
                                        llamada <strong>LARISA</strong> este software permite a los futuros
                                        Ingenieros de sistemas subir en forma virtual sus documentos y códigos
                                        relacionados con el proyecto de grado para que sea más fácil la verificación
                                        de todos los documentos además de apoyar a la universidad con herramientas TIC,
                                        agradecemos que este software tenga un buen uso. </p>


                                </div>
                            </div>
                            <div class="vc_empty_space" style="height: 25px"><span class="vc_empty_space_inner"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="wpb_column vc_column_container vc_col-sm-4">
                    <div class="vc_column-inner">
                        <div class="wpb_wrapper">
                            <div class="wpb_single_image wpb_content_element vc_align_center">

                                <figure class="wpb_wrapper vc_figure">
                                    <img width="467" height="277"
                                            src="https://www.uts.edu.co/sitio/wp-content/uploads/2020/06/logo-graduados.jpg"
                                            class="vc_single_image-img attachment-full" alt="acreditación UTS"
                                            sizes="(max-width: 467px) 100vw, 467px"></a>
                                </figure>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="vc_row wpb_row vc_row-fluid vc_custom_1570461259519">

                <div class="vc_row wpb_row vc_inner vc_row-fluid vc_custom_1570065425434">
                    <div class="wpb_column vc_column_container vc_col-sm-12">
                        <div class="vc_column-inner">
                            <div class="wpb_wrapper">
                                <div
                                    class="vc_separator wpb_content_element vc_separator_align_center vc_sep_width_100 vc_sep_pos_align_center vc_sep_color_green vc_custom_1570065408595 vc_separator-has-text">
                                    <span class="vc_sep_holder vc_sep_holder_l"><span class="vc_sep_line"></span></span>
                                    <h4>Síguenos en nuestras redes sociales</h4><span
                                        class="vc_sep_holder vc_sep_holder_r"><span class="vc_sep_line"></span></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="vc_row wpb_row vc_inner vc_row-fluid vc_custom_1570065620167">
                    <div class="wpb_column vc_column_container vc_col-sm-2">
                        <div class="vc_column-inner">
                            <div class="wpb_wrapper">
                                <div class="vc_icon_element vc_icon_element-outer vc_icon_element-align-center">
                                    <div
                                        class="vc_icon_element-inner vc_icon_element-color-blue vc_icon_element-size-lg vc_icon_element-style- vc_icon_element-background-color-grey">
                                        <span class="vc_icon_element-icon fa fa-facebook-square"></span><a
                                            class="vc_icon_element-link"
                                            href="https://www.facebook.com/UnidadesTecnologicasdeSantanderUTS/?epa=SEARCH_BOX"
                                            title="" target=" _blank"></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="wpb_column vc_column_container vc_col-sm-2">
                        <div class="vc_column-inner">
                            <div class="wpb_wrapper">
                                <div class="vc_icon_element vc_icon_element-outer vc_icon_element-align-center">
                                    <div
                                        class="vc_icon_element-inner vc_icon_element-color-custom vc_icon_element-size-lg vc_icon_element-style- vc_icon_element-background-color-grey">
                                        <span class="vc_icon_element-icon fa fa-youtube-play"
                                            style="color:#dd3333 !important"></span><a class="vc_icon_element-link"
                                            href="https://www.youtube.com/channel/UC-rIi4OnN0R10Wp-cPiLcpQ" title=""
                                            target=" _blank"></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="wpb_column vc_column_container vc_col-sm-2">
                        <div class="vc_column-inner">
                            <div class="wpb_wrapper">
                                <div class="vc_icon_element vc_icon_element-outer vc_icon_element-align-center">
                                    <div
                                        class="vc_icon_element-inner vc_icon_element-color-pink vc_icon_element-size-lg vc_icon_element-style- vc_icon_element-background-color-grey">
                                        <span class="vc_icon_element-icon fa fa-instagram"></span><a
                                            class="vc_icon_element-link"
                                            href="https://www.instagram.com/unidades_uts/?hl=els-la" title=""
                                            target=" _blank"></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="wpb_column vc_column_container vc_col-sm-2">
                        <div class="vc_column-inner">
                            <div class="wpb_wrapper">
                                <div class="vc_icon_element vc_icon_element-outer vc_icon_element-align-center">
                                    <div
                                        class="vc_icon_element-inner vc_icon_element-color-turquoise vc_icon_element-size-lg vc_icon_element-style- vc_icon_element-background-color-grey">
                                        <span class="vc_icon_element-icon fa fa-twitter-square"></span><a
                                            class="vc_icon_element-link" href="https://twitter.com/Unidades_UTS" title=""
                                            target=" _blank"></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="wpb_column vc_column_container vc_col-sm-2">
                        <div class="vc_column-inner">
                            <div class="wpb_wrapper">
                                <div class="vc_icon_element vc_icon_element-outer vc_icon_element-align-center">
                                    <div
                                        class="vc_icon_element-inner vc_icon_element-color-blue vc_icon_element-size-lg vc_icon_element-style- vc_icon_element-background-color-grey">
                                        <span class="vc_icon_element-icon fa fa-linkedin-square"></span><a
                                            class="vc_icon_element-link"
                                            href="https://www.linkedin.com/company/unidades-tecnol%C3%B3gicas-de-santander/"
                                            title="" target=" _blank"></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="wpb_column vc_column_container vc_col-sm-2">
                        <div class="vc_column-inner">
                            <div class="wpb_wrapper"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>


    </div>


    </article><!-- #post -->



    </div>

    </div>

    </div> <!-- end .wrap -->











    @endpanel




@endsection
