@extends('layouts.app2')
<div id="content">
    @include('sbadmin.topbar.barranoti')
    @section('app-body')
    </div>
    @error('auth.failed')
        <div class="modal" tabindex="-1" id="auth-failed">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Error de autenticación</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>{{ $message }}</p>
                    </div>
                    <div class="modal-footer">
                        <button data-dismiss="modal" type="button" class="btn btn-danger">OK</button>
                    </div>
                </div>
            </div>
        </div>


    @enderror
    <div class="page-title">
        <div class="container"><br><br>
            <h1 class="h1">Avisos Institucionales</h1>
        </div>
    </div>
    @if (count($avisos ?? ''))

    @else
        <div class="alert alert-info">
           <h3 align ="center" > No hay Avisos Creados.</h3>
        </div>
    @endif
    <div class="main-page-content default-margin" id="content">
        <div class="site-content-inner container" role="main">

            <div class="blog-archive has-sidebar sidebar-right">
                <div class="blog-main-loop">
                    <div class="row row-cols-1 row-cols-md-3">
                        @foreach ($avisos as $g)
                            <div class="col mb-4" style="width: 18rem;">
                                <div class="card h-100">
                                    <a href="{{ route('contenidoa', ['id' => $g->id]) }}"><img
                                            src="{{ Storage::url($g->imagen) }}" class="card-img-top img-fluid"
                                            ></a>
                                    <div class="card-body">
                                        <a href="{{ route('contenidoa', ['id' => $g->id]) }}">
                                            <h2 class="h2">{!! $g->titulo !!}</h2>
                                        </a>
                                        <p class="card-text"><i class="fas fa-user-plus"><small> Publicado por:
                                                    {{ $g->autor }}</i> <br>
                                            <i class="fas fa-clock"> Desde: {{ $g->fecha_creacion }} -- Hasta:
                                                {{ $g->fecha_expiracion }}</small></i>
                                        </p>
                                    </div>
                                </div>
                            </div>

                        @endforeach
                    </div>
                </div>
                {!! $avisos->render() !!}
            </div>
            @include('sbadmin.footer')
        </div>
