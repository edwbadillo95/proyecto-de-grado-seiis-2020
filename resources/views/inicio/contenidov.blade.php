@extends('layouts.app2')
<div id="content">
    @include('sbadmin.topbar.barranoti')
    @section('app-body')

        @error('auth.failed')
            <div class="modal" tabindex="-1" id="auth-failed">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Error de autenticación</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <p>{{ $message }}</p>
                        </div>
                        <div class="modal-footer">
                            <button data-dismiss="modal" type="button" class="btn btn-danger">OK</button>
                        </div>
                    </div>
                </div>
            </div>
        @enderror
    </div>

<style>
    .center {
  display: block;
  margin-left: auto;
  margin-right: auto;
  width: 50%;
}
</style>


    <div class="container">
        <div class="col-md-10 col-md-offset-1">
                @foreach ($contenidov as $g)
                        <br>
                        <br>
                        <h5 class="h2" align="center">{!! $g->titulo !!}</h5><br>
                        <img src="{{ Storage::url($g->imagen) }}" class="center">
                        <br>
                        <br>
                        <h1>{!! $g->contenido !!}</h1><br>
                        <p><i>Publicado Por: {{ $g->autor }}</i></p>
                        <p><i class="fas fa-clock"> {{ $g->fecha_creacion }}</i></p>

                @endforeach

            </div>
        </div>

    @include('sbadmin.footer')
