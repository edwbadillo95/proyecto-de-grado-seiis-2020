<table class="table table-bordered table-sm table-striped table-hover">
    <thead>
        <tr class="thead-light">
            <th colspan="5">Solicitudes de Estudiantes</th>
        </tr>
        <tr>
            <th>Nombre</th>
            <th>Tipo Documento</th>
            <th>Numero Documento</th>
            <th>Correo</th>
            <th>Opciones</th>
        </tr>
    </thead>
    <tbody>
        @forelse($solicitudes as $s)
        <tr>
            <td>{{$s->estudiante->nombre1 .' '. $s->estudiante->nombre2 .' '. $s->estudiante->apellido1 .' '. $s->estudiante->apellido2}}</td>
            <td>{{$s->estudiante->tipo_documento}}</td>
            <td>{{$s->estudiante->numero_documento}}</td>
            <td>{{$s->estudiante->correo}}</td>
            <td>
                <form action="{{route('aceptar-solicitud-proyecto',[$s->id,$s->estudiante->id,$proyecto->id])}}" method="post" style="display: inline;">
                    @csrf
                    <button type="submit" class="btn btn-sm btn-success" onclick="return confirm('Esta seguro de aceptar esta solicitud?')">
                        <i class="fa fa-check"></i>
                    </button>
                </form>
                <form action="{{route('eliminar-solicitud-proyecto',[$s->id,$proyecto->id])}}" method="post" style="display: inline;">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-sm btn-danger" onclick="return confirm('Esta seguro de rechazar esta solicitud?')">
                        <i class="fa fa-times"></i>
                    </button>
                </form>

            </td>

        </tr>
        @empty
        <tr>
            <td colspan="5">No hay solicitudes de integración para este proyecto</td>
        </tr>
        @endforelse
    </tbody>
</table>
