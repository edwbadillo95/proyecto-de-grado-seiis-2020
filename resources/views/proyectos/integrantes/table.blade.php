
@include('partials.forms.saved')

<table class="table table-bordered table-sm table-striped table-hover">
    <thead>
        <tr class="thead-light">
            <th colspan="4">Integrantes del proyecto</th>
        </tr>
        <tr>
            <th>Nombre</th>
            <th>Rol</th>
            <th>Estado</th>
            <th>Opciones</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($proyecto->integrantes as $i)
            <tr>
                <td>{{ $i->nombre }}</td>
                <td>{{ $i->rolIntegrante->nombre }}</td>
                <td>
                    <span class="badge badge-{{ $i->fecha_retiro ? 'danger' : 'success' }}">
                        {{ $i->fecha_retiro ? 'INACTIVO' : 'ACTIVO' }}
                    </span>
                </td>
                <td>
                    <a href="{{ route('proyectos.integrantes.show', ['proyecto' => $proyecto->id, 'integrante' => $i->id]) }}" class="btn btn-sm btn-primary" title="Mostrar detalles">
                        <i class="fas fa-eye"></i>
                    </a>
                    @if (!$i->soyYo() && $proyecto->puedeAgregarIntegrantes() && !$i->fecha_retiro)
                    <button onclick="mostrarModalRetiro({{ $i->id }})" class="btn btn-sm btn-danger" title="Retirar">
                        <i class="fas fa-times-circle"></i>
                    </button>
                    @endif
                </td>
            </tr>
        @endforeach
    </tbody>
</table>

<form id="form-retirar" action="{{ route('proyectos.integrantes.destroy', ['proyecto' => request()->route('proyecto')]) }}" method="post">
    <div class="modal fade" id="modal-retirar" tabindex="-1" aria-labelledby="modalRetirarLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="modalRetirarLabel">Retirar Integrante</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                @csrf
                @method('DELETE')
                <input id="integrante-id" type="hidden" name="id" value="">

                <p>¿Está seguro que desea registrar el retiro del integrante indicando?</p>

                <label for="descripcion">Descripción (opcional)</label>
                <textarea name="descripcion" id="descripcion" cols="30" rows="10" placeholder="" class="form-control"></textarea>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
              <button type="submit" class="btn btn-danger">Retirar</button>
            </div>
          </div>
        </div>
      </div>
</form>

@push('js')
    <script>
        function mostrarModalRetiro(id) {
            $('#integrante-id').val(id)
            $('#modal-retirar').modal('show')
        }
    </script>
@endpush
