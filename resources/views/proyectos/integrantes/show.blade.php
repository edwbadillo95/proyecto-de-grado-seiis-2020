@extends('sbadmin.index')

@section('sbadmin-body')
@panel
    @slot('icon') user @endslot
    @slot('titulo') Integrante del proyecto @endslot

    @include('partials.forms.saved')

    <table class="table table-bordered">
        <thead>
            <tr>
                <th colspan="4" class="text-center">
                    <i class="fas fa-info-circle"></i> <i>Información del integrante del proyecto</i>
                </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th>Proyecto</th>
                <td colspan="3">
                    <a href="{{ route('comite.proyectos.show', ['proyecto'=>$integrante->proyecto_grado_id]) }}">
                        {{ $integrante->proyectoGrado->nombre }}
                    </a>
                </td>
            </tr>
            <tr>
                <th>Nombre Completo</th>
                <td>{{ $integrante->nombre }}</td>
                <th>Rol</th>
                <td>{{ $integrante->rolIntegrante->nombre }}</td>
            </tr>
            <tr>
                <th>Fecha de integración</th>
                <td>{{ $integrante->fecha_integracion }}</td>
                <th>Fecha de retiro</th>
                <td>{{ $integrante->fecha_retiro }}</td>
            </tr>
            <tr>
                <th>Descripción</th>
                <td colspan="3">{{ $integrante->descripcion }}</td>
            </tr>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="4">
                    <a href="{{ route('proyectos.integrantes.index', ['proyecto'=> $integrante->proyecto_grado_id]) }}" class="btn btn-secondary">
                        <i class="fas fa-list"></i> Ver integrantes del proyecto
                    </a>
                </td>
            </tr>
        </tfoot>
    </table>
@endpanel
@endsection
