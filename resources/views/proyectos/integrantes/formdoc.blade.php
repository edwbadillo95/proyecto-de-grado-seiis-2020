<form class="mb-3" action="{{ route('proyectos.integrantes.create', ['proyecto'=>$proyecto->id]) }}" method="get">
    <div class="card border-primary">
        <div class="card-header text-center">
            <b>Ingrese el documento de la persona que desea agregar.</b>
        </div>
        <div class="row p-3">
            <div class="col-xs-12 col-sm-6 col-md-4">
                <div class="input-group">
                    <input
                        value="{{ request()->documento }}"
                        name="documento"
                        type="text"
                        class="form-control"
                        placeholder="Documento"
                        aria-label="Documento"
                        aria-describedby="btn-doc">
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4">
                <select class="custom-select" name="rol_integrante" id="rol_integrante" value="{{ request()->rol_integrante }}">

                    @if (Auth::user()->docente)
                    <option @if (request()->rol_integrante == '1')
                        selected="selected"
                    @endif value="1">ESTUDIANTE</option>
                    @endif

                    @if (Auth::user()->integranteComite())
                    <option @if (request()->rol_integrante == '2')
                        selected="selected"
                    @endif value="2">DIRECTOR</option>
                    <option @if (request()->rol_integrante == '3')
                        selected="selected"
                    @endif value="3">COODIRECTOR</option>
                    <option @if (request()->rol_integrante == '4')
                        selected="selected"
                    @endif value="4">CALIFICADOR</option>
                    @endif
                </select>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-2">
                <button class="btn btn-block btn-primary">
                    <i class="fas fa-search"></i>
                    Buscar
                </button>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-2">
                <a class="btn btn-block btn-secondary" href="{{ route('proyectos.integrantes.index', ['proyecto'=>$proyecto->id]) }}">
                    <i class="fas fa-times"></i>
                    Cancelar
                </a>
            </div>
        </div>
    </div>
</form>
