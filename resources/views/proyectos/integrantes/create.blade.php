@extends('sbadmin.index')

@section('sbadmin-body')
@panel
    @slot('icon') user-plus @endslot
    @slot('titulo') Registrar integrante @endslot

    @include('proyectos.integrantes.formdoc')

    @if (request()->documento && !isset($persona) && !isset($msg))
    <div class="alert alert-danger">
        No se encontró una persona con el documento <b>{{ request()->documento }}</b> en el sistema.
    </div>
    @endif

    @if (request()->documento && !isset($persona) && isset($msg))
    <div class="alert alert-danger">
        {{ $msg }}
    </div>
    @endif

    @isset($persona)
    <form action="{{ route('proyectos.integrantes.store', ['proyecto' => request()->route('proyecto')]) }}" method="post">
        <div class="card">
            @csrf
            <input type="hidden" name="integrante_id" value="{{ $persona['id'] }}">
            <input type="hidden" name="rol_integrante_id" value="{{ request()->rol_integrante }}">
            <table class="table table-bordered table-sm">
                <thead>
                    <tr class="thead-light">
                        <th colspan="4">Detalles de la persona</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th>Documento</th>
                        <td>{{ $persona['documento'] }}</td>
                        <th>Nombre Completo</th>
                        <td>{{ $persona['nombre'] }}</td>
                    </tr>
                </tbody>
            </table>

            <div class="card-footer text-center">
                <button class="btn btn-uts" type="submit">
                    <i class="fas fa-user-plus"></i> Registrar
                </button>
                <a class="btn btn-secondary" href="{{ route('proyectos.integrantes.create', ['proyecto'=>request()->route('proyecto')]) }}">
                    <i class="fas fa-redo"></i> Cancelar
                </a>
            </div>
        </div>
    </form>
    @endisset
@endpanel
@endsection
