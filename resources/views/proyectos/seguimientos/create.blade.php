@extends('sbadmin.index')

@section('sbadmin-body')
@panel
    @slot('icon') clipboard-list @endslot
    @slot('titulo') Registrar seguimiento @endslot

    <form action="{{ route('proyectos.seguimientos.store', ['proyecto'=>$proyecto->id]) }}" method="post">
        @csrf

        @include('proyectos.seguimientos.form')
    </form>
@endpanel
@endsection
