<table class="table table-bordered table-sm table-striped table-hover">
    <thead>
        <tr class="thead-light">
            <th colspan="4">Seguimientos</th>
        </tr>
        <tr>
            <th>Asunto</th>
            <th>Docente</th>
            <th>Opciones</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($proyecto->seguimientos as $s)
            <tr>
                <td>{{ $s->asunto }}</td>
                <td>{{ $s->integranteComite->docente->nombres }} {{ $s->integranteComite->docente->apellidos }}</td>
                <td>
                    <a href="{{ route('proyectos.seguimientos.show', ['proyecto' => $proyecto->id, 'seguimiento' => $s->id]) }}" class="btn btn-sm btn-primary" title="Mostrar detalles">
                        <i class="fas fa-eye"></i>
                    </a>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>
