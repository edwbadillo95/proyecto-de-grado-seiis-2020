@extends('sbadmin.index')

@section('sbadmin-body')
@panel
    @slot('icon') clipboard-list @endslot
    @slot('titulo') Seguimientos @endslot

    <p>Seguimiento/observaciones registradas para el proyecto.</p>

    <table class="table table-bordered">
        <thead>
            <tr>
                <th colspan="4" class="text-center">
                    <i class="fas fa-info-circle"></i> <i>Información del proyecto de grado</i>
                </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th>Nombre Completo</th>
                <td>{{ $proyecto->nombre }}</td>
                <th>Rol</th>
                <td>{{ $proyecto->descripcion }}</td>
            </tr>
            <tr>
                <th>Estado</th>
                <td>{{ $proyecto->estadoProyecto->nombre }}</td>
                <th>Docente</th>
                <td>{{ $proyecto->docente->nombres }} {{ $proyecto->docente->apellidos }}</td>
            </tr>
            <tr>
                <th>Fecha de registro</th>
                <td>{{ $proyecto->created_at }}</td>
                <th>Fecha de modificación</th>
                <td>{{ $proyecto->updated_at }}</td>
            </tr>
        </tbody>
    </table>

    @if (Auth::user()->puedeAgregarSeguimientosAlProyecto($proyecto))
    <a href="{{ route('proyectos.seguimientos.create', ['proyecto'=> $proyecto->id]) }}" class="btn btn-success mb-2">
        <i class="fas fa-user-plus"></i> Agregar Observación
    </a>
    @endif

    @include('proyectos.seguimientos.table')

@endpanel
@endsection
