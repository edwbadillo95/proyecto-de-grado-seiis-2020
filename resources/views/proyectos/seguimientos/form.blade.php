
<table class="table table-bordered">
    <thead>
        <tr>
            <th colspan="4" class="text-center">
                <i class="fas fa-info-circle"></i> <i>Información del proyecto de grado</i>
            </th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <th>Nombre Completo</th>
            <td>{{ $proyecto->nombre }}</td>
            <th>Descripción</th>
            <td>{{ $proyecto->descripcion }}</td>
        </tr>
        <tr>
            <th>Estado</th>
            <td>{{ $proyecto->estadoProyecto->nombre }}</td>
            <th>Docente</th>
            <td>{{ $proyecto->docente->nombres }} {{ $proyecto->docente->apellidos }}</td>
        </tr>
        <tr>
            <th>Fecha de registro</th>
            <td>{{ $proyecto->created_at }}</td>
            <th>Fecha de modificación</th>
            <td>{{ $proyecto->updated_at }}</td>
        </tr>
    </tbody>
</table>

<p>Digite el siguiente formulario para el registro de un seguimiento al proyecto indicado. Todos los campos son obligatorios.</p>

@include('partials.forms.error')

<div class="row">
    <div class="col-sm-12">
        <div class="form-group">
            <label for="asunto">Asunto *</label>
            <textarea
                required rows="2"
                id="asunto"
                name="asunto"
                class="form-control @error('asunto') is-invalid @enderror">{{ old('asunto', $seguimiento->asunto) }}</textarea>

            @error('asunto')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>
    </div>
    <div class="col-sm-12">
        <div class="form-group">
            <label for="descripcion">Descripción *</label>
            <textarea
                required rows="4"
                id="descripcion"
                name="descripcion"
                class="form-control @error('descripcion') is-invalid @enderror">{{ old('descripcion', $seguimiento->descripcion) }}</textarea>

            @error('descripcion')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>
    </div>
</div>

<div class="float-right">
    <a href="{{ route('comite.proyectos.index') }}" class="btn btn-secondary">
        <i class="fas fa-times"></i> Cancelar
    </a>
    @btnreset @endbtnreset
    @btnsubmit @endbtnsubmit
</div>
