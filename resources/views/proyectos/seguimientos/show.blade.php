@extends('sbadmin.index')

@section('sbadmin-body')
@panel
    @slot('icon') clipboard-list @endslot
    @slot('titulo') Seguimiento del proyecto de grado @endslot

    @include('partials.forms.saved')

    <table class="table table-bordered">
        <thead>
            <tr>
                <th colspan="4" class="text-center">
                    <i class="fas fa-info-circle"></i> <i>Información del seguimiento</i>
                </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th>Proyecto</th>
                <td colspan="3">
                    <a href="{{ route('comite.proyectos.show', ['proyecto'=>$seguimiento->proyecto_grado_id]) }}">
                        {{ $seguimiento->proyectoGrado->nombre }}</a>
                </td>
            </tr>
            <tr>
                <th>Asunto</th>
                <td colspan="3">{{ $seguimiento->asunto }}</td>
            </tr>
            <tr>
                <th>Descripción</th>
                <td colspan="3">{{ $seguimiento->descripcion }}</td>
            </tr>
            <tr>
                <th>Docente</th>
                <td colspan="3">{{ $seguimiento->integranteComite->docente->nombres }} {{ $seguimiento->integranteComite->docente->apellidos }}</td>
            </tr>
            <tr>
                <th>Fecha de registro</th>
                <td>{{ $seguimiento->created_at }}</td>
                <th>Fecha de modificación</th>
                <td>{{ $seguimiento->updated_at }}</td>
            </tr>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="4">
                    <a href="{{ route('proyectos.seguimientos.index', ['proyecto' => $seguimiento->proyecto_grado_id]) }}" class="btn btn-secondary">
                        <i class="fas fa-list"></i> Ver seguimientos
                    </a>
                    @if (!$seguimiento->proyectoGrado->calificado)
                    <a href="{{ route('proyectos.seguimientos.edit', ['proyecto' => $seguimiento->proyecto_grado_id, 'seguimiento' => $seguimiento->id]) }}" class="btn btn-warning">
                        <i class="fas fa-edit"></i> Editar
                    </a>
                    @endif
                </td>
            </tr>
        </tfoot>
    </table>
@endpanel
@endsection
