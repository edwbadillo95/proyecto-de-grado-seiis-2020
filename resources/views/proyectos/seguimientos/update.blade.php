@extends('sbadmin.index')

@section('sbadmin-body')
@panel
    @slot('icon') clipboard-list @endslot
    @slot('titulo') Registrar seguimiento @endslot

    <form action="{{ route('proyectos.seguimientos.update', ['proyecto'=>$proyecto->id, 'seguimiento' => $seguimiento->id]) }}" method="post">
        @csrf
        @method('PUT')

        @include('proyectos.seguimientos.form')
    </form>
@endpanel
@endsection
