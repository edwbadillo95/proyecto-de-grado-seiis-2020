<div class="card">
    <h5 class="card-header">
        @isset($icon)
        <i class="fas fa-{{ $icon }} mr-1"></i>
        @endisset
        {{ $titulo }}
    </h5>
    <div class="card-body">
        {{ $slot }}
    </div>
</div>
