@if (session()->has('deleted'))
<div class="alert alert-warning alert-dismissible fade show" role="alert">
    <p class="mb-0">
        <b><i class="fas fa-trash"></i></b> Los datos han sido eliminados correctamente.
    </p>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif
