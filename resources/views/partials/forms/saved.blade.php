@if (session()->has('saved'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <p class="mb-0">
        <b><i class="fas fa-check-circle"></i></b> Los datos han sido registrados correctamente.
    </p>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif
