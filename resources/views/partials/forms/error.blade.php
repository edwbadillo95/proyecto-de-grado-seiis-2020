@if ($errors->all())
<div class="alert alert-danger alert-dismissible fade show" role="alert">
    <strong>Error de formulario!</strong>
    <p class="mb-0">
        Ha ingresado datos incorrectos, por favor, corrija los datos antes de continuar.
    </p>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif
