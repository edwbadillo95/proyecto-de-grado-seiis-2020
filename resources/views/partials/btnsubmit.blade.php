<button class="btn btn-uts" type="submit">
    @if (isset($icon))
    <i class="fas fa-{{ $icon }}"></i>
    @else
    <i class="fas fa-save"></i>
    @endif

    @if (isset($text))
    <span>{{ $text }}</span>
    @else
    <span>Guardar</span>
    @endif
</button>
