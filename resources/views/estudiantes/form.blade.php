<p>Digite el siguiente formulario, los campos con * son obligatorios.</p>

@include('partials.forms.error')

<div class="row">
    <div class="col-sm-12 col-md-3">
        <div class="form-group">
            <label for="nombre1">Primer Nombre *</label>
            <input id="nombre1" name="nombre1" placeholder="Ingrese Primer Nombre" value="{{ old('nombre1', $estudiante->nombre1) }}" type="text" class="form-control @error('nombre1') is-invalid @enderror">

            @error('nombre1')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
            @enderror
        </div>
    </div>
    <div class="col-sm-12 col-md-3">
        <div class="form-group">
            <label for="nombre2">Segundo Nombre </label>
            <input id="nombre2" name="nombre2" placeholder="Ingrese Segundo Nombre" value="{{ old('nombre2', $estudiante->nombre2) }}" type="text" class="form-control @error('nombre2') is-invalid @enderror">

            @error('nombre2')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
            @enderror
        </div>
    </div>
    <div class="col-sm-12 col-md-3">
        <div class="form-group">
            <label for="apellido1">Primer Apellido *</label>
            <input id="apellido1" name="apellido1" placeholder="Ingrese Primer Apellido" value="{{ old('apellido1', $estudiante->apellido1) }}" type="text" class="form-control @error('apellido1') is-invalid @enderror">

            @error('apellido1')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
            @enderror
        </div>
    </div>
    <div class="col-sm-12 col-md-3">
        <div class="form-group">
            <label for="apellido2">Segundo Apellido *</label>
            <input id="apellido2" name="apellido2" placeholder="Ingrese Segundo Apellido" value="{{ old('apellido2', $estudiante->apellido2) }}" type="text" class="form-control @error('apellido2') is-invalid @enderror">

            @error('apellido2')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
            @enderror
        </div>
    </div>

</div>
<div class="row">
<div class="col-sm-12 col-md-4">
        <div class="form-group">
            <label for="tipo_documento">Tipo Documento *</label><br>
            <select value="{{ old('tipo_documento', $estudiante->tipo_documento) }}" name="tipo_documento" class="custom-select @error('tipo_documento') is-invalid @enderror" id="tipo_documento">
                <option selected value="null">Seleccione Tipo Doc</option>
                <option
                @if (old('tipo_documento', $estudiante->tipo_documento) == 'CC')
                    selected="selected"
                @endif
                value="CC">CEDULA</option>

                <option
                @if (old('tipo_documento', $estudiante->tipo_documento) == 'TI')
                    selected="selected"
                @endif
                value="TI">TARGETA DE IDENTIDAD</option>
            </select>
            @error('tipo_documento')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>
    </div>
    <div class="col-sm-12 col-md-4">
        <div class="form-group">
            <label for="numero_documento">Numero Documento *</label><br>
            <input id="numero_documento" placeholder="Ingrese N. documento" name="numero_documento" value="{{ old('numero_documento', $estudiante->numero_documento) }}" type="number" class="form-control @error('numero_documento') is-invalid @enderror">

            @error('numero_documento')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
            @enderror
        </div>
    </div>
    <div class="col-sm-12 col-md-4">
        <div class="form-group">
            <label for="tipo_carrera">Tipo Carrera *</label><br>
            <select value="{{ old('tipo_carrera', $estudiante->tipo_carrera) }}" name="tipo_carrera" class="custom-select @error('tipo_carrera') is-invalid @enderror" id="tipo_carrera">
            <option selected value="null">Seleccione Tipo Carrera</option>
                <option
                @if (old('tipo_carrera', $estudiante->tipo_carrera) == 'PROFESIONAL')
                    selected="selected"
                @endif
                value="PROFESIONAL">PROFESIONAL</option>

                <option
                @if (old('tipo_carrera', $estudiante->tipo_carrera) == 'TECNOLOGIA')
                    selected="selected"
                @endif
                value="TECNOLOGIA">TECNOLOGIA</option>
            </select>
            @error('tipo_carrera')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12 col-md-4">
        <div class="form-group">
            <label for="fecha_nacimiento">Fecha de Nacimiento *</label><br>
            <input id="fecha_nacimiento" name="fecha_nacimiento" value="{{ old('fecha_nacimiento', $estudiante->fecha_nacimiento) }}" type="date" class="form-control @error('fecha_nacimiento') is-invalid @enderror">

            @error('fecha_nacimiento')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
            @enderror
        </div>
    </div>
    <div class="col-sm-12 col-md-4">
        <div class="form-group">
            <label for="sexo">Sexo *</label><br>
            <select value="{{ old('sexo', $estudiante->sexo) }}" name="sexo" class="custom-select @error('sexo') is-invalid @enderror" id="sexo">
            <option selected value="null">Seleccione Sexo</option>
                <option
                @if (old('sexo', $estudiante->sexo) == 'F')
                    selected="selected"
                @endif
                value="F">FEMENINO</option>

                <option
                @if (old('sexo', $estudiante->sexo) == 'M')
                    selected="selected"
                @endif
                value="M">MASCULINO</option>
            </select>
            @error('sexo')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>
    </div>
    <div class="col-sm-12 col-md-4">
        <div class="form-group">
            <label for="correo">Correo *</label><br>
            <input id="correo" placeholder="Ingrese Correo" name="correo" value="{{ old('correo', $estudiante->correo) }}" type="email" class="form-control @error('correo') is-invalid @enderror">

            @error('correo')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
            @enderror
        </div>
    </div>

</div>

<hr>


<div class="float-right">
    <a href="{{ route('estudiantes.index') }}" class="btn btn-secondary">
        <i class="fas fa-times"></i> Cancelar
    </a>
    @btnreset @endbtnreset
    @btnsubmit @endbtnsubmit
</div>