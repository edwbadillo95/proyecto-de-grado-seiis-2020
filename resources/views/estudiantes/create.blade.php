@extends('sbadmin.index')

@section('sbadmin-body')
@panel
    @slot('icon') user-friends @endslot
    @slot('titulo') Registrar Estudiante @endslot

    <form action="{{ route('estudiantes.store') }}" method="post">
        @csrf

        @include('estudiantes.form')
    </form>
@endpanel
@endsection