@extends('sbadmin.index')

@section('sbadmin-body')
@panel
@slot('icon') user-friends @endslot
@slot('titulo') Inscripción Proyecto de Grado @endslot

<div class="table-responsive">

    <table class="table table-bordered table-sm table-striped table-hover ">
        <thead>
            <tr>
                <th>Nombre Proyecto</th>
                <th>Descripcion</th>
                <th>Estado</th>
                <th>Director</th>
                <th>Opciones</th>
            </tr>
        </thead>
        <tbody>
            @if(Auth::user()->tieneSolicitudPendiente())
            <tr>
                <td colspan="5">
                    Ya cuenta con una solicitud para un proyecto de grado. <br>
                    Estado de solicitud: <span class="badge badge-secondary">PENDIENTE</span>
                </td>
            </tr>
            @elseif(Auth::user()->estaInscriptoProyecto())
            <tr>
                <td colspan="5">
                    Usted está inscripto en un proyecto de grado.
                </td>
            </tr>
            @else
            @forelse ($proyectos as $p)
            <tr>
                <td>{{ $p->nombre}}</td>
                <td>{{ $p->descripcion}}</td>
                <td>{{$p->estadoProyecto->nombre}}</td>
                <td>{{$p->docente->nombres.' '.$p->docente->apellidos}}</td>
                <td>
                    <form action="{{route('enviar-solicitud-proyecto-grado',[Auth::user()->estudiante->id,$p->id])}}" method="post" style="display: inline;">
                        @csrf
                        <button type="submit" class="btn btn-sm btn-success" onclick="return confirm('Esta seguro de inscribirse en este proyecto?')">
                            <i class="fa fa-check"></i>
                        </button>
                    </form>
                </td>
            </tr>
            @empty
            <tr>
                <td colspan="5">No hay Proyectos de Grado disponibles</td>
            </tr>
            @endforelse
            @endif
        </tbody>
    </table>
</div>
@endpanel
@endsection
