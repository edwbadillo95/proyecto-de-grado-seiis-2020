<div class="table-responsive">

    <table class="table table-bordered table-sm table-striped table-hover small">
        <thead>
            <tr>
                <th>Nombre</th>
                <th>Apellidos</th>
                <th>Tipo Doc</th>
                <th>Numero Documento</th>
                <th>Correo</th>
                <th>Tipo Carrera</th>
                <th>Opciones</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($estudiantes as $e)
                <tr>
                    <td>{{ $e->nombre1 .' '. $e->nombre2 }}</td>
                    <td>{{ $e->apellido1 .' '. $e->apellido2}}</td>
                    <td>{{$e->tipo_documento}}</td>
                    <td>{{$e->numero_documento}}</td>
                    <td>{{$e->correo}}</td>
                    <td>{{$e->tipo_carrera}}</td>
                    <td>
                        <a href="{{ route('estudiantes.show', ['estudiante' => $e->id]) }}" class="btn btn-sm btn-primary" title="Mostrar detalles">
                            <i class="fas fa-eye"></i>
                        </a>
                        <a href="{{ route('estudiantes.edit', ['estudiante' => $e->id]) }}" class="btn btn-sm btn-info" title="Editar Estudiante">
                            <i class="fas fa-edit"></i>
                        </a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
