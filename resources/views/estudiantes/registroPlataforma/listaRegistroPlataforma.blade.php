@extends('sbadmin.index')

@section('sbadmin-body')
@panel
@slot('icon') user-friends @endslot
@slot('titulo') Lista Solicitudes de ingreso a la Plataforma @endslot

<table class="table table-bordered table-striped">
    <thead>
        <tr style="background-color:#a2c40d;" class="text-white">
            <th>Nombre</th>
            <th>Tipo Documento</th>
            <th>Numero Documento</th>
            <th>Tipo Carrera</th>
            <th>Correo</th>
            <th>Aceptar/Denegar</th>
        </tr>
    </thead>
    <tbody>
        @if($lista->count())
        @foreach($lista as $l)
        <tr>
            <td>{{$l->nombre1.' '.$l->nombre2.' '.$l->apellido1.' '.$l->apellido2}}</td>
            <td>{{$l->tipo_documento}}</td>
            <td>{{$l->numero_documento}}</td>
            <td>{{$l->tipo_carrerar}}</td>
            <td>{{$l->correo}}</td>
            <td class="text-center">
                <form action="{{route('aceptar-estudiante',$l->id)}}" method="post" style="display: inline;">
                    @csrf
                    @method('PUT')
                    <button type="submit" class="btn btn-sm btn-success" onclick="return confirm('Esta seguro de aceptar esta solicitud?')">
                        <i class="fa fa-check"></i>
                    </button>
                </form>
                <form action="{{route('estudiantes.destroy',$l->id)}}" method="post" style="display: inline;">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-sm btn-danger" onclick="return confirm('Esta seguro de denegar esta solicitud?')">
                        <i class="fa fa-times"></i>
                    </button>
                </form>
            </td>
        </tr>
        @endforeach
        @else 
        <tr>
        <td colspan="6">No hay solicitudes <strond>pendientes.</strond></td></tr>
        @endif
    </tbody>
</table>
@endpanel
@endsection