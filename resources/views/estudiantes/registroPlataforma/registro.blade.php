@extends('sbadmin.indexPublic')

@section('sbadmin-body-public')
<div class="container mt-4">
    @include('estudiantes.registroPlataforma.mensaje')
    <div class="card">
        <div class="card-header text-white" style="background-color: #0b4a75">
            <i class="fas fa-user"></i> Registrar Estudiante
        </div>
        <div class="card-body">
            <form action="{{ route('crear-registro-ingreso-plataforma') }}" method="post">
                @csrf
                @include('estudiantes.form')
            </form>
        </div>
    </div>
</div>
@endsection