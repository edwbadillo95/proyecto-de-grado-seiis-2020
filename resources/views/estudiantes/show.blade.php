@extends('sbadmin.index')

@section('sbadmin-body')
@panel
@slot('icon') user-friends @endslot
@slot('titulo') Estudiante @endslot

@include('partials.forms.saved')

<table class="table table-bordered">
    <thead>
        <tr style="background-color:#a2c40d;">
            <th colspan="4" class="text-center text-white">
                <i class="fas fa-info-circle"></i> <i>Información del Estudiante</i>
            </th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <th>Nombre</th>
            <td colspan="3">{{ $estudiante->nombre1 .' '. $estudiante->nombre2 }}</td>
        </tr>
        <tr>
            <th>Apellidos</th>
            <td colspan="3">{{ $estudiante->apellido1 .' '. $estudiante->apellido2 }}</td>
        </tr>
        <tr>
            <th>Tipo de Documento</th>
            <td>{{ $estudiante->tipo_documento}}</td>
            <th>Numero de Documento</th>
            <td>{{ $estudiante->numero_documento}}</td>

        </tr>
        <tr>
            <th>Sexo</th>
            <td>
                @if($estudiante->sexo == 'M')
                MASCULINO
                @else
                FEMENINO
                @endif

            </td>

            <th>Fecha de Nacimiento</th>
            <td>{{ $estudiante->fecha_nacimiento }}</td>
        </tr>
        <tr>
            <th>Tipo Carrera</th>
            <td>{{$estudiante->tipo_carrera}}</td>
            <th>Correo</th>
            <td>{{ $estudiante->correo }}</td>
        </tr>
        <tr>
            <th>Fecha de creacion</th>
            <td>{{ $estudiante->created_at }}</td>
            <th>Fecha de actualizacion</th>
            <td>{{ $estudiante->updated_at }}</td>
        </tr>
    </tbody>
    <tfoot>
        <tr>
            <td colspan="4" class="text-right">
                @if(Auth::user()->esEstudiante() == $estudiante->id)
                <button type="button" class="btn btn-secondary" data-toggle="modal" data-target="#modalCorreo">
                    <i class="fas fa-list"></i> Editar Correo
                </button>                
                @elseif(Auth::user()->tieneRolCoordinador())
                <a href="{{ route('estudiantes.index') }}" class="btn btn-secondary">
                    <i class="fas fa-list"></i> Lista Estudiantes
                </a>
                <a href="{{ route('estudiantes.edit', ['estudiante'=> $estudiante->id]) }}" class="btn btn-info">
                    <i class="fas fa-edit"></i> Editar Datos Estudiante
                </a>
                @endif
            </td>
        </tr>
    </tfoot>
</table>
@include('estudiantes.modal')
@endpanel
@endsection
@push('js')
@parent

@if($errors->has('correo'))
    <script>
    $(function() {
        $('#modalCorreo').modal({
            show: true
        });
    });
    </script>
@endif
@endpush