@extends('sbadmin.index')

@section('sbadmin-body')
@panel
@slot('icon') table @endslot
@slot('titulo') Historial Extendido de Notas @endslot
<table class="table table-bordered">
    <thead>
        <tr style="background-color:#a2c40d;">
            <th colspan="4" class="text-center text-white">
                <i class="fas fa-info-circle"></i> <i>Historia de Extendido de {{$estudiante->nombre1.' '.$estudiante->apellido1}}</i>
            </th>
        </tr>
    </thead>
    <tbody>

        @if($materias->count() && $estudiante->tipo_carrera == "TECNOLOGIA")
        <tr style="background-color:#a2c40d;">
            <th colspan="4" class="text-center text-white">PRIMER SEMESTRE</th>
        </tr>
        <tr style="background-color:#a2c40d;">
            <th colspan="3" class="text-white">Materia</th>
            <th class="text-center text-white">Nota Final</th>
        </tr>
        @foreach($materias as $i => $m)
        @if($i < 7) <tr>
                <td colspan="3">{{$m->materia->nombre}}</td>
             <td class="text-center">{{number_format($m->nota_definitiva,2)}}</td>
            </tr>
        @endif
        @endforeach

            <tr style="background-color:#a2c40d;">
                <th colspan="4" class="text-center text-white">SEGUNDO SEMESTRE</th>
            </tr>
            <tr style="background-color:#a2c40d;">
                <th colspan="3" class="text-white">Materia</th>
                <th class="text-center text-white">Nota Final</th>
            </tr>
        @foreach($materias as $i => $m)
        @if($i > 6 && $i < 14) 
            <tr>
                <td colspan="3">{{$m->materia->nombre}}</td>
                <td class="text-center">{{number_format($m->nota_definitiva,2)}}</td>
            </tr>
        @endif
        @endforeach
            <tr style="background-color:#a2c40d;">
                <th colspan="4" class="text-center text-white">TERCERO SEMESTRE</th>
            </tr>
            <tr style="background-color:#a2c40d;">
                <th colspan="3" class="text-white">Materia</th>
                <th class="text-center text-white">Nota Final</th>
            </tr>
        @foreach($materias as $i => $m)
        @if($i > 13 && $i < 21) 
            <tr>
                <td colspan="3">{{$m->materia->nombre}}</td>
                <td class="text-center">{{number_format($m->nota_definitiva,2)}}</td>
            </tr>
        @endif
        @endforeach
        <tr style="background-color:#a2c40d;">
            <th colspan="4" class="text-center text-white">CUARTO SEMESTRE</th>
        </tr>
        <tr style="background-color:#a2c40d;">
            <th colspan="3" class="text-white">Materia</th>
            <th class="text-center text-white">Nota Final</th>
        </tr>
        @foreach($materias as $i => $m)
        @if($i > 20 && $i < 28) 
            <tr>
                <td colspan="3">{{$m->materia->nombre}}</td>
                <td class="text-center">{{number_format($m->nota_definitiva,2)}}</td>
            </tr>
        @endif
        @endforeach
            <tr style="background-color:#a2c40d;">
                <th colspan="4" class="text-center text-white">QUINTO SEMESTRE</th>
            </tr>
            <tr style="background-color:#a2c40d;">
                <th colspan="3" class="text-white">Materia</th>
                <th class="text-center text-white">Nota Final</th>
            </tr>
        @foreach($materias as $i => $m)
        @if($i > 27 && $i < 35) <tr>
                <td colspan="3">{{$m->materia->nombre}}</td>
                <td class="text-center">{{number_format($m->nota_definitiva,2)}}</td>
                </tr>
        @endif
        @endforeach

        @elseif($materias->count() && $estudiante->tipo_carrera == "PROFESIONAL")
        
        <tr style="background-color:#a2c40d;">
            <th colspan="4" class="text-center text-white">SEPTIMO SEMESTRE</th>
        </tr>
        <tr style="background-color:#a2c40d;">
            <th colspan="3" class="text-white">Materia</th>
            <th class="text-center text-white">Nota Final</th>
        </tr>
        @foreach($materias as $i => $m)
        @if($i < 6) <tr>
                <td colspan="3">{{$m->materia->nombre}}</td>
             <td class="text-center">{{number_format($m->nota_definitiva,2)}}</td>
            </tr>
        @endif
        @endforeach

            <tr style="background-color:#a2c40d;">
                <th colspan="4" class="text-center text-white">OCTAVO SEMESTRE</th>
            </tr>
            <tr style="background-color:#a2c40d;">
                <th colspan="3" class="text-white">Materia</th>
                <th class="text-center text-white">Nota Final</th>
            </tr>
        @foreach($materias as $i => $m)
        @if($i >5 && $i < 12) 
            <tr>
                <td colspan="3">{{$m->materia->nombre}}</td>
                <td class="text-center">{{number_format($m->nota_definitiva,2)}}</td>
            </tr>
        @endif
        @endforeach
            <tr style="background-color:#a2c40d;">
                <th colspan="4" class="text-center text-white">NOVENO SEMESTRE</th>
            </tr>
            <tr style="background-color:#a2c40d;">
                <th colspan="3" class="text-white">Materia</th>
                <th class="text-center text-white">Nota Final</th>
            </tr>
        @foreach($materias as $i => $m)
        @if($i > 11 && $i < 19) 
            <tr>
                <td colspan="3">{{$m->materia->nombre}}</td>
                <td class="text-center">{{number_format($m->nota_definitiva,2)}}</td>
            </tr>
        @endif
        @endforeach
        @else
                <tr>
                    <td colspan="4">Estudiante No cuenta con materias <strong>Aprobadas</strong></td>
                </tr>
        @endif

    </tbody>
</table>
@endpanel
@endsection