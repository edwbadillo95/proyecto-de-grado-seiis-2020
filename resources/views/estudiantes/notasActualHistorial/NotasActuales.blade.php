@extends('sbadmin.index')

@section('sbadmin-body')
@panel
@slot('icon') table @endslot
@slot('titulo') Notas Actuales @endslot

<div class="container">
    <table class="table table-bordered">
        <thead>
            <tr style="background-color:#a2c40d;">
                <th class="text-white text-center" colspan="6">Calificaciones de las Materias Actuales</th>
            </tr>
        </thead>
        <tbody>
            <tr style="background-color:#a2c40d;">
                <th class="text-white text-center">Materia</th>
                <th class="text-white">Primer corte</th>
                <th class="text-white">Segundo corte</th>
                <th class="text-white">Tercer corte</th>
                <th class="text-white">Habilitacion</th>
                <th class="text-white">Nota Final</th>
            </tr>
            @if($materias->count())
            @foreach($materias as $m)
            <tr class="text-center">
                <th >{{$m->materia->nombre}}</th>
                <td>{{number_format($m->nota1,2)}}</td>
                <td>{{number_format($m->nota2,2)}}</td>
                <td>{{number_format($m->nota3,2)}}</td>
                <td>{{number_format($m->habilitacion,2)}}</td>
                <td>{{number_format(($m->nota1*0.33)+($m->nota2*0.33)+($m->nota3*0.34),2)}}</td>
            </tr>
            @endforeach
            @else 
            <tr>
            <td colspan="6">
            No hay materias <strong>Matriculadas</strong>
            </td></tr>
            @endif
        </tbody>
    </table>
</div>
@endpanel
@endsection