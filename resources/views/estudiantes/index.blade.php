@extends('sbadmin.index')

@section('sbadmin-body')
@panel
    @slot('icon') user-friends @endslot
    @slot('titulo') Estudiantes @endslot

    <p>Se lista a continuación la lista de Estudiantes.</p>

    @if (count($estudiantes))
        @include('estudiantes.table')
    @else
        <div class="alert alert-info">
            No hay estudiantes registrados. Haz clic <a href="{{ route('estudiantes.create') }}" class="text-primary">aquí</a> para registrar uno nuevo.
        </div>
    @endif

@endpanel
@endsection
