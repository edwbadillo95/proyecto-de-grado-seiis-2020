@extends('sbadmin.index')

@section('sbadmin-body')
@panel
    @slot('icon') user-friends @endslot
    @slot('titulo') Editar Estudiante @endslot

    <form action="{{ route('estudiantes.update', ['estudiante' => $estudiante->id]) }}" method="post">
        @csrf
        @method('PUT')

        @include('estudiantes.form')
    </form>
@endpanel

@endsection
