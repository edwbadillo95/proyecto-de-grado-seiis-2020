<table class="table table-bordered table-sm table-striped table-hover">
    <thead>
        <tr align="center">
            <th>T. documento</th>
            <th>Documento</th>
            <th>Nombres</th>
            <th>Apellidos</th>
            <th>Correo</th>
            <th>Genero</th>
            <th>Fecha de nacimiento</th>
            <th>Clasificacion</th>
            <th>Opciones</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($docentes as $d)
            <tr align="center">
                <td>{{ $d->tipoDocumento->nombre }}</td>
                <td>{{ $d->documento }}</td>
                <td>{{ $d->nombres }}</td>
                <td>{{ $d->apellidos }}</td>
                <td>{{ $d->correo }}</td>
                <td>{{ $d->genero }}</td>
                <td>{{ $d->fecha_nacimiento }}</td>
                <td>{{ $d->clasificacion->nombre }}</td>
                <td>
                    <a href="{{ route('docente.show', ['docente' => $d->id]) }}" class="btn btn-sm btn-primary" title="Mostrar detalles">
                        <i class="fas fa-eye"></i>
                    </a>
                    <a href="{{ route('docente.edit', ['docente' => $d->id]) }}" class="btn btn-sm btn-info" title="Editar registro">
                        <i class="fas fa-edit"></i>
                    </a>
                    <a href="{{ route('docente.horarios.index') }}" class="btn btn-sm btn-warning"  title="Ver horario">
                        <i class="fas fa-calendar-alt"></i>
                    </a>
                    @if ($d->archivo || $d->horario)
                    <span class="dropdown" title="Archivos">
                        <a class="btn btn-secondary btn-sm dropdown-toggle" href="#" role="button" id="{{ 'dropdownMenuLink' . $d->id }}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-file-alt"></i>
                        </a>

                        <div class="dropdown-menu" aria-labelledby="{{ 'dropdownMenuLink' . $d->id }}">
                            @if ($d->archivo)
                            <a class="dropdown-item" href="{{ \Storage::url("docente/$d->documento/$d->archivo") }}" target="_blank">
                                <i class="fas fa-book mr-2"></i> Ver hoja de vida
                            </a>
                            @endif
                            @if ($d->horario)
                            <a class="dropdown-item" href="{{ \Storage::url("docente/$d->documento/$d->horario") }}" target="_blank">
                                <i class="fas fa-calendar-alt mr-2"></i> Ver horario subido
                            </a>
                            @endif
                        </div>
                    </span>
                    @endif
                </td>
            </tr>
        @endforeach
    </tbody>
</table>
