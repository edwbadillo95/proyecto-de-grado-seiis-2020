@extends('sbadmin.index')

@section('sbadmin-body')
@panel
    @slot('icon') user-tie @endslot
    @slot('titulo') Editar docente @endslot

    <form action="{{ route('docente.update', ['docente' => $docente->id]) }}" method="post" enctype="multipart/form-data">
        @csrf
        @method('PUT')

        @include('docente.form', ['isUpdate' => true])
    </form>
@endpanel
@endsection
