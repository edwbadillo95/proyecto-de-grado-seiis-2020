<p>Digite el siguiente formulario, los campos con * son obligatorios.</p>

@include('partials.forms.error')

<div class="row">
    <div class="col-sm-12 col-md-4">
        <div class="form-group">
            <label for="dia">Dia *</label>
            <select value="{{ old('dia', $horario->dia) }}" name="dia" class="custom-select @error('dia') is-invalid @enderror" id="dia">
                <option value="">-- Seleccionar --</option>
                <option
                @if (old('dia', $horario->dia) == 'Lunes')
                    selected="selected"
                @endif
                value="Lunes">Lunes</option>
                <option
                @if (old('dia', $horario->dia) == 'Martes')
                    selected="selected"
                @endif
                value="Martes">Martes</option>
                <option
                @if (old('dia', $horario->dia) == 'Miercoles')
                    selected="selected"
                @endif
                value="Miercoles">Miercoles</option>
                <option
                @if (old('dia', $horario->dia) == 'Jueves')
                    selected="selected"
                @endif
                value="Jueves">Jueves</option>
                <option
                @if (old('dia', $horario->dia) == 'Viernes')
                    selected="selected"
                @endif
                value="Viernes">Viernes</option>
                <option
                @if (old('dia', $horario->dia) == 'Sabado')
                    selected="selected"
                @endif
                value="Sabado">Sabado</option>
            </select>

            @error('dia')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>
    </div>
    <div class="col-sm-12 col-md-4">
        <div class="form-group">
            <label for="hora_inicio">Hora de inicio *</label><br>
            <input id="hora_inicio" name="hora_inicio" value="{{ old('hora_inicio', $horario->hora_inicio) }}" type="time" class="form-control @error('hora_inicio') is-invalid @enderror">

            @error('hora_inicio')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
            @enderror
        </div>
    </div>
    <div class="col-sm-12 col-md-4">
        <div class="form-group">
            <label for="hora_fin">Hora de fin *</label><br>
            <input id="hora_fin" name="hora_fin" value="{{ old('hora_fin', $horario->hora_fin) }}" type="time" class="form-control @error('hora_fin') is-invalid @enderror">

            @error('hora_fin')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
            @enderror
        </div>
    </div>
    <div class="col-sm-12 col-md-6">
        <div class="form-group">
            <label for="salon">Salon *</label>
            <select value="{{ old('salon', $horario->salon_id) }}" name="salon" class="custom-select @error('salon') is-invalid @enderror" id="salon">
                <option value="">-- Seleccionar --</option>
                @foreach ($salones as $salon)
                    <option
                    @if (old('salon', $horario->salon_id) == $salon->id) selected="selected" @endif
                    value="{{ $salon->id }}">{{ $salon->nombre }}</option>
                @endforeach
            </select>
            @error('salon')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>
    </div>
    <div class="col-sm-12 col-md-6">
        <div class="form-group">
            <label for="grupo">Grupo *</label>
            <select value="{{ old('grupo', $horario->grupo_id) }}" name="grupo" class="custom-select @error('grupo') is-invalid @enderror" id="grupo">
                <option value="">-- Seleccionar --</option>
                @foreach ($grupos as $grupo)
                    <option
                    @if (old('grupo', $horario->grupo_id) == $grupo->id) selected="selected" @endif
                    value="{{ $grupo->id }}">{{ $grupo->nombre }} - {{ $grupo->materia->nombre }}</option>
                @endforeach
            </select>
            @error('grupo')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>
    </div>
</div>

<hr>

@if ($horario->id)
<div class="float-left">
    @btndelete @endbtndelete
</div>
@endif

<div class="float-right">
    <a href="{{ route('docente.horarios.index') }}" class="btn btn-secondary">
        <i class="fas fa-times"></i> Cancelar
    </a>
    @btnreset @endbtnreset
    @btnsubmit @endbtnsubmit
</div>
