@extends('sbadmin.index')

@section('sbadmin-body')
@panel
    @slot('icon') calendar-alt @endslot
    @slot('titulo') Horarios
        <div class="float-right">
            <a href="{{ route('docente.index') }}" class="btn btn-sm btn-secondary">
                <i class="fas fa-user-tie"></i> Ver docente
            </a>
        </div>
    @endslot
    @include('partials.forms.saved')

    <p>Se lista a continuación el horario del docente.</p>

    @include('partials.forms.deleted')

    @if (count($horarios))
        @include('docente.horarios.table')
    @else
        <div class="alert alert-info">
            No hay horarios registrados. Haz clic <a href="{{ route('docente.horarios.create') }}" class="text-primary">aquí</a> para registrar uno nuevo.
        </div>
    @endif

@endpanel
@endsection
