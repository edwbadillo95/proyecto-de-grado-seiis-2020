@extends('sbadmin.index')

@section('sbadmin-body')
@panel
    @slot('icon') calendar-alt @endslot
    @slot('titulo') Registrar horario @endslot

    <form action="{{ route('docente.horarios.store') }}" method="post">
        @csrf

        @include('docente.horarios.form')
    </form>
@endpanel
@endsection
