@extends('sbadmin.index')

@section('sbadmin-body')
@panel
    @slot('icon') calendar-alt @endslot
    @slot('titulo') Horario @endslot

    @include('partials.forms.saved')

    <table class="table table-bordered">
        <thead>
            <tr>
                <th colspan="4" class="text-center">
                    <i class="fas fa-info-circle"></i> <i>Información del horario</i>
                </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th>Dia</th>
                <td colspan="3">{{ $horario->dia }}</td>
            </tr>
            <tr>
                <th>Hora de inicio</th>
                <td colspan="3">{{ $horario->hora_inicio }}</td>
            </tr>
            <tr>
                <th>Hora de fin</th>
                <td colspan="3">{{ $horario->hora_fin }}</td>
            </tr>
            <tr>
                <th>Salon</th>
                <td colspan="3">{{ $horario->salon->nombre }}</td>
            </tr>
            <tr>
                <th>Grupo</th>
                <td>{{ $horario->grupo->nombre }}</td>
            </tr>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="4">
                    <a href="{{ route('docente.horarios.index') }}" class="btn btn-secondary">
                        <i class="fas fa-list"></i> Ver horarios
                    </a>
                    <a href="{{ route('docente.horarios.edit', ['horario'=> $horario->id]) }}" class="btn btn-info">
                        <i class="fas fa-edit"></i> Editar
                    </a>
                </td>
            </tr>
        </tfoot>
    </table>
@endpanel
@endsection
