@extends('sbadmin.index')

@section('sbadmin-body')
@panel
    @slot('icon') calendar-alt @endslot
    @slot('titulo') Editar horario @endslot

    <form action="{{ route('docente.horarios.update', ['horario' => $horario->id]) }}" method="post">
        @csrf
        @method('PUT')

        @include('docente.horarios.form')
    </form>
@endpanel

{{-- @include('partials.delete', ['route' => route('docente.horarios.destroy', ['horario' => $horario->id])]) --}}
@endsection
