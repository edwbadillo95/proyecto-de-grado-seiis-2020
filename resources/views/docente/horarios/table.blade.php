<table class="table table-bordered table-sm table-striped table-hover">
    <thead>
        <tr align="center">
            <th>Hora</th>
            <th>Lunes</th>
            <th>Martes</th>
            <th>Miercoles</th>
            <th>Jueves</th>
            <th>Viernes</th>
            <th>Sabados</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($horarios as $h)
            <tr>
                <td align="center" style="width:15%"><b>{{ date('H:i A', strtotime($h->hora_inicio)) }} - {{ date('H:i A', strtotime($h->hora_fin)) }}</b></td>
                @if($h->dia == 'Lunes')
                    <td align="center" height="50"><b>Salon:</b> <u><i>{{ $h->salon->nombre }}</i></u> <br> <b>Grupo:</b> <u><i>{{ $h->grupo->nombre }}</i></u> <br> <u><i>{{ $h->grupo->materia->nombre }}</i></u></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                @elseif($h->dia == 'Martes')
                    <td></td>
                    <td align="center" height="50"><b>Salon:</b> <u><i>{{ $h->salon->nombre }}</i></u> <br> <b>Grupo:</b> <u><i>{{ $h->grupo->nombre }}</i></u> <br> <u><i>{{ $h->grupo->materia->nombre }}</i></u></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                @elseif($h->dia == 'Miercoles')
                    <td></td>
                    <td></td>
                    <td align="center" height="50"><b>Salon:</b> <u><i>{{ $h->salon->nombre }}</i></u> <br> <b>Grupo:</b> <u><i>{{ $h->grupo->nombre }}</i></u> <br> <u><i>{{ $h->grupo->materia->nombre }}</i></u></td>
                    <td></td>
                    <td></td>
                    <td></td>
                @elseif($h->dia == 'Jueves')
                    <td></td>
                    <td></td>
                    <td></td>
                    <td align="center" height="50"><b>Salon:</b> <u><i>{{ $h->salon->nombre }}</i></u> <br> <b>Grupo:</b> <u><i>{{ $h->grupo->nombre }}</i></u> <br> <u><i>{{ $h->grupo->materia->nombre }}</i></u></td>
                    <td></td>
                    <td></td>
                @elseif($h->dia == 'Viernes')
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td align="center" height="50"><b>Salon:</b> <u><i>{{ $h->salon->nombre }}</i></u> <br> <b>Grupo:</b> <u><i>{{ $h->grupo->nombre }}</i></u> <br> <u><i>{{ $h->grupo->materia->nombre }}</i></u></td>
                    <td></td>
                @elseif($h->dia == 'Sabado')
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td align="center" height="50"><b>Salon:</b> <u><i>{{ $h->salon->nombre }}</i></u> <br> <b>Grupo:</b> <u><i>{{ $h->grupo->nombre }}</i></u> <br> <u><i>{{ $h->grupo->materia->nombre }}</i></u></td>
                @endif
            </tr>
        @endforeach
    </tbody>
</table>
