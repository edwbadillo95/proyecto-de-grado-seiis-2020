<p>Digite el siguiente formulario, los campos con * son obligatorios.</p>

@include('partials.forms.error')

<div class="row">
    <div class="col-sm-12 col-md-5">
        <div class="form-group">
            <label for="nombre">Nombre del salon *</label>
            <input
                id="nombre"
                name="nombre"
                value="{{ old('nombre', $salon->nombre) }}"
                type="text"
                class="form-control @error('nombre') is-invalid @enderror">

            @error('nombre')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>
    </div>
    <div class="col-sm-12 col-md-5">
        <div class="form-group">
            <label for="capacidad">Capacidad *</label>
            <input
                id="capacidad"
                name="capacidad"
                value="{{ old('capacidad', $salon->capacidad) }}"
                type="text"
                class="form-control @error('capacidad') is-invalid @enderror">

            @error('capacidad')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>
    </div>
    <div class="col-sm-12 col-md-2">
        <div class="form-group">
            <label for="edificio">Edificio *</label>
            <select value="{{ old('edificio', $salon->edificio_id) }}" name="edificio" class="custom-select @error('edificio') is-invalid @enderror" id="edificio">
                <option value="">-- Seleccionar --</option>
                @foreach ($edificios as $edificio)
                    <option
                    @if (old('edificio', $salon->edificio_id) == $edificio->id) selected="selected" @endif
                    value="{{ $edificio->id }}">{{ $edificio->nombre }}</option>
                @endforeach
            </select>
            @error('edificio')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>
    </div>
</div>

<hr>

@if ($salon->id)
<div class="float-left">
    @btndelete @endbtndelete
</div>
@endif

<div class="float-right">
    <a href="{{ route('docente.salones.index') }}" class="btn btn-secondary">
        <i class="fas fa-times"></i> Cancelar
    </a>
    @btnreset @endbtnreset
    @btnsubmit @endbtnsubmit
</div>
