@extends('sbadmin.index')

@section('sbadmin-body')
@panel
    @slot('icon') user-friends @endslot
    @slot('titulo') Salon @endslot

    @include('partials.forms.saved')

    <table class="table table-bordered">
        <thead>
            <tr>
                <th colspan="4" class="text-center">
                    <i class="fas fa-info-circle"></i> <i>Información del salon</i>
                </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th>Nombre</th>
                <td colspan="3">{{ $salon->nombre }}</td>
            </tr>
            <tr>
                <th>Capacidad</th>
                <td colspan="3">{{ $salon->capacidad }}</td>
            </tr>
            <tr>
                <th>Edificio</th>
                <td>{{ $salon->edificio->nombre }}</td>
            </tr>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="4">
                    <a href="{{ route('docente.salones.index') }}" class="btn btn-secondary">
                        <i class="fas fa-list"></i> Ver salones
                    </a>
                    <a href="{{ route('docente.salones.edit', ['salon'=> $salon->id]) }}" class="btn btn-info">
                        <i class="fas fa-edit"></i> Editar
                    </a>
                </td>
            </tr>
        </tfoot>
    </table>
@endpanel
@endsection
