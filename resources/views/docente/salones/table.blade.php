<table class="table table-bordered table-sm table-striped table-hover">
    <thead>
        <tr>
            <th>Nombre</th>
            <th>Capacidad</th>
            <th>Edificio</th>
            <th>Opciones</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($salones as $salon)
            <tr>
                <td>{{ $salon->nombre }}</td>
                <td>{{ $salon->capacidad }}</td>
                <td>{{ $salon->edificio->nombre }}</td>
                <td>
                    <a href="{{ route('docente.salones.show', ['salon' => $salon->id]) }}" class="btn btn-sm btn-primary" title="Mostrar detalles">
                        <i class="fas fa-eye"></i>
                    </a>
                    <a href="{{ route('docente.salones.edit', ['salon' => $salon->id]) }}" class="btn btn-sm btn-info" title="Editar registro">
                        <i class="fas fa-edit"></i>
                    </a>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>
