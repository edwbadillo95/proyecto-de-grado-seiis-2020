@extends('sbadmin.index')

@section('sbadmin-body')
@panel
    @slot('icon') user-friends @endslot
    @slot('titulo') Registrar salon @endslot

    <form action="{{ route('docente.salones.store') }}" method="post">
        @csrf

        @include('docente.salones.form')
    </form>
@endpanel
@endsection
