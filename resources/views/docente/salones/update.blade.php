@extends('sbadmin.index')

@section('sbadmin-body')
@panel
    @slot('icon') user-friends @endslot
    @slot('titulo') Editar salon @endslot

    <form action="{{ route('docente.salones.update', ['salon' => $salon->id]) }}" method="post">
        @csrf
        @method('PUT')

        @include('docente.salones.form')
    </form>
@endpanel

@include('partials.delete', ['route' => route('docente.salones.destroy', ['salon' => $salon->id])])
@endsection
