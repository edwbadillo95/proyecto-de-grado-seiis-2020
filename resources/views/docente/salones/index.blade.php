@extends('sbadmin.index')

@section('sbadmin-body')
@panel
    @slot('icon') user-friends @endslot
    @slot('titulo') Salones @endslot

    <p>Se lista a continuación los salones registrados.</p>

    @include('partials.forms.deleted')

    @if (count($salones))
        @include('docente.salones.table')
    @else
        <div class="alert alert-info">
            No hay salones registrados. Haz clic <a href="{{ route('docente.salones.create') }}" class="text-primary">aquí</a> para registrar uno nuevo.
        </div>
    @endif

@endpanel
@endsection
