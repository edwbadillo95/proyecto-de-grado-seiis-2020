@push('css')
    <link rel="stylesheet" href="{{ asset('vendor/bootstrap-fileinput-be14321/css/fileinput.min.css') }}">
@endpush

<p>Digite el siguiente formulario, los campos con * son obligatorios.</p>

@include('partials.forms.error')

<div class="row">
    <div class="col-sm-12 col-md-2">
        <div class="form-group">
            <label for="tipo_documento">Tipo de documento *</label>
            <select value="{{ old('tipo_documento', $docente->tipo_documento_id) }}" name="tipo_documento" class="custom-select @error('tipo_documento') is-invalid @enderror" id="tipo_documento" disabled>
                <option value="">-- Seleccionar --</option>
                @foreach ($tiposDocumento as $tipodocumento)
                    <option
                    @if (old('tipo_documento', $docente->tipo_documento_id) == $tipodocumento->id) selected="selected" @endif
                    value="{{ $tipodocumento->id }}">{{ $tipodocumento->nombre }}</option>
                @endforeach
            </select>
            @error('tipo_documento')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>
    </div>
    <div class="col-sm-12 col-md-2">
        <div class="form-group">
            <label for="documento">Documento *</label>
            <input
                id="documento"
                name="documento"
                value="{{ old('documento', $docente->documento) }}"
                type="text"
                class="form-control @error('documento') is-invalid @enderror">

            @error('documento')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>
    </div>
    <div class="col-sm-12 col-md-2">
        <div class="form-group">
            <label for="fecha_nacimiento">Fecha de Nacimiento *</label><br>
            <input id="fecha_nacimiento" name="fecha_nacimiento" value="{{ old('fecha_nacimiento', $docente->fecha_nacimiento) }}" type="date" class="form-control @error('fecha_nacimiento') is-invalid @enderror">

            @error('fecha_nacimiento')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
            @enderror
        </div>
    </div>
    <div class="col-sm-12 col-md-2">
        <div class="form-group">
            <label for="genero">Genero *</label><br>
            <select value="{{ old('genero', $docente->genero) }}" name="genero" class="custom-select @error('genero') is-invalid @enderror" id="genero">
                <option value="">-- Seleccionar --</option>
                <option
                @if (old('genero', $docente->genero) == 'H')
                    selected="selected"
                @endif
                value="H">MASCULINO</option>

                <option
                @if (old('genero', $docente->genero) == 'M')
                    selected="selected"
                @endif
                value="M">FEMENINO</option>
            </select>
            @error('genero')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>
    </div>
    <div class="col-sm-12 col-md-4">
        <div class="form-group">
            <label for="correo">Correo *</label><br>
            <input id="correo" name="correo" value="{{ old('correo', $docente->correo) }}" type="email" class="form-control @error('correo') is-invalid @enderror">

            @error('correo')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
            @enderror
        </div>
    </div>
    <div class="col-sm-12 col-md-5">
        <div class="form-group">
            <label for="nombres">Nombre del docente *</label>
            <input
                id="nombres"
                name="nombres"
                value="{{ old('nombres', $docente->nombres) }}"
                type="text"
                class="form-control @error('nombres') is-invalid @enderror">

            @error('nombres')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>
    </div>
    <div class="col-sm-12 col-md-5">
        <div class="form-group">
            <label for="apellidos">Apellidos del docente *</label>
            <input
                id="apellidos"
                name="apellidos"
                value="{{ old('apellidos', $docente->apellidos) }}"
                type="text"
                class="form-control @error('apellidos') is-invalid @enderror">

            @error('apellidos')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>
    </div>
    <div class="col-sm-12 col-md-2">
        <div class="form-group">
            <label for="clasificacion">Clasificacion *</label>
            <select value="{{ old('clasificacion', $docente->clasificacion_id) }}" name="clasificacion" class="custom-select @error('clasificacion') is-invalid @enderror" id="clasificacion" disabled>
                <option value="">-- Seleccionar --</option>
                @foreach ($clasificaciones as $clasificacion)
                    <option
                    @if (old('clasificacion', $docente->clasificacion_id) == $clasificacion->id) selected="selected" @endif
                    value="{{ $clasificacion->id }}">{{ $clasificacion->nombre }}</option>
                @endforeach
            </select>
            @error('clasificacion')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>
    </div>
    <div class="col-sm-12 col-md-6">
        <div class="form-group">
            <label for="archivo">Cargar Hoja de vida</label>
            <input
                id="archivo"
                name="archivo"
                value="{{ old('archivo', $docente->archivo) }}"
                type="file"
                data-initial-preview="{{ isset($docente->archivo) ? "http://placehold.it/200x150/9bbb10/FFFFFF&text=HOJA+DE+VIDA" : "http://placehold.it/200x150/EFEFEF/AAAAAA&text=SUBIR+HOJA+DE+VIDA" }}"
                accept="application/pdf,image/*,.doc,.docx"
                lass="form-control @error('archivo') is-invalid @enderror">

            @error('archivo')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>
    </div>
    <div class="col-sm-12 col-md-6">
        <div class="form-group">
            <label for="horario">Cargar Horario</label>
            <input
                id="horario"
                name="horario"
                value="{{ old('horario', $docente->horario) }}"
                type="file"
                data-initial-preview="{{ isset($docente->horario) ? "http://placehold.it/200x150/9bbb10/FFFFFF&text=HORARIO" : "http://placehold.it/200x150/EFEFEF/AAAAAA&text=SUBIR+HORARIO" }}"
                accept="application/pdf,image/*,.doc,.docx"
                lass="form-control @error('horario') is-invalid @enderror">

            @error('horario')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>
    </div>
</div>

<hr>

<div class="float-right">
    <a href="{{ route('docente.index') }}" class="btn btn-secondary">
        <i class="fas fa-times"></i> Cancelar
    </a>
    @btnsubmit @endbtnsubmit
</div>

@push('js')
    <script src="{{ asset('vendor/bootstrap-fileinput-be14321/js/fileinput.min.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap-fileinput-be14321/js/locales/es.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap-fileinput-be14321/themes/fas/theme.min.js') }}"></script>
    <script src="{{ asset('vendor/docente/js/archivo.js') }}"></script>
@endpush
