@extends('sbadmin.index')

@section('sbadmin-body')
@panel
    @slot('icon') user-tie @endslot
    @slot('titulo') Registrar docente @endslot

    <form action="{{ route('docente.store') }}" method="post" enctype="multipart/form-data">
        @csrf

        @include('docente.form', ['isUpdate' => false])
    </form>
@endpanel
@endsection
