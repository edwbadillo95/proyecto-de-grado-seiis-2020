@extends('sbadmin.index')

@section('sbadmin-body')
@panel
    @slot('icon') chalkboard-teacher @endslot
    @slot('titulo') Registrar asistencia @endslot

    @include('docente.asistencia.form', ['route' => route('docente.grupos.asistencia.store', ['grupo'=>$grupo->id]), 'isUpdate' => false])
@endpanel
@endsection
