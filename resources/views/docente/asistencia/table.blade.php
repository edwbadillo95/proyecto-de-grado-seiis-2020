<table class="table table-bordered table-sm table-striped table-hover">
    <thead>
        <tr align="center">
            <th style="width: 10%;">Documento</th>
            <th>Estudiante</th>
            <th style="width: 20%;">Correo</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($grupo->estudiantes as $e)
            <tr align="center">
                <td>{{ $e->estudiante->numero_documento }}</td>
                <td>{{ $e->estudiante->nombre1 }} {{ $e->estudiante->apellido1 }} </td>
                <td>{{ $e->estudiante->correo }} </td>
            </tr>
        @endforeach
    </tbody>
</table>
