@if (request()->documento && !$estudiante && !isset($estudianteDuplicado))
    <div class="alert alert-danger">
        No se encontró un estudiante con el documento <b>{{ request()->documento }}</b> en el sistema.
    </div>
@elseif(isset($estudianteDuplicado))
    <div class="alert alert-danger">
        El estudiante con el documento <b>{{ request()->documento }}</b> ya se encuentra asignado en el grupo <b>{{ $grupo->nombre }}</b>.
    </div>
@endif

@if ($estudiante)
<form action="{{ $route }}" method="post">
    @csrf

    @if ($isUpdate)
        @method('PUT')
    @endif
    <input type="hidden" name="estudiante" value="{{ $estudiante->id }}">


    <div class="card border-secondary">
        <div class="card-header">
            <i class="fas fa-user-tie"></i> Estudiante a registrar
        </div>

        @if ($errors->has('estudiante'))
            <div class="card-body">
                <div class="alert alert-danger mb-0">
                    El estudiante especificado parecer ser incorrecto. Vuelva a especificar un número de documento de un estudiante en la parte superior de este formulario.
                </div>
            </div>
        @else
            <div class="card-body">
                <table class="table table-bordered table-sm">
                    <tbody>
                        <tr>
                            <th style="width: 10%;">Documento</th>
                            <td>{{ $estudiante->numero_documento }}</td>
                            <th style="width: 10%;">Tipo documento</th>
                            <td>{{ $estudiante->tipo_documento }}</td>
                        </tr>
                        <tr>
                            <th>Estudiante</th>
                            <td colspan="3">{{ $estudiante->nombre1 }} {{ $estudiante->apellido1 }}</td>
                        </tr>
                        <tr>
                            <th>Materia</th>
                            <td>{{ $grupo->materia->nombre }}</td>
                            <th>Grupo</th>
                            <td>{{ $grupo->nombre }}</td>
                        </tr>
                        <tr>
                            <th>Docente</th>
                            <td colspan="3">{{ $grupo->docente->nombres }} {{ $grupo->docente->apellidos }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="card-footer text-center">
                <button class="btn btn-uts" type="submit">
                    <i class="fas fa-user-plus"></i> Registrar
                </button>
                <a class="btn btn-secondary" href="{{ route('docente.grupos.asistencia.create', ['grupo'=>$grupo->id]) }}">
                    <i class="fas fa-times"></i> Cancelar
                </a>
            </div>
        @endif
    </div>
</form>
@endif
