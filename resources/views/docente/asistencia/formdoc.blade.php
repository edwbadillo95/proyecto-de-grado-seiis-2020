<form class="mb-3" action="{{ route('docente.grupos.asistencia.create', ['grupo'=>$grupo->id]) }}" method="get">
    <div class="card border-primary">
        <div class="card-header text-center">
            <b>Ingrese el documento del estudiante que desea agregar.</b>
        </div>
        <div class="row p-3">
            <div class="col-xs-12 col-sm-6 col-md-5 offset-sm-3 offset-md-4">
                <div class="input-group">
                    <input
                        value="{{ request()->documento }}"
                        name="documento"
                        type="text"
                        class="form-control"
                        placeholder="Documento del estudiante"
                        aria-label="Documento del estudiante"
                        aria-describedby="btn-doc">

                    <div class="input-group-append">
                        <button class="btn btn-primary" type="submit" id="btn-doc">
                           <i class="fas fa-search"></i> Buscar
                        </button>
                        <a class="btn btn-secondary" href="{{ route('docente.grupos.index') }}">
                            <i class="fas fa-times"></i> Cancelar
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
