@extends('sbadmin.index')

@section('sbadmin-body')
@panel
    @slot('icon') chalkboard-teacher @endslot
    @slot('titulo') Estudiantes del grupo 
        <div class="float-right">
            <a href="{{ route('docente.grupos.index') }}" class="btn btn-sm btn-secondary">
                <i class="fas fa-chalkboard-teacher"></i> Ver grupos
            </a>
            <a href="{{ route('docente.grupos.asistencia.create', ['grupo' => $grupo->id]) }}" class="btn btn-sm btn-primary">
                <i class="fas fa-user-plus"></i> Agregar estudiante
            </a>
            <button class="btn btn-sm btn-danger" type="button" data-toggle="modal" data-target="#modal-delete">
                <i class="fas fa-trash"> Eliminar asistencia</i>
            </button>
        </div>
    @endslot

    <p>Se lista a continuación los estudiantes del grupo <b>{{ $grupo->nombre }}</b> de <b>{{ $grupo->materia->nombre }}</b>.</p>

    @include('partials.forms.deleted')

    @if ($grupo->estudiantes()->count())
        @include('docente.asistencia.table')
    @else
    <div class="alert alert-info">
        No hay estudiantes registrados en este grupo.
        Haz clic <a href="{{ route('docente.grupos.asistencia.create', ['grupo' => $grupo->id]) }}" class="text-primary">aquí</a> para registrar uno nuevo.
    </div>
    @endif

    @include('partials.delete', ['route' => route('docente.grupos.asistencia.destroy', ['grupo'=> $grupo->id, 'asistencia'=> $grupo->id])])
@endpanel
@endsection
