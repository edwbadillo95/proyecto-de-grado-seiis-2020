<p>
    Digite el siguiente formulario para el registro de un estudiante al grupo <b>{{ $grupo->nombre }}</b> de <b>{{ $grupo->materia->nombre }}</b>.
</p>

@if (!$asistencia->id)
@include('docente.asistencia.formdoc')
@endif

@include('docente.asistencia.formasistencia', ['route' => $route, 'isUpdate' => $isUpdate])
