@extends('sbadmin.index')

@section('sbadmin-body')
@panel
    @slot('icon') user-tie @endslot
    @slot('titulo') Docente @endslot

    @include('partials.forms.saved')

    <table class="table table-bordered">
        <thead>
            <tr>
                <th colspan="4" class="text-center">
                    <i class="fas fa-info-circle"></i> <i>Información del docente</i>
                </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th>Tipo de documento</th>
                <td colspan="3">{{ $docente->tipoDocumento->nombre }}</td>
            </tr>
            <tr>
                <th>Documento</th>
                <td colspan="3">{{ $docente->documento }}</td>
            </tr>
            <tr>
                <th>Nombres</th>
                <td colspan="3">{{ $docente->nombres }}</td>
            </tr>
            <tr>
                <th>Apellidos</th>
                <td colspan="3">{{ $docente->apellidos }}</td>
            </tr>
            <tr>
                <th>Correo</th>
                <td colspan="3">{{ $docente->correo }}</td>
            </tr>
            <tr>
                <th>Genero</th>
                <td colspan="3">
                    @if($docente->genero == 'M')
                        MASCULINO
                    @else
                        FEMENINO
                    @endif
                </td>
            </tr>
            <tr>
                <th>Fecha de nacimiento</th>
                <td colspan="3">{{ $docente->fecha_nacimiento }}</td>
            </tr>
            <tr>
                <th>Clasificacion</th>
                <td colspan="3">{{ $docente->clasificacion->nombre }}</td>
            </tr>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="4">
                    <a href="{{ route('docente.index') }}" class="btn btn-secondary">
                        <i class="fas fa-user-tie"></i> Ver docente
                    </a>
                    <a href="{{ route('docente.edit', ['docente'=> $docente->id]) }}" class="btn btn-info">
                        <i class="fas fa-edit"></i> Editar
                    </a>
                </td>
            </tr>
        </tfoot>
    </table>
@endpanel
@endsection
