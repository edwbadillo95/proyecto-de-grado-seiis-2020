@extends('sbadmin.index')

@section('sbadmin-body')
@panel
    @slot('icon') chalkboard-teacher @endslot
    @slot('titulo') Editar grupo @endslot

    <form action="{{ route('docente.grupos.update', ['grupo' => $grupo->id]) }}" method="post">
        @csrf
        @method('PUT')

        @include('docente.grupos.form')
    </form>
@endpanel

@include('partials.delete', ['route' => route('docente.grupos.destroy', ['grupo' => $grupo->id])])
@endsection
