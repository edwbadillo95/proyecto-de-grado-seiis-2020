@extends('sbadmin.index')

@section('sbadmin-body')
@panel
    @slot('icon') chalkboard-teacher @endslot
    @slot('titulo') Grupo @endslot

    @include('partials.forms.saved')

    <table class="table table-bordered">
        <thead>
            <tr>
                <th colspan="4" class="text-center">
                    <i class="fas fa-info-circle"></i> <i>Información del grupo</i>
                </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th>Nombre</th>
                <td colspan="3">{{ $grupo->nombre }}</td>
            </tr>
            <tr>
                <th>Materia</th>
                <td colspan="3">{{ $grupo->materia->nombre }}</td>
            </tr>
            <tr>
                <th>Docente</th>
                <td>{{ $grupo->docente->nombres }} {{ $grupo->docente->apellidos}}</td>
            </tr>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="4">
                    <a href="{{ route('docente.grupos.index') }}" class="btn btn-secondary">
                        <i class="fas fa-chalkboard-teacher"></i> Ver grupos
                    </a>
                    <a href="{{ route('docente.grupos.edit', ['grupo'=> $grupo->id]) }}" class="btn btn-info">
                        <i class="fas fa-edit"></i> Editar
                    </a>
                </td>
            </tr>
        </tfoot>
    </table>
@endpanel
@endsection
