<p>Digite el siguiente formulario, los campos con * son obligatorios.</p>

@include('partials.forms.error')

<div class="row">
    <div class="col-sm-12 col-md-6">
        <div class="form-group">
            <label for="nombre">Nombre del grupo *</label>
            <input
                id="nombre"
                name="nombre"
                value="{{ old('nombre', $grupo->nombre) }}"
                type="text"
                class="form-control @error('nombre') is-invalid @enderror">

            @error('nombre')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>
    </div>
    <div class="col-sm-12 col-md-6">
        <div class="form-group">
            <label for="materia">Materia *</label>
            <select value="{{ old('materia', $grupo->materia_id) }}" name="materia" class="custom-select @error('materia') is-invalid @enderror" id="materia">
                <option value="">-- Seleccionar --</option>
                @foreach ($materias as $materia)
                    <option
                    @if (old('materia', $grupo->materia_id) == $materia->id) selected="selected" @endif
                    value="{{ $materia->id }}">{{ $materia->nombre }}</option>
                @endforeach
            </select>
            @error('materia')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>
    </div>
    {{-- <div class="col-sm-12 col-md-5">
        <div class="form-group">
            <label for="docente">Docente *</label>
            <select value="{{ old('docente', $grupo->docente_id) }}" name="docente" class="custom-select @error('docente') is-invalid @enderror" id="docente">
                <option value="">-- Seleccionar --</option>
                @foreach ($docentes as $docente)
                    <option
                    @if (old('docente', $grupo->docente_id) == $docente->id) selected="selected" @endif
                    value="{{ $docente->id }}">{{ $docente->nombres }} {{ $docente->apellidos }}</option>
                @endforeach
            </select>
            @error('docente')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>
    </div> --}}
</div>

<hr>

@if ($grupo->id)
<div class="float-left">
    @btndelete @endbtndelete
</div>
@endif

<div class="float-right">
    <a href="{{ route('docente.grupos.index') }}" class="btn btn-secondary">
        <i class="fas fa-times"></i> Cancelar
    </a>
    @btnsubmit @endbtnsubmit
</div>
