@extends('sbadmin.index')

@section('sbadmin-body')
@panel
    @slot('icon') chalkboard-teacher @endslot
    @slot('titulo') Registrar grupo @endslot

    <form action="{{ route('docente.grupos.store') }}" method="post">
        @csrf

        @include('docente.grupos.form')
    </form>
@endpanel
@endsection
