@extends('sbadmin.index')

@section('sbadmin-body')
@panel
    @slot('icon') chalkboard-teacher @endslot
    @slot('titulo') Grupos @endslot

    <p>Se lista a continuación los grupos registrados.</p>

    @include('partials.forms.deleted')

    @if (count($grupos))
        @include('docente.grupos.table')
    @else
        <div class="alert alert-info">
            No hay grupos registrados. Haz clic <a href="{{ route('docente.grupos.create') }}" class="text-primary">aquí</a> para registrar uno nuevo.
        </div>
    @endif

@endpanel
@endsection
