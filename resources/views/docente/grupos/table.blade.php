<table class="table table-bordered table-sm table-striped table-hover" width="50%">
    <thead>
        <tr align="center">
            <th>Nombre</th>
            <th>Materia</th>
            {{-- <th>Docente</th> --}}
            <th>Opciones</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($grupos as $g)
            <tr align="center">
                <td>{{ $g->nombre }}</td>
                <td>{{ $g->materia->nombre }}</td>
                {{-- <td>{{ $g->docente->nombres }} {{ $g->docente->apellidos }}</td> --}}
                <td>
                    <a href="{{ route('docente.grupos.show', ['grupo' => $g->id]) }}" class="btn btn-sm btn-primary" title="Mostrar detalles">
                        <i class="fas fa-eye"></i>
                    </a>
                    <a href="{{ route('docente.grupos.edit', ['grupo' => $g->id]) }}" class="btn btn-sm btn-info" title="Editar registro">
                        <i class="fas fa-edit"></i>
                    </a>
                    {{-- <a href="{{ route('docente.grupos.horario', ['materia' => $g->materia_id]) }}" class="btn btn-sm btn-warning" title="Ver horario">
                        <i class="fas fa-calendar-alt"></i>
                    </a> --}}

                    <span class="dropdown" title="Asistencia">
                        <a class="btn btn-secondary btn-sm dropdown-toggle" href="#" role="button" id="{{ 'dropdownMenuLink' . $g->id }}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-users"></i>
                        </a>

                        <div class="dropdown-menu" aria-labelledby="{{ 'dropdownMenuLink' . $g->id }}">
                            <a class="dropdown-item" href="{{ route('docente.grupos.asistencia.index', [ 'grupo' => $g->id ]) }}">
                                <i class="fas fa-users mr-2"></i> Ver estudiantes
                            </a>
                            <a class="dropdown-item" href="{{ route('docente.grupos.asistencia.create', [ 'grupo' => $g->id ]) }}">
                                <i class="fas fa-user-plus mr-2"></i> Agregar estudiante
                            </a>
                        </div>
                    </span>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>
