@extends('sbadmin.index')

@section('sbadmin-body')
@panel
    @slot('icon') calendar-alt @endslot
    @slot('titulo') Horario @endslot

    <div class="text-center">
        <i class="fas fa-calendar-alt"></i> <i>Horario de la materia: "{{ $nombreMateria[0]->nombre }}".</p>
        </i>
    </div>
    {{-- <p>Se lista a continuación el horario de la materia "{{ $nombreMateria[0]->nombre }}".</p> --}}

    @include('partials.forms.deleted')

    @if (count($horarioDocente))
        <table class="table table-bordered table-sm table-striped table-hover">
            <thead>
                <tr align="center">
                    <th>Hora</th>
                    <th>Lunes</th>
                    <th>Martes</th>
                    <th>Miercoles</th>
                    <th>Jueves</th>
                    <th>Viernes</th>
                    <th>Sabados</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($horarioDocente as $h)
                    <tr>
                        <td align="center" style="width:15%">{{ date('H:i A', strtotime($h->hora_inicio)) }} - {{ date('H:i A', strtotime($h->hora_fin)) }}</td>
                        @if($h->dia == 'Lunes')
                            <td align="center" height="50">Salon: {{ $h->salon->nombre }} <br> Grupo: {{ $h->grupo->nombre }} <br> Materia: {{ $h->grupo->materia->nombre }}</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        @elseif($h->dia == 'Martes')
                            <td></td>
                            <td align="center" height="50">Salon: {{ $h->salon->nombre }} <br> Grupo: {{ $h->grupo->nombre }} <br> Materia: {{ $h->grupo->materia->nombre }}</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        @elseif($h->dia == 'Miercoles')
                            <td></td>
                            <td></td>
                            <td align="center" height="50">Salon: {{ $h->salon->nombre }} <br> Grupo: {{ $h->grupo->nombre }} <br> Materia: {{ $h->grupo->materia->nombre }}</td>
                            <td></td>
                            <td></td>
                            <td></td>
                        @elseif($h->dia == 'Jueves')
                            <td></td>
                            <td></td>
                            <td></td>
                            <td align="center" height="50">Salon: {{ $h->salon->nombre }} <br> Grupo: {{ $h->grupo->nombre }} <br> Materia: {{ $h->grupo->materia->nombre }}</td>
                            <td></td>
                            <td></td>
                        @elseif($h->dia == 'Viernes')
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td align="center" height="50">Salon: {{ $h->salon->nombre }} <br> Grupo: {{ $h->grupo->nombre }} <br> Materia: {{ $h->grupo->materia->nombre }}</td>
                            <td></td>
                        @elseif($h->dia == 'Sabado')
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td align="center" height="50">Salon: {{ $h->salon->nombre }} <br> Grupo: {{ $h->grupo->nombre }} <br> Materia: {{ $h->grupo->materia->nombre }}</td>
                        @endif
                    </tr>
                @endforeach
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="7">
                        <a href="{{ route('docente.grupos.index') }}" class="btn btn-secondary">
                            <i class="fas fa-list"></i> Ver grupos
                        </a>
                    </td>
                </tr>
            </tfoot>
        </table>
    @endif

@endpanel
@endsection


