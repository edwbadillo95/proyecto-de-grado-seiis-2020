@extends('sbadmin.index')

@section('sbadmin-body')
@panel
    @slot('icon') user-friends @endslot
    @slot('titulo') Editar materia @endslot

    <form action="{{ route('docente.materias.update', ['materia' => $materia->id]) }}" method="post">
        @csrf
        @method('PUT')

        @include('docente.materias.form')
    </form>
@endpanel

@include('partials.delete', ['route' => route('docente.materias.destroy', ['materia' => $materia->id])])
@endsection
