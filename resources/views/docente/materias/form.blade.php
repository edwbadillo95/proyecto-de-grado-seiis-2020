<p>Digite el siguiente formulario, los campos con * son obligatorios.</p>

@include('partials.forms.error')

<div class="row">
    <div class="col-sm-12 col-md-8">
        <div class="form-group">
            <label for="nombre">Nombre de la materia *</label>
            <input
                id="nombre"
                name="nombre"
                value="{{ old('nombre', $materia->nombre) }}"
                type="text"
                class="form-control @error('nombre') is-invalid @enderror">

            @error('nombre')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>
    </div>
    <div class="col-sm-12 col-md-4">
        <div class="form-group">
            <label for="creditos">Creditos *</label>
            <input
                id="creditos"
                name="creditos"
                value="{{ old('creditos', $materia->creditos) }}"
                type="text"
                class="form-control @error('creditos') is-invalid @enderror">

            @error('creditos')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>
    </div>
</div>

<hr>

@if ($materia->id)
<div class="float-left">
    @btndelete @endbtndelete
</div>
@endif

<div class="float-right">
    <a href="{{ route('docente.materias.index') }}" class="btn btn-secondary">
        <i class="fas fa-times"></i> Cancelar
    </a>
    @btnreset @endbtnreset
    @btnsubmit @endbtnsubmit
</div>
