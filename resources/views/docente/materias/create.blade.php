@extends('sbadmin.index')

@section('sbadmin-body')
@panel
    @slot('icon') user-friends @endslot
    @slot('titulo') Registrar materia @endslot

    <form action="{{ route('docente.materias.store') }}" method="post">
        @csrf

        @include('docente.materias.form')
    </form>
@endpanel
@endsection
