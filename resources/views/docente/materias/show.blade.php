@extends('sbadmin.index')

@section('sbadmin-body')
@panel
    @slot('icon') user-friends @endslot
    @slot('titulo') Materia @endslot

    @include('partials.forms.saved')

    <table class="table table-bordered">
        <thead>
            <tr>
                <th colspan="4" class="text-center">
                    <i class="fas fa-info-circle"></i> <i>Información de la materia</i>
                </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th>Nombre</th>
                <td colspan="3">{{ $materia->nombre }}</td>
            </tr>
            <tr>
                <th>Creditos</th>
                <td colspan="3">{{ $materia->creditos }}</td>
            </tr>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="4">
                    <a href="{{ route('docente.materias.index') }}" class="btn btn-secondary">
                        <i class="fas fa-list"></i> Ver materias
                    </a>
                    <a href="{{ route('docente.materias.edit', ['materia'=> $materia->id]) }}" class="btn btn-info">
                        <i class="fas fa-edit"></i> Editar
                    </a>
                </td>
            </tr>
        </tfoot>
    </table>
@endpanel
@endsection
