@extends('sbadmin.index')

@section('sbadmin-body')
@panel
    @slot('icon') user-friends @endslot
    @slot('titulo') Materias @endslot

    <p>Se lista a continuación las materias registrados.</p>

    @include('partials.forms.deleted')

    @if (count($materias))
        @include('docente.materias.table')
    @else
        <div class="alert alert-info">
            No hay materias registradas. Haz clic <a href="{{ route('docente.materias.create') }}" class="text-primary">aquí</a> para registrar uno nuevo.
        </div>
    @endif

@endpanel
@endsection
