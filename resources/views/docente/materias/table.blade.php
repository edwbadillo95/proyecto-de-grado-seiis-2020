<table class="table table-bordered table-sm table-striped table-hover">
    <thead>
        <tr>
            <th>Nombre</th>
            <th>Creditos</th>
            <th>Opciones</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($materias as $materia)
            <tr>
                <td>{{ $materia->nombre }}</td>
                <td>{{ $materia->creditos }}</td>
                <td>
                    <a href="{{ route('docente.materias.show', ['materia' => $materia->id]) }}" class="btn btn-sm btn-primary" title="Mostrar detalles">
                        <i class="fas fa-eye"></i>
                    </a>
                    <a href="{{ route('docente.materias.edit', ['materia' => $materia->id]) }}" class="btn btn-sm btn-info" title="Editar registro">
                        <i class="fas fa-edit"></i>
                    </a>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>
