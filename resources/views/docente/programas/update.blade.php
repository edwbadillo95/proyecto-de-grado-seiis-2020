@extends('sbadmin.index')

@section('sbadmin-body')
@panel
    @slot('icon') user-friends @endslot
    @slot('titulo') Editar programa académico @endslot

    <form action="{{ route('docente.programas.update', ['programa' => $programa->id]) }}" method="post">
        @csrf
        @method('PUT')

        @include('docente.programas.form')
    </form>
@endpanel

@include('partials.delete', ['route' => route('docente.programas.destroy', ['programa' => $programa->id])])
@endsection
