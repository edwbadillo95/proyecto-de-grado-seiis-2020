<table class="table table-bordered table-sm table-striped table-hover">
    <thead>
        <tr>
            <th>Nombre</th>
            <th>Tipo de programa</th>
            <th>Facultad</th>
            <th>Opciones</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($programas as $p)
            <tr>
                <td>{{ $p->nombre }}</td>
                <td>{{ $p->tipoPrograma->nombre }}</td>
                <td>{{ $p->facultad->nombre }}</td>
                <td>
                    <a href="{{ route('docente.programas.show', ['programa' => $p->id]) }}" class="btn btn-sm btn-primary" title="Mostrar detalles">
                        <i class="fas fa-eye"></i>
                    </a>
                    <a href="{{ route('docente.programas.edit', ['programa' => $p->id]) }}" class="btn btn-sm btn-info" title="Editar registro">
                        <i class="fas fa-edit"></i>
                    </a>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>
