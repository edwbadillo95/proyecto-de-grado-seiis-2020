@extends('sbadmin.index')

@section('sbadmin-body')
@panel
    @slot('icon') user-friends @endslot
    @slot('titulo') Registrar programa académico @endslot

    <form action="{{ route('docente.programas.store') }}" method="post">
        @csrf

        @include('docente.programas.form')
    </form>
@endpanel
@endsection
