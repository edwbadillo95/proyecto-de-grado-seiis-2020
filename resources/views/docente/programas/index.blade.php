@extends('sbadmin.index')

@section('sbadmin-body')
@panel
    @slot('icon') user-friends @endslot
    @slot('titulo') Programas académicos @endslot

    <p>Se lista a continuación los programas académicos registrados.</p>

    @include('partials.forms.deleted')

    @if (count($programas))
        @include('docente.programas.table')
    @else
        <div class="alert alert-info">
            No hay programas académicos registrados. Haz clic <a href="{{ route('docente.programas.create') }}" class="text-primary">aquí</a> para registrar uno nuevo.
        </div>
    @endif

@endpanel
@endsection
