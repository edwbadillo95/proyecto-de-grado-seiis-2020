<p>Digite el siguiente formulario, los campos con * son obligatorios.</p>

@include('partials.forms.error')

<div class="row">
    <div class="col-sm-12 col-md-8">
        <div class="form-group">
            <label for="nombre">Nombre del programa académico *</label>
            <input
                id="nombre"
                name="nombre"
                value="{{ old('nombre', $programa->nombre) }}"
                type="text"
                class="form-control @error('nombre') is-invalid @enderror">

            @error('nombre')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>
    </div>
    <div class="col-sm-12 col-md-4">
        <div class="form-group">
            <label for="tipoPrograma">Tipo de programa *</label>
            <select value="{{ old('tipo_programa', $programa->tipo_programa_id) }}" name="tipo_programa" class="custom-select @error('tipo_programa') is-invalid @enderror" id="tipo_programa">
                <option value="">-- Seleccionar --</option>
                @foreach ($tiposPrograma as $tipo)
                    <option
                    @if (old('tipo_programa', $programa->tipo_programa_id) == $tipo->id) selected="selected" @endif
                    value="{{ $tipo->id }}">{{ $tipo->nombre }}</option>
                @endforeach
            </select>
            @error('tipo_programa')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>
    </div>
    <div class="col-sm-12 col-md-4">
        <div class="form-group">
            <label for="facultad">Facultad *</label>
            <select value="{{ old('facultad', $programa->facultad_id) }}" name="facultad" class="custom-select @error('facultad') is-invalid @enderror" id="facultad">
                <option value="">-- Seleccionar --</option>
                @foreach ($facultades as $facultad)
                    <option
                    @if (old('facultad', $programa->facultad_id) == $facultad->id) selected="selected" @endif
                    value="{{ $facultad->id }}">{{ $facultad->nombre }}</option>
                @endforeach
            </select>
            @error('facultad')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>
    </div>
</div>

<hr>

@if ($programa->id)
<div class="float-left">
    @btndelete @endbtndelete
</div>
@endif

<div class="float-right">
    <a href="{{ route('docente.programas.index') }}" class="btn btn-secondary">
        <i class="fas fa-times"></i> Cancelar
    </a>
    @btnreset @endbtnreset
    @btnsubmit @endbtnsubmit
</div>
