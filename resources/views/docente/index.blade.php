@extends('sbadmin.index')

@section('sbadmin-body')
@panel
    @slot('icon') user-tie @endslot
    @slot('titulo') Docente @endslot

    @include('partials.forms.deleted')

    @if (count($docentes))
        @include('docente.table')
    @else
        {{-- <div class="alert alert-info">
            No hay docentes registrados. Haz clic <a href="{{ route('docente.create') }}" class="text-primary">aquí</a> para registrar uno nuevo.
        </div> --}}
    @endif

@endpanel
@endsection
