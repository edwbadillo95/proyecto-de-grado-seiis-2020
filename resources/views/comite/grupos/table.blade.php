<table class="table table-bordered table-sm table-striped table-hover">
    <thead>
        <tr>
            <th>Nombre</th>
            <th>Tipo</th>
            <th>Estado</th>
            <th>Opciones</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($grupos as $g)
            <tr>
                <td>{{ $g->nombre }}</td>
                <td>{{ $g->tipoComite->nombre }}</td>
                <td>
                    <span class="badge badge-{{ $g->estado ? 'success' : 'danger' }}">
                        {{ $g->estado ? 'ACTIVO' : 'INACTIVO' }}
                    </span>
                </td>
                <td>
                    <a href="{{ route('comite.grupos.show', ['grupo' => $g->id]) }}" class="btn btn-sm btn-primary" title="Mostrar detalles">
                        <i class="fas fa-eye"></i>
                    </a>
                    <a href="{{ route('comite.grupos.edit', ['grupo' => $g->id]) }}" class="btn btn-sm btn-info" title="Editar registro">
                        <i class="fas fa-edit"></i>
                    </a>

                    <span class="dropdown" title="Integrantes">
                        <a class="btn btn-secondary btn-sm dropdown-toggle" href="#" role="button" id="{{ 'dropdownMenuLink' . $g->id }}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-users"></i>
                        </a>

                        <div class="dropdown-menu" aria-labelledby="{{ 'dropdownMenuLink' . $g->id }}">
                            <a class="dropdown-item" href="{{ route('comite.grupos.integrantes.index', [ 'grupo' => $g->id ]) }}">
                                <i class="fas fa-users mr-2"></i> Listar integrantes
                            </a>
                            @if ($g->estado)
                            <a class="dropdown-item" href="{{ route('comite.grupos.integrantes.create', [ 'grupo' => $g->id ]) }}">
                                <i class="fas fa-user-plus mr-2"></i> Crear integrante
                            </a>
                            @endif
                        </div>
                    </span>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>
