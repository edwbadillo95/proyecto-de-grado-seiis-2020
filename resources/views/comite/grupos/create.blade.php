@extends('sbadmin.index')

@section('sbadmin-body')
@panel
    @slot('icon') user-friends @endslot
    @slot('titulo') Registrar grupo de comité @endslot

    <form action="{{ route('comite.grupos.store') }}" method="post">
        @csrf

        @include('comite.grupos.form')
    </form>
@endpanel
@endsection
