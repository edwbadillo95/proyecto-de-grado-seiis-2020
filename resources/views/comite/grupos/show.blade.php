@extends('sbadmin.index')

@section('sbadmin-body')
@panel
    @slot('icon') user-friends @endslot
    @slot('titulo') Grupo de comité @endslot

    @include('partials.forms.saved')

    <table class="table table-bordered">
        <thead>
            <tr>
                <th colspan="4" class="text-center">
                    <i class="fas fa-info-circle"></i> <i>Información del grupo de comité</i>
                </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th>Nombre</th>
                <td colspan="3">{{ $grupo->nombre }}</td>
            </tr>
            <tr>
                <th>Descripción</th>
                <td colspan="3">{{ $grupo->descripcion }}</td>
            </tr>
            <tr>
                <th>Tipo de comité</th>
                <td>{{ $grupo->tipoComite->nombre }}</td>
                <th>Estado</th>
                <td>
                    <span class="badge badge-{{ $grupo->estado ? 'success' : 'danger' }}">
                        {{ $grupo->estado ? 'ACTIVO' : 'INACTIVO' }}
                    </span>
                </td>
            </tr>
            <tr>
                <th>Fecha de inicio</th>
                <td>{{ $grupo->fecha_inicio }}</td>
                <th>Fecha de finalización</th>
                <td>{{ $grupo->fecha_finalizacion }}</td>
            </tr>
            <tr>
                <th>Fecha de registro</th>
                <td>{{ $grupo->created_at }}</td>
                <th>Fecha de modificación</th>
                <td>{{ $grupo->updated_at }}</td>
            </tr>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="4">
                    <a href="{{ route('comite.grupos.index') }}" class="btn btn-secondary">
                        <i class="fas fa-list"></i> Ver grupos de comité
                    </a>
                    <a href="{{ route('comite.grupos.edit', ['grupo'=> $grupo->id]) }}" class="btn btn-info">
                        <i class="fas fa-edit"></i> Editar
                    </a>
                </td>
            </tr>
        </tfoot>
    </table>
@endpanel
@endsection
