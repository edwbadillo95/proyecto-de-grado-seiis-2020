@extends('sbadmin.index')

@section('sbadmin-body')
@panel
    @slot('icon') user-friends @endslot
    @slot('titulo') Editar grupo de comité @endslot

    <form action="{{ route('comite.grupos.update', ['grupo' => $grupo->id]) }}" method="post">
        @csrf
        @method('PUT')

        @include('comite.grupos.form')
    </form>
@endpanel

@include('partials.delete', ['route' => route('comite.grupos.destroy', ['grupo' => $grupo->id])])
@endsection
