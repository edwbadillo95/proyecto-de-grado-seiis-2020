@extends('sbadmin.index')

@section('sbadmin-body')
@panel
    @slot('icon') user-friends @endslot
    @slot('titulo') Grupos de comité @endslot

    <p>Se lista a continuación los grupos de comité registrados.</p>

    @include('partials.forms.deleted')

    @if (count($grupos))
        @include('comite.grupos.table')
    @else
        <div class="alert alert-info">
            No hay grupos de comité registrados. Haz clic <a href="{{ route('comite.grupos.create') }}" class="text-primary">aquí</a> para registrar uno nuevo.
        </div>
    @endif

@endpanel
@endsection
