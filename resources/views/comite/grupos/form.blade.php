<p>Digite el siguiente formulario, los campos con * son obligatorios.</p>

@include('partials.forms.error')

<div class="row">
    <div class="col-sm-12 col-md-8">
        <div class="form-group">
            <label for="nombre">Nombre del grupo *</label>
            <input
                id="nombre"
                name="nombre"
                value="{{ old('nombre', $grupo->nombre) }}"
                type="text"
                class="form-control @error('nombre') is-invalid @enderror">

            @error('nombre')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>
    </div>
    <div class="col-sm-12 col-md-4">
        <div class="form-group">
            <label for="tipoComite">Tipo de comité *</label>
            <select value="{{ old('tipo_comite', $grupo->tipo_comite_id) }}" name="tipo_comite" class="custom-select @error('tipo_comite') is-invalid @enderror" id="tipo_comite">
                <option value="">-- Seleccionar --</option>
                @foreach ($tiposComite as $tipo)
                    <option
                    @if (old('tipo_comite', $grupo->tipo_comite_id) == $tipo->id) selected="selected" @endif
                    value="{{ $tipo->id }}">{{ $tipo->nombre }}</option>
                @endforeach
            </select>
            @error('tipo_comite')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="form-group">
            <label for="descripcion">Descripción</label>
            <textarea name="descripcion" id="descripcion" rows="3" class="form-control">{{ $grupo->descripcion }}</textarea>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12 col-md-4">
        <div class="form-group">
            <label for="descripcion">Fecha de inicio *</label><br>
            <input
                id="fecha_inicio"
                name="fecha_inicio"
                value="{{ old('fecha_inicio', $grupo->fecha_inicio) }}"
                type="date"
                class="form-control @error('fecha_inicio') is-invalid @enderror">

            @error('fecha_inicio')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>
    </div>
    <div class="col-sm-12 col-md-4">
        <div class="form-group">
            <label for="descripcion">Fecha de terminación *</label><br>
            <input
                id="fecha_finalizacion"
                name="fecha_finalizacion"
                value="{{ old('fecha_finalizacion', $grupo->fecha_finalizacion) }}"
                type="date"
                class="form-control @error('fecha_finalizacion') is-invalid @enderror">

            @error('fecha_finalizacion')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>
    </div>
    <div class="col-sm-12 col-md-4">
        <div class="form-group">
            <label for="descripcion">Estado</label><br>
            <select value="{{ old('estado', $grupo->estado) }}" name="estado" class="custom-select @error('estado') is-invalid @enderror" id="estado">
                <option
                @if (old('estado', $grupo->estado) == 1)
                    selected="selected"
                @endif
                value="1">ACTIVO</option>

                <option
                @if (old('estado', $grupo->estado) == 0)
                    selected="selected"
                @endif
                value="0">INACTIVO</option>
            </select>
            @error('estado')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>
    </div>
</div>

<hr>

@if ($grupo->id)
<div class="float-left">
    @btndelete @endbtndelete
</div>
@endif

<div class="float-right">
    <a href="{{ route('comite.grupos.index') }}" class="btn btn-secondary">
        <i class="fas fa-times"></i> Cancelar
    </a>
    @btnreset @endbtnreset
    @btnsubmit @endbtnsubmit
</div>
