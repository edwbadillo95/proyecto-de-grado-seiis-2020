@if (request()->documento && !$docente && !isset($integranteDuplicado))
    <div class="alert alert-danger">
        No se encontró un docente con el documento <b>{{ request()->documento }}</b> en el sistema.
    </div>
@elseif(isset($integranteDuplicado))
    <div class="alert alert-danger">
        El docente con el documento <b>{{ request()->documento }}</b> ya se encuentra asignado en el grupo <b>{{ $grupo->nombre }}</b>.
    </div>
@endif

@if ($docente)
<form action="{{ $route }}" method="post">
    @csrf

    @if ($isUpdate)
        @method('PUT')
    @endif
    <input type="hidden" name="docente" value="{{ $docente->id }}">


    <div class="card border-secondary">
        <div class="card-header">
            <i class="fas fa-user-tie"></i> Docente a registrar
        </div>

        @if ($errors->has('docente'))
            <div class="card-body">
                <div class="alert alert-danger mb-0">
                    El docente especificado parecer ser incorrecto. Vuelva a especificar un número de documento de un docente en la parte superior de este formulario.
                </div>
            </div>
        @else
            <div class="card-body">
                <table class="table table-bordered table-sm">
                    <tbody>
                        <tr>
                            <th>Documento</th>
                            <td>{{ $docente->documento }}</td>
                            <th>Tipo documento</th>
                            <td>{{ $docente->tipoDocumento->nombre }}</td>
                        </tr>
                        <tr>
                            <th>Nombre</th>
                            <td>{{ $docente->nombres }}</td>
                            <th>Apellidos</th>
                            <td>{{ $docente->apellidos }}</td>
                        </tr>
                        <tr>
                            <th>Clasificación</th>
                            <td colspan="3">{{ $docente->clasificacion->nombre }}</td>
                        </tr>
                    </tbody>
                </table>

                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="descripcion">Descripción</label>
                            <textarea name="descripcion" id="descripcion" rows="3" class="form-control">{{ $integrante->descripcion }}</textarea>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <div class="custom-control custom-checkbox">
                            <input name="programar_sesiones" type="checkbox" class="custom-control-input" id="programarSesiones" {{ $integrante->programar_sesiones ? 'checked' : '' }}>
                            <label class="custom-control-label" for="programarSesiones">Programar sesiones de comité (solo para el grupo al que pertenece)</label>
                        </div>
                    </div>
                </div>

                @include('comite.integrantes.retirar')
            </div>
            <div class="card-footer text-center">
                <button class="btn btn-uts" type="submit">
                    <i class="fas fa-user-plus"></i> Registrar
                </button>
                <a class="btn btn-secondary" href="{{ route('comite.grupos.integrantes.create', ['grupo'=>$grupo->id]) }}">
                    <i class="fas fa-redo"></i> Cancelar
                </a>
            </div>
        @endif
    </div>
</form>
@endif
