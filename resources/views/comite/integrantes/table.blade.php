<table class="table table-bordered table-sm table-striped table-hover">
    <thead>
        <tr>
            <th>Nombre Completo</th>
            <th>Clasificación</th>
            <th>Estado (en el grupo)</th>
            <th>Opciones</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($grupo->integrantes as $i)
            <tr>
                <td>{{ $i->docente->nombres }} {{ $i->docente->apellidos }}</td>
                <td>{{ $i->docente->clasificacion->nombre }}</td>
                <td>
                    <span class="badge badge-{{ $i->fecha_retiro ? 'danger' : 'success' }}">
                        {{ $i->fecha_retiro ? 'RETIRADO' : 'ACTIVO' }}
                    </span>
                </td>
                <td>
                    <a href="{{ route('comite.grupos.integrantes.show', ['grupo' => $grupo->id, 'integrante' => $i->id]) }}" class="btn btn-sm btn-primary" title="Mostrar detalles">
                        <i class="fas fa-eye"></i>
                    </a>
                    @if (!$i->fecha_retiro)
                    <a href="{{ route('comite.grupos.integrantes.edit', ['grupo' => $grupo->id, 'integrante' => $i->id]) }}" class="btn btn-sm btn-info" title="Editar registro">
                        <i class="fas fa-edit"></i>
                    </a>
                    @endif
                </td>
            </tr>
        @endforeach
    </tbody>
</table>
