@extends('sbadmin.index')

@section('sbadmin-body')
@panel
    @slot('titulo')

    <div class="clearfix">
        <div class="float-left"> <i class="fa fa-users"></i> Integrantes de comité</div>

        <div class="float-right">
            <a href="{{ route('comite.grupos.integrantes.create', ['grupo' => $grupo->id]) }}" class="btn btn-uts btn-sm">Agregar integrante</a>
        </div>
    </div>

    @endslot

    <p>Se lista a continuación los integrantes de comité registrados en el grupo <b>{{ $grupo->nombre }}</b>.</p>

    @include('partials.forms.deleted')

    @if ($grupo->integrantes()->count())
        @include('comite.integrantes.table')
    @else
        <div class="alert alert-info">
            No hay integrantes registrados en este grupo. @if ($grupo->estado)
            Haz clic <a href="{{ route('comite.grupos.integrantes.create', ['grupo' => $grupo->id]) }}" class="text-primary">aquí</a> para registrar uno nuevo.
            @endif
        </div>
    @endif

@endpanel
@endsection
