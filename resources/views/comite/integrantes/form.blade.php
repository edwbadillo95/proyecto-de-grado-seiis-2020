<p>
    Digite el siguiente formulario para el registro de un integrante de comité para el grupo <b>{{ $grupo->nombre }}</b>.
</p>

@if (!$integrante->id)
@include('comite.integrantes.formdoc')
@endif

@include('comite.integrantes.formintegrante', ['route' => $route, 'isUpdate' => $isUpdate])
