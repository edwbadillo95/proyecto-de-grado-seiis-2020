<form class="mb-3" action="{{ route('comite.grupos.integrantes.create', ['grupo'=>$grupo->id]) }}" method="get">
    <div class="card border-primary">
        <div class="card-header text-center">
            <b>Ingrese el documento del docente que desea agregar.</b>
        </div>
        <div class="row p-3">
            <div class="col-xs-12 col-sm-6 col-md-4 offset-sm-3 offset-md-4">
                <div class="input-group">
                    <input
                        value="{{ request()->documento }}"
                        name="documento"
                        type="text"
                        class="form-control"
                        placeholder="Documento del docente"
                        aria-label="Documento del docente"
                        aria-describedby="btn-doc">

                    <div class="input-group-append">
                        <button class="btn btn-primary" type="submit" id="btn-doc">
                           <i class="fas fa-search"></i> Buscar
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
