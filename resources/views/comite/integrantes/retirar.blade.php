@if ($integrante->id)
<div class="row">
    <div class="col-sm-12">
        <div class="card border-danger mt-3">
            <div class="card-body">
                <div class="custom-control custom-checkbox">
                    <input name="retirar" type="checkbox" class="custom-control-input" id="retirar">
                    <label class="custom-control-label" for="retirar">Retirar integrante de comité. Una vez registrado el retiro no se podrá deshacer.</label>
                </div>
            </div>
        </div>
    </div>
</div>
@endif
