@extends('sbadmin.index')

@section('sbadmin-body')
@panel
    @slot('icon') user-plus @endslot
    @slot('titulo') Registrar integrante @endslot

    @include('comite.integrantes.form', ['route' => route('comite.grupos.integrantes.update', ['grupo'=>$grupo->id, 'integrante' => $integrante->id]), 'isUpdate' => true])
@endpanel
@endsection
