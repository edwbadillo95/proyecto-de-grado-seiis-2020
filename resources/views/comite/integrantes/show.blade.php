@extends('sbadmin.index')

@section('sbadmin-body')
@panel
    @slot('icon') user-tie @endslot
    @slot('titulo') Integrante de comité @endslot

    @include('partials.forms.saved')

    <table class="table table-bordered">
        <thead>
            <tr>
                <th colspan="4" class="text-center">
                    <i class="fas fa-info-circle"></i> <i>Información del integrante de comité</i>
                </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th>Nombre completo</th>
                <td>{{ $integrante->docente->nombres }} {{ $integrante->docente->apellidos }}</td>
                <th>Documento</th>
                <td>{{ $integrante->docente->documento }}</td>
            </tr>
            <tr>
                <th>Clasificación del docente</th>
                <td>{{ $integrante->docente->clasificacion->nombre }}</td>
                <th>¿Puede programar sesiones?</th>
                <td>
                    <span class="badge badge-{{ $integrante->programar_sesiones ? 'info' : 'secondary' }}">
                        {{ $integrante->programar_sesiones ? 'SÍ' : 'NO' }}
                    </span>
                </td>
            </tr>
            <tr>
                <th>Descripción</th>
                <td colspan="3">{{ $integrante->descripcion }}</td>
            </tr>
            <tr>
                <th>Fecha de asignación</th>
                <td>{{ $integrante->fecha_integracion }}</td>
                <th>Fecha de retiro</th>
                <td>{{ $integrante->fecha_retiro }}</td>
            </tr>

            <tr>
                <th>Grupo de comité</th>
                <td colspan="3">{{ $grupo->nombre }}</td>
            </tr>
            <tr>
                <th>Tipo de comité</th>
                <td>{{ $grupo->tipoComite->nombre }}</td>
                <th>Estado del grupo de comité</th>
                <td>
                    <span class="badge badge-{{ $grupo->estado ? 'success' : 'danger' }}">
                        {{ $grupo->estado ? 'ACTIVO' : 'INACTIVO' }}
                    </span>
                </td>
            </tr>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="4">
                    <a href="{{ route('comite.grupos.integrantes.index', ['grupo'=> $grupo->id]) }}" class="btn btn-secondary">
                        <i class="fas fa-list"></i> Ver integrantes del grupo de comité
                    </a>
                    @if (!$integrante->fecha_retiro)
                    <a href="{{ route('comite.grupos.integrantes.edit', ['grupo'=> $grupo->id, 'integrante' => $integrante->id]) }}" class="btn btn-info">
                        <i class="fas fa-edit"></i> Editar
                    </a>
                    @endif
                </td>
            </tr>
        </tfoot>
    </table>
@endpanel
@endsection
