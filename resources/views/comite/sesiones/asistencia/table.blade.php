<table class="table table-bordered table-sm table-striped table-hover">
    <thead>
        <tr>
            <th>Nombre Completo</th>
            <th>Clasificación</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($asistencia as $i)
            <tr>
                <td>{{ $i->docente->nombres }} {{ $i->docente->apellidos }}</td>
                <td>{{ $i->docente->clasificacion->nombre }}</td>
            </tr>
        @endforeach
    </tbody>
</table>
