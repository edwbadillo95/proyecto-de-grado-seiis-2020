@extends('sbadmin.index')

@section('sbadmin-body')
@panel
    @slot('icon') user-check @endslot
    @slot('titulo') Registro de asistencia de la sesión @endslot

    <p>Listado de personas que estuvieron en la sesión.</p>

    @include('partials.forms.saved')

    @if (count($asistencia))
        <a class="btn btn-success mb-3" href="{{ route('comite.sesiones.asistentes.create', ['sesion'=>$sesion->id]) }}">
            Registrar Asistencia
        </a>

        @include('comite.sesiones.asistencia.table')
    @else
        <div class="alert alert-info">
            No hay asistencia registrada. Haz clic <a href="{{ route('comite.sesiones.asistentes.create', [ 'sesion' => $sesion->id ]) }}" class="text-primary">aquí</a> para registrar una nueva asistencia a esta sesión.
        </div>
    @endif

@endpanel
@endsection
