@extends('sbadmin.index')

@section('sbadmin-body')
@panel
    @slot('icon') user-check @endslot
    @slot('titulo') Registrar asistencia @endslot

    <form action="{{ route('comite.sesiones.asistentes.store', ['sesion' => $sesion->id]) }}" method="post">
        @csrf

        <table class="table table-bordered">
            <thead>
                <tr>
                    <th colspan="4" class="text-center">
                        <i class="fas fa-info-circle"></i> <i>Información de la sesión de comité</i>
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th>Asunto</th>
                    <td colspan="3">{{ $sesion->asunto }}</td>
                </tr>
                <tr>
                    <th>Descripción</th>
                    <td colspan="3">{{ $sesion->descripcion }}</td>
                </tr>
                <tr>
                    <th>Grupo de comité</th>
                    <td colspan="3">{{ $sesion->grupoComite->nombre }}</td>
                </tr>
                <tr>
                    <th>Fecha programada</th>
                    <td colspan="3">{{ $sesion->fecha }}</td>
                </tr>
            </tbody>
        </table>

        <table class="table table-bordered">
            <thead>
                <tr>
                    <th colspan="4" class="text-center">
                        <i class="fas fa-info-circle"></i> <i>Asistencia de integrantes del comité</i>
                    </th>
                </tr>
                <tr>
                    <th>Nombre</th>
                    <th>¿Asistió?</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($integrantes as $i)
                <tr>
                    <td>{{ $i->docente->nombres }} {{ $i->docente->apellidos }}</td>
                    <td>
                        <input type="checkbox" name="integrantes[]" value="{{ $i->id }}">
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>

        <a href="{{ route('comite.sesiones.asistentes.index', ['sesion' => $sesion->id]) }}" class="btn btn-secondary">
            <i class="fas fa-times"></i> Cancelar
        </a>
        @btnsubmit @endbtnsubmit
    </form>
@endpanel
@endsection
