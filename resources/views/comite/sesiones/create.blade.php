@extends('sbadmin.index')

@section('sbadmin-body')
@panel
    @slot('icon') calendar-alt @endslot
    @slot('titulo') Registrar sesión de comité @endslot

    <form action="{{ route('comite.sesiones.store') }}" method="post">
        @csrf

        @include('comite.sesiones.form')
    </form>
@endpanel
@endsection
