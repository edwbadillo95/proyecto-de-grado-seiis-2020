@extends('sbadmin.index')

@section('sbadmin-body')
@panel
    @slot('icon') calendar-alt @endslot
    @slot('titulo') Sesión de comité @endslot

    @include('partials.forms.saved')

    <table class="table table-bordered">
        <thead>
            <tr>
                <th colspan="4" class="text-center">
                    <i class="fas fa-info-circle"></i> <i>Información de la sesión de comité</i>
                </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th>Asunto</th>
                <td colspan="3">{{ $sesion->asunto }}</td>
            </tr>
            <tr>
                <th>Descripción</th>
                <td colspan="3">{{ $sesion->descripcion }}</td>
            </tr>
            <tr>
                <th>Grupo de comité</th>
                <td colspan="3">{{ $sesion->grupoComite->nombre }}</td>
            </tr>
            <tr>
                <th>Fecha programada</th>
                <td colspan="3">{{ $sesion->fecha }}</td>
            </tr>
            <tr>
                <th>Fecha de registro</th>
                <td>{{ $sesion->created_at }}</td>
                <th>Fecha de modificación</th>
                <td>{{ $sesion->updated_at }}</td>
            </tr>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="4">
                    <a href="{{ route('comite.sesiones.index') }}" class="btn btn-secondary">
                        <i class="fas fa-list"></i> Ver sesiones de comité
                    </a>
                    <a href="{{ route('comite.sesiones.edit', ['sesion'=> $sesion->id]) }}" class="btn btn-info">
                        <i class="fas fa-edit"></i> Editar
                    </a>
                </td>
            </tr>
        </tfoot>
    </table>
@endpanel
@endsection
