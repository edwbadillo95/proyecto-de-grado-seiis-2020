@extends('sbadmin.index')

@section('sbadmin-body')
@panel
    @slot('icon') calendar-alt @endslot
    @slot('titulo') Sesiones de comité @endslot

    <p>Se lista a continuación las sesiones de comité registradas.</p>

    @include('partials.forms.deleted')

    @if (count($sesiones))
        @include('comite.sesiones.table')
    @else
        <div class="alert alert-info">
            No hay sesiones de comité registradas. @if (Auth::user()->puedeRegistrarSesiones())
            Haz clic <a href="{{ route('comite.sesiones.create') }}" class="text-primary">aquí</a> para registrar una nueva sesión.
            @endif
        </div>
    @endif

@endpanel
@endsection
