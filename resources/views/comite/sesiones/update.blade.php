@extends('sbadmin.index')

@section('sbadmin-body')
@panel
    @slot('icon') calendar-alt @endslot
    @slot('titulo') Editar sesión de comité @endslot

    <form action="{{ route('comite.sesiones.update', ['sesion' => $sesion->id]) }}" method="post">
        @csrf
        @method('PUT')

        @include('comite.sesiones.form')
    </form>
@endpanel

@include('partials.delete', ['route' => route('comite.sesiones.destroy', ['sesion' => $sesion->id])])
@endsection
