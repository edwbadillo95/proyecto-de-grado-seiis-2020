@if (!$gruposComite)
<div class="alert alert-danger" role="alert">
    <h4 class="alert-heading"><i class="fas fa-ban"></i> Error! No hay grupos de comité.</h4>
    <hr>
    <p class="mb-0">Para registrar una sesión de comité es necesario que en el sistema exista algún grupo de comité vigente activo.</p>
  </div>
@else
<p>
    Digite el siguiente formulario para el registro de una nueva sesión de comité, los campos con * son obligatorios.
</p>

@include('partials.forms.error')

<div class="row">
    <div class="col-sm-12 col-md-8">
        <div class="form-group">
            <label for="nombre">Asunto *</label>
            <input
                id="asunto"
                name="asunto"
                value="{{ old('asunto', $sesion->asunto) }}"
                type="text"
                class="form-control @error('asunto') is-invalid @enderror">

            @error('asunto')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>
    </div>
    <div class="col-sm-12 col-md-4">
        <div class="form-group">
            <label for="fecha">Fecha para la sesión *</label><br>
            <input
                id="fecha"
                name="fecha"
                value="{{ old('fecha', $sesion->fecha) }}"
                type="date"
                class="form-control @error('fecha') is-invalid @enderror">

            @error('fecha')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="form-group">
            <label for="descripcion">Descripción</label>
            <textarea name="descripcion" id="descripcion" rows="3" class="form-control">{{ $sesion->descripcion }}</textarea>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12 col-md-4">
        <div class="form-group">
            <label for="tipoComite">Grupo de comité *</label>
            <select value="{{ old('grupo_comite', $sesion->grupo_comite_id) }}" name="grupo_comite" class="custom-select @error('grupo_comite') is-invalid @enderror" id="grupo_comite">
                <option value="">-- Seleccionar --</option>
                @foreach ($gruposComite as $g)
                    <option
                    @if (old('grupo_comite', $sesion->grupo_comite_id) == $g->id) selected="selected" @endif
                    value="{{ $g->id }}">{{ $g->nombre }}</option>
                @endforeach
            </select>
            @error('grupo_comite')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>
    </div>
</div>

<hr>

@if ($sesion->id && Auth::user()->tieneRolCoordinador())
<div class="float-left">
    @btndelete @endbtndelete
</div>
@endif

<div class="float-right">
    <a href="{{ route('comite.sesiones.index') }}" class="btn btn-secondary">
        <i class="fas fa-times"></i> Cancelar
    </a>
    @btnreset @endbtnreset
    @btnsubmit @endbtnsubmit
</div>
@endif


