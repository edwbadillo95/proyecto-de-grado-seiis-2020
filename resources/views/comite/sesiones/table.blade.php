<table class="table table-bordered table-sm table-striped table-hover">
    <thead>
        <tr>
            <th>Fecha</th>
            <th>Asunto</th>
            <th>Opciones</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($sesiones as $s)
            <tr>
                <td>{{ $s->fecha }}</td>
                <td>{{ $s->asunto }}</td>
                <td>
                    <a href="{{ route('comite.sesiones.show', ['sesion' => $s->id]) }}" class="btn btn-sm btn-primary" title="Mostrar detalles">
                        <i class="fas fa-eye"></i>
                    </a>
                    <a href="{{ route('comite.sesiones.edit', ['sesion' => $s->id]) }}" class="btn btn-sm btn-info" title="Editar registro">
                        <i class="fas fa-edit"></i>
                    </a>
                    <a href="{{ route('comite.sesiones.asistentes.index', ['sesion' => $s->id]) }}" class="btn btn-sm btn-info" title="Asistencia">
                        <i class="fas fa-user-check"></i>
                    </a>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>
