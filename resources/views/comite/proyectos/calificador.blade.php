@extends('sbadmin.index')

@section('sbadmin-body')
@panel
    @slot('titulo')

    <div class="clearfix">
        <div class="float-left"> <i class="fa fa-lightbulb"></i> Proyectos de grado | Calificador</div>

        @if (Auth::user()->docente)
            <div class="float-right">
                <a href="{{ route('comite.proyectos.index', ['misproyectos' => true]) }}" class="btn btn-uts btn-sm">Mis proyectos</a>
            </div>
        @endif
    </div>

    @endslot

    @if ($proyectos)
        @include('comite.proyectos.table')
    @else
        <div class="alert alert-info">
            No tiene proyectos asignados para calificar.
        </div>
    @endif

@endpanel
@endsection
