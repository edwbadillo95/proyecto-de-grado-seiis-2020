<table class="table table-bordered table-sm table-striped table-hover">
    <thead>
        <tr>
            <th>Título</th>
            <th>Estado</th>
            <th>Opciones</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($proyectos as $p)
            <tr>
                <td>{{ $p->nombre }}</td>
                <td>{{ $p->estadoProyecto->nombre }}</td>
                <td>
                    <a href="{{ route('comite.proyectos.show', ['proyecto' => $p->id]) }}" class="btn btn-sm btn-primary" title="Mostrar detalles">
                        <i class="fas fa-eye"></i>
                    </a>
                    @if (Auth::user()->puedeVerificarProyecto($p))
                        <a href="{{ route('comite.proyectos.verificar', ['proyecto' => $p->id]) }}" class="btn btn-sm btn-info" title="Verificar">
                            <i class="fas fa-check"></i>
                        </a>
                    @endif
                    @if (Auth::user()->esIntegranteProyecto($p))
                        <a href="{{ route('proyecto.documentacion', ['proyecto' => $p->id]) }}" class="btn btn-sm btn-info" title="Documentacion">
                            <i class="fas fa-folder-open"></i>
                        </a>
                    @endif
                </td>
            </tr>
        @endforeach
    </tbody>
</table>
