<p>Digite el siguiente formulario para registrar un proyecto de grado. Integrantes del comité de proyectos de grado se encargarán de evaluar la viabilidad del proyecto para ser aprobado y desarrollado.</p>

@include('partials.forms.error')

<div class="row">
    <div class="col-sm-12">
        <div class="form-group">
            <label for="nombre">Título o nombre del proyecto *</label>
            <textarea name="nombre" id="nombre" required rows="3" class="form-control">{{ $proyecto->nombre }}</textarea>
        </div>
    </div>
    <div class="col-sm-12">
        <div class="form-group">
            <label for="descripcion">Descripción</label>
            <textarea name="descripcion" id="descripcion" rows="10" class="form-control">{{ $proyecto->descripcion }}</textarea>
        </div>
    </div>
</div>

<div class="float-right">
    <a href="{{ route('comite.proyectos.index') }}" class="btn btn-secondary">
        <i class="fas fa-times"></i> Cancelar
    </a>
    @btnreset @endbtnreset
    @btnsubmit @endbtnsubmit
</div>
