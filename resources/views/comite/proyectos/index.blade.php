@extends('sbadmin.index')

@section('sbadmin-body')
@panel
    @slot('titulo')

    <div class="clearfix">
        <div class="float-left"> <i class="fa fa-lightbulb"></i> Proyectos de grado</div>

        @if (Auth::user()->docente)
            <div class="float-right ml-2">
                <a href="{{ route('comite.proyectos.index', ['misproyectos' => true]) }}" class="btn btn-uts btn-sm">Mis proyectos</a>
            </div>

            <div class="float-right">
                <a href="{{ route('comite.proyectos.calificador') }}" class="btn btn-uts btn-sm">Calificar proyectos</a>
            </div>
        @endif
    </div>

    @endslot

    @if (!request()->misproyectos && !request()->miproyecto)
    <p>Se lista a continuación todos los proyectos de grado registrados en los programas de tecnología e ingeniería de la Coordinación de Sistemas.</p>
    @elseif (request()->miproyecto && Auth::user()->estudiante)
    <p>Listando proyecto al que está inscrito.</p>
    @endif

    @include('partials.forms.deleted')

    @if ($proyectos->count())
        @if (request()->misproyectos && Auth::user()->docente)
        <div class="alert alert-info">
            <p class="mb-0">Listando mis proyectos, para volver a ver todos los proyectos haz clic <a class="text-primary" href="{{ route('comite.proyectos.index') }}">aquí</a>.</p>
        </div>
        @endif

        @include('comite.proyectos.table')
    @else
        <div class="alert alert-info">
            @if (request()->misproyectos && Auth::user()->docente)
            <p>Listando mis proyectos, para volver a ver todos los proyectos haz clic <a class="text-primary" href="{{ route('comite.proyectos.index') }}">aquí</a>.</p>
            @endif

            <p class="mb-0">
                No hay proyectos registrados. @if (Auth::user()->docente)
                Haz clic <a href="{{ route('comite.proyectos.create') }}" class="text-primary">aquí</a> para registrar uno nuevo. Cualquier docente puede registrar un proyecto de grado.
                @endif
            </p>
        </div>
    @endif

@endpanel
@endsection
