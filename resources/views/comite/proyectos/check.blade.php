@extends('sbadmin.index')

@section('sbadmin-body')
@panel
    @slot('icon') check-circle @endslot
    @slot('titulo') Verificar proyecto de grado @endslot

    <p>Verifique el siguiente proyecto de grado que ha sido registrado para que pueda ser aprobado para su desarrollo.</p>

    <div class="card">
        <h6 class="card-header"><b><i class="fas fa-info-circle"></i> Título</b></h6>
        <div class="card-body mb-0">
            <p class="mb-0">{{ $proyecto->nombre }}</p>
        </div>
    </div>

    <div class="card mt-3">
        <h6 class="card-header"><b><i class="fas fa-comment-alt"></i> Descripción</b></h6>
        <div class="card-body mb-0">
            <p class="mb-0">{{ $proyecto->descripcion }}</p>
        </div>
    </div>

    <div class="card mt-3">
        <h6 class="card-header"><b><i class="fas fa-user-tie"></i> Docente</b></h6>
        <div class="card-body mb-0">
            <p class="mb-0">{{ $proyecto->docente->nombres }} {{ $proyecto->docente->apellidos }}</p>
        </div>
    </div>

    <hr>

    <div class="float-left">
        <a href="{{ route('comite.proyectos.show', ['proyecto' => $proyecto->id]) }}" class="btn btn-secondary">
            <i class="fas fa-arrow-left"></i> Cancelar
        </a>
    </div>

    <div class="float-right">
        <button class="btn btn-danger" onclick="document.getElementById('rechazar').submit()">
            <i class="fas fa-times"></i> Rechazar
        </button>
        <button class="btn btn-uts" onclick="document.getElementById('aprobar').submit()">
            <i class="fas fa-check"></i> Aprobar/Admitir
        </button>

    </div>

    <form style="display: none;" id="aprobar" action="{{ route('comite.proyectos.verificar.aprobado', ['proyecto' => $proyecto->id]) }}" method="post">
        @csrf
    </form>
    <form style="display: none;" id="rechazar" action="{{ route('comite.proyectos.verificar.rechazado', ['proyecto' => $proyecto->id]) }}" method="post">
        @csrf
    </form>
@endpanel
@endsection
