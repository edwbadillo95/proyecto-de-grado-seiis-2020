@extends('sbadmin.index')

@section('sbadmin-body')
@panel
    @slot('icon') lightbulb @endslot
    @slot('titulo') Proyecto de grado @endslot

    @include('partials.forms.saved')

    <table class="table table-bordered">
        <thead>
            <tr>
                <th colspan="4" class="text-center">
                    <i class="fas fa-info-circle"></i> <i>Información del proyecto de grado</i>
                </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th>Nombre</th>
                <td colspan="3">{{ $proyecto->nombre }}</td>
            </tr>
            <tr>
                <th>Descripción</th>
                <td colspan="3">{{ $proyecto->descripcion }}</td>
            </tr>
            <tr>
                <th>Estado</th>
                <td>{{ $proyecto->estadoProyecto->nombre }}</td>
                <th>Docente</th>
                <td>{{ $proyecto->docente->nombres }} {{ $proyecto->docente->apellidos }}</td>
            </tr>
            <tr>
                <th>Fecha de registro</th>
                <td>{{ $proyecto->created_at }}</td>
                <th>Fecha de modificación</th>
                <td>{{ $proyecto->updated_at }}</td>
            </tr>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="4">
                    <a href="{{ route('comite.proyectos.index') }}" class="btn btn-secondary">
                        <i class="fas fa-list"></i> Ver proyectos
                    </a>
                    @if (Auth::user()->puedeVerificarProyecto($proyecto))
                        <a href="{{ route('comite.proyectos.verificar', ['proyecto' => $proyecto->id]) }}" class="btn btn-info">
                            <i class="fas fa-check"></i> Verificar
                        </a>
                    @endif
                    @if ($proyecto->revisado)
                    <a href="{{ route('proyectos.integrantes.index', ['proyecto'=> $proyecto->id]) }}" class="btn btn-secondary">
                        <i class="fas fa-users"></i> Ver integrantes del proyecto
                    </a>
                    @endif
                    @if (Auth::user()->puedeAgregarSeguimientosAlProyecto($proyecto))
                    <a href="{{ route('proyectos.seguimientos.index', ['proyecto'=> $proyecto->id]) }}" class="btn btn-info">
                        <i class="fas fa-clipboard-list"></i> Ver seguimientos
                    </a>
                    @endif
                </td>
            </tr>
        </tfoot>
    </table>
@endpanel
@endsection
