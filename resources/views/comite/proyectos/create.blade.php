@extends('sbadmin.index')

@section('sbadmin-body')
@panel
    @slot('icon') lightbulb @endslot
    @slot('titulo') Registrar proyecto @endslot

    <form action="{{ route('comite.proyectos.store') }}" method="post">
        @csrf

        @include('comite.proyectos.form')
    </form>
@endpanel
@endsection
