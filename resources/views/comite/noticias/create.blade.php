@extends('sbadmin.index')

@section('sbadmin-body')
@panel
    @slot('icon') comment-alt @endslot
    @slot('titulo') Crear Noticia o Aviso @endslot

    <form action="{{ route('comite.noticias.store') }}" method="post" enctype="multipart/form-data">
        @csrf

        @include('comite.noticias.form')
    </form>
@endpanel
@endsection


