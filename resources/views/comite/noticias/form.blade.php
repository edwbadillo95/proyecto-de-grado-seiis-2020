<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">


<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.css" rel="stylesheet">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>

@push('css')
    <link rel="stylesheet" href="{{ asset('vendor/bootstrap-fileinput-be14321/css/fileinput.min.css') }}">
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.css" rel="stylesheet">
    <style>
        .oculto {
            display: none;
        }

    </style>
@endpush


<p class="my-3">Digite el siguiente formulario, los campos con * son obligatorios.</p>

<div class="row">
    <div class="col-sm-12 col-md-4">
        <label for="status">Tipo anuncio*</label>
        <select id="status" class="form-control" name="status" required>
            <option value="">Seleccione una Opcion </option>
            <option value="noticias">Noticias</option>
            <option value="avisos">Avisos</option>
        </select>
    </div>
    <div class="col-sm-12 col-md-4">
        <div class="form-group">
            <label for="descripcion">fecha_creacion*</label><br>
            <input id="fecha_creacion" name="fecha_creacion"
                value="{{ old('fecha_creacion', $noticia->fecha_creacion) }}" type="date"
                class="form-control @error('fecha_creacion') is-invalid @enderror">

            @error('fecha_creacion')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>
    </div>
    <div class="col-sm-12 col-md-4">

        <div class="form-group fexpiracion oculto">
            <label for="descripcion">Fecha de expiración</label><br>
            <input id="fecha_expiracion" name="fecha_expiracion"
                value="{{ old('fecha_expiracion', $noticia->fecha_expiracion) }}" type="date"
                min="{{ old('fecha_creacion', $noticia->fecha_creacion) }}"
                class="form-control @error('fecha_expiracion') is-invalid @enderror"><br>
                <label for="descripcion">(las imagenes deben ser de tipo panoramica) </label>

            @error('fecha_expiracionn')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>
    </div>
</div>


<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="form-group">
            <label for="Titulo">Titulo *</label>
            <input id="titulo" name="titulo" type="text" class="form-control @error('titulo') is-invalid @enderror"
                value="{{ old('titulo', $noticia->titulo) }}">
        </div>
    </div>
    <div class="col-sm-12 col-md-12">
        <div class="form-group">
            <label for="autor">Autor*</label>
            <input id="autor" name="autor" type="text" class="form-control @error('autor') is-invalid @enderror"
                value="{{ old('autor', $noticia->autor) }}">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="form-group">
            <label for="contenido">Contenido *</label>
            <textarea required name="contenido" id="contenido" rows="8"
                class="form-control @error('contenido') is-invalid @enderror"  >{{ $noticia->contenido }}</textarea>

        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12 col-md-8">
        <div class="form-group">
            <label for="imagen">Selecciona una imagen:</label>
            <input name="imagen" id="imagen" class="form-control @error('imagen') is-invalid @enderror" required
                value="{{ old('imagen', $noticia->imagen) }}" type="file"
                data-initial-preview="{{ isset($noticia->imagen) ? 'https://placehold.it/200x150/9bbb10/FFFFFF&text=IMAGEN' : 'http://placehold.it/200x150/EFEFEF/AAAAAA&text=SUBIR+IMAGEN' }}"
                accept="application/pdf,image/*,.doc,.docx">
        </div>
    </div>
</div>


@if ($noticia->id)
    <div class="float-left">
        @btndelete @endbtndelete
    </div>
@endif

<div class="float-right">
    <a href="{{ route('comite.noticias.create') }}" class="btn btn-secondary">
        <i class="fas fa-times"></i> Cancelar
    </a>
    @btnreset @endbtnreset
    @btnsubmit @endbtnsubmit
</div>

@include('partials.forms.error')


<!-- summernote css/js -->

@push('js')
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.js"></script>
    <script type="text/javascript">
        $('#contenido').summernote({
            height: 400
        });

    </script>
    <script type="text/javascript">
        fecha_expiracion.min = new Date().toISOString().split("T")[0];

    </script>

    <script type="text/javascript">
        fecha_creacion.min = new Date().toISOString().split("T")[0];
        fecha_creacion.max = new Date().toISOString().split("T")[0];

    </script>

    <script type="text/javascript" src="js/jquery.js"></script>




    <script src="{{ asset('vendor/bootstrap-fileinput-be14321/js/fileinput.min.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap-fileinput-be14321/js/locales/es.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap-fileinput-be14321/themes/fas/theme.min.js') }}"></script>
    <script src="{{ asset('vendor/docente/js/archivo.js') }}"></script>
    <script>
        $('#status').on('change', function() {
            var selectValor = $(this).val();
            if (selectValor == 'avisos') {
                $('.fexpiracion').removeClass('oculto');
            } else {
                $('.fexpiracion').addClass('oculto');
            }
        });

    </script>
@endpush
