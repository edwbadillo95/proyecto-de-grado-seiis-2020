@extends('sbadmin.index')

@section('sbadmin-body')
@panel
    @slot('icon') check @endslot
    @slot('titulo') Noticia creada @endslot

    @include('partials.forms.saved')

    <table class="table table-bordered">
        <thead>
            <tr>
                <th colspan="4" class="text-center">
                    <i class="fas fa-info-circle"></i> <i>Noticias</i>
                </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th>Titulo</th>
                <td colspan="3">{{ $noticia->titulo }}</td>
            </tr>

            <tr>
                <th>Contenido</th>
                <td colspan="3" >{!! $noticia->contenido !!}</td>
            </tr>
            <tr>
                <th colspan="1">Imagen</th>
                 <td align="center" colspan="3"><img  width="250" height="250"src="{{ Storage::url($noticia->imagen) }}"></td>
            </tr>
            <tr>
                <th>Publicado por:<br>{{ $noticia->autor }}</th>
                <th>Fecha de Edicion : <br>
                 <p>{{ $noticia->fecha_creacion }}</p>
                </th>
                <th>Fecha de limite : <br>
                    <p>{{ $noticia->fecha_expiracion }}</p>
                   </th>

            </tr>

        </tbody>
        <tfoot>
            <tr>
                <td colspan="4">
                    <a href="{{ route('comite.noticias.index') }}" class="btn btn-secondary">
                        <i class="fas fa-list"></i> Ver Todas la Noticias
                    </a>
                    <a href="{{ route('comite.noticias.edit', ['noticia'=> $noticia->id]) }}" class="btn btn-info">
                        <i class="fas fa-edit"></i> Editar
                    </a>
                </td>
            </tr>
        </tfoot>
    </table>
@endpanel
@endsection
