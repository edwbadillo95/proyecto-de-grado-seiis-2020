@extends('sbadmin.index')

@section('sbadmin-body')
@panel
    @slot('icon') comment-alt @endslot
    @slot('titulo') Noticias  @endslot

    <p>Se lista a continuación todas las noticias.</p>

    @include('partials.forms.deleted')

    @if (count($noticia ?? ''))
        @include('comite.noticias.table')
    @else
        <div class="alert alert-info">
            No hay Noticias Creadas. Haz clic <a href="{{ route('comite.noticias.create') }}" class="text-primary">aquí</a> para registrar uno nuevo.
        </div>
    @endif

@endpanel

@panel
    @slot('icon') comment-alt @endslot
    @slot('titulo')Avisos @endslot

    <p>Se lista a continuación tadas las noticias.</p>

    @include('partials.forms.deleted')

    @if (count($avisos ?? ''))
    @include('comite.noticias.table2')
@else
    <div class="alert alert-info">
        No hay Noticias Creadas. Haz clic <a href="{{ route('comite.noticias.create') }}" class="text-primary">aquí</a> para registrar uno nuevo.
    </div>
@endif


@endpanel
@endsection
