@extends('sbadmin.index')

@section('sbadmin-body')
@panel
    @slot('icon') user-friends @endslot
    @slot('titulo') Editar Noticia @endslot

    <form action="{{ route('comite.noticias.update', ['noticia' => $noticia->id]) }}" method="post" enctype="multipart/form-data">
        @csrf
        @method('PUT')

        @include('comite.noticias.form')
    </form>
@endpanel

@include('partials.delete', ['route' => route('comite.noticias.destroy', ['noticia' => $noticia->id])])
@endsection
