

<table class="table table-bordered table-sm table-striped table-hover">

    <thead>
        <tr>
            <th>Titulo</th>
            <th>Autor</th>
            <th>publicado</th>
            <th>Opciones</th>
        </tr>
    </thead>
    <tbody>

        @foreach ($noticia as $g)

                <tr>
                <td>{{ $g->titulo }}</td>
                <td>{{ $g->autor }}</td>
                <td>{{ $g->fecha_creacion  }}</td>
                <td>
                <a href="{{ route('comite.noticias.show', ['noticia' => $g->id]) }}" class="btn btn-sm btn-primary" title="Mostrar Completa">
                <i class="fas fa-eye"></i></a>
                <a href="{{ route('comite.noticias.edit', ['noticia' => $g->id]) }}" class="btn btn-sm btn-info" title="Editar registro">
                 <i class="fas fa-edit"></i>
                </a>

            </td>
            </tr>
        @endforeach

    </tbody>

</table>



