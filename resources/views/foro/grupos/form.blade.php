<p>Digite el siguiente formulario, los campos con * son obligatorios.</p>

@include('partials.forms.error')

<div class="row">
    <div class="col-sm-12 col-md-8">
        <div class="form-group">
            <label for="nombre">Nombre del grupo *</label>
            <input id="nombre" name="nombre" value="{{ old('nombre', $grupo->nombre) }}" type="text"
                class="form-control @error('nombre') is-invalid @enderror">

            @error('nombre')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12 col-md-8">
        <div class="form-group">
            <label for="codigogrupo">Codigo del grupo
                <b>{{ empty($grupo->codigo_grupo) ? 'Autogenerado' : $grupo->codigo_grupo }}</b></label>
        </div>
    </div>
</div>

<hr>

@if ($grupo->id)
    <div class="float-left">
        @btndelete @endbtndelete
    </div>
@endif

<div class="float-right">
    <a href="{{ route('foro.grupos.index') }}" class="btn btn-secondary">
        <i class="fas fa-times"></i> Cancelar
    </a>
    @btnreset @endbtnreset
    @btnsubmit @endbtnsubmit
</div>
