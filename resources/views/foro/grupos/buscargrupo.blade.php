@extends('sbadmin.index')

@section('sbadmin-body')
    @panel
    @slot('icon') user-friends @endslot
    @slot('titulo') Buscar Grupo @endslot

    <p>Ingresa el codigo del grupo al cual vincularte.</p>

    <form action="{{ route('foro.buscargrupo') }}" method="post">
        @csrf
        <div class="row">
            <div class="col-sm-12 col-md-8">
                <div class="form-group">
                    <label for="idgrupo">Codigo del grupo</label>
                    <input id="idgrupo" name="idgrupo" value="" type="text" class="form-control">

                    @error('idgrupo')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
            </div>
        </div>
        <div class="">
            @btnsubmit
            @slot('text') Unirme @endslot 
            @slot('icon') users @endslot 
            @endbtnsubmit
        </div>
    </form>
    @if (isset($idGrupo))
        <br />
        <div class="alert alert-danger">
            No se ha encontrado el grupo con codigo {{ $idGrupo }}.
        </div>
    @endif

    @endpanel
@endsection
