@extends('sbadmin.index')

@section('sbadmin-body')
@panel
@slot('icon') user-friends @endslot
@slot('titulo') Grupo de Foro @endslot

@include('partials.forms.saved')

<table class="table table-bordered">
    @csrf
    <thead>
        <tr>
            <th colspan="4" class="text-center">
                <i class="fas fa-info-circle"></i> <i>Información del grupo de foro</i>
            </th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <th>Nombre</th>
            <td >{{ $grupo->nombre }}</td>
            <th>Codigo</th>
            <td>{{ $grupo->codigo_grupo }}</td>
        </tr>
    </tbody>
    <tfoot>
        <tr>
            <td colspan="4">
                <a href="{{ route('foro.grupos.posts.index', ['grupo' => $grupo->id]) }}" class="btn btn-secondary">
                    <i class="fas fa-list"></i> Ver Publicaciones
                </a>
                <a href="{{ route('foro.grupos.index') }}" class="btn btn-secondary">
                    <i class="fas fa-list"></i> Ver grupos de foro
                </a>
                <a href="{{ route('foro.grupos.edit', ['grupo'=> $grupo->id]) }}" class="btn btn-info">
                    <i class="fas fa-edit"></i> Editar
                </a>
            </td>
        </tr>
    </tfoot>
</table>

@if ($grupo->usuarios_grupo()->count())
@include('foro.grupos.usuariosgrupo')
@else
<div class="alert alert-info">
    No hay usuarios vinculados al grupo. Haz clic <a href="{{ route('foro.grupos.usuariosgrupo.create', ['grupo' => $grupo->id]) }}" class="text-primary">aquí</a> para registrar uno nuevo.
</div>
@endif

@endpanel
@endsection