<table class="table table-bordered table-sm table-striped table-hover">
    <thead>
        <tr>
            <th>Nombre</th>
            <th>Codigo Grupo</th>
            <th>Opciones</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($grupos as $g)
        <tr>
            <td>{{ $g->nombre }}</td>
            <td>{{ $g->codigo_grupo }}</td>
            <td>
                <a href="{{ route('foro.grupos.show', ['grupo' => $g->id]) }}" class="btn btn-sm btn-primary" title="Mostrar detalles">
                    <i class="fas fa-eye"></i>
                </a>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>