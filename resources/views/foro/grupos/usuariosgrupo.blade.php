<table class="table table-bordered table-sm table-striped table-hover">
    <thead>
        <tr>
            <th colspan="{{ true ? '3' : '2' }}" class="text-center">
                <i class="fas fa-info-circle"></i> <i>Usuarios del grupo</i>
            </th>
        </tr>
        <tr>
            <th>Nombre</th>
            <th>Email</th>
            @if(true)
            <th>Opciones</th>
            @endif
        </tr>
    </thead>
    <tbody>
        @foreach ($grupo->usuarios_grupo as $i)
        <tr id="{{ $grupo->id }}_{{ $i->usuario->id }}">
            <td>{{ $i->usuario->name }}</td>
            <td>{{ $i->usuario->email }}</td>
            @if(true)
            <td>
                <a onclick="removeUsuarioGrupo('{{ $grupo->id }}','{{ $i->usuario->id }}')" class="btn btn-sm btn-danger" title="Eliminar">
                    <i class="fas fa-trash"></i>
                </a>
                <a href="{{ route('foro.grupos.usuariosgrupo.edit', ['grupo' => $grupo->id, 'usuariosgrupo' => $i->usuario->id ]) }}" class="btn btn-sm btn-info" title="Editar">
                    <i class="fas fa-edit"></i>
                </a>
            </td>
            @endif
        </tr>
        @endforeach
    </tbody>
</table>
<a href="{{ route('foro.grupos.usuariosgrupo.create', ['grupo' => $grupo->id]) }}" class="btn btn-primary">
    <i class="fas fa-plus"></i> Vincular Usuarios
</a>

@push('css')
<link rel="stylesheet" href="{{ asset('vendor/jAlert/css/jAlert.css') }}">
@endpush

@push('js')
<script type="text/javascript">
    var deletePostUri = "{{ route('foro.grupos.usuariosgrupo.destroy', ['grupo'=>':idGrupo', 'usuariosgrupo' => ':idUsuario']) }}";
</script>
<script src="{{ asset('vendor/jAlert/js/jAlert.js') }}"></script>
<script src="{{ asset('vendor/jAlert/js/jAlert-functions.js') }}"></script>
<script src="{{ asset('vendor/foro/js/usuariosgrupo.js') }}"></script>
@endpush