@extends('sbadmin.index')

@section('sbadmin-body')
@panel
    @slot('icon') user-friends @endslot
    @slot('titulo') Registrar grupo de foro @endslot

    <form action="{{ route('foro.grupos.store') }}" method="post">
        @csrf

        @include('foro.grupos.form')
    </form>
@endpanel
@endsection
