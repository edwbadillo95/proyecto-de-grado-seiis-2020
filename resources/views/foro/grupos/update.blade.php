@extends('sbadmin.index')

@section('sbadmin-body')
@panel
    @slot('icon') user-friends @endslot
    @slot('titulo') Editar grupo de foro @endslot

    <form action="{{ route('foro.grupos.update', ['grupo' => $grupo->id]) }}" method="post">
        @csrf
        @method('PUT')

        @include('foro.grupos.form')
    </form>
@endpanel

@include('partials.delete', ['route' => route('foro.grupos.destroy', ['grupo' => $grupo->id])])
@endsection
