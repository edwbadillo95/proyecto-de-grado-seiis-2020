@extends('sbadmin.index')

@section('sbadmin-body')
@panel
    @slot('icon') user-friends @endslot
    @slot('titulo') Mis Grupos @endslot

    <p>Se lista a continuación los usuarios vinculados al grupo {{ $grupo->nombre }}.</p>

    @include('partials.forms.deleted')

    @if (count($grupos))
        @include('foro.grupos.table')
    @else
        <div class="alert alert-info">
            No hay grupos a los que estes vinculado. Haz clic <a href="{{ route('foro.grupos.create') }}" class="text-primary">aquí</a> para registrar uno nuevo.
        </div>
    @endif

@endpanel
@endsection