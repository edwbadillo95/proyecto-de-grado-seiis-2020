@if (request()->email && !$usuario)
    <div class="alert alert-danger">
        No se encontró un usuario con el email <b>{{ request()->email }}</b> en el sistema.
    </div>
@elseif(!$isUpdate && !empty($usuarioGrupo->id))
    <div class="alert alert-danger">
        El usuario con email <b>{{ $usuario->email }}</b> ya se encuentra vinculado al grupo <b>{{ $grupo->nombre }}</b>.
    </div>
@endif

@if ($isUpdate || (!$isUpdate && empty($usuarioGrupo->id) && $usuario->id))
<form action="{{ $route }}" method="post">
    @csrf

    @if ($isUpdate)
        @method('PUT')
    @endif
    <input type="hidden" name="usuario" value="{{ $usuario->id }}">


    <div class="card border-secondary">
        <div class="card-header">
            <i class="fas fa-user-tie"></i> Usuario a vincular
        </div>

        @if ($errors->has('usuario'))
            <div class="card-body">
                <div class="alert alert-danger mb-0">
                    El docente especificado parecer ser incorrecto. Vuelva a especificar un número de documento de un docente en la parte superior de este formulario.
                </div>
            </div>
        @else
            <div class="card-body">
                <table class="table table-bordered table-sm">
                    <tbody>
                        <tr>
                            <th>Nombre</th>
                            <td>{{ $usuario->name }}</td>
                            <th>Email</th>
                            <td>{{ $usuario->email }}</td>
                        </tr>
                    </tbody>
                </table>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="custom-control custom-checkbox">
                            <input name="es_admin" type="checkbox" class="custom-control-input" id="es_admin" {{ $usuarioGrupo->administrador ? 'checked' : '' }}>
                            <label class="custom-control-label" for="es_admin">Es Administrador del grupo?</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer text-center">
                <button class="btn btn-primary" type="submit">
                    <i class="fas fa-user-plus"></i> {{ (!$isUpdate) ? 'Vincular' : 'Actualizar'}}
                </button>
                <a class="btn btn-secondary" href="{{ route('foro.grupos.usuariosgrupo.create', ['grupo' => $grupo->id]) }}">
                    <i class="fas fa-redo"></i> Cancelar
                </a>
            </div>
        @endif
    </div>
</form>
@endif
