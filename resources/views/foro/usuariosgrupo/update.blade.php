@extends('sbadmin.index')

@section('sbadmin-body')
    @panel
        @slot('icon') user-plus @endslot
        @slot('titulo') Registrar integrante @endslot

        @include('foro.usuariosgrupo.formusuario', ['route' => route('foro.grupos.usuariosgrupo.update', ['grupo'=>$grupo->id, 'usuariosgrupo' => $usuario->id]), 'isUpdate' => true])

        @endpanel
@endsection