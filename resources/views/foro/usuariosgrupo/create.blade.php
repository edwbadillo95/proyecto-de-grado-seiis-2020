@extends('sbadmin.index')

@section('sbadmin-body')
    @panel
        @slot('icon') user-plus @endslot
        @slot('titulo') Registrar integrante @endslot

        <p>
            Digite el siguiente formulario para el registro de un integrante de comité para el grupo <b>{{ $grupo->nombre }}</b>.
        </p>

        
        <form class="mb-3" action="{{ route('foro.grupos.usuariosgrupo.create', ['grupo'=>$grupo->id]) }}" method="get">
            <div class="card border-primary">
                <div class="card-header text-center">
                    <b>Ingrese el email del usuario a vincular.</b>
                </div>
                <div class="row p-3">
                    <div class="col-xs-12 col-sm-8 col-md-8 offset-sm-2 offset-md-2">
                        <div class="input-group">
                            <input
                                value="{{ request()->email }}"
                                name="email"
                                type="text"
                                class="form-control"
                                placeholder="Email del usuario"
                                aria-label="Email del usuario"
                                aria-describedby="btn-doc">

                            <div class="input-group-append">
                                <button class="btn btn-primary" type="submit" id="btn-doc">
                                    <i class="fas fa-search"></i> Buscar
                                </button>
                                <a href="{{ route('foro.grupos.show', ['grupo' => $grupo->id]) }}" class="btn btn-sm btn-danger" title="Mostrar detalles">
                                    <i class="fas fa-redo"></i> Volver
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        

        @include('foro.usuariosgrupo.formusuario', ['route' => route('foro.grupos.usuariosgrupo.store', ['grupo'=>$grupo->id]), 'isUpdate' => false])

        
    @endpanel
@endsection