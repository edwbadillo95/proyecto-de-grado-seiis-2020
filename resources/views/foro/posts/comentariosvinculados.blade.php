<div class="col-md-12 col-sm-12 pl-4 pt-0">
    @foreach ($comentariosVinculados as $comentario)
    <div class="row">
        <div class="col-md-12 col-sm-12 pt-2">
            <i class="fas fa-trash iconoAdjuntos" title="Eliminar Comentario" onclick="eliminarComentario('{{ $comentario->id }}')"></i>&nbsp;<b>{{ $comentario->usuario->name }}:</b> {{ $comentario->comentario }}<br />
            @if($comentario->archivos()->count())
            @foreach ($comentario->archivos as $archivo)
            <a href="{{ route('foro.descargararchivocomentario', ['idarchivocomentario'=> $archivo->id]) }}" download="" class="font11">
                <i class="fas fa-file"></i> {{ $archivo->nombre }}
            </a> &nbsp;
            @endforeach
            @endif
        </div>
        <div class="col-md-12 col-sm-12 pt-0" style="font-size: 10px;" onclick="habilitarComentario(this, '{{ $comentario->id }}');">
            <span style="text-decoration: underline; cursor:pointer;">Responder</span>
        </div>
        @if($comentario->comentarios()->count())
        @include('foro.posts.comentariosvinculados', ['comentariosVinculados' => $comentario->comentarios])
        @endif
    </div>
    @endforeach
</div>