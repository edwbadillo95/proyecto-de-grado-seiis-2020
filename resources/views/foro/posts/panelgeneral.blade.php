@extends('sbadmin.index')

@section('sbadmin-body')
@include('foro.posts.form', ['route' => route('foro.posts.store'), 'isGrupo' => false])
@panel

    @slot('icon') poll-h @endslot
    @slot('titulo') Publicaciones @endslot


    <!-- <p>Publicaciones.</p> -->

    @include('partials.forms.deleted')

    @include('foro.posts.table')

@endpanel
@endsection
