@extends('sbadmin.index')

@section('sbadmin-body')
@include('foro.posts.form', ['route' => route('foro.posts.store', ['grupo' => $grupo->id]), 'isGrupo' => true])
@panel
    @slot('icon') poll-h @endslot
    @slot('titulo') Publicaciones Grupo <b>{{ $grupo->nombre }}</b> @endslot


    <!-- <p>Publicaciones.</p> -->

    @include('partials.forms.deleted')

    @include('foro.posts.table')

@endpanel
@endsection
