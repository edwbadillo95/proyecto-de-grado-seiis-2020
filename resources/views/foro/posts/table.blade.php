@foreach ($posts as $p)
<div class="card card-post">
    <div class="card-header">
        <div class="row">
            <div class="col-sm-6 col-md-6">
                <h6 class="card-title"><b>{{ $p->usuario->name }}</b> Publico {{ $p->nombre }}</h6>
            </div>
            <div class="col-sm-6 col-md-6">
                <a href="{{ route('foro.posts.show', ['post' => $p->id]) }}" class="button btn btn-primary font13 float-right">Ver Publicaci&oacute;n</a>
            </div>
        </div>
    </div>
    <div class="card-body">
        <p class="card-text font13">{{ (strlen($p->contenidopost) > 150) ? substr($p->contenidopost, 0, 150).'...' : $p->contenidopost }}</p>
    </div>
</div>
@endforeach
<div class="col-sm-12 col-md-12 text-center p-2">
    {{ $posts->links() }}
</div>

@if(!count($posts))
<div class="alert alert-info">
    No hay publicaciones en este grupo.
</div>
@endif

@push('css')
<link rel="stylesheet" href="{{ asset('vendor/foro/css/posts.css') }}">
@endpush

@push('js')
<script src="{{ asset('vendor/foro/js/posts.js') }}"></script>
@endpush