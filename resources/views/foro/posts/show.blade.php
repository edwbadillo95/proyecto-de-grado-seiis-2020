@extends('sbadmin.index')

@section('sbadmin-body')
@panel
@slot('icon') poll-h @endslot
@slot('titulo') Publicaci&oacute;n @endslot

@include('partials.forms.saved')

<table class="table table-bordered">
    @csrf
    <thead>
        <tr>
            <td>
                <div class="row">
                    <div class="col-sm-6 col-md-6">
                        <b>{{ $post->nombre }}</b>
                    </div>
                    <div class="col-sm-6 col-md-6">
                        <!-- AJUSTARUSUARIO -->
                        @if(true)
                            <button class="btn btn-danger font12 float-right" type="button" data-toggle="modal" data-target="#modal-delete">
                                <i class="fas fa-trash"></i> Eliminar
                            </button>
                        <!-- <a href="{{ route('foro.posts.destroy', ['post'=> $post->id]) }}" class="btn btn-danger font12 float-right">
                            <i class="fas fa-trash"></i> Eliminar
                        </a> -->
                        @endif
                    </div>
                </div>
            </td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td colspan="4">
                {{ $post->contenidopost }}
                <br />
                @if($post->archivos)
                @foreach ($post->archivos as $archivo)
                <a href="{{ route('foro.descargararchivopost', ['idarchivopost'=> $archivo->id]) }}" download="" class="font11">
                    <i class="fas fa-file"></i> {{ $archivo->nombre }}
                </a> &nbsp;
                @endforeach
                @endif
            </td>
        </tr>
        @if ($post->etiquetas()->count())
        <tr>
            <td colspan="4">
                {{ implode(',', $post->etiquetas)}}
            </td>
        </tr>
        @endif
        <tr>
            <td colspan="4">
                <div class="row">
                    <div class="col-sm-7 col-md-7">
                        {{ $usuario->name }}
                    </div>
                    <div class="col-sm-5 col-md-5 text-right">
                        {{ $post->fechapublicacion }}
                    </div>
                </div>
            </td>
        </tr>
    </tbody>
    <tfoot>
        <tr>
            <td colspan="4">
                <a href="{{ route('foro.posts') }}" class="btn btn-secondary font12">
                    <i class="fas fa-list"></i> Todas las publicaciones
                </a>
            </td>
        </tr>
    </tfoot>
</table>
@include('foro.posts.tablacomentarios', ['post' => $post])
@include('partials.delete', ['route' => route('foro.posts.destroy', ['post'=> $post->id])])
@include('foro.posts.modaleliminar')


@endpanel
@endsection

@push('css')
<link rel="stylesheet" href="{{ asset('vendor/foro/css/posts.css') }}">
@endpush

@push('js')
<script type="text/javascript">
    function obtenerUrlComentario() {
        return "{{ route('foro.posts.comentario.vincularComentario', ['comentario'=> ':idComentario']) }}";
    }

    function obtenerCsrf() {
        return '@csrf';
    }
</script>
<script src="{{ asset('vendor/foro/js/estructuraComentarios.js') }}"></script>
@endpush