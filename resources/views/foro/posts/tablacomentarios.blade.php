<table class="table table-bordered">
    <thead>
        <tr>
            <th class="font13">Comentarios</th>
        </tr>
        <tr>
            <td>
                <form action="{{ route('foro.posts.vincularComentario', ['post'=> $post->id]) }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <textarea class="form-control font11" name="comentariopost" id="comentariopost" rows="1"></textarea>
                    <div class="col-md-12 col-sm-12 p-0">
                        <div class="row">
                            <div class="col-md-6 col-sm-6">
                                <i class="fas fa-file-alt iconoAdjuntos" onclick="$('#adjuntosComentario').trigger('click');"></i>
                                <span id="nombreAdjuntos" class="font11"> Archivos</span>
                                <input type="file" style="display: none;" name="adjuntosComentario[]" id="adjuntosComentario" multiple>
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <button class="btn btn-uts font12 float-right" type="submit">Comentar</button>
                            </div>
                        </div>
                    </div>
                </form>
            </td>
        </tr>
    </thead>
    <tbody>
        @if($post->comentarios()->count())
        @foreach ($post->comentarios as $comentario)
        <tr>
            <td>
                <div class="col-md-12 col-sm-12 p-0">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <i class="fas fa-trash iconoAdjuntos" title="Eliminar Comentario" onclick="eliminarComentario('{{ $comentario->id }}')"></i>&nbsp;<b>{{ $comentario->usuario->name }}:</b> {{ $comentario->comentario }}<br />
                            @if($comentario->archivos()->count())
                            @foreach ($comentario->archivos as $archivo)
                            <a href="{{ route('foro.descargararchivocomentario', ['idarchivocomentario'=> $archivo->id]) }}" download="" class="font11">
                                <i class="fas fa-file"></i> {{ $archivo->nombre }}
                            </a> &nbsp;
                            @endforeach
                            @endif
                        </div>
                        <div class="col-md-12 col-sm-12" style="font-size: 10px;">
                            <span style="text-decoration: underline; cursor:pointer;" onclick="habilitarComentario(this, '{{ $comentario->id }}');">Responder</span>
                        </div>
                    </div>
                    @if($comentario->comentarios()->count())
                    @include('foro.posts.comentariosvinculados', ['comentariosVinculados' => $comentario->comentarios])
                    @endif
                </div>
            </td>
        </tr>
        @endforeach
        @endif
    </tbody>
</table>