<div class="card border-secondary">
    <div class="card-header" data-toggle="collapse" href="#collapsePost" role="button" aria-expanded="false" aria-controls="collapsePost">
        <i class="fas fa-pen"></i> Crea tu Publicaci&oacute;n
    </div>
    <div class="collapse" id="collapsePost">
        <div class="card-body">
            <div class="col-sm-12 col-md-12 p-0">
                <!-- <div class="row"> -->
                <form method="post" action="{{ $route }}" enctype="multipart/form-data">
                    @csrf
                    @if($isGrupo)
                    <input type="hidden" name="grupo" id="idgrupo" value="{{ $grupo->id }}">
                    @else
                    <select class="custom-select font13" name="grupo">
                        <option value="">Selecciona Grupo</option>
                        @foreach ($grupos as $g)
                        <option value="{{ $g->id }}">{{ $g->nombre }}</option>
                        @endforeach
                    </select>
                    @endif
                    <div class="col-md-12 col-sm-12 p-0">
                        <input class="form-control font13" type="text" name="nombre" id="nombre" placeholder="Que Titulo deseas poner?">
                        <textarea class="form-control font13" name="contenidopost" id="contenidopost" cols="30" rows="3" placeholder="Escribe aqui tu publicacion"></textarea>
                    </div>
                    <div class="col-md-12 col-sm-12 p-0 font12">
                        <div class="row">
                            <div class="col-md-6 col-sm-6">
                                <i class="fas fa-file-alt" id="fireFiles"></i><span id="nombreAdjuntos" class=""> No se han cargado archivos</span>
                                <input type="file" name="adjuntos[]" id="adjuntos" multiple>
                            </div>
                            <div class="col-md-12 col-sm-12">
                                <button class="btn btn-primary float-right font13" type="submit">
                                    <i class="fas fa-user-plus"></i> Publicar
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
                <!-- </div> -->
            </div>
        </div>
    </div>
</div>
<br/>