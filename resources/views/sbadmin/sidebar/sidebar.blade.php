<ul class="navbar-nav sidebar sidebar-dark sidebar-uts accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="/">
        <div class="sidebar-brand-icon">
            <i class="fas fa-graduation-cap"></i>
        </div>
        <div class="sidebar-brand-text mx-3">UTS <sub><small>v0.0.1</small></sub></div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item">
        <a class="nav-link" href="#">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span></a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
        Comité
    </div>

    @if (Auth::user()->tieneRolCoordinador())
        <!-- Comite / Grupos -->
        <li class="nav-item {{ request()->is('comite/grupos*') ? 'active' : '' }}">
            <a class="nav-link {{ request()->is('comite/grupos') ? 'collapsed' : '' }}" href="#"
                data-toggle="collapse" data-target="#collapseGrupoComite"
                aria-expanded="{{ request()->is('comite/grupos') ? 'true' : 'false' }}" aria-controls="collapseTwo">
                <i class="fas fa-fw fa-user-friends"></i>
                <span>Grupos</span>
            </a>
            <div id="collapseGrupoComite"
                class="collapse {{ request()->is('comite/grupos/*') || request()->is('comite/grupos') ? 'show' : '' }}"
                aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <a class="collapse-item {{ request()->is('comite/grupos') ? 'active' : '' }}"
                        href="{{ route('comite.grupos.index') }}">Ver Grupos</a>
                    <a class="collapse-item {{ request()->is('comite/grupos/create') ? 'active' : '' }}"
                        href="{{ route('comite.grupos.create') }}">Crear Grupo</a>
                </div>
            </div>
        </li>
    @endif

    @if (Auth::user()->tieneRolCoordinador() || Auth::user()->integranteComite())
        <!-- Comite / Sesiones -->
        <li class="nav-item {{ request()->is('comite/sesiones*') ? 'active' : '' }}">
            <a class="nav-link {{ request()->is('comite/sesiones') ? 'collapsed' : '' }}" href="#"
                data-toggle="collapse" data-target="#collapseSesionComite"
                aria-expanded="{{ request()->is('comite/sesiones') ? 'true' : 'false' }}"
                aria-controls="collapseTwo">
                <i class="fas fa-fw fa-calendar-alt"></i>
                <span>Sesiones</span>
            </a>
            <div id="collapseSesionComite"
                class="collapse {{ request()->is('comite/sesiones/*') || request()->is('comite/sesiones') ? 'show' : '' }}"
                aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <a class="collapse-item {{ request()->is('comite/sesiones') ? 'active' : '' }}"
                        href="{{ route('comite.sesiones.index') }}">Ver sesiones</a>
                    @if (Auth::user()->puedeRegistrarSesiones())
                        <a class="collapse-item {{ request()->is('comite/sesiones/create') ? 'active' : '' }}"
                            href="{{ route('comite.sesiones.create') }}">Crear sesión</a>
                    @endif
                </div>
            </div>
        </li>
    @endif

    <!-- Comite / Proyectos -->
    <li class="nav-item {{ request()->is('comite/proyectos*') ? 'active' : '' }}">
        <a class="nav-link {{ request()->is('comite/proyectos') ? 'collapsed' : '' }}" href="#"
            data-toggle="collapse" data-target="#collapseComiteProyectos"
            aria-expanded="{{ request()->is('comite/proyectos') ? 'true' : 'false' }}" aria-controls="collapseTwo">
            <i class="fas fa-fw fa-lightbulb"></i>
            <span>Proyectos</span>
        </a>
        <div id="collapseComiteProyectos"
            class="collapse {{ request()->is('comite/proyectos/*') || request()->is('comite/proyectos') ? 'show' : '' }}"
            aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item {{ request()->is('comite/proyectos') && !request()->miproyecto ? 'active' : '' }}"
                    href="{{ route('comite.proyectos.index') }}">Ver proyectos</a>

                @if (Auth::user()->estudiante && Auth::user()->estudiante->estaEnUnProyecto())
                <a class="collapse-item {{ request()->is('comite/proyectos') && request()->miproyecto ? 'active' : '' }}"
                    href="{{ route('comite.proyectos.index', ['miproyecto' => 'true']) }}">Mi proyecto</a>
                @endif

                @if (Auth::user()->puedeRegistrarProyectos())
                    <a class="collapse-item {{ request()->is('comite/proyectos/create') ? 'active' : '' }}"
                        href="{{ route('comite.proyectos.create') }}">Registrar proyecto</a>
                @endif

                @if (Auth::user()->integranteComite())
                <a class="collapse-item {{ request()->is('comite/proyectos/calificador') ? 'active' : '' }}"
                    href="{{ route('comite.proyectos.calificador') }}">Calificar proyectos</a>
                @endif

            </div>
        </div>
    </li>

    <!-- Nav Item - Noticias Collapse Menu -->
    <!-- Divider -->
    <!-- Estudiantes -->
    @if(Auth::user()->estudiante )
    <hr class="sidebar-divider">

    <div class="sidebar-heading">
        Estudiante
    </div>
    <li class="nav-item  {{ request()->is('estudiantes*') ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('estudiantes.show', ['estudiante' => Auth::user()->estudiante->id]) }}">
            <i class="fas fa-fw fa-calendar-alt"></i>
            <span>Datos Generales</span>
        </a>
    </li>
    <li class="nav-item  {{ request()->is('notasActuales*') ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('notasActuales', Auth::user()->estudiante->id) }}">
            <i class="fas fa-fw fa-table"></i>
            <span>Notas Actuales</span>
        </a>
    </li>
    <li class="nav-item  {{ request()->is('historialNotas*') ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('historialNotas', Auth::user()->estudiante->id) }}">
            <i class="fas fa-fw fa-table"></i>
            <span>Historial Ext Notas</span>
        </a>
    </li>
    <li class="nav-item {{ request()->is('proyectos_inscripcion') || request()->is('proyecto.documentacion*') ? 'active' : '' }}">
        <a class="nav-link {{ request()->is('proyectos_inscripcion') || request()->is('proyecto.documentacion*') ? 'collapsed' : '' }}" href="#" data-toggle="collapse" data-target="#collapseProyectoEstudiante" aria-expanded="{{ request()->is('proyectos_inscripcion') || request()->is('proyecto.documentacion*') ? 'true' : 'false' }}" aria-controls="collapseTwo">
            <i class="fas fa-fw fa-lightbulb"></i>
            <span>Proyecto de Grado</span>
        </a>
        <div id="collapseProyectoEstudiante" class="collapse {{  request()->is('proyectos_inscripcion') || request()->is('proyecto.documentacion*') ? 'show' : '' }}" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                @if(!Auth::user()->estaInscriptoProyecto())
                <a class="collapse-item {{ request()->is('proyectos_inscripcion') ? 'active' : '' }}" href="{{ route('proyectosInscripcion') }}">Inscripcion</a>
                @else
                <a class="collapse-item {{ request()->is('proyecto.documentacion*') ? 'active' : '' }}" href="{{ route('proyecto.documentacion',Auth::user()->proyectoEstudiante()->proyecto_grado_id) }}">Ver proyecto</a>
                @endif
            </div>

    </li>
    @elseif(Auth::user()->tieneRolCoordinador())
    <li class="nav-item {{ request()->is('estudiantes*') ? 'active' : '' }}">
        <a class="nav-link {{ request()->is('estudiantes') ? 'collapsed' : '' }}" href="#" data-toggle="collapse" data-target="#collapseEstudiante" aria-expanded="{{ request()->is('estudiantes') ? 'true' : 'false' }}" aria-controls="collapseTwo">
            <i class="fas fa-fw fa-user-friends"></i>
            <span>Estudiantes</span>
        </a>
        <div id="collapseEstudiante" class="collapse {{ request()->is('estudiantes*') || request()->is('estudiantes') || request()->is('lista-registro-ingreso-plataforma')  ? 'show' : '' }}" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item {{ request()->is('estudiantes') ? 'active' : '' }}" href="{{ route('estudiantes.index') }}">Lista Estudiantes</a>
                <a class="collapse-item {{ request()->is('estudiantes/create') ? 'active' : '' }}" href="{{ route('estudiantes.create') }}">Crear Estudiante</a>
                <a class="collapse-item {{ request()->is('lista-registro-ingreso-plataforma') ? 'active' : '' }}" href="{{ route('lista-registro-ingreso-plataforma') }}">Solicitudes de Ingresos</a>

                </div>
            </div>
        </li>

    @endif

    @if (Auth::user()->docente)
        <hr class="sidebar-divider">

        <!-- Heading -->
        <div class="sidebar-heading">
            Docente
        </div>

        <!-- Docente / Docentes -->
        <li class="nav-item {{ request()->is('docente') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('docente.index') }}">
                <i class="fas fa-fw fa-user-tie"></i>
                <span>Datos Generales</span>
            </a>
        </li>

        <!-- Docente / Horarios -->
        <li class="nav-item {{ request()->is('docente/horarios*') ? 'active' : '' }}">
            <a class="nav-link {{ request()->is('docente/horarios') ? 'collapsed' : '' }}" href="#"
                data-toggle="collapse" data-target="#collapseHorarios"
                aria-expanded="{{ request()->is('docente/horarios') ? 'true' : 'false' }}"
                aria-controls="collapseTwo">
                <i class="fas fa-fw fa-calendar-alt"></i>
                <span>Horario</span>
            </a>
            <div id="collapseHorarios"
                class="collapse {{ request()->is('docente/horarios/*') || request()->is('docente/horarios') ? 'show' : '' }}"
                aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <a class="collapse-item {{ request()->is('docente/horarios') ? 'active' : '' }}"
                        href="{{ route('docente.horarios.index') }}">Ver Horarios</a>
                    <a class="collapse-item {{ request()->is('docente/horarios/create') ? 'active' : '' }}"
                        href="{{ route('docente.horarios.create') }}">Crear Horarios</a>
                </div>
            </div>
        </li>

        <!-- Docente / Grupos -->
        <li class="nav-item {{ request()->is('docente/grupos*') ? 'active' : '' }}">
            <a class="nav-link {{ request()->is('docente/grupos') ? 'collapsed' : '' }}" href="#"
                data-toggle="collapse" data-target="#collapseGrupos"
                aria-expanded="{{ request()->is('docente/grupos') ? 'true' : 'false' }}" aria-controls="collapseTwo">
                <i class="fas fa-fw fa-chalkboard-teacher"></i>
                <span>Grupos</span>
            </a>
            <div id="collapseGrupos"
                class="collapse {{ request()->is('docente/grupos/*') || request()->is('docente/grupos') ? 'show' : '' }}"
                aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <a class="collapse-item {{ request()->is('docente/grupos') ? 'active' : '' }}"
                        href="{{ route('docente.grupos.index') }}">Ver Grupos</a>
                    <a class="collapse-item {{ request()->is('docente/grupos/create') ? 'active' : '' }}"
                        href="{{ route('docente.grupos.create') }}">Crear Grupos</a>
                </div>
            </div>
        </li>
    @elseif (Auth::user()->tieneRolCoordinador())
        <!-- Heading -->
        <div class="sidebar-heading">
            Docentes
        </div>
        <li class="nav-item {{ request()->is('docente*') ? 'active' : '' }}">
            <a class="nav-link {{ request()->is('docente') ? 'collapsed' : '' }}" href="#" data-toggle="collapse"
                data-target="#collapseDocente"
                aria-expanded="{{ request()->is('docente') ? 'true' : 'false' }}" aria-controls="collapseTwo">
                <i class="fas fa-fw fa-user-friends"></i>
                <span>Docentes</span>
            </a>
            <div id="collapseDocente"
                class="collapse {{ request()->is('docente*') || request()->is('docente') || request()->is('docente') ? 'show' : '' }}"
                aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <a class="collapse-item {{ request()->is('docente') ? 'active' : '' }}"
                        href="{{ route('docente.index') }}">Lista Docentes</a>
                    <a class="collapse-item {{ request()->is('docente/create') ? 'active' : '' }}"
                        href="{{ route('docente.create') }}">Crear Docente</a>
                </div>
            </div>
        </li>
    @endif

    <!-- Docente / Materias -->
    {{-- <li class="nav-item {{ request()->is('docente/materias*') ? 'active' : '' }}">
    <a class="nav-link {{ request()->is('docente/materias') ? 'collapsed' : '' }}" href="#" data-toggle="collapse" data-target="#collapseMateria" aria-expanded="{{ request()->is('docente/materias') ? 'true' : 'false' }}" aria-controls="collapseTwo">
        <i class="fas fa-fw fa-user-friends"></i>
        <span>Materias</span>
    </a>
    <div id="collapseMateria" class="collapse {{ request()->is('docente/materias/*') || request()->is('docente/materias')  ? 'show' : '' }}" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
            <a class="collapse-item {{ request()->is('docente/materias') ? 'active' : '' }}" href="{{ route('docente.materias.index') }}">Ver Materias</a>
            <a class="collapse-item {{ request()->is('docente/materias/create') ? 'active' : '' }}" href="{{ route('docente.materias.create') }}">Crear Materias</a>
        </div>
    </div>
    </li> --}}

    <!-- Docente / Salones -->
    {{-- <li class="nav-item {{ request()->is('docente/salones') ? 'active' : '' }}">
    <a class="nav-link {{ request()->is('docente/salones') ? 'collapsed' : '' }}" href="#" data-toggle="collapse" data-target="#collapseSalon" aria-expanded="{{ request()->is('docente/salones') ? 'true' : 'false' }}" aria-controls="collapseTwo">
        <i class="fas fa-fw fa-user-friends"></i>
        <span>Salones</span>
    </a>
    <div id="collapseSalon" class="collapse {{ request()->is('docente/salones/') || request()->is('docente/salones')  ? 'show' : '' }}" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
            <a class="collapse-item {{ request()->is('docente/salones') ? 'active' : '' }}" href="{{ route('docente.salones.index') }}">Ver Salones</a>
            <a class="collapse-item {{ request()->is('docente/salones/create') ? 'active' : '' }}" href="{{ route('docente.salones.create') }}">Crear Salon</a>
        </div>
    </div>
    </li> --}}

    <!-- Docente / Programas -->
    {{-- <li class="nav-item {{ request()->is('docente/programas*') ? 'active' : '' }}">
    <a class="nav-link {{ request()->is('docente/programas') ? 'collapsed' : '' }}" href="#" data-toggle="collapse" data-target="#collapseProgramaAcademico" aria-expanded="{{ request()->is('docente/programas') ? 'true' : 'false' }}" aria-controls="collapseTwo">
        <i class="fas fa-fw fa-user-friends"></i>
        <span>Programas académicos</span>
    </a>
    <div id="collapseProgramaAcademico" class="collapse {{ request()->is('docente/programas/*') || request()->is('docente/programas')  ? 'show' : '' }}" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
            <a class="collapse-item {{ request()->is('docente/programas') ? 'active' : '' }}" href="{{ route('docente.programas.index') }}">Ver Programas</a>
            <a class="collapse-item {{ request()->is('docente/programas/create') ? 'active' : '' }}" href="{{ route('docente.programas.create') }}">Crear programa</a>
        </div>
    </div>
    </li> --}}





    <!-- Divider -->
    <hr class="sidebar-divider">




    <!-- Aca inicia el foro-->
    <div class="sidebar-heading">
        Foro
    </div>

    <!-- Principal -->
    <li class="nav-item">
        <a class="nav-link" href="{{ route('foro.posts') }}">
            <i class="fas fa-poll-h mr-1"></i>
            <span>Publicaciones</span>
        </a>
    </li>

    <!-- Foro / Grupos -->
    <li class="nav-item {{ request()->is('foro/grupos*') ? 'active' : '' }}">
        <a class="nav-link {{ request()->is('foro/grupos') ? 'collapsed' : '' }}" href="#" data-toggle="collapse"
            data-target="#collapseGrupoForo" aria-expanded="{{ request()->is('foro/grupos') ? 'true' : 'false' }}"
            aria-controls="collapseTwo">
            <i class="fas fa-fw fa-user-friends"></i>
            <span>Mis Grupos</span>
        </a>
        <div id="collapseGrupoForo"
            class="collapse {{ request()->is('foro/grupos/*') || request()->is('foro/grupos') ? 'show' : '' }}"
            aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item {{ request()->is('foro/grupos') ? 'active' : '' }}"
                    href="{{ route('foro.grupos.index') }}">Ver Mis Grupos</a>
                <a class="collapse-item {{ request()->is('foro/grupos/create') ? 'active' : '' }}"
                    href="{{ route('foro.grupos.create') }}">Crear Grupo Foro</a>
                <a class="collapse-item {{ request()->is('foro/grupos/buscar') ? 'active' : '' }}"
                    href="{{ route('foro.buscargrupo') }}">Buscar Grupo</a>
                <!-- Divider -->
                <hr class="sidebar-divider">

                <!-- Heading -->
                <div class="sidebar-heading">
                    Estudiantes
                </div>


                <!-- Divider -->
                <hr class="sidebar-divider">


                <!-- Heading -->


                <!-- Nav Item - Utilities Collapse Menu -->
                @if (Auth::user()->tieneRolSecretaria())

    <li class="nav-item">
        <div class="sidebar-heading">
            Avisos y Noticias
        </div>
        <hr class="sidebar-divider">

        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities"
            aria-expanded="true" aria-controls="collapseUtilities">
            <i class="far fa-newspaper"></i>
            <span>Noticias</span>
        </a>
        <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Edicion de Noticias:</h6>
                <a class="collapse-item {{ request()->is('comite/noticias/index') ? 'active' : '' }}"
                    href="{{ route('comite.noticias.index') }}">Ver Noticias y Aviso</a>
                <a class="collapse-item {{ request()->is('comite/noticias/create') ? 'active' : '' }}"
                    href="{{ route('comite.noticias.create') }}">Crear Noticias y Avisos</a>
            </div>
        </div>
    </li>
    @endif
    <!-- Nav Item - Charts -->

    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>
</ul>

<div id="glt-translate-trigger"><span class="notranslate">Traductor</span></div>
<div id="glt-toolbar"></div>
<div id="flags" style="display:none" class="size18">
    <ul id="sortable" class="ui-sortable">
        <li id="English"><a href="#" title="English" class="nturl notranslate en flag united-states"
                data-lang="English"></a></li>
        <li id="French"><a href="#" title="French" class="nturl notranslate fr flag French" data-lang="French"></a>
        </li>
        <li id="German"><a href="#" title="German" class="nturl notranslate de flag German" data-lang="German"></a>
        </li>
        <li id="Portuguese"><a href="#" title="Portuguese" class="nturl notranslate pt flag Portuguese"
                data-lang="Portuguese"></a></li>
        <li id="Spanish"><a href="#" title="Spanish" class="nturl notranslate es flag Spanish" data-lang="Spanish"></a>
        </li>
    </ul>
</div>
<div id="glt-footer">
    <div id="google_language_translator" class="default-language-es">
        <div class="skiptranslate goog-te-gadget" dir="ltr" style="">
            <div id=":0.targetLanguage"><select class="goog-te-combo" aria-label="Widget de idiomas del Traductor">
                    <option value="">Seleccionar idioma</option>
                    <option value="de">Alemán</option>
                    <option value="fr">Francés</option>
                    <option value="en">Inglés</option>
                    <option value="pt">Portugués</option>
                </select></div>Con la tecnología de <span style="white-space:nowrap"><a class="goog-logo-link"
                    href="https://translate.google.com" target="_blank"><img
                        src="https://www.gstatic.com/images/branding/googlelogo/1x/googlelogo_color_42x16dp.png"
                        width="37px" height="14px" style="padding-right: 3px" alt="Google Traductor de Google">Traductor
                    de Google</a></span>
        </div>
    </div>
</div>
<script>
    function GoogleLanguageTranslatorInit() {
        new google.translate.TranslateElement({
            pageLanguage: 'es',
            includedLanguages: 'en,fr,de,pt,es',
            autoDisplay: false
        }, 'google_language_translator');
    }

</script>



<link rel="stylesheet" id="google-language-translator-css"
    href="https://www.uts.edu.co/sitio/wp-content/plugins/google-language-translator/css/style.css?ver=6.0.8" media="">
<link rel="stylesheet" id="glt-toolbar-styles-css"
    href="https://www.uts.edu.co/sitio/wp-content/plugins/google-language-translator/css/toolbar.css?ver=6.0.8"
    media="">
<script src="https://www.uts.edu.co/sitio/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp"></script>



<style type="text/css">



    .goog-te-banner-frame {
        visibility: hidden !important;
    }

    body {
        top: 0px !important;
    }

    #glt-translate-trigger {
        left: auto;
        right: 20xp;
    }



    .goog-te-gadget .goog-te-combo {
        width: 100%;
    }

</style>


<script src="https://www.uts.edu.co/sitio/wp-content/plugins/google-language-translator/js/scripts.js?ver=6.0.8">
</script>
<script src="//translate.google.com/translate_a/element.js?cb=GoogleLanguageTranslatorInit">
</script>
