@extends('layouts.app')

@section('app-body')
<div id="wrapper">
    @include('sbadmin.sidebar.sidebar')

    <div id="content-wrapper" class="d-flex flex-column h-screen justify-content-between ">

        <!-- Main Content -->
        <div id="content">
            @include('sbadmin.topbar.bar')

            <div class="container-fluid">
                @yield('sbadmin-body')
            </div>
        </div>


    </div>
</div>

@endsection
