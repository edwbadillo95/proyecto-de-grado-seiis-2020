@extends('layouts.app2')

@section('app-body')

    <div id="content-wrapper" class="d-flex flex-column h-screen justify-content-between ">


        <!-- Main Content -->
        <div id="content">
            @include('sbadmin.topbar.barranoti')

            <div class="container-fluid">
                @yield('sbadmin-body-public')
            </div>
        </div>

        @include('sbadmin.footer')
    </div>
@endsection
