@extends('sbadmin.indexPublic')


@section('sbadmin-body-public')
<style>
.carousel {
    padding: 10px;

}

</style>

    <div id="carouselinicio" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">


            @foreach ($avisos as $slide)
                <li data-target="#carouselinicio" data-slide-to="{{ $loop->index }}"
                    class="{{ $loop->first ? 'active' : '' }}"></li>
            @endforeach

        </ol>



            @foreach ($avisos as $slide)
            <div class="carousel-inner" >
                <div class="carousel-item {{ $loop->first ? 'active' : '' }}">
                    <a href="{{ route('contenidoa', ['id' => $slide->id]) }}">
                        <img class="w-100 d-block" id="imgslide-{{ $loop->index }}"
                            src="{{ Storage::url($slide->imagen) }}" alt="First slide" object-fit=scale-down height=500vh
                            width=100% alt="Slide Image">
                    </a>
                    <div class="carousel-caption d-none d-md-block">
                        <h2 style="text-shadow: 1px 1px 10px black;">{!! $slide->titulo !!}</h2>
                    </div>

                 </div>
                 @endforeach
            </div>

                <a class="carousel-control-prev" href="#carouselinicio" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselinicio" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>

            @if (count($avisos ?? ''))

            @else
                <div class="alert alert-info">
                   <h3 align ="center" > No hay Avisos Creados.</h3>
                </div>
            @endif




    <div class="row mt-5">
        <div class="col-12">
            <div class="container">
                <h1 class="h1">Noticias Institucionales</h1>
            </div>
            <div class="container">
                <div class="row row-cols-1 row-cols-md-3">

                    @foreach ($noticia as $g)
                        <div class="col mb-2">
                            <div class="card ">
                                <a href="{{ route('contenidov', ['id' => $g->id]) }}"><img
                                        src="{{ Storage::url($g->imagen) }}" class="card-img-top img-fluid"
                                        style="height:300px; width:100%" sizes="(max-width: 1139px) 100vw, 1139px"></a>
                                <div class="card-body">
                                    <a href="{{ route('contenidov', ['id' => $g->id]) }}">
                                        <h2 class="h2">{!! $g->titulo !!}</h2>
                                    </a>
                                    <p class="card-text"><i class="fas fa-user-plus"><small> Publicado por:
                                                {{ $g->autor }}</i>
                                        <br>
                                        <i class="fas fa-clock"> {{ $g->fecha_creacion }}</small></i>
                                    </p>
                                </div>
                            </div>
                        </div>

                    @endforeach
                </div>
                @if (count($noticia ?? ''))

                @else
                    <div class="alert alert-info">
                       <h3 align ="center" > No hay Noticias Creadas.</h3>
                    </div>
                @endif
            </div>
        </div>


    </div>
    </div>





@endsection
