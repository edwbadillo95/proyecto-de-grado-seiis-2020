<nav class="navbar navbar-expand-lg navbar-light py-3 " style="background-color: #0b4a75">
  <a  href="/" style="color:white; font-size: 20px; font-weight: bold" ><i class="fa fa-graduation-cap fa-lg mr-2"></i>UTS</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <div class="container-fluid">
        <li class="nav-item">
          <a class="nav-link {{ request()->is('avisosv') ? 'active' : '' }}" href="{{ route('avisosv') }}"><i class="fa fa-info-circle fa-fw mr-1"></i>Avisos</a>
        </li>
        <li class="nav-item">
          <a class="nav-link {{ request()->is('noticiav') ? 'active' : '' }} " href="{{ route('noticiav') }}"><i class="fa fa-th-list fa-fw mr-1"></i>Noticias</a>
        </li>
        <li class="nav-item">
          <a class="nav-link {{ request()->is('registro-estudiante') ? 'active' : '' }}" href="{{ route('registro-estudiante') }}"><i class="fa fa-user-plus fa-fw mr-1"></i>Registro</a>
        </li>
      </div>
    </ul>
      <form class="form-inline my-2 my-lg-0" action="{{ route('login') }}" method="POST">
        @csrf
        <div class="row">
          <input type="text" class="form-control mr-2" placeholder="Correo" name="email">
          <input type="password" class="form-control mr-2" placeholder="Contraseña" name="password">
          <button class="btn btn-uts" type="submit">Ingresar</button>
        </div>
      </form>
  </div>
</nav>

@error('auth.failed')
<div class="modal" tabindex="-1" id="auth-failed">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Error de autenticación</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>{{ $message }}</p>
      </div>
      <div class="modal-footer">
        <button data-dismiss="modal" type="button" class="btn btn-danger">OK</button>
      </div>
    </div>
  </div>
</div>
@enderror
@push('js')
<script>
  $(document).ready(function() {
    let authFailed = $('#auth-failed')
    if (authFailed.length) {
      console.log("show");
      authFailed.modal('show')
    }
  });
</script>
@endpush
