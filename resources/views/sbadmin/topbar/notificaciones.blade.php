<div id="app">
    <vue-notificaciones/>
</div>
{{-- <li class="nav-item dropdown no-arrow mx-1">
    <a class="nav-link dropdown-toggle" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true"
        aria-expanded="false" onclick="marcarComoLeida({{ Auth::user()->unreadNotifications }})">
        <i class="fas fa-bell fa-fw"></i>
        <!-- Counter - Alerts -->
        @if (count(Auth::user()->unreadNotifications) > 0)
            <span class="badge badge-danger badge-counter">
                {{ count(Auth::user()->unreadNotifications) }}@if (count(Auth::user()->unreadNotifications) > 9)+@endif
            </span>
        @endif
    </a>
    <!-- Dropdown - Alerts -->
    <div id="notificaciones" class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in"
        style="max-height: 80vh;" aria-labelledby="alertsDropdown">
        <h6 class="dropdown-header">
            Notificaciones
        </h6>
        <div class="scroll-noti">

        </div>
        @foreach (Auth::user()->notifications as $notification)
            <a class="dropdown-item d-flex align-items-center" href="{{ $notification->data['url'] }}">
                <div class="mr-3">
                    <div class="icon-circle bg-{{ $notification->data['color'] }}">
                        <i class="fas {{ $notification->data['interface_icon'] }} text-white"></i>
                    </div>
                </div>
                <div>
                    <div class="small text-gray-500">{{ $notification->created_at }}</div>
                    <span class="font-weight-bold">{{ $notification->data['title'] }}</span><br>
                    <span>De: {{ $notification->data['user'] }}</span>
                </div>
            </a>
        @endforeach
        <a class="dropdown-item text-center small text-gray-500" href="#">Más notificaciones</a>
    </div>
</li>
<script>

</script> --}}