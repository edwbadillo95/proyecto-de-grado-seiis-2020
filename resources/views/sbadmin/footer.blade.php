<footer class="sticky-footer bg-white py-2">
    <div class="container">
        <div class="copyright text-center">
            <div class="row">
                <div class="mx-auto mb-1">
                    <a  href="https://www.facebook.com/UnidadesTecnologicasdeSantanderUTS/?epa=SEARCH_BOX" title="" target=" _blank">
                        <i style="color:#1a58b6 !important" class="fab fa-facebook fa-3x"></i>
                    </a>
                    <a style="color:#8134AF !important" href="https://www.instagram.com/unidades_uts/?hl=els-la" title="" target=" _blank">
                        <i class="fab fa-instagram-square fa-3x"></i>
                    </a>
                    <a style="color:#dd3333 !important" href="https://www.youtube.com/channel/UC-rIi4OnN0R10Wp-cPiLcpQ" title="" target=" _blank">
                        <i class="fab fa-youtube fa-3x"></i>
                    </a>
                </div>
            </div>
            <span>Copyright &copy; 2020</span>
        </div>
    </div>
    <div id="glt-translate-trigger"><span class="notranslate">Traductor </span></div>
    <div id="glt-toolbar"></div>
    <div id="flags" style="display:none" class="size18">
        <ul id="sortable" class="ui-sortable">
            <li id="English"><a href="#" title="English" class="nturl notranslate en flag united-states"
                    data-lang="English"></a></li>
            <li id="French"><a href="#" title="French" class="nturl notranslate fr flag French" data-lang="French"></a>
            </li>
            <li id="German"><a href="#" title="German" class="nturl notranslate de flag German" data-lang="German"></a>
            </li>
            <li id="Portuguese"><a href="#" title="Portuguese" class="nturl notranslate pt flag Portuguese"
                    data-lang="Portuguese"></a></li>
            <li id="Spanish"><a href="#" title="Spanish" class="nturl notranslate es flag Spanish" data-lang="Spanish"></a>
            </li>
        </ul>
    </div>
    <div id="glt-footer">
        <div id="google_language_translator" class="default-language-es">
            <div class="skiptranslate goog-te-gadget" dir="ltr" style="">
                <div id=":0.targetLanguage"><select class="goog-te-combo" aria-label="Widget de idiomas del Traductor">
                        <option value="">Seleccionar idioma</option>
                        <option value="de">Alemán</option>
                        <option value="fr">Francés</option>
                        <option value="en">Inglés</option>
                        <option value="pt">Portugués</option>
                    </select></div>Con la tecnología de <span style="white-space:nowrap"><a class="goog-logo-link"
                        href="https://translate.google.com" target="_blank"><img
                            src="https://www.gstatic.com/images/branding/googlelogo/1x/googlelogo_color_42x16dp.png"
                            width="37px" height="14px" style="padding-right: 3px" alt="Google Traductor de Google">Traductor
                        de Google</a></span>
            </div>
        </div>
    </div>
    <script>
        function GoogleLanguageTranslatorInit() {
            new google.translate.TranslateElement({
                pageLanguage: 'es',
                includedLanguages: 'en,fr,de,pt,es',
                autoDisplay: false
            }, 'google_language_translator');
        }

    </script>




        <link rel="stylesheet" id="google-language-translator-css"
            href="https://www.uts.edu.co/sitio/wp-content/plugins/google-language-translator/css/style.css?ver=6.0.8"
            media="">
        <link rel="stylesheet" id="glt-toolbar-styles-css"
            href="https://www.uts.edu.co/sitio/wp-content/plugins/google-language-translator/css/toolbar.css?ver=6.0.8"
            media="">


        <script src="https://www.uts.edu.co/sitio/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp"></script>

        <style type="text/css">

            #google_language_translator select.goog-te-combo {
                color: #32373c;
            }

            .goog-te-banner-frame {
                visibility: hidden !important;
            }

            body {
                top: 0px !important;
            }

            #glt-translate-trigger {
                left: auto;
                right: 20xp;
            }

            #glt-translate-trigger>span {
                color: #ffffff;
            }

            #glt-translate-trigger {
                background: rgb(4, 43, 70);
            }

            .goog-te-gadget .goog-te-combo {
                width: 100%;
            }

        </style>




                        <script
                            src="https://www.uts.edu.co/sitio/wp-content/plugins/google-language-translator/js/scripts.js?ver=6.0.8">
                        </script>
                        <script src="//translate.google.com/translate_a/element.js?cb=GoogleLanguageTranslatorInit">
                        </script>


</footer>
