<div class="tab-pane fade" id="nav-manual-usuario" role="tabpanel">
  @if (Auth::user()->esIntegranteProyecto($item->proyectoGrado) )
    <input type="hidden" id="manual-usuario-aprobado" value="{{ $item->estado == 'APROBADO' ? true : false }}">
    @if($item->estado != "APROBADO")
      <div class="row">
        <div class="col-12 mx-auto">
          <div class="card shadow-sm">
            <div class="card-header text-center card-titulo"> Manual Usuario</div>
            <div class="card-body  d-flex flex-column">
              <div class="row">
                @if (Auth::user()->esEstudianteProyecto($item->proyectoGrado) && $item->estado != "APROBADO")
                <div class="col-xl-6 col-lg-6 col-md-6  col-sm-12  col-xs-12 mx-auto">
                  <div class="card">
                    @if($item->fecha_ult_cargue || Auth::user()->esEstudianteProyecto($item->proyectoGrado))
                    <form method="POST" id="formulario-manual-usuario" action="{{route('subir-archivo-proyecto-estudiante',$item->id)}}" enctype="multipart/form-data">
                      @csrf
                      @method('PUT')
                      <div class="card-header text-center"> <i class="fas fa-file-upload"></i> Subir archivo</div>
                      <div class="card-body m-2 p-2 text-center mb-0" style="font-size: 11px;">
                        <div style="cursor: pointer;" onclick="document.getElementById('archivo-manual-usuario').click()">
                          <input type="file" name="archivo" id="archivo-manual-usuario" hidden accept="application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/msword, .pdf">
                          <i class="fas fa-file-upload fa-3x"></i>
                          <br>
                          <span id="nombre-archivo-manual-usuario">Seleccionar archivo</span>
                        </div>

                      </div>
                      <div class="card-footer">
                        <button type="submit" class="btn btn-uts btn-block"> <i class="fas fa-file-upload"></i> Subir</button>

                      </div>
                    </form>
                    @else
                    <div class="card-header">
                      <i class="fas fa-file-upload"></i> Subir archivo
                    </div>
                    <div class="card-body m-2 p-2 text-center mb-0" style="font-size: 11px;">
                      <div>
                        <i class="fas fa-file-upload fa-3x"></i>
                        <br>
                        <span>Aun no hay un archivo cargado</span>
                      </div>

                    </div>
                    <div class="card-footer">


                    </div>

                    @endif
                  </div>
                </div>
                @endif
                @if(!$item->fecha_ult_cargue && !Auth::user()->esEstudianteProyecto($item->proyectoGrado))
                  <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-6 mx-auto">
                    <div class="card">
                      <div class="card-header text-center"> <i class="fas fa-file-download"></i> Manual de usuario</div>
                      <div class="card-body text-center m-2 p-2">
                        <strong>El estudiante aun no sube el manual</strong>
                      </div>
                    </div>
                  </div>
                @endif
              </div>
            </div>
          </div>
        </div>
      </div>
    @else
      <div class="col-6 mx-auto">
        <div class="card">
          <div class="card-header text-center card-titulo"> Manual Usuario</div>
          <div class="card-body text-center m-2 p-2" style="font-size: 11px;">
            <a href="{{Storage::url($item->url_documento)}}" target="_blank">
              <i class="fas fa-file-download fa-3x"></i> <br>
              <span>Descargar archivo</span>
            </a>
          </div>
          <div class="card-footer">
            <div class="col-12 text-center">
              <strong>Documento Aprobado<br>Fecha:</strong> <span class="badge {{ $item->fecha_ult_cargue ? 'btn-uts' : '' }}"> {{$item->fecha_ult_cargue}}</span>
            </div>
          </div>
        </div>
      </div>
    @endif
  @endif
</div>
@if(Auth::user()->documentoAprobado($item,4) && $item->estado != 'APROBADO')
@push('js')
<script>
  document.addEventListener("DOMContentLoaded", function() {
    document.getElementById("formulario-manual-usuario").addEventListener('submit', validarFormularioMU);
    document.getElementById('archivo-manual-usuario').onchange = function() {
      document.getElementById('nombre-archivo-manual-usuario').innerHTML = document.getElementById('archivo-manual-usuario').files[0].name;
    }

  });

  function validarFormularioMU(evento) {
    evento.preventDefault();
    var archivo = document.getElementById('archivo-manual-usuario').files[0];
    if (archivo == undefined) {
      return errorArchivo();
    };
    confirmarArchivo().then((result) => {
      if (result.isConfirmed) {
        this.submit();
      }
    })
  };
</script>
@endpush
@endif
