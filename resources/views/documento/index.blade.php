@extends('sbadmin.index')

@push('css')
<link rel="stylesheet" href="{{ asset('vendor/documentos/css/style.css') }}">
@endpush

@section('sbadmin-body')
@panel
  @slot('titulo')
  @include('documento.nav')
  @endslot

  @include('documento.error')
  @include('partials.forms.saved')

  <div class="tab-content">

    @foreach ($documentos as $item)
      {{-- Nav Propuesta --}}
      @if($item->tipoDocumento->id == 1 )
        @include('documento.propuesta')

      {{-- Nav RDC --}}
      @elseif( $item->tipoDocumento->id == 2)
        @include('documento.rdc')

      {{-- Nav Manual instalacion--}}
      @elseif($item->tipoDocumento->id == 4)
        @include('documento.manualInstalacion')

      {{-- Nav Manual usuario--}}
      @elseif($item->tipoDocumento->id == 5)
        @include('documento.manualUsuario')

      {{-- Nav Codigo fuente --}}
      @elseif($item->tipoDocumento->id == 6)
        @include('documento.codigoFuente')

      {{-- Nav Evalucion --}}
      @elseif($item->tipoDocumento->id == 3)
        @include('documento.evaluacion')      

      @endif

    @endforeach

    @if (Auth::user()->proyectoEstudiante())
      {{-- Nav Derechos de grado --}}    
      @include('documento.requisitosfinales')
    @endif

  </div>
@endpanel
@endsection

@push('js')
{{-- <script src="{{ asset('vendor/documentos/js/functions.js') }}"></script> --}}
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>

<script>
  $(document).ready(function() {

    let propuestaAprobada = $('#propuesta-aprobada').val()
    if (propuestaAprobada) {

      let rdcAprobado = $('#rdc-aprobada').val()
      if (rdcAprobado) {

        let manualInstalacionAprobado = $('#manual-instalacion-aprobado').val()
        if (manualInstalacionAprobado) {

          let manualUsuarioAprobado = $('#manual-usuario-aprobado').val()
          if (manualUsuarioAprobado) {

            let codigoFuente = $('#codigo-fuente-aprobada').val()
            if (codigoFuente) {

              let evaluacionAprobada = $('#evaluacion-aprobada').val()
              if (evaluacionAprobada) {

                let esEstudiante = $('#es-estudiante').val()
                if(esEstudiante){

                  // evaluacion aprovada
                  check('evaluacion')
                  check('requisitos')
                  $('#tab-requisitos').removeClass('disabled');
                  quitarTooltip($('#tooltip-requisitos'))
                  setTabActive('requisitos')

                } else {
                  // check('evaluacion')
                  setTabActive('evaluacion')
                }

              } else {
                setTabActive('evaluacion')
              }
              // documentacion final aprovada
              check('codigo-fuente')
              $('#tab-evaluacion').removeClass('disabled');
              quitarTooltip($('#tooltip-evaluacion'))
              // setTabActive('evaluacion')

            } else {
              setTabActive('codigo-fuente')
            }
            // manual de usuario aprovado
            $('#tab-codigo-fuente').removeClass('disabled');
            check('manual-usuario')
            quitarTooltip($('#tooltip-codigo-fuente'))

          } else {
            setTabActive('manual-usuario')
          }
          // manual de instalacion aprovado
          $('#tab-manual-usuario').removeClass('disabled');
          check('manual-instalacion')
          quitarTooltip($('#tooltip-manual-usuario'))

        } else {
          setTabActive('manual-instalacion')
        }
        // rdc-125 aprovado
        $('#tab-manual-instalacion').removeClass('disabled');
        quitarTooltip($('#tooltip-manual-instalacion'))
        check('rdc')

      } else {
        setTabActive('rdc')
      }
      // propuesta aprovada
      $('#tab-rdc').removeClass('disabled');
      quitarTooltip($('#tooltip-rdc'))
      check('propuesta')

    } else {
      $('#nav-propuesta').addClass('active show')
      $('#tab-propuesta').addClass('active')
    }
  });

  function quitarTooltip(t) {
    if (t.length) {
      t.removeAttr('data-toggle')
      t.removeAttr('data-placement')
      t.removeAttr('title')
      t.removeAttr('data-original-title')
    }
  }

  function setTabActive(nombre) {
    $('#nav-' + nombre).addClass('active show')
    $('#tab-' + nombre).addClass('active')
    $('#tab-' + nombre).removeClass('disabled')
  }

  function check(icon) {
    $('#icon-' + icon).attr('class', 'fas fa-check-circle')
  }

  function errorArchivo(){
    Swal.fire({
        icon: 'error',
        title: 'Error',
        text: 'Selecciona un archivo!',
      })
  }

  function confirmarArchivo(){
    return Swal.fire({
      title: 'Estas seguro de subir el archivo?',
      text: "No podras volver a subir el archivo!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText: 'Cancelar',
      confirmButtonText: 'Si, subir archivo!'
    })
  }
</script>
@endpush
