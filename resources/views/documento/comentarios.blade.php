<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">


  <div class="card mb-4">
    <div class="card-header text-center card-titulo">Comentarios</div>
    <div class="card-body">
      {{-- Card Comentarios --}}
      <form method="POST" action="{{route('nuevo-comentario',$item->id)}}">
        @csrf
        <div class="form-group">
          <textarea required minlength="8" name="comentario" class="form-control bg-light text-left mb-1" rows="3" placeholder="Escribe un comentario..."></textarea>
          <div class="input-group-append">
            <button type="submit" class="btn btn-uts"> <i class="fas fa-paper-plane"></i> Comentar</button>
          </div>
        </div>
      </form>
    </div>
    <div class="card-footer">


      @if($item->comentarios->count())
      @foreach ($item->comentarios as $c)
      @if($c->user->docente)
      <div class="d-flex m-2">
        <div class="flex-grow-1">
          <div class="card border-0 shadow-sm">
            <div class="card-body p-2 text-secondary">
              <div class="text-truncate">{{$c->comentario}}.</div>
              <div class="small text-gray-500">{{$c->user->name}} · {{$c->created_at->diffForHumans()}}</div>
            </div>
          </div>
        </div>
        <div class="icon-circle bg-primary rounded shadow-sm ml-2" height="34px" width="34px">
          <i class="fas fa-user-tie text-white"></i>
        </div>
      </div>


      @elseif($c->user->estudiante)
      <div class="d-flex">

        <div class="icon-circle btn-uts rounded shadow-sm mr-2" height="34px" width="34px">
          <i class="fas fa-user-circle text-white"></i>
        </div>
        <div class="flex-grow-1">
          <div class="card border-0 shadow-sm">
            <div class="card-body p-2 text-secondary">
              <div class="text-truncate">{{$c->comentario}}.</div>
              <div class="small text-gray-500">{{$c->user->name}} · {{$c->created_at->diffForHumans()}}</div>
            </div>
          </div>
        </div>
      </div>

      @endif
      @endforeach
      @else
      <div class="d-flex">
        <div class="flex-grow-1">
          <div class="card border-0 shadow-sm">
            <div class="card-body p-2 text-secondary">
              <div class="text-truncate text-gray-500">No hay comentarios para este documento...</div>
            </div>
          </div>
        </div>
      </div>
      @endif


    </div>
  </div>
</div>