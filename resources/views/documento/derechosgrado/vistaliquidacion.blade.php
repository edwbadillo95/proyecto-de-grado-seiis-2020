<!DOCTYPE html>
<html lang="es">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Derechos grado</title>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
        integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

    <style>
        @page {
            margin: 10px 10px 10px 10px !important;
            padding: 0px 0px 0px 0px !important;
        }
    </style>
</head>

<body>

    <table width="100%">
        <tbody>
            <tr>
                <td width="25%">
                    <img height="50"
                        src="https://www.uts.edu.co/sitio/wp-content/uploads/2019/08/uts-virtal-logo-nuevo01.png"
                        alt="">
                </td>
                <td width="50%" class="text-center" style="line-height:80%;">
                    <span>UNIDADES TECNOLÓGICAS DE SANTANDER</span>
                    <br>
                    <small style="font-size: 9px;">NIT 65785454857</small>
                    <br>
                    <small style="font-size: 9px;">LIQUIDACIÓN OTROS PECUNIARIOS</small>
                </td>
                <td width="25%" class="text-right" style="line-height:100%;">
                    <small>RECIBO LQ-00770796 <br>
                        REFERENCIA. 00770796</small>
                </td>
            </tr>
        </tbody>
    </table>


    <table class="table table-sm mb-0" style="font-size: 9px;">
        <tbody>
            <tr>
                <td colspan="2">
                    <b>NOMBRE:</b> {{ $usuario->name }}
                </td>
                <td>
                    <b>DOCUMENTO:</b> {{ $usuario->estudiante->numero_documento }}
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <b>CONCEPTO:</b> DERECHOS DE GRADO
                </td>
            </tr>
            <tr>
                <td>
                    <b>REGIONAL:</b> BUCARAMANGA
                </td>
                <td>
                    <b>JORNADA:</b> NOCTURNA
                </td>
                <td>
                    <b>FECHA EXPEDICIÓN:</b> {{ today() }}
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <b>PROGRAMA:</b> INGENIERÍA DE SISTEMAS
                </td>
            </tr>
        </tbody>
    </table>

    <table class="table table-bordered table-sm mb-0" style="font-size: 9px;">
        <thead class="thead-ligth">
            <tr>
                <th width="70%">CONCEPTO</th>
                <th width="30%">VALOR</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>CICLO PROFESIONAL</td>
                <td>$872,000</td>
            </tr>
        </tbody>
        <tfoot>
            <tr>
                <th>TOTAL A PAGAR</th>
                <th>$872,000</th>
            </tr>
            <tr>
                <td colspan="2">
                    <b>BANCOS AUTORIZADOS: </b> BANCOLOMBIA, BBVA, COOPROFESORES
                </td>
            </tr>
        </tfoot>
    </table>

    <p class="text-center mb-1" style="font-size: 12px;">
        <b>*** VÁLIDO HASTA EL 31 DE DICIEMBRE DEL 2021 ***</b> <br>
        <b>SOLO SE RECIBEN PAGOS REALIZADOS CON ESTE FORMATO DE CODIGO DE BARRAS EN LOS BANCOS AUTORIZADOS</b> <br>
        <b>- USUARIO -</b>
    </p>
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    <table width="100%">
        <tbody>
            <tr>
                <td width="25%">
                    <img height="50"
                        src="https://www.uts.edu.co/sitio/wp-content/uploads/2019/08/uts-virtal-logo-nuevo01.png"
                        alt="">
                </td>
                <td width="50%" class="text-center" style="line-height:80%;">
                    <span>UNIDADES TECNOLÓGICAS DE SANTANDER</span>
                    <br>
                    <small style="font-size: 9px;">NIT 65785454857</small>
                    <br>
                    <small style="font-size: 9px;">LIQUIDACIÓN OTROS PECUNIARIOS</small>
                </td>
                <td width="25%" class="text-right" style="line-height:100%;">
                    <small>RECIBO LQ-00770796 <br>
                        REFERENCIA. 00770796</small>
                </td>
            </tr>
        </tbody>
    </table>

    <table class="table table-sm mb-0" style="font-size: 9px;">
        <tbody>
            <tr>
                <td colspan="2">
                    <b>NOMBRE:</b> {{ $usuario->name }}
                </td>
                <td>
                    <b>DOCUMENTO:</b> {{ $usuario->estudiante->numero_documento }}
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <b>CONCEPTO:</b> DERECHOS DE GRADO
                </td>
            </tr>
            <tr>
                <td>
                    <b>REGIONAL:</b> BUCARAMANGA
                </td>
                <td>
                    <b>JORNADA:</b> NOCTURNA
                </td>
                <td>
                    <b>FECHA EXPEDICIÓN:</b> {{ today() }}
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <b>PROGRAMA:</b> INGENIERÍA DE SISTEMAS
                </td>
            </tr>
        </tbody>
    </table>

    <table class="table table-bordered table-sm mb-0" style="font-size: 9px;">
        <thead class="thead-ligth">
            <tr>
                <th width="70%">CONCEPTO</th>
                <th width="30%">VALOR</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>CICLO PROFESIONAL</td>
                <td>$872,000</td>
            </tr>
        </tbody>
        <tfoot>
            <tr>
                <th>TOTAL A PAGAR</th>
                <th>$872,000</th>
            </tr>
            <tr>
                <td colspan="2">
                    <b>BANCOS AUTORIZADOS: </b> BANCOLOMBIA, BBVA, COOPROFESORES
                </td>
            </tr>
        </tfoot>
    </table>

    <p class="text-center mt-2 mb-0" style="font-size: 12px;">
        <img src="{{ asset('img/barras.png') }}" alt="barras" height="60">
        <br>
        <b>- UTS -</b>
    </p>
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    <table width="100%">
        <tbody>
            <tr>
                <td width="25%">
                    <img height="50"
                        src="https://www.uts.edu.co/sitio/wp-content/uploads/2019/08/uts-virtal-logo-nuevo01.png"
                        alt="">
                </td>
                <td width="50%" class="text-center" style="line-height:80%;">
                    <span>UNIDADES TECNOLÓGICAS DE SANTANDER</span>
                    <br>
                    <small style="font-size: 9px;">NIT 65785454857</small>
                    <br>
                    <small style="font-size: 9px;">LIQUIDACIÓN OTROS PECUNIARIOS</small>
                </td>
                <td width="25%" class="text-right" style="line-height:100%;">
                    <small>RECIBO LQ-00770796 <br>
                        REFERENCIA. 00770796</small>
                </td>
            </tr>
        </tbody>
    </table>

    <table class="table table-sm mb-0" style="font-size: 9px;">
        <tbody>
            <tr>
                <td colspan="2">
                    <b>NOMBRE:</b> {{ $usuario->name }}
                </td>
                <td>
                    <b>DOCUMENTO:</b> {{ $usuario->estudiante->numero_documento }}
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <b>CONCEPTO:</b> DERECHOS DE GRADO
                </td>
            </tr>
            <tr>
                <td>
                    <b>REGIONAL:</b> BUCARAMANGA
                </td>
                <td>
                    <b>JORNADA:</b> NOCTURNA
                </td>
                <td>
                    <b>FECHA EXPEDICIÓN:</b> {{ today() }}
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <b>PROGRAMA:</b> INGENIERÍA DE SISTEMAS
                </td>
            </tr>
        </tbody>
    </table>

    <table class="table table-bordered table-sm mb-0" style="font-size: 9px;">
        <thead class="thead-ligth">
            <tr>
                <th width="70%">CONCEPTO</th>
                <th width="30%">VALOR</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>CICLO PROFESIONAL</td>
                <td>$872,000</td>
            </tr>
        </tbody>
        <tfoot>
            <tr>
                <th>TOTAL A PAGAR</th>
                <th>$872,000</th>
            </tr>
            <tr>
                <td colspan="2">
                    <b>BANCOS AUTORIZADOS: </b> BANCOLOMBIA, BBVA, COOPROFESORES
                </td>
            </tr>
        </tfoot>
    </table>

    <p class="text-center mt-2 mb-0" style="font-size: 12px;">
        <img src="{{ asset('img/barras.png') }}" alt="barras" height="60">
        <br>
        <b>- BANCO -</b>
    </p>
</body>

</html>
