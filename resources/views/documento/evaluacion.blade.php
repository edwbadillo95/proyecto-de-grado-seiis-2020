<div class="tab-pane fade" id="nav-evaluacion" role="tabpanel">
  @if (Auth::user()->esIntegranteProyecto($item->proyectoGrado))
    <input type="hidden" id="evaluacion-aprobada" value="{{ $item->estado == 'APROBADO' ? true : false }}">
    <input type="hidden" id="es-estudiante" value="{{ Auth::user()->proyectoEstudiante() ? true : false }}">
    @if($item->estado == "PENDIENTE")
      <div class="row">
        <div class="col-12 mx-auto">
          <div class="card shadow-sm">
            <div class="card-header text-center card-titulo"> Evaluacion</div>
            <div class="card-body  d-flex flex-column">
              <div class="row justify-content-center">
                @if(!$item->fecha_ult_revision && $item->estado != "RECHAZADO" && Auth::user()->esCalificadorProyecto($item->proyectoGrado))
                  <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-xs-12">
                      <div class="card">
                          <form method="POST" id="evaluacion-formulario" action="{{route('subir-archivo-proyecto-estudiante',$item->id)}}" enctype="multipart/form-data">
                              @csrf
                              @method('PUT')
                              <div class="card-header text-center"> <i class="fas fa-file-upload"></i> Subir archivo</div>
                              <div class="card-body m-2 p-2 text-center mb-0" style="font-size: 11px;">
                                  <div style="cursor: pointer;" onclick="document.getElementById('archivo-evaluacion').click()">
                                      <input type="file" name="archivo" id="archivo-evaluacion" hidden accept="application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/msword, .pdf">
                                      <i class="fas fa-file-upload fa-3x"></i>
                                      <br>
                                      <span id="nombre-archivo-evaluacion">Seleccionar archivo</span>
                                  </div>

                              </div>
                              <div class="card-footer">
                                  <button type="submit" class="btn btn-uts btn-block"> <i class="fas fa-file-upload"></i> Subir</button>

                              </div>
                          </form>
                      </div>
                  </div>
                  <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="card">
                      <div class="card-header text-center"> <i class="fas fa-file-download"></i> Descargar archivo</div>
                      @if(!$item->fecha_ult_cargue)
                      <div class="card-body text-center m-2 p-2" style="font-size: 11px;">
                        <a href="{{asset('/documentos/F-DC-129 Rejilla Evaluación Informe Final Trabajo Grado V1.docx')}}" target="_blank">
                          <i class="fas fa-file-download fa-3x"></i> <br>
                          <span>Descargar formato FDC-129 Rejilla Evaluacion</span>
                        </a>
                      </div>
                      <div class="card-footer">
                        <div class="col-12">
                          <a class="btn btn-block btn-uts" href="{{asset('/documentos/F-DC-129 Rejilla Evaluación Informe Final Trabajo Grado V1.docx')}}" target="_blank">
                            <i class="fas fa-file-download"></i>
                            <span> Descargar </span>
                          </a>
                        </div>
                      </div>
                      @else
                      <div class="card-body text-center m-2 p-2" style="font-size: 11px;">
                        <a href="{{Storage::url($item->url_documento)}}" target="_blank">
                          <i class="fas fa-file-download fa-3x"></i> <br>
                          <span>Descargar archivo</span>
                        </a>
                      </div>
                      <div class="card-footer">
                        <div class="col-12">
                          <a class="btn btn-block btn-uts" href="{{Storage::url($item->url_documento)}}" target="_blank">
                            <i class="fas fa-file-download"></i> Descargar
                          </a>
                        </div>
                      </div>
                      @endif
                    </div>
                  </div>
                @elseif($item->estado == "APROBADO" && !Auth::user()->esCalificadorProyecto($item->proyectoGrado) )
                  <div class="col-xl-4 col-lg-4 col-md-4  col-sm-12  col-xs-12 mx-auto">
                      <div class="card">
                          <div class="card-header text-center"> <i class="fas fa-file-download"></i> Descargar archivo</div>
                          @if(!$item->fecha_ult_revision)
                          <div class="card-body text-center m-2 p-2" style="font-size: 11px;">
                              <a href="https://sites.google.com/site/utsadmon2020/F-DC-124%20Propuesta%20Trabajo%20Grado%20PI%20DTeI%20Mono%20Emprend%20V1.doc?attredirects=0&d=1" target="_blank">
                                  <i class="fas fa-file-download fa-3x"></i> <br>
                                  <span>Descargar formato Evaluacion</span>
                              </a>
                          </div>
                          <div class="card-footer">
                              <div class="col-12">
                                  <a class="btn btn-block btn-uts" href="https://sites.google.com/site/utsadmon2020/F-DC-124%20Propuesta%20Trabajo%20Grado%20PI%20DTeI%20Mono%20Emprend%20V1.doc?attredirects=0&d=1" target="_blank">
                                      <i class="fas fa-file-download"></i>
                                      <span> Descargar </span>
                                  </a>
                              </div>
                          </div>
                          @else
                          <div class="card-body text-center m-2 p-2" style="font-size: 11px;">
                              <a href="{{Storage::url($item->url_documento)}}" target="_blank">
                                  <i class="fas fa-file-download fa-3x"></i> <br>
                                  <span>Descargar archivo</span>
                              </a>
                          </div>
                          <div class="card-footer">
                              <div class="col-12">
                                  <a class="btn btn-block btn-uts" href="{{Storage::url($item->url_documento)}}" target="_blank">
                                      <i class="fas fa-file-download"></i> Descargar
                                  </a>
                              </div>
                          </div>
                          @endif


                      </div>
                  </div>
                @elseif(!Auth::user()->esCalificadorProyecto($item->proyectoGrado))
                  <div class="col-12 text-center">
                      <strong>El calificador aun no sube la rejilla de evaluacion</strong>
                  </div>
                @elseif ($item->proyectoGrado->aprobado == null && $item->estado == "PENDIENTE" && Auth::user()->esCalificadorProyecto($item->proyectoGrado) && $item->url_documento != null)
                  <div class="col-xl-4 mx-auto">
                    <div class="card text-center">
                      <div class="card-header"><strong>Aprobar o Rechazar</strong></div>
                      <div class="card-body">
                        <div class="row">
                          <div class="col-xl-12">
                            <form action="{{route('aprobar-documento',$item->id)}}" method="post" style="display:inline">
                              @csrf
                              <button class="btn btn-uts">
                                  <i class="fas fa-check-circle"></i> Aprobar
                              </button>
                            </form>
                            <form action="{{route('rechazar-documento',$item->id)}}" method="post" style="display:inline">
                              @csrf
                              <button class="btn btn-danger">
                                  <i class="fas fa-times-circle"></i> Rechazar
                              </button>
                            </form>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                @endif
              </div>
            </div>
          </div>
        </div>
      </div>
    @elseif (!$item->proyectoGrado->aprobado || $item->proyectoGrado->aprobado)
    <div class="col-6 mx-auto">
      <div class="card">
      <div class="card-header text-center card-titulo"> Evaluacion</div>
        <div class="card-body text-center m-2 p-2" style="font-size: 11px;">
          <a href="{{Storage::url($item->url_documento)}}" target="_blank">
            <i class="fas fa-file-download fa-3x"></i> <br>
            <span>Descargar archivo</span>
          </a>
        </div>
        <div class="card-footer">
        <div class="col-12 text-center">
          @if ($item->proyectoGrado->aprobado  && $item->estado == "APROBADO")
          <strong>Documento Aprobado<br>Fecha:</strong> <span class="badge {{ $item->fecha_ult_revision ? 'btn-uts' : '' }}"> {{$item->fecha_ult_revision}}</span>
          @else
          <strong>Documento Rechazado<br>Fecha:</strong> <span class="badge {{ $item->fecha_ult_revision ? 'btn-uts' : '' }}"> {{$item->fecha_ult_revision}}</span>
          @endif
        </div>
        </div>
      </div>
    </div>
    @endif
  @endif
</div>
@if(Auth::user()->documentoAprobado($item,6) && $item->estado != "APROBADO")
@push('js')
  <script>
    document.addEventListener("DOMContentLoaded", function() {
      var evalForm = document.getElementById("evaluacion-formulario")
      if (evalForm) {
        evalForm.addEventListener('submit', validarFormularioEvaluacion);
      }
      var archivoEvaluacion = document.getElementById('archivo-evaluacion')
      if (archivoEvaluacion) {
        archivoEvaluacion.onchange = function() {
            document.getElementById('nombre-archivo-evaluacion').innerHTML = document.getElementById('archivo-evaluacion').files[0].name;
        }
      }
    });

    function validarFormularioEvaluacion(evento) {
      evento.preventDefault();
      var archivo = document.getElementById('archivo-evaluacion').files[0];
      if (archivo == undefined) {
        return errorArchivo();
      };
      confirmarArchivo().then((result) => {
        if (result.isConfirmed) {
            this.submit();
        }
      })
    };
  </script>
@endpush
@endif
