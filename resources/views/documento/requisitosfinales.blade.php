<div class="tab-pane fade" id="nav-requisitos" role="tabpanel">
    <h2 class="text-center">
        <i class="fas fa-flag-checkered"></i>
        FELICIDADES
        <i class="fas fa-flag-checkered"></i>
    </h2>
    <p class="text-center">
        <i>Estás a pocos pasos de ser egresado de las UNIDADES TECNOLÓGICAS DE SANTANDER.</i>
    </p>

    <table class="table table-bordered table-striped" style="font-size: 13px">
        <thead>
            <tr>
                <th>Sigue los siguientes pasos para culminar con el proceso de graduación.</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    Descargue la liquidación de derechos de grado para realizar el pago en alguno de los bancos autorizados definidos en dicha liquidación.
                    <p class="mb-0">
                        <a target="_blank" class="text-primary" href="{{ route('documento.derechosdegrado') }}">Liquidación de derechos de grado</a>.
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    Descargue el siguiente documento con los pasos finales para la entrega de documentos de grado.
                    <p class="mb-0"><a target="_blank" class="text-primary" href="{{ asset('pdf/procedimientogrado.pdf') }}">Documento</a>.</p>
                </td>
            </tr>
            <tr>
                <td>
                    Descargue todo los documentos aprobados de su proyecto de grado.
                    <p class="mb-0"><a target="_blank" class="text-primary" href="{{route('documentacion-zip',$item->id)}}">Documento Aprobados</a>.</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
