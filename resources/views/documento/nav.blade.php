<ul class="nav nav-pills nav-fill" style="font-size: 12px;">
  <li class="nav-item " id="tooltip-propuesta">
    <a id="tab-propuesta" class="nav-link" data-toggle="tab" href="#nav-propuesta" role="tab">
      <i id="icon-propuesta" class="fas fa-angle-double-right"></i> FDC-124</a>
  </li>
  <li class="nav-item " id="tooltip-rdc" style="cursor: no-drop;" data-toggle="tooltip" data-placement="top" title="Debes subir primero la propuesta y que ésta esté aprobada.">
    <a id="tab-rdc" class="nav-link disabled" data-toggle="tab" href="#nav-rdc" role="tab">
      <i id="icon-rdc" class="fas fa-angle-double-right"></i> FDC-125</a>
  </li>
  <li class="nav-item " id="tooltip-manual-instalacion" style="cursor: no-drop;" data-toggle="tooltip" data-placement="top" title="Debes subir el documento final y además debe estar aprobado.">
    <a id="tab-manual-instalacion" class="nav-link disabled" data-toggle="tab" href="#nav-manual-instalacion" role="tab"> 
      <i id="icon-manual-instalacion" class="fas fa-angle-double-right"></i> Manual Instalacion</a>
  </li>
  <li class="nav-item " id="tooltip-manual-usuario" style="cursor: no-drop;" data-toggle="tooltip" data-placement="top" title="Debes subir el manual de instalacion y además debe estar aprobado.">
    <a id="tab-manual-usuario" class="nav-link disabled" data-toggle="tab" href="#nav-manual-usuario" role="tab"> 
      <i id="icon-manual-usuario" class="fas fa-angle-double-right"></i> Manual Usuario</a>
  </li>
  <li class="nav-item " id="tooltip-codigo-fuente" style="cursor: no-drop;" data-toggle="tooltip" data-placement="top" title="Debes subir el manual de usuario y además debe estar aprobado.">
    <a id="tab-codigo-fuente" class="nav-link disabled" data-toggle="tab" href="#nav-codigo-fuente" role="tab">
      <i id="icon-codigo-fuente" class="fas fa-angle-double-right"></i> Codigo Fuente</a>
  </li>
  <li class="nav-item " id="tooltip-evaluacion" style="cursor: no-drop;" data-toggle="tooltip" data-placement="top" title="Debes subir el codigo fuente y además debe estar aprobado.">
    <a id="tab-evaluacion" class="nav-link disabled" data-toggle="tab" href="#nav-evaluacion" role="tab">
      <i id="icon-evaluacion" class="fas fa-angle-double-right"></i> Evaluación</a>
  </li>
  @if (Auth::user()->proyectoEstudiante())
  <li class="nav-item " id="tooltip-requisitos" style="cursor: no-drop;" data-toggle="tooltip" data-placement="top" title="Tu proyecto debe estar aprobado y tener todos los documentos adjuntos.">
    <a id="tab-requisitos" class="nav-link disabled" data-toggle="tab" href="#nav-requisitos" role="tab">
      <i id="icon-requisitos" class="fas fa-angle-double-right"></i> Requisitos finales</a>
  </li>
  @endif 
</ul>
