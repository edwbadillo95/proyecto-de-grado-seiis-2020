<div class="tab-pane fade" id="nav-rdc" role="tabpanel">
  @if (Auth::user()->esIntegranteProyecto($item->proyectoGrado))
    <input type="hidden" id="rdc-aprobada" value="{{ $item->estado == 'APROBADO' ? true : false }}">
    @if($item->estado != "APROBADO")
      <div class="row">
        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-xs-12">
          <div class="card shadow-sm">
            <div class="card-header text-center card-titulo"> FDC-125</div>
            <div class="card-body  d-flex flex-column">
              <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-6  col-sm-12  col-xs-12 mx-auto">
                  <div class="card">
                    @if($item->fecha_ult_cargue || Auth::user()->esEstudianteProyecto($item->proyectoGrado))
                    <form method="POST" id="formulario-rdc" action="{{route('subir-archivo-proyecto-estudiante',$item->id)}}" enctype="multipart/form-data">
                      @csrf
                      @method('PUT')
                      <div class="card-header text-center"> <i class="fas fa-file-upload"></i> Subir archivo</div>
                      <div class="card-body m-2 p-2 text-center mb-0" style="font-size: 11px;">
                        <div style="cursor: pointer;" onclick="document.getElementById('archivo-rdc').click()">
                          <input type="file" name="archivo" id="archivo-rdc" hidden accept="application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/msword">
                          <i class="fas fa-file-upload fa-3x"></i>
                          <br>
                          <span id="nombre-archivo-rdc">Seleccionar archivo</span>
                        </div>
                      </div>
                      <div class="card-footer">
                        <button type="submit" class="btn btn-uts btn-block"> <i class="fas fa-file-upload"></i> Subir</button>
                      </div>
                    </form>
                    @else
                    <div class="card-header text-center">
                      <i class="fas fa-file-upload"></i> Subir archivo
                    </div>
                    <div class="card-body m-2 p-2 text-center mb-0" style="font-size: 11px;">
                      <div>
                        <i class="fas fa-file-upload fa-3x"></i>
                        <br>
                        <span>Aun no hay un archivo cargado</span>
                      </div>
                    </div>
                    @endif
                  </div>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-6  col-sm-12  col-xs-12 mx-auto">
                  <div class="card">
                    <div class="card-header text-center"> <i class="fas fa-file-download"></i> Descargar archivo</div>
                    @if(!$item->fecha_ult_cargue)
                    <div class="card-body text-center m-2 p-2" style="font-size: 11px;">
                      <a href="{{asset('/documentos/F-DC-125  Informe Final Trabajo Grado Modalidad P Inv DT Mgfía Emdto Semi V1.docx')}}" target="_blank">
                        <i class="fas fa-file-download fa-3x"></i> <br>
                        <span>Descargar formato FDC-125</span>
                      </a>
                    </div>
                    <div class="card-footer">
                      <div class="col-12">
                        <a class="btn btn-block btn-uts" href="{{asset('/documentos/F-DC-125  Informe Final Trabajo Grado Modalidad P Inv DT Mgfía Emdto Semi V1.docx')}}" target="_blank">
                          <i class="fas fa-file-download"></i>
                          <span> Descargar </span>
                        </a>
                      </div>
                    </div>
                    @else
                    <div class="card-body text-center m-2 p-2" style="font-size: 11px;">
                      <a href="{{Storage::url($item->url_documento)}}" target="_blank">
                        <i class="fas fa-file-download fa-3x"></i> <br>
                        <span>Descargar archivo</span>
                      </a>
                    </div>
                    <div class="card-footer">
                      <div class="col-12">
                        <a class="btn btn-block btn-uts" href="{{Storage::url($item->url_documento)}}" target="_blank">
                          <i class="fas fa-file-download"></i> Descargar
                        </a>
                      </div>
                    </div>
                    @endif
                  </div>
                </div>
                @if ($item->fecha_ult_cargue && Auth::user()->esDirectorProyecto($item->proyectoGrado))
                <div class="col-12">
                  <div class="card mt-4">
                    <div class="card-body">
                      <form action="{{route('aprobar-documento',$item->id)}}" method="post">
                        @csrf
                        <button class="btn btn-uts btn-lg btn-block">
                          <i class="fas fa-check-circle"></i> Aprobar
                        </button>
                      </form>
                    </div>
                  </div>
                </div>
                @endif
              </div>
            </div>
            @if ($item->url_documento)
            <div class="card-footer">
              <strong>Fecha de subida:</strong> <span class="badge {{ $item->fecha_ult_cargue ? 'btn-uts' : '' }}"> {{$item->fecha_ult_cargue}}</span> <br>
              <strong>Fecha de revision:</strong> <span class="badge badge-{{ $item->fecha_ult_revision ? 'primary' : '' }}"> {{$item->fecha_ult_revision}}</span>
            </div>
            @endif
          </div>
        </div>
        @include('documento.comentarios')
      </div>
    @else
      <div class="col-6 mx-auto">
        <div class="card">
          <div class="card-header text-center card-titulo"> FDC-125</div>
          <div class="card-body text-center m-2 p-2" style="font-size: 11px;">
            <a href="{{Storage::url($item->url_documento)}}" target="_blank">
              <i class="fas fa-file-download fa-3x"></i> <br>
              <span>Descargar archivo</span>
            </a>
          </div>
          <div class="card-footer">
            <div class="col-12 text-center">
              <strong>Documento Aprobado<br>Fecha:</strong> <span class="badge {{ $item->fecha_ult_revision ? 'btn-uts' : '' }}"> {{$item->fecha_ult_revision}}</span>
            </div>
          </div>
        </div>
      </div>
    @endif
  @endif
</div>
@if(Auth::user()->documentoAprobado($item,1) && $item->estado != 'APROBADO')
@push('js')
<script>
  document.addEventListener("DOMContentLoaded", function() {
    document.getElementById("formulario-rdc").addEventListener('submit', validarFormularioRDC);
    document.getElementById('archivo-rdc').onchange = function() {
      document.getElementById('nombre-archivo-rdc').innerHTML = document.getElementById('archivo-rdc').files[0].name;
    }

  });

  function validarFormularioRDC(evento) {
    evento.preventDefault();
    var archivo = document.getElementById('archivo-rdc').files[0];
    console.log("archivo rdc");
    if (archivo == undefined) {
      return errorArchivo();
    };
    this.submit();
  };
</script>
@endpush
@endif