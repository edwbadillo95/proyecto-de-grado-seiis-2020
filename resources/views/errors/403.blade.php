@extends('errors::minimal')

@section('title', 'Pagina Prohibida')
@section('code', '403')
@section('message','No tiene los permisos suficientes para realizar esta operación.' ?: 'Pagina Prohibida')
