<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use APP\Noticia;
use App\Providers\RouteServiceProvider;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $noticia = DB::table('noticia')->whereNull('fecha_expiracion')->orderBy('id', 'DESC')->paginate(6);
    $avisos = DB::table('noticia')->whereNotNull('fecha_expiracion')->orderBy('id', 'DESC')->paginate(4);
    return view('sbadmin.noticias',['noticia' =>$noticia],['avisos' =>$avisos]);
})
->middleware('guest')->name(RouteServiceProvider::LOGIN_ROUTE);



Route::post('/markasread', 'NotificacionController@markasread');
Route::post('/notifications', 'NotificacionController@notifications');

Route::get('/index', function () {
    return view('index');
})->name('index')->middleware('auth');

Auth::routes(['register' => false]);

Route::name('comite.')->prefix('comite')->group(function () {
    Route::resource('grupos', 'GrupoComiteController');
    Route::resource('grupos.integrantes', 'IntegranteComiteController')
        ->except('destroy');
    Route::resource('sesiones', 'SesionComiteController')
        ->parameters(['sesiones' => 'sesion']);
    Route::resource('sesiones.asistentes', 'AsistenciaController')
        ->parameters(['sesiones' => 'sesion'])
        ->only('index', 'create', 'store');

    Route::get('proyectos/{proyecto}/verificar', 'ProyectoGradoController@verificar')
        ->name('proyectos.verificar');
    Route::post('proyectos/{proyecto}/verificar/aprobado', 'ProyectoGradoController@aprobarVerificacion')
        ->name('proyectos.verificar.aprobado');
    Route::post('proyectos/{proyecto}/verificar/rechazado', 'ProyectoGradoController@rechazarVerificacion')
        ->name('proyectos.verificar.rechazado');
    Route::get('proyectos/calificador', 'ProyectoGradoController@mostrarAsignadosACalificador')
        ->name('proyectos.calificador');
    Route::resource('proyectos', 'ProyectoGradoController')
        ->except('edit', 'update', 'destroy');
    Route::resource('noticias', 'NoticiaController');
});

Route::name('proyectos.')->prefix('proyectos/{proyecto}')->group(function () {

    // Integrantes de proyecto
    Route::delete('integrantes', 'IntegranteProyectoController@destroy')
        ->name('integrantes.destroy');
    Route::resource('integrantes', 'IntegranteProyectoController')
        ->except('edit', 'update', 'destroy');

    // Seguimientos (observaciones)
    Route::resource('seguimientos', 'SeguimientoProyectoController')
        ->except('destroy');
});

Route::name('foro.')->prefix('foro')->group(function () {
    Route::resource('grupos', 'GrupoForoController');
    Route::resource('grupos.usuariosgrupo', 'UsuarioGrupoController');
    Route::resource('grupos.posts', 'PostController')->only("index");
    Route::get('buscargrupo', ['as' => 'buscargrupo', 'uses' => function () {
        return view('foro.grupos.buscargrupo');
    }]);
    Route::post('buscargrupo', ['as' => 'buscargrupo', 'uses' => 'GrupoForoController@buscarGrupo']);
    Route::resource('posts', 'PostController');
    Route::get('listarposts', ['as' => 'posts', 'uses' => 'PostController@cargarTodosPosts']);
    Route::get('descargararchivopost/{idarchivopost}', ['as' => 'descargararchivopost', 'uses' => 'PostController@descargarArchivoPost']);
    Route::get('descargararchivocomentario/{idarchivocomentario}', ['as' => 'descargararchivocomentario', 'uses' => 'PostController@descargarArchivoComentario']);
    Route::post(
        'posts/eliminarComentario/{comentario}',
        ['as' => 'posts.eliminarComentario', 'uses' => 'PostController@eliminarComentario']
    );
    Route::post(
        'posts/vincularComentario/{post}',
        ['as' => 'posts.vincularComentario', 'uses' => 'PostController@vincularComentarioPost']
    );
    Route::post(
        'posts/vincularComentarioAComentario/{comentario}',
        ['as' => 'posts.comentario.vincularComentario', 'uses' => 'PostController@vincularComentarioAComentario']
    );
});

Route::name('docente.')->prefix('docente')->group(function () {
    Route::resource('salones', 'SalonController')->parameters(['salones' => 'salon']);
    Route::resource('programas', 'ProgramaAcademicoController');
    Route::resource('materias', 'MateriaController');
    Route::resource('grupos', 'GrupoController');
    Route::resource('horarios', 'HorarioController');
    Route::resource('grupos.asistencia', 'AsistenciaDocenteController')->parameters(['asistencia' => 'asistencia']);
});

Route::get('docente/grupos/{materia}/horario', 'GrupoController@horarioPorMateria')->name('docente.grupos.horario');
Route::resource('docente', 'DocenteController')->except('destroy');

Route::get('derechosdegrado', 'DerechoGradoController@descargar')
    ->name('documento.derechosdegrado');

Route::get('proyectos/{proyecto}/documentacion', 'DocumentoController@index')->name('proyecto.documentacion');

Route::get('descargar-documentacion-final/{documento}', 'DocumentoController@comprimirYDescargar')->name('documentacion-zip');

Route::post('nuevo-comentario/{documento}','ComentarioDocumentoProyectoController')->name('nuevo-comentario');
Route::put('subir-archivo-proyecto-estudiante/{documento}','DocumentoController@subir_archivo_proyecto_estudiante')->name('subir-archivo-proyecto-estudiante');
Route::get('noticiav', 'NoticiaspublicasController@paginacion')->name('noticiav');
Route::get('avisosv', 'NoticiaspublicasController@avisosv')->name('avisosv');
Route::get('contenidov/{id}', 'NoticiaspublicasController@contenidov')->name('contenidov');
Route::get('contenidoa/{id}', 'NoticiaspublicasController@contenidoa')->name('contenidoa');

Route::resource('estudiantes', 'EstudianteController');
Route::get('historialNostas/{id}','NotasActualHistorialController@historialNotas')->name('historialNotas');
Route::get('notasActuales/{id}','NotasActualHistorialController@notasActuales')->name('notasActuales');
Route::get('registro-estudiante','RegistroEstudianteController@registroEstudiante')->name('registro-estudiante');
Route::get('lista-registro-ingreso-plataforma','EstudianteController@lista_registro_ingreso_plataforma')->name('lista-registro-ingreso-plataforma');
Route::post('crear-registro-ingreso-plataforma','RegistroEstudianteController@crear_registro_ingreso_plataforma')->name('crear-registro-ingreso-plataforma');
Route::put('aceptar-estudiante/{estudiante}','EstudianteController@aceptar_estudiante')->name('aceptar-estudiante');
Route::post('enviar-solicitud-proyecto-grado/{estudianteId}/{proyectoId}','ProyectoEstudianteController@enviar_solicitud_proyecto_grado')->name('enviar-solicitud-proyecto-grado');
Route::post('aceptar-solicitud-proyecto/{id}/{estudianteId}/{proyectoId}','ProyectoEstudianteController@aceptar_solicitud_proyecto')->name('aceptar-solicitud-proyecto');
Route::delete('eliminar-solicitud-proyecto/{id}/{proyectoId}','ProyectoEstudianteController@eliminar_solicitud_proyecto')->name('eliminar-solicitud-proyecto');
Route::resource('estudiantes', 'EstudianteController')->except('destroy');
Route::get('horarioEstudiante/{id}','HorarioHistorialController@horarioEstudiante')->name('horarioEstudiante');
Route::get('historialMaterias/{id}','HorarioHistorialController@historialMaterias')->name('historialMaterias');
Route::get('proyectos_inscripcion','ProyectoEstudianteController@proyectos_inscripcion')->name('proyectosInscripcion');
Route::post('aprobar-documento/{documento}','DocumentoController@aprobar_documento')->name('aprobar-documento');
Route::post('rechazar-documento/{documento}','DocumentoController@rechazar_documento')->name('rechazar-documento');
Route::put('cambiar-correo/{estudiante}','EstudianteController@cambiar_correo')->name('cambiar-correo');
