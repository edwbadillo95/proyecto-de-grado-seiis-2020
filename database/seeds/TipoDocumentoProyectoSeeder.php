<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TipoDocumentoProyectoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tipo_documento_proyecto')->insert([
            ['nombre' => 'RDC-124'],
            ['nombre' => 'RDC-125'],
            ['nombre' => 'Rejilla de Evaluacion'],
            ['nombre' => 'Manuel Instalacion'],
            ['nombre' => 'Manuel Usuario'],
            ['nombre' => 'Codigo fuente']
        ]);
    }
}
