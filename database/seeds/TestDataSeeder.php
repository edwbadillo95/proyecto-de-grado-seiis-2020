<?php

use Illuminate\Database\Seeder;

class TestDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CoordinadorSeeder::class);
        $this->call(SecretariaSeeder::class);
        $this->call(EstudianteRegistroSeeder::class);
    }
}
