<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class HorarioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Horario Carlos Adolfo
        DB::table('horario')->insert([
            'dia'                   => 'Lunes',
            'hora_inicio'           => '06:30:00',
            'hora_fin'              => '08:00:00',
            'salon_id'              => 5,
            'grupo_id'              => 1,
            'created_at'            => now(),
            'updated_at'            => now()
        ]);
        DB::table('horario')->insert([
            'dia'                   => 'Martes',
            'hora_inicio'           => '09:00:00',
            'hora_fin'              => '10:30:00',
            'salon_id'              => 10,
            'grupo_id'              => 2,
            'created_at'            => now(),
            'updated_at'            => now()
        ]);
        DB::table('horario')->insert([
            'dia'                   => 'Miercoles',
            'hora_inicio'           => '011:00:00',
            'hora_fin'              => '12:30:00',
            'salon_id'              => 15,
            'grupo_id'              => 3,
            'created_at'            => now(),
            'updated_at'            => now()
        ]);
        DB::table('horario')->insert([
            'dia'                   => 'Viernes',
            'hora_inicio'           => '14:00:00',
            'hora_fin'              => '15:30:00',
            'salon_id'              => 20,
            'grupo_id'              => 4,
            'created_at'            => now(),
            'updated_at'            => now()
        ]);
        DB::table('horario')->insert([
            'dia'                   => 'Jueves',
            'hora_inicio'           => '16:30:00',
            'hora_fin'              => '18:00:00',
            'salon_id'              => 25,
            'grupo_id'              => 5,
            'created_at'            => now(),
            'updated_at'            => now()
        ]);
        DB::table('horario')->insert([
            'dia'                   => 'Sabado',
            'hora_inicio'           => '18:30:00',
            'hora_fin'              => '20:00:00',
            'salon_id'              => 30,
            'grupo_id'              => 6,
            'created_at'            => now(),
            'updated_at'            => now()
        ]);
        // Horario Eliecer Montero
        DB::table('horario')->insert([
            'dia'                   => 'Martes',
            'hora_inicio'           => '06:30:00',
            'hora_fin'              => '08:00:00',
            'salon_id'              => 3,
            'grupo_id'              => 7,
            'created_at'            => now(),
            'updated_at'            => now()
        ]);
        DB::table('horario')->insert([
            'dia'                   => 'Lunes',
            'hora_inicio'           => '09:00:00',
            'hora_fin'              => '10:30:00',
            'salon_id'              => 6,
            'grupo_id'              => 8,
            'created_at'            => now(),
            'updated_at'            => now()
        ]);
        DB::table('horario')->insert([
            'dia'                   => 'Jueves',
            'hora_inicio'           => '011:00:00',
            'hora_fin'              => '12:30:00',
            'salon_id'              => 9,
            'grupo_id'              => 9,
            'created_at'            => now(),
            'updated_at'            => now()
        ]);
        DB::table('horario')->insert([
            'dia'                   => 'Viernes',
            'hora_inicio'           => '14:00:00',
            'hora_fin'              => '15:30:00',
            'salon_id'              => 12,
            'grupo_id'              => 10,
            'created_at'            => now(),
            'updated_at'            => now()
        ]);
        DB::table('horario')->insert([
            'dia'                   => 'Miercoles',
            'hora_inicio'           => '16:30:00',
            'hora_fin'              => '18:00:00',
            'salon_id'              => 15,
            'grupo_id'              => 11,
            'created_at'            => now(),
            'updated_at'            => now()
        ]);
        DB::table('horario')->insert([
            'dia'                   => 'Sabado',
            'hora_inicio'           => '18:30:00',
            'hora_fin'              => '20:00:00',
            'salon_id'              => 18,
            'grupo_id'              => 12,
            'created_at'            => now(),
            'updated_at'            => now()
        ]);
        // Horario Julián Jaimes
        DB::table('horario')->insert([
            'dia'                   => 'Viernes',
            'hora_inicio'           => '06:30:00',
            'hora_fin'              => '08:00:00',
            'salon_id'              => 40,
            'grupo_id'              => 13,
            'created_at'            => now(),
            'updated_at'            => now()
        ]);
        DB::table('horario')->insert([
            'dia'                   => 'Sabado',
            'hora_inicio'           => '09:00:00',
            'hora_fin'              => '10:30:00',
            'salon_id'              => 50,
            'grupo_id'              => 14,
            'created_at'            => now(),
            'updated_at'            => now()
        ]);
        DB::table('horario')->insert([
            'dia'                   => 'Lunes',
            'hora_inicio'           => '011:00:00',
            'hora_fin'              => '12:30:00',
            'salon_id'              => 60,
            'grupo_id'              => 15,
            'created_at'            => now(),
            'updated_at'            => now()
        ]);
        DB::table('horario')->insert([
            'dia'                   => 'Martes',
            'hora_inicio'           => '14:00:00',
            'hora_fin'              => '15:30:00',
            'salon_id'              => 70,
            'grupo_id'              => 16,
            'created_at'            => now(),
            'updated_at'            => now()
        ]);
        DB::table('horario')->insert([
            'dia'                   => 'Miercoles',
            'hora_inicio'           => '16:30:00',
            'hora_fin'              => '18:00:00',
            'salon_id'              => 80,
            'grupo_id'              => 17,
            'created_at'            => now(),
            'updated_at'            => now()
        ]);
        DB::table('horario')->insert([
            'dia'                   => 'Jueves',
            'hora_inicio'           => '18:30:00',
            'hora_fin'              => '20:00:00',
            'salon_id'              => 90,
            'grupo_id'              => 18,
            'created_at'            => now(),
            'updated_at'            => now()
        ]);
    }
}
