<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EdificioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('edificio')->insert([
            ['id'=> 1, 'nombre' => 'A'],
            ['id'=> 2, 'nombre' => 'B'],
            ['id'=> 3, 'nombre' => 'C'],
        ]);
    }
}
