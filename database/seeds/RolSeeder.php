<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RolSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('rol')->insert([
            ['id'   => 1, 'nombre' => 'ADMIN'],
            ['id'   => 2, 'nombre' => 'COORDINADOR'],
            ['id'   => 3, 'nombre' => 'SECRETARÍA']
        ]);
    }
}
