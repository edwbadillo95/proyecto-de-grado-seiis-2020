<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RolIntegranteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('rol_integrante')->insert([
            ['id' => 1, 'nombre' => 'ESTUDIANTE'],
            ['id' => 2, 'nombre' => 'DIRECTOR'],
            ['id' => 3, 'nombre' => 'COODIRECTOR'],
            ['id' => 4, 'nombre' => 'CALIFICADOR'],
        ]);
    }
}
