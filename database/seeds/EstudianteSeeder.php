<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EstudianteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
     

        DB::table('estudiante')->insert([
            [
                'nombre1' => 'Carlos',
                'nombre2' => null,
                'apellido1' => 'Perez',
                'apellido2' => 'Flores',
                'fecha_nacimiento' => '1996-10-03',
                'sexo' => 'M',
                'tipo_documento' => 'CC',
                'numero_documento' => 12341234,
                'correo' => 'carlosperez@uts.com',
                'user_id' => DB::table('users')->insertGetId([
                    'name' => 'Carlos Perez',
                    'email' => 'carlosperez@uts.com',
                    'email_verified_at' => now(),
                    'password' => bcrypt('password'),
                    'created_at'        => now(),
                    'updated_at'        => now(),
                    'rol_id'            => null
                ]),
                'created_at' => now(),
                'updated_at' => now(),
                'tipo_carrera' => 'TECNOLOGIA',
            ],

            [
                'nombre1' => 'Jesus',
                'nombre2' => 'Alberto',
                'apellido1' => 'Castro',
                'apellido2' => 'Gomez',
                'fecha_nacimiento' => '1992-11-22',
                'sexo' => 'M',
                'tipo_documento' => 'CC',
                'numero_documento' => 123123123,
                'correo' => 'jesuscastro@uts.com',
                'user_id' =>  DB::table('users')->insertGetId([
                    'name' => 'Jesus Castro',
                    'email' => 'jesuscastro@uts.com',
                    'email_verified_at' => now(),
                    'password' => bcrypt('password'),
                    'created_at'        => now(),
                    'updated_at'        => now(),
                    'rol_id'            => null
                ]),
                'created_at' => now(),
                'updated_at' => now(),
                'tipo_carrera' => 'PROFESIONAL',
            ],

        ]);
    }
}
