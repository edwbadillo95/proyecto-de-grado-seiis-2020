<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TipoComiteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tipo_comite')->insert([
            ['nombre' => 'PROYECTOS DE GRADO'],
            ['nombre' => 'CURRICULUM'],
        ]);
    }
}
