<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ClasificacionSeeder::class);
        $this->call(EdificioSeeder::class);
        $this->call(FacultadSeeder::class);
        $this->call(TipoDocumentoSeeder::class);
        $this->call(TipoProgramaSeeder::class);
        $this->call(RolSeeder::class);
        // $this->call(MatriculaSeeder::class);
        $this->call(EstudianteSeeder::class);
        $this->call(EstadoProyectoSeeder::class);
        $this->call(RolIntegranteSeeder::class);
        $this->call(TipoComiteSeeder::class);
        $this->call(MateriaSeeder::class);
        $this->call(SalonSeeder::class);
        $this->call(MateriaSeeder::class);
        $this->call(DocenteSeeder::class);
        $this->call(TipoDocumentoProyectoSeeder::class);
        $this->call(GrupoSeeder::class);
        $this->call(HorarioSeeder::class);
        $this->call(TestDataSeeder::class);
    }
}
