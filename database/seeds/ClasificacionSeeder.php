<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ClasificacionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('clasificacion')->insert([
            ['id'=> 1, 'nombre' => 'PLANTA'],
            ['id'=> 2, 'nombre' => 'CATEDRA']
        ]);

    }
}
