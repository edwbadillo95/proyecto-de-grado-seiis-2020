<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MateriaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('materia')->insert([
            ['nombre' => 'Cálculo diferencial', 'creditos' => 1],
            ['nombre' => 'Matemáticas discretas', 'creditos' => 1],
            ['nombre' => 'Cultura física', 'creditos' => 1],
            ['nombre' => 'Herramientas digitales', 'creditos' => 1],
            ['nombre' => 'Procesos de lectura y escritura', 'creditos' => 1],
            ['nombre' => 'Pensamiento algorítmico', 'creditos' => 1],
            ['nombre' => 'Álgebra superior', 'creditos' => 1],
            ['nombre' => 'Sistemas digitales', 'creditos' => 1],
            ['nombre' => 'Estructura de computadores', 'creditos' => 1],
            ['nombre' => 'Mecánica', 'creditos' => 1],
            ['nombre' => 'Cálculo integral', 'creditos' => 1],
            ['nombre' => 'Fundamentos de POO', 'creditos' => 1],
            ['nombre' => 'Diseño de bases de datos', 'creditos' => 1],
            ['nombre' => 'Optativa I', 'creditos' => 1],
            ['nombre' => 'Planeación de sistemas informáticos', 'creditos' => 1],
            ['nombre' => 'Programación orientada a objetos', 'creditos' => 1],
            ['nombre' => 'Sistemas operativos', 'creditos' => 1],
            ['nombre' => 'Programación de dispositivos', 'creditos' => 1],
            ['nombre' => 'Epistemología', 'creditos' => 1],
            ['nombre' => 'Motores de bases de datos', 'creditos' => 1],
            ['nombre' => 'Electromagnetismo', 'creditos' => 1],
            ['nombre' => 'Redes', 'creditos' => 1],
            ['nombre' => 'Programación web', 'creditos' => 1],
            ['nombre' => 'Laboratorio de física', 'creditos' => 1],
            ['nombre' => 'Electiva de profundización I', 'creditos' => 1],
            ['nombre' => 'Optativa II', 'creditos' => 1],
            ['nombre' => 'Inglés I', 'creditos' => 1],
            ['nombre' => 'Estructura de datos', 'creditos' => 1],
            ['nombre' => 'Aplicaciones móviles', 'creditos' => 1],
            ['nombre' => 'Administración de servidores', 'creditos' => 1],
            ['nombre' => 'Programación en java', 'creditos' => 1],
            ['nombre' => 'Ética', 'creditos' => 1],
            ['nombre' => 'Electiva de profundización II', 'creditos' => 1],
            ['nombre' => 'Metodología de la Investigacion I', 'creditos' => 1],
            ['nombre' => 'Inglés II', 'creditos' => 1],
            ['nombre' => 'Cálculo multivariable', 'creditos' => 1],
            ['nombre' => 'Introducción a la ingeniería', 'creditos' => 1],
            ['nombre' => 'Desarrollo de aplicaciones empresariales', 'creditos' => 1],
            ['nombre' => 'Selección y evaluación de tecnología', 'creditos' => 1],
            ['nombre' => 'Seguridad de las tecnologías de la información', 'creditos' => 1],
            ['nombre' => 'Nuevas tecnologías de desarrollo', 'creditos' => 1],
            ['nombre' => 'Electiva de profundización III', 'creditos' => 1],
            ['nombre' => 'Álgebra lineal', 'creditos' => 1],
            ['nombre' => 'Ecuaciones diferenciales', 'creditos' => 1],
            ['nombre' => 'Estadística para ingenieros', 'creditos' => 1],
            ['nombre' => 'Ondas y partículas', 'creditos' => 1],
            ['nombre' => 'Linux', 'creditos' => 1],
            ['nombre' => 'Inglés III', 'creditos' => 1],
            ['nombre' => 'Análisis numérico', 'creditos' => 1],
            ['nombre' => 'Autómatas y lenguajes formales', 'creditos' => 1],
            ['nombre' => 'Investigación en operaciones', 'creditos' => 1],
            ['nombre' => 'Arquitectura de software', 'creditos' => 1],
            ['nombre' => 'Optativa III', 'creditos' => 1],
            ['nombre' => 'Inglés IV', 'creditos' => 1],
            ['nombre' => 'Compiladores', 'creditos' => 1],
            ['nombre' => 'Simulación digital', 'creditos' => 1],
            ['nombre' => 'Calidad de software', 'creditos' => 1],
            ['nombre' => 'Minería de datos', 'creditos' => 1],
            ['nombre' => 'Metodología de la investigación II', 'creditos' => 1],
            ['nombre' => 'Electiva de profundización V', 'creditos' => 1],
            ['nombre' => 'Emprendimiento', 'creditos' => 1],
            ['nombre' => 'Dinámica de sistemas', 'creditos' => 1],
            ['nombre' => 'Ética y legislación informática', 'creditos' => 1],
            ['nombre' => 'Gestión de proyectos de software', 'creditos' => 1],
            ['nombre' => 'Patrones de software', 'creditos' => 1],
            ['nombre' => 'Análisis de datos a gran escala', 'creditos' => 1],
            ['nombre' => 'Electiva de profundización VI', 'creditos' => 1],
            ['nombre' => 'Optativa IV', 'creditos' => 1]
        ]);
    }
}
