<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SecretariaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'email'             => 'secretaria@uts.edu.co',
            'name'              => 'Diego Mendoza',
            'email_verified_at' => now(),
            'password'          => bcrypt('password'),
            'created_at'        => now(),
            'updated_at'        => now(),
            'rol_id'            => 3
        ]);
    }
}
