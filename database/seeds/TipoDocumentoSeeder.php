<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TipoDocumentoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tipo_documento')->insert([
            ['id'=> 1, 'nombre' => 'CC'],
            ['id'=> 2, 'nombre' => 'CE'],
            ['id'=> 3, 'nombre' => 'TI']
        ]);
    }
}
