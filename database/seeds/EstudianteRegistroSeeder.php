<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EstudianteRegistroSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('estudiante')->insert([
            'nombre1'           => 'Andres',
            'nombre2'           => null,
            'apellido1'         => 'Benjumea',
            'apellido2'         => 'Carrascal',
            'correo'            => 'andres@gmail.com',
            'fecha_nacimiento'  => '2000-01-01',
            'sexo'              => 'H',
            'tipo_documento'    => 'CC',
            'numero_documento'  => '6541230789',            
            'user_id'           => DB::table('users')->insertGetId([
                'email'             => 'andres@gmail.com',
                'name'              => 'Andres Benjumea Carrascal',
                'email_verified_at' => now(),
                'password'          => bcrypt('password'),
                'created_at'        => now(),
                'updated_at'        => now(),
                'rol_id'            => null
            ]),
            'tipo_carrera' => 'TECNOLOGIA'
        ]);
        DB::table('estudiante')->insert([
            'nombre1'           => 'Camilo',
            'nombre2'           => null,
            'apellido1'         => 'Ulloa',
            'apellido2'         => 'Florez',
            'correo'            => 'ulloa@gmail.com',
            'fecha_nacimiento'  => '2000-01-01',
            'sexo'              => 'H',
            'tipo_documento'    => 'CC',
            'numero_documento'  => '147852369',
            'user_id'           => DB::table('users')->insertGetId([
                'email'             => 'ulloa@gmail.com',
                'name'              => 'Camilo Ulloa Florez',
                'email_verified_at' => now(),
                'password'          => bcrypt('password'),
                'created_at'        => now(),
                'updated_at'        => now(),
                'rol_id'            => null
            ]),            
            'tipo_carrera' => 'PROFESIONAL'
        ]);
    }
}
