<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DocenteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('docente')->insert([
            'documento'             => '123456789',
            'nombres'               => 'Carlos Adolfo',
            'apellidos'             => 'Beltrán Castro',
            'correo'             => 'osocarbel@uts.edu.co',
            'genero'                => 'H',
            'fecha_nacimiento'      => '1970-01-01',
            'correo'                => 'osocarbel@uts.edu.co',
            'clasificacion_id'      => 2,
            'tipo_documento_id'     => 1,
            'created_at'            => now(),
            'updated_at'            => now(),

            'usuario_id'   => DB::table('users')->insertGetId([
                'email'             => 'osocarbel@uts.edu.co',
                'name'              => 'Carlos Adolfo Beltrán Castro',
                'email_verified_at' => now(),
                'password'          => bcrypt('password'),
                'created_at'        => now(),
                'updated_at'        => now(),
                'rol_id'            => null
            ])
        ]);

        DB::table('docente')->insert([
            'documento'             => '987654321',
            'nombres'               => 'Eliecer',
            'apellidos'             => 'Montero',
            'genero'                => 'H',
            'correo'             => 'eliecermontero@uts.edu.co',
            'fecha_nacimiento'      => '1970-01-01',
            'correo'                => 'eliecermontero@uts.edu.co',
            'clasificacion_id'      => 1,
            'tipo_documento_id'     => 1,
            'created_at'            => now(),
            'updated_at'            => now(),

            'usuario_id'   => DB::table('users')->insertGetId([
                'email'             => 'eliecermontero@uts.edu.co',
                'name'              => 'Eliecer Montero',
                'email_verified_at' => now(),
                'password'          => bcrypt('password'),
                'created_at'        => now(),
                'updated_at'        => now(),
                'rol_id'            => null
            ])
        ]);

        DB::table('docente')->insert([
            'documento'             => '456789321',
            'nombres'               => 'Julián',
            'apellidos'             => 'Jaimes',
            'correo'             => 'julianjaimes@uts.edu.co',
            'genero'                => 'H',
            'fecha_nacimiento'      => '1970-01-01',
            'correo'                => 'julianjaimes@uts.edu.co',
            'clasificacion_id'      => 1,
            'tipo_documento_id'     => 1,
            'created_at'            => now(),
            'updated_at'            => now(),

            'usuario_id'   => DB::table('users')->insertGetId([
                'email'             => 'julianjaimes@uts.edu.co',
                'name'              => 'Julián Jaimes',
                'email_verified_at' => now(),
                'password'          => bcrypt('password'),
                'created_at'        => now(),
                'updated_at'        => now(),
                'rol_id'            => null
            ])
        ]);

        DB::table('docente')->insert([
            'documento'             => '963258741',
            'nombres'               => 'Sebastian',
            'apellidos'             => 'Cardenas',
            'correo'             => 'sebastian@uts.edu.co',
            'genero'                => 'H',
            'fecha_nacimiento'      => '1970-01-01',
            'correo'                => 'sebastian@uts.edu.co',
            'clasificacion_id'      => 2,
            'tipo_documento_id'     => 1,
            'created_at'            => now(),
            'updated_at'            => now(),

            'usuario_id'   => DB::table('users')->insertGetId([
                'email'             => 'sebastian@uts.edu.co',
                'name'              => 'Sebastian Cardenas',
                'email_verified_at' => now(),
                'password'          => bcrypt('password'),
                'created_at'        => now(),
                'updated_at'        => now(),
                'rol_id'            => null
            ])
        ]);

        DB::table('docente')->insert([
            'documento'             => '9517534862',
            'nombres'               => 'Ruben',
            'apellidos'             => 'Fontecha',
            'correo'             => 'rubenfontecha@uts.edu.co',
            'genero'                => 'H',
            'fecha_nacimiento'      => '1970-01-01',
            'correo'                => 'rubenfontecha@uts.edu.co',
            'clasificacion_id'      => 1,
            'tipo_documento_id'     => 1,
            'created_at'            => now(),
            'updated_at'            => now(),

            'usuario_id'   => DB::table('users')->insertGetId([
                'email'             => 'rubenfontecha@uts.edu.co',
                'name'              => 'Ruben Fontecha',
                'email_verified_at' => now(),
                'password'          => bcrypt('password'),
                'created_at'        => now(),
                'updated_at'        => now(),
                'rol_id'            => null
            ])
        ]);

        DB::table('docente')->insert([
            'documento'             => '75332145',
            'nombres'               => 'Leidy',
            'apellidos'             => 'Polo',
            'correo'             => 'leidypolo@uts.edu.co',
            'genero'                => 'H',
            'fecha_nacimiento'      => '1970-01-01',
            'correo'                => 'leidypolo@uts.edu.co',
            'clasificacion_id'      => 1,
            'tipo_documento_id'     => 1,
            'created_at'            => now(),
            'updated_at'            => now(),

            'usuario_id'   => DB::table('users')->insertGetId([
                'email'             => 'leidypolo@uts.edu.co',
                'name'              => 'Leidy Polo',
                'email_verified_at' => now(),
                'password'          => bcrypt('password'),
                'created_at'        => now(),
                'updated_at'        => now(),
                'rol_id'            => null
            ])
        ]);
    }
}
