<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TipoProgramaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tipo_programa')->insert([
            ['nombre' => 'TECNOLOGÍA'],
            ['nombre' => 'NIVEL UNIVERSITARIO'],
        ]);
    }
}
