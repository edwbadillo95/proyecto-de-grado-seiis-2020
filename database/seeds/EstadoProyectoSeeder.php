<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EstadoProyectoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('estado_proyecto')->insert([
            ['id'=> 1, 'nombre' => 'POR REVISAR'],
            ['id'=> 2, 'nombre' => 'APROBADO'],
            ['id'=> 3, 'nombre' => 'RECHAZADO'],
            ['id'=> 4, 'nombre' => 'EN DESARROLLO'],
            ['id'=> 5, 'nombre' => 'CALIFICADO APROBADO'],
            ['id'=> 6, 'nombre' => 'CALIFICADO RECHAZADO'],
        ]);
    }
}
