<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GrupoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Grupos Carlos Adolfo
        DB::table('grupo')->insert([
            'nombre'                => 'A101',
            'materia_id'            => 4,
            'docente_id'            => 1,
            'created_at'            => now(),
            'updated_at'            => now()
        ]);
        DB::table('grupo')->insert([
            'nombre'                => 'B204',
            'materia_id'            => 6,
            'docente_id'            => 1,
            'created_at'            => now(),
            'updated_at'            => now()
        ]);
        DB::table('grupo')->insert([
            'nombre'                => 'C302',
            'materia_id'            => 8,
            'docente_id'            => 1,
            'created_at'            => now(),
            'updated_at'            => now()
        ]);
        DB::table('grupo')->insert([
            'nombre'                => 'D406',
            'materia_id'            => 9,
            'docente_id'            => 1,
            'created_at'            => now(),
            'updated_at'            => now()
        ]);
        DB::table('grupo')->insert([
            'nombre'                => 'E505',
            'materia_id'            => 12,
            'docente_id'            => 1,
            'created_at'            => now(),
            'updated_at'            => now()
        ]);
        DB::table('grupo')->insert([
            'nombre'                => 'F606',
            'materia_id'            => 13,
            'docente_id'            => 1,
            'created_at'            => now(),
            'updated_at'            => now()
        ]);
        // Grupos Eliecer Montero
        DB::table('grupo')->insert([
            'nombre'                => 'A102',
            'materia_id'            => 15,
            'docente_id'            => 2,
            'created_at'            => now(),
            'updated_at'            => now()
        ]);
        DB::table('grupo')->insert([
            'nombre'                => 'B205',
            'materia_id'            => 16,
            'docente_id'            => 2,
            'created_at'            => now(),
            'updated_at'            => now()
        ]);
        DB::table('grupo')->insert([
            'nombre'                => 'C303',
            'materia_id'            => 17,
            'docente_id'            => 2,
            'created_at'            => now(),
            'updated_at'            => now()
        ]);
        DB::table('grupo')->insert([
            'nombre'                => 'D407',
            'materia_id'            => 18,
            'docente_id'            => 2,
            'created_at'            => now(),
            'updated_at'            => now()
        ]);
        DB::table('grupo')->insert([
            'nombre'                => 'E506',
            'materia_id'            => 20,
            'docente_id'            => 2,
            'created_at'            => now(),
            'updated_at'            => now()
        ]);
        DB::table('grupo')->insert([
            'nombre'                => 'F607',
            'materia_id'            => 22,
            'docente_id'            => 2,
            'created_at'            => now(),
            'updated_at'            => now()
        ]);
        // Grupos Julián Jaimes
        DB::table('grupo')->insert([
            'nombre'                => 'A103',
            'materia_id'            => 23,
            'docente_id'            => 3,
            'created_at'            => now(),
            'updated_at'            => now()
        ]);
        DB::table('grupo')->insert([
            'nombre'                => 'B206',
            'materia_id'            => 28,
            'docente_id'            => 3,
            'created_at'            => now(),
            'updated_at'            => now()
        ]);
        DB::table('grupo')->insert([
            'nombre'                => 'C304',
            'materia_id'            => 29,
            'docente_id'            => 3,
            'created_at'            => now(),
            'updated_at'            => now()
        ]);
        DB::table('grupo')->insert([
            'nombre'                => 'D408',
            'materia_id'            => 30,
            'docente_id'            => 3,
            'created_at'            => now(),
            'updated_at'            => now()
        ]);
        DB::table('grupo')->insert([
            'nombre'                => 'E507',
            'materia_id'            => 31,
            'docente_id'            => 3,
            'created_at'            => now(),
            'updated_at'            => now()
        ]);
        DB::table('grupo')->insert([
            'nombre'                => 'F608',
            'materia_id'            => 34,
            'docente_id'            => 3,
            'created_at'            => now(),
            'updated_at'            => now()
        ]);
    }
}
