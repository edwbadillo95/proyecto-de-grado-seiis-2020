<?php

use App\Rol;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CoordinadorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'email'             => 'coordinacion@uts.edu.co',
            'name'              => 'Abigail',
            'email_verified_at' => now(),
            'password'          => bcrypt('password'),
            'created_at'        => now(),
            'updated_at'        => now(),
            'rol_id'            => 2
        ]);
    }
}
