<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FacultadSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('facultad')->insert([
            ['nombre' => 'FACULTAD DE CIENCIAS SOCIOECONÓMICAS Y EMPRESARIALES'],
            ['nombre' => 'FACULTAD DE CIENCIAS NATURALES E INGENIERÍAS'],
        ]);
    }
}
