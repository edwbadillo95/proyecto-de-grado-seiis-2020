<?php

use App\Estudiante;
use App\Materia;
use App\Matricula;
use App\MatriculaAsignatura;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MatriculaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $estudiantes_t = Estudiante::where('tipo_carrera', 'TECNOLOGIA')->get();
        $estudiantes_p = Estudiante::where('tipo_carrera', 'PROFESIONAL')->get();
        $materias_t = Materia::whereBetween('id', [1, 42])->get();
        $materias_p = Materia::whereBetween('id', [43, 67])->get();
        foreach ($estudiantes_t as $e) {
            $matricula = Matricula::create([
                'fecha_matricula' => $f1 = Carbon::now(),
                'fecha_pago' => $f1 = Carbon::now(),
                'estudiante_id' => $e->id,
                'periodo' => 'sexto'
            ]);
            $matricula2 = Matricula::create([
                'fecha_matricula' => Carbon::now()->subMonth(6),
                'fecha_pago' => Carbon::now()->subMonth(6),
                'estudiante_id' => $e->id,
                'periodo' => 'quinto'
            ]);
            foreach ($materias_t as $m) {
                if ($m->id > 36) {
                    MatriculaAsignatura::create([
                        'matricula_id' => $matricula->id,
                        'estado' => 'Matriculada',
                        'materia_id' => $m->id,
                        'grupo_id' => rand(1, 3)
                    ]);
                } else {
                    MatriculaAsignatura::create([
                        'matricula_id' => $matricula2->id,
                        'nota1'=>'4.0',
                        'nota2'=>'4.0',
                        'nota3'=>'4.0',
                        'nota_definitiva'=>'4.0',
                        'fecha_aprobacion'=> Carbon::now()->subMonth(1),
                        'estado' => 'Aprobada',
                        'materia_id' => $m->id,
                        'grupo_id' => 3
                    ]);
                }
            }
        }
        foreach ($estudiantes_p as $e) {
            $matricula = Matricula::create([
                'fecha_matricula' => $f1 = Carbon::now(),
                'fecha_pago' => $f1 = Carbon::now(),
                'estudiante_id' => $e->id,
                'periodo' => 'sexto'
            ]);
            $matricula2 = Matricula::create([
                'fecha_matricula' => Carbon::now()->subMonth(6),
                'fecha_pago' => Carbon::now()->subMonth(6),
                'estudiante_id' => $e->id,
                'periodo' => 'quinto'
            ]);
            foreach ($materias_p as $m) {
                if ($m->id > 60) {
                    MatriculaAsignatura::create([
                        'matricula_id' => $matricula->id,
                        'estado' => 'Matriculada',
                        'materia_id' => $m->id,
                        'grupo_id' => rand(1, 3)
                    ]);
                } else {
                    MatriculaAsignatura::create([
                        'matricula_id' => $matricula2->id,
                        'nota1'=>'4.0',
                        'nota2'=>'4.0',
                        'nota3'=>'4.0',
                        'nota_definitiva'=>'4.0',
                        'fecha_aprobacion'=> Carbon::now()->subMonth(1),
                        'estado' => 'Aprobada',
                        'materia_id' => $m->id,
                        'grupo_id' => 3
                    ]);
                }
            }
        }
    }
}
