<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Estudiante;
use Faker\Generator as Faker;

$factory->define(Estudiante::class, function (Faker $faker) {
    return [
        'nombre1'           => $faker->firstName,
        'nombre2'           => $faker->firstName,
        'apellido1'         => $faker->lastName,
        'apellido2'         => $faker->lastName,
        'fecha_nacimiento'  => '2000-01-01',
        'sexo'              => array_rand(['H', 'M']),
        'tipo_documento'    => 'CC',
        'numero_documento'  => $faker->unique()->randomNumber(8),

        'user_id'            => function() {
            $u = factory(App\User::class)->create();
            return $u->id;
        },
    ];
});
