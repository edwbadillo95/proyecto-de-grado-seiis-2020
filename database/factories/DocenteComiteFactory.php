<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Clasificacion;
use App\Docente;
use App\TipoDocumento;
use Faker\Generator as Faker;

$factory->define(Docente::class, function (Faker $faker) {
    $genero = array_rand(['male', 'female']);

    return [
        'documento'             => $faker->unique()->randomNumber(8),
        'nombres'               => $faker->firstName($genero),
        'apellidos'             => $faker->lastName($genero),
        'genero'                => $genero == 'male' ? 'M' : 'F',
        'fecha_nacimiento'      => '1980-01-01',
        'clasificacion_id'      => Clasificacion::first()->id,
        'tipo_documento_id'     => TipoDocumento::first()->id,

        'usuario_id'            => function() {
            $u = factory(App\User::class)->create();
            return $u->id;
        },
    ];
});
