<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDocumentoProyectoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documento_proyecto', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->dateTime('fecha_ult_cargue')->nullable();
            $table->dateTime('fecha_ult_revision')->nullable();
            $table->string('url_documento')->nullable();
            $table->enum('estado', ['PENDIENTE','EN PROCESO','APROBADO','RECHAZADO'])->default('PENDIENTE');
            $table->unsignedBigInteger('proyecto_grado_id');
            $table->unsignedBigInteger('tipo_documento_proyecto_id');
            $table->timestamps();

            $table->foreign('proyecto_grado_id')
                ->references('id')
                ->on('proyecto_grado')
                ->onDelete('cascade');
            $table->foreign('tipo_documento_proyecto_id')
                ->references('id')
                ->on('tipo_documento_proyecto')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('documento_proyecto');
    }
}
