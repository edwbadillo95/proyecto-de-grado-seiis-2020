<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEstudianteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estudiante', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre1',45);
            $table->string('nombre2',45)->nullable();
            $table->string('apellido1',45);
            $table->string('apellido2',45);
            $table->string('correo')->unique();
            $table->date('fecha_nacimiento');
            $table->char('sexo',1);
            $table->char('tipo_documento',2);
            $table->string('numero_documento',45)->unique();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->timestamps();            
            $table->foreign('user_id')->references('id')->on('users');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('estudiante');
    }
}
