<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSesionComiteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sesion_comite', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('fecha');
            $table->string('asunto', 100);
            $table->text('descripcion')->nullable();
            $table->timestamps();
            $table->unsignedBigInteger('grupo_comite_id');

            // Indica si fue un integrante de comité quien registró la sesión.
            $table->unsignedBigInteger('integrante_comite_id')->nullable();

            $table->foreign('grupo_comite_id')
                ->references('id')
                ->on('grupo_comite')
                ->onDelete('cascade');
            $table->foreign('integrante_comite_id')
                ->references('id')
                ->on('integrante_comite')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sesion_comite');
    }
}
