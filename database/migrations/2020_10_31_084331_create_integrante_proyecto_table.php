<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIntegranteProyectoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('integrante_proyecto', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->dateTime('fecha_retiro')->nullable();
            $table->dateTime('fecha_integracion')->useCurrent();
            $table->unsignedBigInteger('integrante_id');
            $table->text('descripcion')->nullable();
            $table->unsignedBigInteger('rol_integrante_id');
            $table->unsignedBigInteger('proyecto_grado_id');

            $table->foreign('rol_integrante_id')->references('id')->on('rol_integrante');
            $table->foreign('proyecto_grado_id')
                ->references('id')
                ->on('proyecto_grado')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('integrante_proyecto');
    }
}
