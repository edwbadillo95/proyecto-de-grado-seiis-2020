<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateComentarioDocumentoProyectoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comentario_documento_proyecto', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('comentario');
            $table->unsignedBigInteger('documento_proyecto_id');
            $table->unsignedBigInteger('user_id');
            $table->timestamps();

            $table->foreign('documento_proyecto_id')
                ->references('id')
                ->on('documento_proyecto')
                ->onDelete('cascade');
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comentario_documento_proyecto');
    }
}
