<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAutorEstudianteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('autor_estudiante', function (Blueprint $table) {
            $table->unsignedBigInteger('proyecto_grado_id');
            $table->unsignedBigInteger('estudiante_id');

            $table->foreign('proyecto_grado_id')
                ->references('id')
                ->on('proyecto_grado')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('autor_estudiante');
    }
}
