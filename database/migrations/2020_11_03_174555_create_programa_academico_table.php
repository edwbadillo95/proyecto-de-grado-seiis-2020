<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProgramaAcademicoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('programa_academico', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre', 50);
            $table->unsignedBigInteger('tipo_programa_id');
            $table->unsignedBigInteger('facultad_id');
            $table->timestamps();

            $table->foreign('tipo_programa_id')->references('id')->on('tipo_programa')->onDelete('cascade');
            $table->foreign('facultad_id')->references('id')->on('facultad')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('programa_academico');
    }
}
