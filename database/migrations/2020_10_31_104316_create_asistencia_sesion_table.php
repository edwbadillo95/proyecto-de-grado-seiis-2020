<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAsistenciaSesionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asistencia_sesion', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('sesion_comite_id');
            $table->unsignedBigInteger('integrante_comite_id');

            $table->foreign('sesion_comite_id')
                ->references('id')
                ->on('sesion_comite')
                ->onDelete('cascade');
            $table->foreign('integrante_comite_id')
                ->references('id')
                ->on('integrante_comite')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asistencia_sesion');
    }
}
