<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMatriculaAsignaturaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('matricula_asignatura', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('matricula_id');
            $table->float('nota1')->default('0.0');
            $table->float('nota2')->default('0.0');
            $table->float('nota3')->default('0.0');
            $table->float('nota_habilitacion')->nullable();
            $table->float('nota_supletorio1')->nullable();
            $table->float('nota_supletorio2')->nullable();
            $table->float('nota_supletorio3')->nullable();
            $table->string('estado',45);
            $table->date('fecha_cancelacion')->nullable();
            $table->string('motivo_cancelacion',45)->nullable();
            $table->float('nota_definitiva')->nullable();
            $table->date('fecha_aprobacion')->nullable();
            $table->unsignedBigInteger('materia_id');
            $table->unsignedBigInteger('grupo_id');
            $table->timestamps();
            $table->foreign('materia_id')->references('id')->on('materia');
            // $table->foreign('grupo_id')->references('id')->on('grupo');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('matricula_asignatura');
    }
}
