<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProyectoGradoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proyecto_grado', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre', 150)->unique();
            $table->text('descripcion')->nullable();
            $table->boolean('aprobado')->nullable();
            $table->unsignedBigInteger('docente_id');
            $table->unsignedBigInteger('estado_proyecto_id');
            $table->timestamps();

            $table->foreign('estado_proyecto_id')->references('id')->on('estado_proyecto');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proyecto_grado');
    }
}
