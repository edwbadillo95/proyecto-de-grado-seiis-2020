<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateComentarioPostTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comentario_post', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->mediumText('comentario');
            $table->timestamp('fechapublicacion');
            $table->boolean('estado')->default(true);
            $table->unsignedBigInteger('post_id')->nullable();
            $table->unsignedBigInteger('usuario_id');
            $table->unsignedBigInteger('comentario_post_id')->nullable();

            $table->foreign('usuario_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
            $table->foreign('post_id')
                ->references('id')
                ->on('post')
                ->onDelete('cascade');
            $table->foreign('comentario_post_id')
                ->references('id')
                ->on('comentario_post')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comentario_post');
    }
}
