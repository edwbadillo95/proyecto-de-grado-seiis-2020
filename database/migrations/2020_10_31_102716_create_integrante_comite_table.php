<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIntegranteComiteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('integrante_comite', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('fecha_integracion')->useCurrent();
            $table->date('fecha_retiro')->nullable();
            $table->text('descripcion')->nullable();
            $table->boolean('programar_sesiones')->default(false);
            $table->unsignedBigInteger('docente_id');
            $table->unsignedBigInteger('grupo_comite_id');

            $table->foreign('grupo_comite_id')->references('id')->on('grupo_comite');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('integrante_comite');
    }
}
