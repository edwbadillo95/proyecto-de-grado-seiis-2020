<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSeguimientoProyectoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seguimiento_proyecto', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('asunto', 100);
            $table->text('descripcion');
            $table->timestamps();
            $table->unsignedBigInteger('proyecto_grado_id');
            $table->unsignedBigInteger('integrante_comite_id');

            $table->foreign('proyecto_grado_id')
                ->references('id')
                ->on('proyecto_grado')
                ->onDelete('cascade');

            $table->foreign('integrante_comite_id')
                ->references('id')
                ->on('integrante_comite')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('seguimiento_proyecto');
    }
}
