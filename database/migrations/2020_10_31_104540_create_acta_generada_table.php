<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActaGeneradaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('acta_generada', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->dateTime('fecha')->useCurrent();
            $table->string('asunto', 100);
            $table->text('descripcion')->nullable();
            $table->unsignedBigInteger('sesion_comite_id');

            $table->foreign('sesion_comite_id')
                ->references('id')
                ->on('sesion_comite')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('acta_generada');
    }
}
