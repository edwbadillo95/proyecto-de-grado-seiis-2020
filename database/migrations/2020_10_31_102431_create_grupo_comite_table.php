<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGrupoComiteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('grupo_comite', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre', 100)->unique();
            $table->boolean('estado')->default(true);
            $table->date('fecha_inicio');
            $table->date('fecha_finalizacion');
            $table->text('descripcion')->nullable();
            $table->timestamps();
            $table->unsignedBigInteger('tipo_comite_id');

            $table->foreign('tipo_comite_id')->references('id')->on('tipo_comite');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('grupo_comite');
    }
}
