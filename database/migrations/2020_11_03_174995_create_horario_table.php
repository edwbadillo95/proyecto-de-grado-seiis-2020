<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHorarioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('horario', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('dia', 50);
            $table->time('hora_inicio');
            $table->time('hora_fin');
            $table->unsignedBigInteger('salon_id');
            $table->unsignedBigInteger('grupo_id');
            $table->timestamps();

            $table->foreign('salon_id')->references('id')->on('salon')->onDelete('cascade');
            $table->foreign('grupo_id')->references('id')->on('grupo')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('horario');
    }
}
