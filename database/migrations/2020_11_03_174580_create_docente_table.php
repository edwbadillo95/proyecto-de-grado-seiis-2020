<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDocenteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('docente', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('documento', 10)->unique();
            $table->string('nombres', 50);
            $table->string('apellidos', 50);
            $table->string('correo')->unique();
            $table->char('genero', 1);
            $table->date('fecha_nacimiento');
            $table->string('archivo')->nullable();
            $table->string('horario')->nullable();
            $table->unsignedBigInteger('clasificacion_id');
            $table->unsignedBigInteger('tipo_documento_id');
            $table->unsignedBigInteger('usuario_id');
            $table->timestamps();

            $table->foreign('clasificacion_id')->references('id')->on('clasificacion')->onDelete('cascade');
            $table->foreign('tipo_documento_id')->references('id')->on('tipo_documento')->onDelete('cascade');
            $table->foreign('usuario_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('docente');
    }
}
