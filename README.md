## Requerimientos de instalación
Para entorno de desarrollo en Windows bastará con descargar laragon. Si no se usa laragon se deben cumplir los siguientes requerimientos:
- [Requerimientos Laravel 6.x](https://laravel.com/docs/6.x#server-requirements)
- [Composer](https://getcomposer.org/download/)
- [MySQL 5.7](https://dev.mysql.com/downloads/mysql/5.7.html)

## Instalación
Clone el repositorio en la ubicación que desee, si usa laragon debe estar bajo la carpeta *www*.

Antes de continuar, asegúrese de cumplir con los requerimientos anteriormente indicados. Posteriormente, debe indicar los parámetros de conexión de la base de datos MySQL en el archivo **.env**. Dicho archivo debe ser creado como una copia del archivo **.env.example**.
 
Ingrese a la carpeta del proyecto y ejecute en la terminal los siguientes comandos (si usa laragon, debe usar la terminal que trae dicha herramienta).

    composer install

    php artisan key:generate

    php artisan migrate --seed
